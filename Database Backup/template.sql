-- MySQL dump 10.13  Distrib 8.0.31, for Linux (x86_64)
--
-- Host: localhost    Database: eshikshya_utarbahini
-- ------------------------------------------------------
-- Server version	8.0.31-0ubuntu0.20.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `alumni_events`
--

DROP TABLE IF EXISTS `alumni_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `alumni_events` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `event_for` varchar(100) NOT NULL,
  `session_id` int NOT NULL,
  `class_id` varchar(255) NOT NULL,
  `section` varchar(255) NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `note` text NOT NULL,
  `photo` varchar(255) NOT NULL,
  `is_active` int NOT NULL,
  `event_notification_message` text NOT NULL,
  `show_onwebsite` int NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alumni_events`
--

LOCK TABLES `alumni_events` WRITE;
/*!40000 ALTER TABLE `alumni_events` DISABLE KEYS */;
INSERT INTO `alumni_events` VALUES (1,'Christmas Day','all',0,'','null','2079-09-10','2079-09-10','','',0,'Merry Christmas',0,'2022-12-21 08:30:16'),(2,'TAMU LOSAR','all',0,'','null','2079-09-15','2079-09-15','','',0,'',0,'2022-12-21 10:11:21');
/*!40000 ALTER TABLE `alumni_events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alumni_students`
--

DROP TABLE IF EXISTS `alumni_students`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `alumni_students` (
  `id` int NOT NULL AUTO_INCREMENT,
  `current_email` varchar(255) NOT NULL,
  `current_phone` varchar(255) NOT NULL,
  `occupation` text NOT NULL,
  `address` text NOT NULL,
  `student_id` int NOT NULL,
  `photo` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `student_id` (`student_id`),
  CONSTRAINT `alumni_students_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alumni_students`
--

LOCK TABLES `alumni_students` WRITE;
/*!40000 ALTER TABLE `alumni_students` DISABLE KEYS */;
/*!40000 ALTER TABLE `alumni_students` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `attendence_type`
--

DROP TABLE IF EXISTS `attendence_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `attendence_type` (
  `id` int NOT NULL AUTO_INCREMENT,
  `type` varchar(50) DEFAULT NULL,
  `key_value` varchar(50) NOT NULL,
  `is_active` varchar(255) DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attendence_type`
--

LOCK TABLES `attendence_type` WRITE;
/*!40000 ALTER TABLE `attendence_type` DISABLE KEYS */;
INSERT INTO `attendence_type` VALUES (1,'Present','<b class=\"text text-success\">P</b>','yes','2016-06-23 18:11:37','0000-00-00'),(2,'Late With Excuse','<b class=\"text text-warning\">E</b>','no','2018-05-29 08:19:48','0000-00-00'),(3,'Late','<b class=\"text text-warning\">L</b>','yes','2016-06-23 18:12:28','0000-00-00'),(4,'Absent','<b class=\"text text-danger\">A</b>','yes','2016-10-11 11:35:40','0000-00-00'),(5,'Holiday','H','yes','2016-10-11 11:35:01','0000-00-00'),(6,'Half Day','<b class=\"text text-warning\">F</b>','yes','2016-06-23 18:12:28','0000-00-00');
/*!40000 ALTER TABLE `attendence_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `book_issues`
--

DROP TABLE IF EXISTS `book_issues`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `book_issues` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `book_id` int DEFAULT NULL,
  `duereturn_date` date DEFAULT NULL,
  `return_date` date DEFAULT NULL,
  `issue_date` date DEFAULT NULL,
  `is_returned` int DEFAULT '0',
  `member_id` int DEFAULT NULL,
  `is_active` varchar(10) NOT NULL DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book_issues`
--

LOCK TABLES `book_issues` WRITE;
/*!40000 ALTER TABLE `book_issues` DISABLE KEYS */;
/*!40000 ALTER TABLE `book_issues` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `books`
--

DROP TABLE IF EXISTS `books`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `books` (
  `id` int NOT NULL AUTO_INCREMENT,
  `book_title` varchar(100) NOT NULL,
  `book_no` varchar(50) NOT NULL,
  `isbn_no` varchar(100) NOT NULL,
  `subject` varchar(100) DEFAULT NULL,
  `rack_no` varchar(100) NOT NULL,
  `publish` varchar(100) DEFAULT NULL,
  `author` varchar(100) DEFAULT NULL,
  `qty` int DEFAULT NULL,
  `perunitcost` float(10,2) DEFAULT NULL,
  `postdate` date DEFAULT NULL,
  `description` text,
  `available` varchar(10) DEFAULT 'yes',
  `is_active` varchar(255) DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `books`
--

LOCK TABLES `books` WRITE;
/*!40000 ALTER TABLE `books` DISABLE KEYS */;
/*!40000 ALTER TABLE `books` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `captcha`
--

DROP TABLE IF EXISTS `captcha`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `captcha` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `status` int NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `captcha`
--

LOCK TABLES `captcha` WRITE;
/*!40000 ALTER TABLE `captcha` DISABLE KEYS */;
INSERT INTO `captcha` VALUES (1,'userlogin',0,'2022-09-22 08:34:04'),(2,'login',0,'2021-01-19 08:10:31'),(3,'admission',0,'2021-01-19 04:48:11'),(4,'complain',0,'2021-01-19 04:48:13'),(5,'contact_us',0,'2022-09-22 11:30:33');
/*!40000 ALTER TABLE `captcha` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `categories` (
  `id` int NOT NULL AUTO_INCREMENT,
  `category` varchar(100) DEFAULT NULL,
  `is_active` varchar(255) DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `certificates`
--

DROP TABLE IF EXISTS `certificates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `certificates` (
  `id` int NOT NULL AUTO_INCREMENT,
  `certificate_name` varchar(100) NOT NULL,
  `certificate_text` text NOT NULL,
  `left_header` varchar(100) NOT NULL,
  `center_header` varchar(100) NOT NULL,
  `right_header` varchar(100) NOT NULL,
  `left_footer` varchar(100) NOT NULL,
  `right_footer` varchar(100) NOT NULL,
  `center_footer` varchar(100) NOT NULL,
  `background_image` varchar(100) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  `created_for` tinyint(1) NOT NULL COMMENT '1 = staff, 2 = students',
  `status` tinyint(1) NOT NULL,
  `header_height` int NOT NULL,
  `content_height` int NOT NULL,
  `footer_height` int NOT NULL,
  `content_width` int NOT NULL,
  `enable_student_image` tinyint(1) NOT NULL COMMENT '0=no,1=yes',
  `enable_image_height` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `certificates`
--

LOCK TABLES `certificates` WRITE;
/*!40000 ALTER TABLE `certificates` DISABLE KEYS */;
/*!40000 ALTER TABLE `certificates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chat_connections`
--

DROP TABLE IF EXISTS `chat_connections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `chat_connections` (
  `id` int NOT NULL AUTO_INCREMENT,
  `chat_user_one` int NOT NULL,
  `chat_user_two` int NOT NULL,
  `ip` varchar(30) DEFAULT NULL,
  `time` int DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `chat_user_one` (`chat_user_one`),
  KEY `chat_user_two` (`chat_user_two`),
  CONSTRAINT `chat_connections_ibfk_1` FOREIGN KEY (`chat_user_one`) REFERENCES `chat_users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `chat_connections_ibfk_2` FOREIGN KEY (`chat_user_two`) REFERENCES `chat_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chat_connections`
--

LOCK TABLES `chat_connections` WRITE;
/*!40000 ALTER TABLE `chat_connections` DISABLE KEYS */;
/*!40000 ALTER TABLE `chat_connections` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chat_messages`
--

DROP TABLE IF EXISTS `chat_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `chat_messages` (
  `id` int NOT NULL AUTO_INCREMENT,
  `message` text,
  `chat_user_id` int NOT NULL,
  `ip` varchar(30) DEFAULT NULL,
  `time` int DEFAULT NULL,
  `is_first` int DEFAULT '0',
  `is_read` int NOT NULL DEFAULT '0',
  `chat_connection_id` int NOT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `chat_user_id` (`chat_user_id`),
  KEY `chat_connection_id` (`chat_connection_id`),
  CONSTRAINT `chat_messages_ibfk_1` FOREIGN KEY (`chat_user_id`) REFERENCES `chat_users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `chat_messages_ibfk_2` FOREIGN KEY (`chat_connection_id`) REFERENCES `chat_connections` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chat_messages`
--

LOCK TABLES `chat_messages` WRITE;
/*!40000 ALTER TABLE `chat_messages` DISABLE KEYS */;
/*!40000 ALTER TABLE `chat_messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chat_users`
--

DROP TABLE IF EXISTS `chat_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `chat_users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_type` varchar(20) DEFAULT NULL,
  `staff_id` int DEFAULT NULL,
  `student_id` int DEFAULT NULL,
  `create_staff_id` int DEFAULT NULL,
  `create_student_id` int DEFAULT NULL,
  `is_active` int DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `staff_id` (`staff_id`),
  KEY `student_id` (`student_id`),
  KEY `create_staff_id` (`create_staff_id`),
  KEY `create_student_id` (`create_student_id`),
  CONSTRAINT `chat_users_ibfk_1` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`id`) ON DELETE CASCADE,
  CONSTRAINT `chat_users_ibfk_2` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`) ON DELETE CASCADE,
  CONSTRAINT `chat_users_ibfk_3` FOREIGN KEY (`create_staff_id`) REFERENCES `staff` (`id`) ON DELETE CASCADE,
  CONSTRAINT `chat_users_ibfk_4` FOREIGN KEY (`create_student_id`) REFERENCES `students` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chat_users`
--

LOCK TABLES `chat_users` WRITE;
/*!40000 ALTER TABLE `chat_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `chat_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `class_sections`
--

DROP TABLE IF EXISTS `class_sections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `class_sections` (
  `id` int NOT NULL AUTO_INCREMENT,
  `class_id` int DEFAULT NULL,
  `section_id` int DEFAULT NULL,
  `is_active` varchar(255) DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `class_id` (`class_id`),
  KEY `section_id` (`section_id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `class_sections`
--

LOCK TABLES `class_sections` WRITE;
/*!40000 ALTER TABLE `class_sections` DISABLE KEYS */;
/*!40000 ALTER TABLE `class_sections` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `class_teacher`
--

DROP TABLE IF EXISTS `class_teacher`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `class_teacher` (
  `id` int NOT NULL AUTO_INCREMENT,
  `class_id` int NOT NULL,
  `staff_id` int NOT NULL,
  `section_id` int NOT NULL,
  `session_id` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `class_teacher`
--

LOCK TABLES `class_teacher` WRITE;
/*!40000 ALTER TABLE `class_teacher` DISABLE KEYS */;
/*!40000 ALTER TABLE `class_teacher` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `classes`
--

DROP TABLE IF EXISTS `classes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `classes` (
  `id` int NOT NULL AUTO_INCREMENT,
  `class` varchar(60) DEFAULT NULL,
  `is_active` varchar(255) DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `classes`
--

LOCK TABLES `classes` WRITE;
/*!40000 ALTER TABLE `classes` DISABLE KEYS */;
/*!40000 ALTER TABLE `classes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `complaint`
--

DROP TABLE IF EXISTS `complaint`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `complaint` (
  `id` int NOT NULL AUTO_INCREMENT,
  `complaint_type` varchar(255) NOT NULL,
  `source` varchar(15) NOT NULL,
  `name` varchar(100) NOT NULL,
  `contact` varchar(15) NOT NULL,
  `email` varchar(200) NOT NULL,
  `date` date NOT NULL,
  `description` text NOT NULL,
  `action_taken` varchar(200) DEFAULT NULL,
  `assigned` varchar(50) DEFAULT NULL,
  `note` text,
  `image` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `complaint`
--

LOCK TABLES `complaint` WRITE;
/*!40000 ALTER TABLE `complaint` DISABLE KEYS */;
/*!40000 ALTER TABLE `complaint` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `complaint_type`
--

DROP TABLE IF EXISTS `complaint_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `complaint_type` (
  `id` int NOT NULL AUTO_INCREMENT,
  `complaint_type` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `complaint_type`
--

LOCK TABLES `complaint_type` WRITE;
/*!40000 ALTER TABLE `complaint_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `complaint_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content_for`
--

DROP TABLE IF EXISTS `content_for`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `content_for` (
  `id` int NOT NULL AUTO_INCREMENT,
  `role` varchar(50) DEFAULT NULL,
  `content_id` int DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `content_id` (`content_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `content_for_ibfk_1` FOREIGN KEY (`content_id`) REFERENCES `contents` (`id`) ON DELETE CASCADE,
  CONSTRAINT `content_for_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content_for`
--

LOCK TABLES `content_for` WRITE;
/*!40000 ALTER TABLE `content_for` DISABLE KEYS */;
/*!40000 ALTER TABLE `content_for` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contents`
--

DROP TABLE IF EXISTS `contents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `contents` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `is_public` varchar(10) DEFAULT 'No',
  `class_id` int DEFAULT NULL,
  `cls_sec_id` int NOT NULL,
  `file` varchar(250) DEFAULT NULL,
  `created_by` int NOT NULL,
  `note` text,
  `is_active` varchar(255) DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contents`
--

LOCK TABLES `contents` WRITE;
/*!40000 ALTER TABLE `contents` DISABLE KEYS */;
/*!40000 ALTER TABLE `contents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `custom_field_values`
--

DROP TABLE IF EXISTS `custom_field_values`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `custom_field_values` (
  `id` int NOT NULL AUTO_INCREMENT,
  `belong_table_id` int DEFAULT NULL,
  `custom_field_id` int DEFAULT NULL,
  `field_value` longtext,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `custom_field_id` (`custom_field_id`),
  CONSTRAINT `custom_field_values_ibfk_1` FOREIGN KEY (`custom_field_id`) REFERENCES `custom_fields` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `custom_field_values`
--

LOCK TABLES `custom_field_values` WRITE;
/*!40000 ALTER TABLE `custom_field_values` DISABLE KEYS */;
/*!40000 ALTER TABLE `custom_field_values` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `custom_fields`
--

DROP TABLE IF EXISTS `custom_fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `custom_fields` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `belong_to` varchar(100) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `bs_column` int DEFAULT NULL,
  `validation` int DEFAULT '0',
  `field_values` text,
  `show_table` varchar(100) DEFAULT NULL,
  `visible_on_table` int NOT NULL,
  `weight` int DEFAULT NULL,
  `is_active` int DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `custom_fields`
--

LOCK TABLES `custom_fields` WRITE;
/*!40000 ALTER TABLE `custom_fields` DISABLE KEYS */;
/*!40000 ALTER TABLE `custom_fields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `department`
--

DROP TABLE IF EXISTS `department`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `department` (
  `id` int NOT NULL AUTO_INCREMENT,
  `department_name` varchar(200) NOT NULL,
  `is_active` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `department`
--

LOCK TABLES `department` WRITE;
/*!40000 ALTER TABLE `department` DISABLE KEYS */;
/*!40000 ALTER TABLE `department` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `disable_reason`
--

DROP TABLE IF EXISTS `disable_reason`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `disable_reason` (
  `id` int NOT NULL AUTO_INCREMENT,
  `reason` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `disable_reason`
--

LOCK TABLES `disable_reason` WRITE;
/*!40000 ALTER TABLE `disable_reason` DISABLE KEYS */;
/*!40000 ALTER TABLE `disable_reason` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dispatch_receive`
--

DROP TABLE IF EXISTS `dispatch_receive`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dispatch_receive` (
  `id` int NOT NULL AUTO_INCREMENT,
  `reference_no` varchar(50) NOT NULL,
  `to_title` varchar(100) NOT NULL,
  `address` varchar(500) NOT NULL,
  `note` varchar(500) NOT NULL,
  `from_title` varchar(200) NOT NULL,
  `date` varchar(20) NOT NULL,
  `image` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  `type` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dispatch_receive`
--

LOCK TABLES `dispatch_receive` WRITE;
/*!40000 ALTER TABLE `dispatch_receive` DISABLE KEYS */;
/*!40000 ALTER TABLE `dispatch_receive` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `email_config`
--

DROP TABLE IF EXISTS `email_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `email_config` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `email_type` varchar(100) DEFAULT NULL,
  `smtp_server` varchar(100) DEFAULT NULL,
  `smtp_port` varchar(100) DEFAULT NULL,
  `smtp_username` varchar(100) DEFAULT NULL,
  `smtp_password` varchar(100) DEFAULT NULL,
  `ssl_tls` varchar(100) DEFAULT NULL,
  `smtp_auth` varchar(10) NOT NULL,
  `is_active` varchar(10) NOT NULL DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email_config`
--

LOCK TABLES `email_config` WRITE;
/*!40000 ALTER TABLE `email_config` DISABLE KEYS */;
/*!40000 ALTER TABLE `email_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `enquiry`
--

DROP TABLE IF EXISTS `enquiry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `enquiry` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `contact` varchar(20) NOT NULL,
  `address` text NOT NULL,
  `reference` varchar(20) NOT NULL,
  `date` date NOT NULL,
  `description` varchar(500) NOT NULL,
  `follow_up_date` date NOT NULL,
  `note` text NOT NULL,
  `source` varchar(50) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `assigned` varchar(100) NOT NULL,
  `class` int NOT NULL,
  `no_of_child` varchar(11) DEFAULT NULL,
  `status` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `enquiry`
--

LOCK TABLES `enquiry` WRITE;
/*!40000 ALTER TABLE `enquiry` DISABLE KEYS */;
/*!40000 ALTER TABLE `enquiry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `enquiry_type`
--

DROP TABLE IF EXISTS `enquiry_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `enquiry_type` (
  `id` int NOT NULL AUTO_INCREMENT,
  `enquiry_type` varchar(100) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `enquiry_type`
--

LOCK TABLES `enquiry_type` WRITE;
/*!40000 ALTER TABLE `enquiry_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `enquiry_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `events`
--

DROP TABLE IF EXISTS `events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `events` (
  `id` int NOT NULL AUTO_INCREMENT,
  `event_title` varchar(200) NOT NULL,
  `event_description` varchar(300) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `event_type` varchar(100) NOT NULL,
  `event_color` varchar(200) NOT NULL,
  `event_for` varchar(100) NOT NULL,
  `role_id` int NOT NULL,
  `is_active` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `events`
--

LOCK TABLES `events` WRITE;
/*!40000 ALTER TABLE `events` DISABLE KEYS */;
/*!40000 ALTER TABLE `events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exam_group_class_batch_exam_students`
--

DROP TABLE IF EXISTS `exam_group_class_batch_exam_students`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `exam_group_class_batch_exam_students` (
  `id` int NOT NULL AUTO_INCREMENT,
  `exam_group_class_batch_exam_id` int NOT NULL,
  `student_id` int NOT NULL,
  `student_session_id` int NOT NULL,
  `roll_no` int NOT NULL DEFAULT '0',
  `teacher_remark` text,
  `is_active` int DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `exam_group_class_batch_exam_id` (`exam_group_class_batch_exam_id`),
  KEY `student_id` (`student_id`),
  KEY `student_session_id` (`student_session_id`),
  CONSTRAINT `exam_group_class_batch_exam_students_ibfk_1` FOREIGN KEY (`exam_group_class_batch_exam_id`) REFERENCES `exam_group_class_batch_exams` (`id`) ON DELETE CASCADE,
  CONSTRAINT `exam_group_class_batch_exam_students_ibfk_2` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`) ON DELETE CASCADE,
  CONSTRAINT `exam_group_class_batch_exam_students_ibfk_3` FOREIGN KEY (`student_session_id`) REFERENCES `student_session` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=256 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exam_group_class_batch_exam_students`
--

LOCK TABLES `exam_group_class_batch_exam_students` WRITE;
/*!40000 ALTER TABLE `exam_group_class_batch_exam_students` DISABLE KEYS */;
/*!40000 ALTER TABLE `exam_group_class_batch_exam_students` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exam_group_class_batch_exam_subjects`
--

DROP TABLE IF EXISTS `exam_group_class_batch_exam_subjects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `exam_group_class_batch_exam_subjects` (
  `id` int NOT NULL AUTO_INCREMENT,
  `exam_group_class_batch_exams_id` int DEFAULT NULL,
  `subject_id` int NOT NULL,
  `date_from` date NOT NULL,
  `time_from` time NOT NULL,
  `duration` varchar(50) NOT NULL,
  `room_no` varchar(100) DEFAULT NULL,
  `max_marks` float(10,2) DEFAULT NULL,
  `min_marks` float(10,2) DEFAULT NULL,
  `credit_hours` float(10,2) DEFAULT '0.00',
  `date_to` datetime DEFAULT NULL,
  `is_active` int DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  `practical_max` float(10,2) DEFAULT NULL,
  `practical_min` float(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `exam_group_class_batch_exams_id` (`exam_group_class_batch_exams_id`),
  KEY `subject_id` (`subject_id`),
  CONSTRAINT `exam_group_class_batch_exam_subjects_ibfk_1` FOREIGN KEY (`exam_group_class_batch_exams_id`) REFERENCES `exam_group_class_batch_exams` (`id`) ON DELETE CASCADE,
  CONSTRAINT `exam_group_class_batch_exam_subjects_ibfk_2` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=139 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exam_group_class_batch_exam_subjects`
--

LOCK TABLES `exam_group_class_batch_exam_subjects` WRITE;
/*!40000 ALTER TABLE `exam_group_class_batch_exam_subjects` DISABLE KEYS */;
/*!40000 ALTER TABLE `exam_group_class_batch_exam_subjects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exam_group_class_batch_exams`
--

DROP TABLE IF EXISTS `exam_group_class_batch_exams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `exam_group_class_batch_exams` (
  `id` int NOT NULL AUTO_INCREMENT,
  `exam` varchar(250) DEFAULT NULL,
  `session_id` int NOT NULL,
  `date_from` date DEFAULT NULL,
  `date_to` date DEFAULT NULL,
  `description` text,
  `exam_group_id` int DEFAULT NULL,
  `use_exam_roll_no` int NOT NULL DEFAULT '1',
  `is_publish` int DEFAULT '0',
  `is_active` int DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `exam_group_id` (`exam_group_id`),
  CONSTRAINT `exam_group_class_batch_exams_ibfk_1` FOREIGN KEY (`exam_group_id`) REFERENCES `exam_groups` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exam_group_class_batch_exams`
--

LOCK TABLES `exam_group_class_batch_exams` WRITE;
/*!40000 ALTER TABLE `exam_group_class_batch_exams` DISABLE KEYS */;
/*!40000 ALTER TABLE `exam_group_class_batch_exams` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exam_group_exam_connections`
--

DROP TABLE IF EXISTS `exam_group_exam_connections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `exam_group_exam_connections` (
  `id` int NOT NULL AUTO_INCREMENT,
  `exam_group_id` int DEFAULT NULL,
  `exam_group_class_batch_exams_id` int DEFAULT NULL,
  `exam_weightage` float(10,2) DEFAULT '0.00',
  `is_active` int DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `exam_group_id` (`exam_group_id`),
  KEY `exam_group_class_batch_exams_id` (`exam_group_class_batch_exams_id`),
  CONSTRAINT `exam_group_exam_connections_ibfk_1` FOREIGN KEY (`exam_group_id`) REFERENCES `exam_groups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `exam_group_exam_connections_ibfk_2` FOREIGN KEY (`exam_group_class_batch_exams_id`) REFERENCES `exam_group_class_batch_exams` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exam_group_exam_connections`
--

LOCK TABLES `exam_group_exam_connections` WRITE;
/*!40000 ALTER TABLE `exam_group_exam_connections` DISABLE KEYS */;
/*!40000 ALTER TABLE `exam_group_exam_connections` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exam_group_exam_results`
--

DROP TABLE IF EXISTS `exam_group_exam_results`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `exam_group_exam_results` (
  `id` int NOT NULL AUTO_INCREMENT,
  `exam_group_class_batch_exam_student_id` int NOT NULL,
  `exam_group_class_batch_exam_subject_id` int DEFAULT NULL,
  `attendence` varchar(10) DEFAULT NULL,
  `get_marks` float(10,2) DEFAULT '0.00',
  `note` text,
  `is_active` int DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  `exam_group_student_id` int DEFAULT NULL,
  `get_marks_practical` float(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `exam_group_class_batch_exam_subject_id` (`exam_group_class_batch_exam_subject_id`),
  KEY `exam_group_student_id` (`exam_group_student_id`),
  KEY `exam_group_class_batch_exam_student_id` (`exam_group_class_batch_exam_student_id`),
  CONSTRAINT `exam_group_exam_results_ibfk_1` FOREIGN KEY (`exam_group_class_batch_exam_subject_id`) REFERENCES `exam_group_class_batch_exam_subjects` (`id`) ON DELETE CASCADE,
  CONSTRAINT `exam_group_exam_results_ibfk_2` FOREIGN KEY (`exam_group_student_id`) REFERENCES `exam_group_students` (`id`) ON DELETE CASCADE,
  CONSTRAINT `exam_group_exam_results_ibfk_3` FOREIGN KEY (`exam_group_class_batch_exam_student_id`) REFERENCES `exam_group_class_batch_exam_students` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1236 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exam_group_exam_results`
--

LOCK TABLES `exam_group_exam_results` WRITE;
/*!40000 ALTER TABLE `exam_group_exam_results` DISABLE KEYS */;
/*!40000 ALTER TABLE `exam_group_exam_results` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exam_group_students`
--

DROP TABLE IF EXISTS `exam_group_students`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `exam_group_students` (
  `id` int NOT NULL AUTO_INCREMENT,
  `exam_group_id` int DEFAULT NULL,
  `student_id` int DEFAULT NULL,
  `student_session_id` int DEFAULT NULL,
  `is_active` int DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `exam_group_id` (`exam_group_id`),
  KEY `student_id` (`student_id`),
  CONSTRAINT `exam_group_students_ibfk_1` FOREIGN KEY (`exam_group_id`) REFERENCES `exam_groups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `exam_group_students_ibfk_2` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exam_group_students`
--

LOCK TABLES `exam_group_students` WRITE;
/*!40000 ALTER TABLE `exam_group_students` DISABLE KEYS */;
/*!40000 ALTER TABLE `exam_group_students` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exam_groups`
--

DROP TABLE IF EXISTS `exam_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `exam_groups` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  `exam_type` varchar(250) DEFAULT NULL,
  `description` text,
  `is_active` int DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exam_groups`
--

LOCK TABLES `exam_groups` WRITE;
/*!40000 ALTER TABLE `exam_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `exam_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exam_results`
--

DROP TABLE IF EXISTS `exam_results`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `exam_results` (
  `id` int NOT NULL AUTO_INCREMENT,
  `attendence` varchar(10) NOT NULL,
  `exam_schedule_id` int DEFAULT NULL,
  `student_id` int DEFAULT NULL,
  `get_marks` float(10,2) DEFAULT NULL,
  `note` text,
  `is_active` varchar(255) DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `exam_schedule_id` (`exam_schedule_id`),
  KEY `student_id` (`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exam_results`
--

LOCK TABLES `exam_results` WRITE;
/*!40000 ALTER TABLE `exam_results` DISABLE KEYS */;
/*!40000 ALTER TABLE `exam_results` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exam_schedules`
--

DROP TABLE IF EXISTS `exam_schedules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `exam_schedules` (
  `id` int NOT NULL AUTO_INCREMENT,
  `session_id` int NOT NULL,
  `exam_id` int DEFAULT NULL,
  `teacher_subject_id` int DEFAULT NULL,
  `date_of_exam` date DEFAULT NULL,
  `start_to` varchar(50) DEFAULT NULL,
  `end_from` varchar(50) DEFAULT NULL,
  `room_no` varchar(50) DEFAULT NULL,
  `full_marks` int DEFAULT NULL,
  `passing_marks` int DEFAULT NULL,
  `note` text,
  `is_active` varchar(255) DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `teacher_subject_id` (`teacher_subject_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exam_schedules`
--

LOCK TABLES `exam_schedules` WRITE;
/*!40000 ALTER TABLE `exam_schedules` DISABLE KEYS */;
/*!40000 ALTER TABLE `exam_schedules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exams`
--

DROP TABLE IF EXISTS `exams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `exams` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `sesion_id` int NOT NULL,
  `note` text,
  `is_active` varchar(255) DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exams`
--

LOCK TABLES `exams` WRITE;
/*!40000 ALTER TABLE `exams` DISABLE KEYS */;
/*!40000 ALTER TABLE `exams` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `expense_head`
--

DROP TABLE IF EXISTS `expense_head`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `expense_head` (
  `id` int NOT NULL AUTO_INCREMENT,
  `exp_category` varchar(50) DEFAULT NULL,
  `description` text,
  `is_active` varchar(255) DEFAULT 'yes',
  `is_deleted` varchar(255) DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `expense_head`
--

LOCK TABLES `expense_head` WRITE;
/*!40000 ALTER TABLE `expense_head` DISABLE KEYS */;
/*!40000 ALTER TABLE `expense_head` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `expenses`
--

DROP TABLE IF EXISTS `expenses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `expenses` (
  `id` int NOT NULL AUTO_INCREMENT,
  `exp_head_id` int DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `invoice_no` varchar(200) NOT NULL,
  `date` date DEFAULT NULL,
  `amount` float(10,2) DEFAULT NULL,
  `documents` varchar(255) DEFAULT NULL,
  `note` text,
  `is_active` varchar(255) DEFAULT 'yes',
  `is_deleted` varchar(255) DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `expenses`
--

LOCK TABLES `expenses` WRITE;
/*!40000 ALTER TABLE `expenses` DISABLE KEYS */;
/*!40000 ALTER TABLE `expenses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fee_groups`
--

DROP TABLE IF EXISTS `fee_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `fee_groups` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `is_system` int NOT NULL DEFAULT '0',
  `description` text,
  `is_active` varchar(10) NOT NULL DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fee_groups`
--

LOCK TABLES `fee_groups` WRITE;
/*!40000 ALTER TABLE `fee_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `fee_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fee_groups_feetype`
--

DROP TABLE IF EXISTS `fee_groups_feetype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `fee_groups_feetype` (
  `id` int NOT NULL AUTO_INCREMENT,
  `fee_session_group_id` int DEFAULT NULL,
  `fee_groups_id` int DEFAULT NULL,
  `feetype_id` int DEFAULT NULL,
  `session_id` int DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `fine_type` varchar(50) NOT NULL DEFAULT 'none',
  `due_date` text,
  `fine_percentage` float(10,2) NOT NULL DEFAULT '0.00',
  `fine_amount` float(10,2) NOT NULL DEFAULT '0.00',
  `is_active` varchar(10) NOT NULL DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fee_session_group_id` (`fee_session_group_id`),
  KEY `fee_groups_id` (`fee_groups_id`),
  KEY `feetype_id` (`feetype_id`),
  KEY `session_id` (`session_id`),
  CONSTRAINT `fee_groups_feetype_ibfk_1` FOREIGN KEY (`fee_session_group_id`) REFERENCES `fee_session_groups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fee_groups_feetype_ibfk_2` FOREIGN KEY (`fee_groups_id`) REFERENCES `fee_groups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fee_groups_feetype_ibfk_3` FOREIGN KEY (`feetype_id`) REFERENCES `feetype` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fee_groups_feetype_ibfk_4` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fee_groups_feetype`
--

LOCK TABLES `fee_groups_feetype` WRITE;
/*!40000 ALTER TABLE `fee_groups_feetype` DISABLE KEYS */;
/*!40000 ALTER TABLE `fee_groups_feetype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fee_receipt_no`
--

DROP TABLE IF EXISTS `fee_receipt_no`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `fee_receipt_no` (
  `id` int NOT NULL AUTO_INCREMENT,
  `payment` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fee_receipt_no`
--

LOCK TABLES `fee_receipt_no` WRITE;
/*!40000 ALTER TABLE `fee_receipt_no` DISABLE KEYS */;
/*!40000 ALTER TABLE `fee_receipt_no` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fee_session_groups`
--

DROP TABLE IF EXISTS `fee_session_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `fee_session_groups` (
  `id` int NOT NULL AUTO_INCREMENT,
  `fee_groups_id` int DEFAULT NULL,
  `session_id` int DEFAULT NULL,
  `is_active` varchar(10) NOT NULL DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fee_groups_id` (`fee_groups_id`),
  KEY `session_id` (`session_id`),
  CONSTRAINT `fee_session_groups_ibfk_1` FOREIGN KEY (`fee_groups_id`) REFERENCES `fee_groups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fee_session_groups_ibfk_2` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fee_session_groups`
--

LOCK TABLES `fee_session_groups` WRITE;
/*!40000 ALTER TABLE `fee_session_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `fee_session_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feecategory`
--

DROP TABLE IF EXISTS `feecategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `feecategory` (
  `id` int NOT NULL AUTO_INCREMENT,
  `category` varchar(50) DEFAULT NULL,
  `is_active` varchar(255) DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feecategory`
--

LOCK TABLES `feecategory` WRITE;
/*!40000 ALTER TABLE `feecategory` DISABLE KEYS */;
/*!40000 ALTER TABLE `feecategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feemasters`
--

DROP TABLE IF EXISTS `feemasters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `feemasters` (
  `id` int NOT NULL AUTO_INCREMENT,
  `session_id` int DEFAULT NULL,
  `feetype_id` int NOT NULL,
  `class_id` int DEFAULT NULL,
  `amount` float(10,2) DEFAULT NULL,
  `description` text,
  `is_active` varchar(255) DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feemasters`
--

LOCK TABLES `feemasters` WRITE;
/*!40000 ALTER TABLE `feemasters` DISABLE KEYS */;
/*!40000 ALTER TABLE `feemasters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fees_discounts`
--

DROP TABLE IF EXISTS `fees_discounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `fees_discounts` (
  `id` int NOT NULL AUTO_INCREMENT,
  `session_id` int DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `code` varchar(100) DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `description` text,
  `is_active` varchar(10) NOT NULL DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `feetype_id` int NOT NULL,
  `discount_percent` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `session_id` (`session_id`),
  CONSTRAINT `fees_discounts_ibfk_1` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fees_discounts`
--

LOCK TABLES `fees_discounts` WRITE;
/*!40000 ALTER TABLE `fees_discounts` DISABLE KEYS */;
/*!40000 ALTER TABLE `fees_discounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fees_reminder`
--

DROP TABLE IF EXISTS `fees_reminder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `fees_reminder` (
  `id` int NOT NULL AUTO_INCREMENT,
  `reminder_type` varchar(10) DEFAULT NULL,
  `day` int DEFAULT NULL,
  `is_active` int DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fees_reminder`
--

LOCK TABLES `fees_reminder` WRITE;
/*!40000 ALTER TABLE `fees_reminder` DISABLE KEYS */;
/*!40000 ALTER TABLE `fees_reminder` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feetype`
--

DROP TABLE IF EXISTS `feetype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `feetype` (
  `id` int NOT NULL AUTO_INCREMENT,
  `is_system` int NOT NULL DEFAULT '0',
  `feecategory_id` int DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `code` varchar(100) NOT NULL,
  `is_active` varchar(255) DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  `description` text NOT NULL,
  `glname` varchar(255) DEFAULT NULL,
  `submission_type` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feetype`
--

LOCK TABLES `feetype` WRITE;
/*!40000 ALTER TABLE `feetype` DISABLE KEYS */;
/*!40000 ALTER TABLE `feetype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `filetypes`
--

DROP TABLE IF EXISTS `filetypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `filetypes` (
  `id` int NOT NULL AUTO_INCREMENT,
  `file_extension` text,
  `file_mime` text,
  `file_size` int NOT NULL,
  `image_extension` text,
  `image_mime` text,
  `image_size` int NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `filetypes`
--

LOCK TABLES `filetypes` WRITE;
/*!40000 ALTER TABLE `filetypes` DISABLE KEYS */;
INSERT INTO `filetypes` VALUES (1,'pdf, zip, jpg, jpeg, png, txt, 7z, gif, csv, docx, mp3, mp4, accdb, odt, ods, ppt, pptx, xlsx, wmv, jfif, apk, ppt, bmp, jpe, mdb, rar, xls, svg','application/pdf, image/zip, image/jpg, image/png, image/jpeg, text/plain, application/x-zip-compressed, application/zip, image/gif, text/csv, application/vnd.openxmlformats-officedocument.wordprocessingml.document, audio/mpeg, application/msaccess, application/vnd.oasis.opendocument.text, application/vnd.oasis.opendocument.spreadsheet, application/vnd.ms-powerpoint, application/vnd.openxmlformats-officedocument.presentationml.presentation, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, video/x-ms-wmv, video/mp4, image/jpeg, application/vnd.android.package-archive, application/x-msdownload, application/vnd.ms-powerpoint, image/bmp, image/jpeg, application/msaccess, application/vnd.ms-excel, image/svg+xml',100048576,'jfif, png, jpe, jpeg, jpg, bmp, gif, svg','image/jpeg, image/png, image/jpeg, image/jpeg, image/bmp, image/gif, image/x-ms-bmp, image/svg+xml',10048576,'2021-01-30 13:03:03');
/*!40000 ALTER TABLE `filetypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `follow_up`
--

DROP TABLE IF EXISTS `follow_up`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `follow_up` (
  `id` int NOT NULL AUTO_INCREMENT,
  `enquiry_id` int NOT NULL,
  `date` date NOT NULL,
  `next_date` date NOT NULL,
  `response` text NOT NULL,
  `note` text NOT NULL,
  `followup_by` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `follow_up`
--

LOCK TABLES `follow_up` WRITE;
/*!40000 ALTER TABLE `follow_up` DISABLE KEYS */;
/*!40000 ALTER TABLE `follow_up` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `front_cms_media_gallery`
--

DROP TABLE IF EXISTS `front_cms_media_gallery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `front_cms_media_gallery` (
  `id` int NOT NULL AUTO_INCREMENT,
  `image` varchar(300) DEFAULT NULL,
  `thumb_path` varchar(300) DEFAULT NULL,
  `dir_path` varchar(300) DEFAULT NULL,
  `img_name` varchar(300) DEFAULT NULL,
  `thumb_name` varchar(300) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `file_type` varchar(100) NOT NULL,
  `file_size` varchar(100) NOT NULL,
  `vid_url` text NOT NULL,
  `vid_title` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `front_cms_media_gallery`
--

LOCK TABLES `front_cms_media_gallery` WRITE;
/*!40000 ALTER TABLE `front_cms_media_gallery` DISABLE KEYS */;
/*!40000 ALTER TABLE `front_cms_media_gallery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `front_cms_menu_items`
--

DROP TABLE IF EXISTS `front_cms_menu_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `front_cms_menu_items` (
  `id` int NOT NULL AUTO_INCREMENT,
  `menu_id` int NOT NULL,
  `menu` varchar(100) DEFAULT NULL,
  `page_id` int NOT NULL,
  `parent_id` int NOT NULL,
  `ext_url` text,
  `open_new_tab` int DEFAULT '0',
  `ext_url_link` text,
  `slug` varchar(200) DEFAULT NULL,
  `weight` int DEFAULT NULL,
  `publish` int NOT NULL DEFAULT '0',
  `description` text,
  `is_active` varchar(10) DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `front_cms_menu_items`
--

LOCK TABLES `front_cms_menu_items` WRITE;
/*!40000 ALTER TABLE `front_cms_menu_items` DISABLE KEYS */;
INSERT INTO `front_cms_menu_items` VALUES (1,1,'Home',1,0,NULL,NULL,NULL,'home',1,0,NULL,'no','2019-12-02 22:11:50'),(2,1,'Contact Us',4,0,NULL,NULL,NULL,'contact-us',4,0,NULL,'no','2022-07-25 04:27:49'),(3,1,'Complain',2,0,NULL,NULL,NULL,'complain',3,0,NULL,'no','2022-07-25 04:27:49'),(4,1,'Online Admission',5,0,NULL,NULL,'https://design.eshikshya.com.np/online_admission','online-admission',2,0,NULL,'no','2022-10-16 11:30:16'),(5,2,'Online Admission',5,0,NULL,NULL,NULL,'online-admission-1',NULL,0,NULL,'no','2022-11-23 04:04:09'),(6,2,'Contact Us',4,0,NULL,NULL,NULL,'contact-us-1',NULL,0,NULL,'no','2022-11-23 04:04:42');
/*!40000 ALTER TABLE `front_cms_menu_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `front_cms_menus`
--

DROP TABLE IF EXISTS `front_cms_menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `front_cms_menus` (
  `id` int NOT NULL AUTO_INCREMENT,
  `menu` varchar(100) DEFAULT NULL,
  `slug` varchar(200) DEFAULT NULL,
  `description` text,
  `open_new_tab` int NOT NULL DEFAULT '0',
  `ext_url` text NOT NULL,
  `ext_url_link` text NOT NULL,
  `publish` int NOT NULL DEFAULT '0',
  `content_type` varchar(10) NOT NULL DEFAULT 'manual',
  `is_active` varchar(10) DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `front_cms_menus`
--

LOCK TABLES `front_cms_menus` WRITE;
/*!40000 ALTER TABLE `front_cms_menus` DISABLE KEYS */;
INSERT INTO `front_cms_menus` VALUES (1,'Main Menu','main-menu','Main menu',0,'','',0,'default','no','2018-04-20 14:54:49'),(2,'Bottom Menu','bottom-menu','Bottom Menu',0,'','',0,'default','no','2018-04-20 14:54:55');
/*!40000 ALTER TABLE `front_cms_menus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `front_cms_page_contents`
--

DROP TABLE IF EXISTS `front_cms_page_contents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `front_cms_page_contents` (
  `id` int NOT NULL AUTO_INCREMENT,
  `page_id` int DEFAULT NULL,
  `content_type` varchar(50) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `page_id` (`page_id`),
  CONSTRAINT `front_cms_page_contents_ibfk_1` FOREIGN KEY (`page_id`) REFERENCES `front_cms_pages` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `front_cms_page_contents`
--

LOCK TABLES `front_cms_page_contents` WRITE;
/*!40000 ALTER TABLE `front_cms_page_contents` DISABLE KEYS */;
/*!40000 ALTER TABLE `front_cms_page_contents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `front_cms_pages`
--

DROP TABLE IF EXISTS `front_cms_pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `front_cms_pages` (
  `id` int NOT NULL AUTO_INCREMENT,
  `page_type` varchar(10) NOT NULL DEFAULT 'manual',
  `is_homepage` int DEFAULT '0',
  `title` varchar(250) DEFAULT NULL,
  `url` varchar(250) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `slug` varchar(200) DEFAULT NULL,
  `meta_title` text,
  `meta_description` text,
  `meta_keyword` text,
  `feature_image` varchar(200) NOT NULL,
  `description` longtext,
  `publish_date` date NOT NULL,
  `publish` int DEFAULT '0',
  `sidebar` int DEFAULT '0',
  `is_active` varchar(10) DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `front_cms_pages`
--

LOCK TABLES `front_cms_pages` WRITE;
/*!40000 ALTER TABLE `front_cms_pages` DISABLE KEYS */;
INSERT INTO `front_cms_pages` VALUES (1,'default',1,'Home','page/home','page','home','','','','','<h1><span style=\"font-size:18px\"><strong><img alt=\"\" src=\"https://static.vecteezy.com/system/resources/thumbnails/002/878/209/small/back-to-school-banner-with-realistic-school-supplies-on-chalkboard-background-vector.jpg\" style=\"max-width:100%; width:100%\" />e-Shikshya is a complete solution of school management, taking lead to digitize school and digitize Nepal.</strong></span></h1>\r\n\r\n<p><span style=\"font-size:16px\">We are the subsidiary company of prabhu group. One of the most trusted and loved brand of Nepal.&nbsp; We are serving nation since for more than 15 years.</span></p>\r\n\r\n<p><span style=\"font-size:16px\">This is one of the innovative products of our brand developed by most passionate researcher and developer. We are taking strong lead to digitize. We make every school digitalize through eShikshya Platform.&nbsp;</span></p>\r\n\r\n<p><span style=\"font-size:16px\"><strong><u>Why to choose us?</u></strong></span></p>\r\n\r\n<ul>\r\n	<li><span style=\"font-size:16px\">It is simple and easily accessible.</span></li>\r\n	<li><span style=\"font-size:16px\">10+ years of experience in IT filed.</span></li>\r\n	<li><span style=\"font-size:16px\">Managed under Prabhu group.</span></li>\r\n	<li><span style=\"font-size:16px\">Best after sale services.</span></li>\r\n	<li><span style=\"font-size:16px\">It is cost-effective product which saves your time and energy.</span></li>\r\n	<li><span style=\"font-size:16px\">Increases your productivity.</span></li>\r\n</ul>\r\n\r\n<h1><strong><u>Product Description</u></strong></h1>\r\n\r\n<p><span style=\"font-size:16px\">We provide simple, fast, secure and up-to-date school management software. A complete package of school solution i.e. student profile, hostel, library, billing and many more with in the same web application. This creative and innovative product helps you to saves your time effort manpower money and increase the accuracy and perfection of your work.</span></p>\r\n\r\n<p><span style=\"font-size:16px\">1. Why this software?</span></p>\r\n\r\n<ul>\r\n	<li><span style=\"font-size:16px\">this system will be digitalized.</span></li>\r\n	<li><span style=\"font-size:16px\">Quick and easy set up.</span></li>\r\n	<li><span style=\"font-size:16px\">Modern, simple and Affordable.</span></li>\r\n</ul>\r\n\r\n<p><span style=\"font-size:16px\">​​2. ​</span><span style=\"font-size:16px\">Do I get support after subscription?</span></p>\r\n\r\n<ul>\r\n	<li><span style=\"font-size:16px\">Yes, we will provide 24*7 services.</span></li>\r\n</ul>\r\n\r\n<p><span style=\"font-size:16px\">3.How can I believe my data is secure?</span></p>\r\n\r\n<ul>\r\n	<li><span style=\"font-size:16px\">Yes, your data is end-to end encrypted. We are using cloud based data storage system.</span></li>\r\n</ul>\r\n\r\n<p><span style=\"font-size:16px\">4. Is it available in mobile app?</span></p>\r\n\r\n<ul>\r\n	<li><span style=\"font-size:16px\">Yes, it is available in mobile app.</span></li>\r\n</ul>\r\n\r\n<p><span style=\"font-size:16px\">5.Can we customize this package?</span></p>\r\n\r\n<ul>\r\n	<li><span style=\"font-size:16px\">Well, this is web based app so you can use it according to your size of student.</span></li>\r\n</ul>\r\n\r\n<p><span style=\"font-size:16px\">6. Is there any extra charge ?</span></p>\r\n\r\n<ul>\r\n	<li><span style=\"font-size:16px\">No, there is no extra charge. We will charge per student according to the size of student.</span></li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>','0000-00-00',1,NULL,'no','2022-11-16 09:30:28'),(2,'default',0,'Complain','page/complain','page','complain','Complain form','                                                                                                                                                                                    complain form                                                                                                                                                                                                                                ','complain form','','<p>[form-builder:complain]</p>','0000-00-00',1,NULL,'no','2019-11-13 10:16:36'),(3,'default',0,'404 page','page/404-page','page','404-page','','                                ','','','<html>\r\n<head>\r\n <title></title>\r\n</head>\r\n<body>\r\n<p>404 page found</p>\r\n</body>\r\n</html>','0000-00-00',0,NULL,'no','2018-05-18 14:46:04'),(4,'default',0,'Contact us','page/contact-us','page','contact-us','','','','','<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-md-12 col-sm-12\">\r\n<div class=\"mapWrapper fullwidth\"><iframe height=\"500\" src=\"https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14128.554739549578!2d85.3263405!3d27.713004!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xcd532953589e3af9!2sCreation%20Soft%20Nepal!5e0!3m2!1sen!2snp!4v1668589442829!5m2!1sen!2snp\" style=\"border:0;\" width=\"100%\"></iframe></div>\r\n</div>\r\n\r\n<div class=\"col-md-4 col-sm-4\">\r\n<div class=\"contact-item\"><img src=\"https://demo.eshikshya.com.np/uploads/demo.eshikshya.com.np/gallery/media/pin.svg\" />\r\n<h3>Our Location</h3>\r\n\r\n<p>Sama Marg, Kathmandu</p>\r\n&nbsp;\r\n\r\n<p>Bagmati, Nepal</p>\r\n</div>\r\n<!--./contact-item--></div>\r\n<!--./col-md-4-->\r\n\r\n<div class=\"col-md-4 col-sm-4\">\r\n<div class=\"contact-item\"><img src=\"https://demo.eshikshya.com.np/uploads/demo.eshikshya.com.np/gallery/media/phone.png\" />\r\n<h3>CALL US</h3>\r\n\r\n<p>E-mail : info@eshikshya.com.np</p>\r\n&nbsp;\r\n\r\n<p>Mobile : +977-9818092407</p>\r\n</div>\r\n<!--./contact-item--></div>\r\n<!--./col-md-4-->\r\n\r\n<div class=\"col-md-4 col-sm-4\">\r\n<div class=\"contact-item\"><img src=\"https://demo.eshikshya.com.np/uploads/demo.eshikshya.com.np/gallery/media/clock.svg\" />\r\n<h3>Working Hours</h3>\r\n\r\n<p>Mon-Fri : 9 am to 5 pm</p>\r\n&nbsp;\r\n\r\n<p>Sat : 9 am to 3 pm</p>\r\n</div>\r\n<!--./contact-item--></div>\r\n\r\n<h2 style=\"text-align:center\">Feel Free to Contact Us.</h2>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<div class=\"col-md-12 col-sm-12 sm-pd0\">[form-builder:contact_us]<!--./row--></div>\r\n<!--./col-md-12--></div>\r\n<!--./row--></div>\r\n<!--./container--><!--./col-md-4-->','0000-00-00',0,NULL,'no','2022-11-27 06:45:25'),(5,'default',0,'Online Admission','online_admission','page','online-admission',NULL,NULL,NULL,'',NULL,'0000-00-00',1,0,'no','2022-09-16 09:30:22');
/*!40000 ALTER TABLE `front_cms_pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `front_cms_program_photos`
--

DROP TABLE IF EXISTS `front_cms_program_photos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `front_cms_program_photos` (
  `id` int NOT NULL AUTO_INCREMENT,
  `program_id` int DEFAULT NULL,
  `media_gallery_id` int NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `program_id` (`program_id`),
  CONSTRAINT `front_cms_program_photos_ibfk_1` FOREIGN KEY (`program_id`) REFERENCES `front_cms_programs` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `front_cms_program_photos`
--

LOCK TABLES `front_cms_program_photos` WRITE;
/*!40000 ALTER TABLE `front_cms_program_photos` DISABLE KEYS */;
/*!40000 ALTER TABLE `front_cms_program_photos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `front_cms_programs`
--

DROP TABLE IF EXISTS `front_cms_programs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `front_cms_programs` (
  `id` int NOT NULL AUTO_INCREMENT,
  `type` varchar(50) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `url` text,
  `title` varchar(200) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `event_start` date DEFAULT NULL,
  `event_end` date DEFAULT NULL,
  `event_venue` text,
  `description` text,
  `is_active` varchar(10) DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `meta_title` text NOT NULL,
  `meta_description` text NOT NULL,
  `meta_keyword` text NOT NULL,
  `feature_image` text NOT NULL,
  `publish_date` date NOT NULL,
  `publish` varchar(10) DEFAULT '0',
  `sidebar` int DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `front_cms_programs`
--

LOCK TABLES `front_cms_programs` WRITE;
/*!40000 ALTER TABLE `front_cms_programs` DISABLE KEYS */;
/*!40000 ALTER TABLE `front_cms_programs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `front_cms_settings`
--

DROP TABLE IF EXISTS `front_cms_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `front_cms_settings` (
  `id` int NOT NULL AUTO_INCREMENT,
  `theme` varchar(50) DEFAULT NULL,
  `is_active_rtl` int DEFAULT '0',
  `is_active_front_cms` int DEFAULT '0',
  `is_active_sidebar` int DEFAULT '0',
  `logo` varchar(200) DEFAULT NULL,
  `contact_us_email` varchar(100) DEFAULT NULL,
  `complain_form_email` varchar(100) DEFAULT NULL,
  `sidebar_options` text NOT NULL,
  `whatsapp_url` varchar(255) NOT NULL,
  `fb_url` varchar(200) NOT NULL,
  `twitter_url` varchar(200) NOT NULL,
  `youtube_url` varchar(200) NOT NULL,
  `google_plus` varchar(200) NOT NULL,
  `instagram_url` varchar(200) NOT NULL,
  `pinterest_url` varchar(200) NOT NULL,
  `linkedin_url` varchar(200) NOT NULL,
  `google_analytics` text,
  `footer_text` varchar(500) DEFAULT NULL,
  `cookie_consent` varchar(255) NOT NULL,
  `fav_icon` varchar(250) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `front_cms_settings`
--

LOCK TABLES `front_cms_settings` WRITE;
/*!40000 ALTER TABLE `front_cms_settings` DISABLE KEYS */;
INSERT INTO `front_cms_settings` VALUES (1,'yellow',NULL,1,1,'./uploads/demo.eshikshya.com.np/school_content/logo/front_logo-636de3d00c2405.24707519.jpg','','','[\"news\",\"complain\"]','','https://www.facebook.com/TRTruthNepal','','','','','','','','Copyright © CreationsoftNepal','','./uploads/demo.eshikshya.com.np/school_content/logo/front_fav_icon-634bec49340252.82037319.png','2022-11-16 11:19:26');
/*!40000 ALTER TABLE `front_cms_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `general_calls`
--

DROP TABLE IF EXISTS `general_calls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `general_calls` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `contact` varchar(12) NOT NULL,
  `date` date NOT NULL,
  `description` varchar(500) NOT NULL,
  `follow_up_date` date NOT NULL,
  `call_dureation` varchar(50) NOT NULL,
  `note` text NOT NULL,
  `call_type` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `general_calls`
--

LOCK TABLES `general_calls` WRITE;
/*!40000 ALTER TABLE `general_calls` DISABLE KEYS */;
/*!40000 ALTER TABLE `general_calls` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `grades`
--

DROP TABLE IF EXISTS `grades`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `grades` (
  `id` int NOT NULL AUTO_INCREMENT,
  `exam_type` varchar(250) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `point` float(10,1) DEFAULT NULL,
  `mark_from` float(10,2) DEFAULT NULL,
  `mark_upto` float(10,2) DEFAULT NULL,
  `description` text,
  `is_active` varchar(255) DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `grades`
--

LOCK TABLES `grades` WRITE;
/*!40000 ALTER TABLE `grades` DISABLE KEYS */;
/*!40000 ALTER TABLE `grades` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `homework`
--

DROP TABLE IF EXISTS `homework`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `homework` (
  `id` int NOT NULL AUTO_INCREMENT,
  `class_id` int NOT NULL,
  `section_id` int NOT NULL,
  `session_id` int NOT NULL,
  `homework_date` date NOT NULL,
  `submit_date` date NOT NULL,
  `staff_id` int NOT NULL,
  `subject_group_subject_id` int DEFAULT NULL,
  `subject_id` int NOT NULL,
  `description` text,
  `create_date` date NOT NULL,
  `evaluation_date` date NOT NULL,
  `document` varchar(200) NOT NULL,
  `created_by` int NOT NULL,
  `evaluated_by` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `subject_group_subject_id` (`subject_group_subject_id`),
  CONSTRAINT `homework_ibfk_1` FOREIGN KEY (`subject_group_subject_id`) REFERENCES `subject_group_subjects` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `homework`
--

LOCK TABLES `homework` WRITE;
/*!40000 ALTER TABLE `homework` DISABLE KEYS */;
/*!40000 ALTER TABLE `homework` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `homework_evaluation`
--

DROP TABLE IF EXISTS `homework_evaluation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `homework_evaluation` (
  `id` int NOT NULL AUTO_INCREMENT,
  `homework_id` int NOT NULL,
  `student_id` int NOT NULL,
  `student_session_id` int DEFAULT NULL,
  `date` date NOT NULL,
  `status` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `homework_evaluation`
--

LOCK TABLES `homework_evaluation` WRITE;
/*!40000 ALTER TABLE `homework_evaluation` DISABLE KEYS */;
/*!40000 ALTER TABLE `homework_evaluation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hostel`
--

DROP TABLE IF EXISTS `hostel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hostel` (
  `id` int NOT NULL AUTO_INCREMENT,
  `hostel_name` varchar(100) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `address` text,
  `intake` int DEFAULT NULL,
  `description` text,
  `is_active` varchar(255) DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hostel`
--

LOCK TABLES `hostel` WRITE;
/*!40000 ALTER TABLE `hostel` DISABLE KEYS */;
/*!40000 ALTER TABLE `hostel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hostel_rooms`
--

DROP TABLE IF EXISTS `hostel_rooms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hostel_rooms` (
  `id` int NOT NULL AUTO_INCREMENT,
  `hostel_id` int DEFAULT NULL,
  `room_type_id` int DEFAULT NULL,
  `room_no` varchar(200) DEFAULT NULL,
  `no_of_bed` int DEFAULT NULL,
  `cost_per_bed` float(10,2) DEFAULT '0.00',
  `title` varchar(200) DEFAULT NULL,
  `description` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hostel_rooms`
--

LOCK TABLES `hostel_rooms` WRITE;
/*!40000 ALTER TABLE `hostel_rooms` DISABLE KEYS */;
/*!40000 ALTER TABLE `hostel_rooms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `id_card`
--

DROP TABLE IF EXISTS `id_card`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `id_card` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `school_name` varchar(100) NOT NULL,
  `school_address` varchar(500) NOT NULL,
  `background` varchar(100) NOT NULL,
  `logo` varchar(100) NOT NULL,
  `sign_image` varchar(100) NOT NULL,
  `enable_vertical_card` int NOT NULL DEFAULT '0',
  `header_color` varchar(100) NOT NULL,
  `enable_admission_no` tinyint(1) NOT NULL COMMENT '0=disable,1=enable',
  `enable_student_name` tinyint(1) NOT NULL COMMENT '0=disable,1=enable',
  `enable_class` tinyint(1) NOT NULL COMMENT '0=disable,1=enable',
  `enable_fathers_name` tinyint(1) NOT NULL COMMENT '0=disable,1=enable',
  `enable_mothers_name` tinyint(1) NOT NULL COMMENT '0=disable,1=enable',
  `enable_address` tinyint(1) NOT NULL COMMENT '0=disable,1=enable',
  `enable_phone` tinyint(1) NOT NULL COMMENT '0=disable,1=enable',
  `enable_dob` tinyint(1) NOT NULL COMMENT '0=disable,1=enable',
  `enable_blood_group` tinyint(1) NOT NULL COMMENT '0=disable,1=enable',
  `status` tinyint(1) NOT NULL COMMENT '0=disable,1=enable',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `id_card`
--

LOCK TABLES `id_card` WRITE;
/*!40000 ALTER TABLE `id_card` DISABLE KEYS */;
/*!40000 ALTER TABLE `id_card` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `income`
--

DROP TABLE IF EXISTS `income`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `income` (
  `id` int NOT NULL AUTO_INCREMENT,
  `inc_head_id` varchar(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `invoice_no` varchar(200) NOT NULL,
  `date` date DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `note` text,
  `is_active` varchar(255) DEFAULT 'yes',
  `is_deleted` varchar(255) DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  `documents` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `income`
--

LOCK TABLES `income` WRITE;
/*!40000 ALTER TABLE `income` DISABLE KEYS */;
/*!40000 ALTER TABLE `income` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `income_head`
--

DROP TABLE IF EXISTS `income_head`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `income_head` (
  `id` int NOT NULL AUTO_INCREMENT,
  `income_category` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `is_active` varchar(255) NOT NULL DEFAULT 'yes',
  `is_deleted` varchar(255) NOT NULL DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `income_head`
--

LOCK TABLES `income_head` WRITE;
/*!40000 ALTER TABLE `income_head` DISABLE KEYS */;
/*!40000 ALTER TABLE `income_head` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item`
--

DROP TABLE IF EXISTS `item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `item` (
  `id` int NOT NULL AUTO_INCREMENT,
  `item_category_id` int DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `unit` varchar(100) NOT NULL,
  `item_photo` varchar(225) DEFAULT NULL,
  `description` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  `item_store_id` int DEFAULT NULL,
  `item_supplier_id` int DEFAULT NULL,
  `quantity` int NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item`
--

LOCK TABLES `item` WRITE;
/*!40000 ALTER TABLE `item` DISABLE KEYS */;
INSERT INTO `item` VALUES (1,1,'Note Book','10',NULL,'Notebook','2022-12-19 09:07:12',NULL,NULL,NULL,0,'0000-00-00'),(2,1,'Pen','1',NULL,'','2022-12-20 10:22:15',NULL,NULL,NULL,0,'0000-00-00'),(3,1,'Copy','10',NULL,'','2022-12-20 10:22:33',NULL,NULL,NULL,0,'0000-00-00'),(4,1,'File','20',NULL,'','2022-12-20 10:22:49',NULL,NULL,NULL,0,'0000-00-00');
/*!40000 ALTER TABLE `item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_category`
--

DROP TABLE IF EXISTS `item_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `item_category` (
  `id` int NOT NULL AUTO_INCREMENT,
  `item_category` varchar(255) NOT NULL,
  `is_active` varchar(255) NOT NULL DEFAULT 'yes',
  `description` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_category`
--

LOCK TABLES `item_category` WRITE;
/*!40000 ALTER TABLE `item_category` DISABLE KEYS */;
INSERT INTO `item_category` VALUES (1,'Stationary','yes','Stationary','2022-12-19 09:06:29',NULL);
/*!40000 ALTER TABLE `item_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_issue`
--

DROP TABLE IF EXISTS `item_issue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `item_issue` (
  `id` int NOT NULL AUTO_INCREMENT,
  `issue_type` varchar(15) DEFAULT NULL,
  `issue_to` varchar(100) DEFAULT NULL,
  `issue_by` varchar(100) DEFAULT NULL,
  `issue_date` date DEFAULT NULL,
  `return_date` date DEFAULT NULL,
  `item_category_id` int DEFAULT NULL,
  `item_id` int DEFAULT NULL,
  `quantity` int NOT NULL,
  `note` text NOT NULL,
  `is_returned` int NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_active` varchar(10) DEFAULT 'no',
  PRIMARY KEY (`id`),
  KEY `item_id` (`item_id`),
  KEY `item_category_id` (`item_category_id`),
  CONSTRAINT `item_issue_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `item` (`id`) ON DELETE CASCADE,
  CONSTRAINT `item_issue_ibfk_2` FOREIGN KEY (`item_category_id`) REFERENCES `item_category` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_issue`
--

LOCK TABLES `item_issue` WRITE;
/*!40000 ALTER TABLE `item_issue` DISABLE KEYS */;
INSERT INTO `item_issue` VALUES (1,'2','10','Creation Soft  (5555)','2079-09-04','2079-09-04',1,1,1,'For Official Use only',1,'2022-12-19 09:12:04','no'),(2,'2','11','Creation Soft  (5555)','2079-09-05','2079-09-06',1,1,3,'',1,'2022-12-20 10:20:06','no'),(3,'2','6','Creation Soft  (5555)','2079-09-06','2079-09-06',1,2,2,'',1,'2022-12-20 10:29:48','no');
/*!40000 ALTER TABLE `item_issue` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_stock`
--

DROP TABLE IF EXISTS `item_stock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `item_stock` (
  `id` int NOT NULL AUTO_INCREMENT,
  `item_id` int DEFAULT NULL,
  `supplier_id` int DEFAULT NULL,
  `symbol` varchar(10) NOT NULL DEFAULT '+',
  `store_id` int DEFAULT NULL,
  `quantity` int DEFAULT NULL,
  `purchase_price` varchar(100) NOT NULL,
  `date` date NOT NULL,
  `attachment` varchar(250) DEFAULT NULL,
  `description` text NOT NULL,
  `is_active` varchar(10) DEFAULT 'yes',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `item_id` (`item_id`),
  KEY `supplier_id` (`supplier_id`),
  KEY `store_id` (`store_id`),
  CONSTRAINT `item_stock_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `item` (`id`) ON DELETE CASCADE,
  CONSTRAINT `item_stock_ibfk_2` FOREIGN KEY (`supplier_id`) REFERENCES `item_supplier` (`id`) ON DELETE CASCADE,
  CONSTRAINT `item_stock_ibfk_3` FOREIGN KEY (`store_id`) REFERENCES `item_store` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_stock`
--

LOCK TABLES `item_stock` WRITE;
/*!40000 ALTER TABLE `item_stock` DISABLE KEYS */;
INSERT INTO `item_stock` VALUES (1,1,1,'+',1,10,'200','2079-09-04',NULL,'','yes','2022-12-19 09:32:16'),(2,1,1,'+',1,100,'5000','2079-09-04',NULL,'','yes','2022-12-19 09:32:03'),(3,2,2,'+',2,10,'5','2079-09-05',NULL,'','yes','2022-12-20 10:28:56');
/*!40000 ALTER TABLE `item_stock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_store`
--

DROP TABLE IF EXISTS `item_store`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `item_store` (
  `id` int NOT NULL AUTO_INCREMENT,
  `item_store` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_store`
--

LOCK TABLES `item_store` WRITE;
/*!40000 ALTER TABLE `item_store` DISABLE KEYS */;
INSERT INTO `item_store` VALUES (1,'ABC Store','abc',''),(2,'Pragati Stationary','Pragati','');
/*!40000 ALTER TABLE `item_store` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_supplier`
--

DROP TABLE IF EXISTS `item_supplier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `item_supplier` (
  `id` int NOT NULL AUTO_INCREMENT,
  `item_supplier` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `contact_person_name` varchar(255) NOT NULL,
  `contact_person_phone` varchar(255) NOT NULL,
  `contact_person_email` varchar(255) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_supplier`
--

LOCK TABLES `item_supplier` WRITE;
/*!40000 ALTER TABLE `item_supplier` DISABLE KEYS */;
INSERT INTO `item_supplier` VALUES (1,'Xyz','984563636','xyz@gmail.com','Naxal','xyz','9845698547','','xyz@gmail.com'),(2,'Pragati Stationary','9845658956','abc123@gmail.com','Naxal','Pragati','9845566764','abc123@gmail.com','');
/*!40000 ALTER TABLE `item_supplier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `languages`
--

DROP TABLE IF EXISTS `languages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `languages` (
  `id` int NOT NULL AUTO_INCREMENT,
  `language` varchar(50) DEFAULT NULL,
  `short_code` varchar(255) NOT NULL,
  `country_code` varchar(255) NOT NULL,
  `is_deleted` varchar(10) NOT NULL DEFAULT 'yes',
  `is_active` varchar(255) DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `languages`
--

LOCK TABLES `languages` WRITE;
/*!40000 ALTER TABLE `languages` DISABLE KEYS */;
INSERT INTO `languages` VALUES (4,'English','en','us','no','no','2019-11-20 11:38:50','0000-00-00'),(53,'Nepali','ne','np','no','no','2022-07-24 04:21:13','0000-00-00');
/*!40000 ALTER TABLE `languages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `leave_types`
--

DROP TABLE IF EXISTS `leave_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `leave_types` (
  `id` int NOT NULL AUTO_INCREMENT,
  `type` varchar(200) NOT NULL,
  `is_active` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `type` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `leave_types`
--

LOCK TABLES `leave_types` WRITE;
/*!40000 ALTER TABLE `leave_types` DISABLE KEYS */;
/*!40000 ALTER TABLE `leave_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lesson`
--

DROP TABLE IF EXISTS `lesson`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lesson` (
  `id` int NOT NULL AUTO_INCREMENT,
  `session_id` int NOT NULL,
  `subject_group_subject_id` int NOT NULL,
  `subject_group_class_sections_id` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `session_id` (`session_id`),
  KEY `subject_group_subject_id` (`subject_group_subject_id`),
  KEY `subject_group_class_sections_id` (`subject_group_class_sections_id`),
  CONSTRAINT `lesson_ibfk_1` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `lesson_ibfk_2` FOREIGN KEY (`subject_group_subject_id`) REFERENCES `subject_group_subjects` (`id`) ON DELETE CASCADE,
  CONSTRAINT `lesson_ibfk_3` FOREIGN KEY (`subject_group_class_sections_id`) REFERENCES `subject_group_class_sections` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lesson`
--

LOCK TABLES `lesson` WRITE;
/*!40000 ALTER TABLE `lesson` DISABLE KEYS */;
/*!40000 ALTER TABLE `lesson` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `libarary_members`
--

DROP TABLE IF EXISTS `libarary_members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `libarary_members` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `library_card_no` varchar(50) DEFAULT NULL,
  `member_type` varchar(50) DEFAULT NULL,
  `member_id` int DEFAULT NULL,
  `is_active` varchar(10) NOT NULL DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `libarary_members`
--

LOCK TABLES `libarary_members` WRITE;
/*!40000 ALTER TABLE `libarary_members` DISABLE KEYS */;
INSERT INTO `libarary_members` VALUES (1,'5585','student',23,'no','2022-10-10 10:48:38'),(2,'01','student',24,'no','2022-12-15 10:01:17'),(3,'02','student',35,'no','2022-12-15 10:01:24'),(4,'03','student',34,'no','2022-12-15 10:01:33'),(5,'04','student',36,'no','2022-12-15 10:01:41'),(6,'05','student',37,'no','2022-12-15 10:01:46'),(7,'06','student',38,'no','2022-12-15 10:01:52'),(8,'07','student',39,'no','2022-12-15 10:01:57'),(9,'08','student',40,'no','2022-12-15 10:02:03'),(10,'11','teacher',1,'no','2022-12-15 10:02:37'),(11,'12','teacher',4,'no','2022-12-15 10:02:41'),(12,'132','teacher',5,'no','2022-12-15 10:02:46'),(13,'14','teacher',6,'no','2022-12-15 10:02:51'),(14,'15','teacher',8,'no','2022-12-15 10:02:57'),(15,'16','teacher',9,'no','2022-12-15 10:03:31'),(16,'17','teacher',10,'no','2022-12-15 10:03:35'),(17,'18','teacher',11,'no','2022-12-15 10:03:41'),(18,'0123','student',107,'no','2022-12-19 09:18:20'),(19,'0124','student',108,'no','2022-12-19 09:18:29'),(20,'0125','student',109,'no','2022-12-19 09:18:37'),(21,'0126','student',110,'no','2022-12-19 09:18:45'),(22,'0127','student',111,'no','2022-12-19 09:18:56'),(23,'223','teacher',12,'no','2022-12-21 05:08:17'),(24,'2314','student',185,'no','2022-12-21 10:07:54'),(25,'3214','student',186,'no','2022-12-21 10:08:05'),(26,'1232','student',187,'no','2022-12-21 10:08:12');
/*!40000 ALTER TABLE `libarary_members` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `logs`
--

DROP TABLE IF EXISTS `logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `logs` (
  `id` int NOT NULL AUTO_INCREMENT,
  `message` text,
  `record_id` text,
  `user_id` int DEFAULT NULL,
  `action` varchar(50) DEFAULT NULL,
  `ip_address` varchar(50) DEFAULT NULL,
  `platform` varchar(50) DEFAULT NULL,
  `agent` varchar(50) DEFAULT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3479 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logs`
--

LOCK TABLES `logs` WRITE;
/*!40000 ALTER TABLE `logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `messages` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(200) DEFAULT NULL,
  `template_id` varchar(100) NOT NULL,
  `message` text,
  `send_mail` varchar(10) DEFAULT '0',
  `send_sms` varchar(10) DEFAULT '0',
  `is_group` varchar(10) DEFAULT '0',
  `is_individual` varchar(10) DEFAULT '0',
  `is_class` int NOT NULL DEFAULT '0',
  `group_list` text,
  `user_list` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messages`
--

LOCK TABLES `messages` WRITE;
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `migrations` (
  `version` bigint NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `multi_class_students`
--

DROP TABLE IF EXISTS `multi_class_students`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `multi_class_students` (
  `id` int NOT NULL AUTO_INCREMENT,
  `student_id` int DEFAULT NULL,
  `student_session_id` int DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `student_id` (`student_id`),
  KEY `student_session_id` (`student_session_id`),
  CONSTRAINT `multi_class_students_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`) ON DELETE CASCADE,
  CONSTRAINT `multi_class_students_ibfk_2` FOREIGN KEY (`student_session_id`) REFERENCES `student_session` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `multi_class_students`
--

LOCK TABLES `multi_class_students` WRITE;
/*!40000 ALTER TABLE `multi_class_students` DISABLE KEYS */;
/*!40000 ALTER TABLE `multi_class_students` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notification_roles`
--

DROP TABLE IF EXISTS `notification_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `notification_roles` (
  `id` int NOT NULL AUTO_INCREMENT,
  `send_notification_id` int DEFAULT NULL,
  `role_id` int DEFAULT NULL,
  `is_active` int DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `send_notification_id` (`send_notification_id`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `notification_roles_ibfk_1` FOREIGN KEY (`send_notification_id`) REFERENCES `send_notification` (`id`) ON DELETE CASCADE,
  CONSTRAINT `notification_roles_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notification_roles`
--

LOCK TABLES `notification_roles` WRITE;
/*!40000 ALTER TABLE `notification_roles` DISABLE KEYS */;
INSERT INTO `notification_roles` VALUES (1,1,1,0,'2022-11-15 09:35:09'),(2,1,2,0,'2022-11-15 09:35:09'),(3,1,3,0,'2022-11-15 09:35:09'),(4,1,4,0,'2022-11-15 09:35:09'),(5,1,6,0,'2022-11-15 09:35:09'),(6,1,7,0,'2022-11-15 09:35:09'),(7,2,1,0,'2022-12-21 05:06:30'),(8,2,2,0,'2022-12-21 05:06:30'),(9,2,3,0,'2022-12-21 05:06:30'),(10,2,4,0,'2022-12-21 05:06:30'),(11,2,6,0,'2022-12-21 05:06:30'),(12,2,7,0,'2022-12-21 05:06:30'),(13,3,7,0,'2022-12-22 06:01:20'),(14,4,7,0,'2022-12-22 06:39:39'),(15,4,1,0,'2022-12-22 09:08:19');
/*!40000 ALTER TABLE `notification_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notification_setting`
--

DROP TABLE IF EXISTS `notification_setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `notification_setting` (
  `id` int NOT NULL AUTO_INCREMENT,
  `type` varchar(100) DEFAULT NULL,
  `is_mail` varchar(10) DEFAULT '0',
  `is_sms` varchar(10) DEFAULT '0',
  `is_notification` int NOT NULL DEFAULT '0',
  `display_notification` int NOT NULL DEFAULT '0',
  `display_sms` int NOT NULL DEFAULT '1',
  `subject` varchar(255) NOT NULL,
  `template_id` varchar(100) NOT NULL,
  `template` longtext NOT NULL,
  `variables` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notification_setting`
--

LOCK TABLES `notification_setting` WRITE;
/*!40000 ALTER TABLE `notification_setting` DISABLE KEYS */;
INSERT INTO `notification_setting` VALUES (1,'student_admission','1','0',0,0,1,'Student Admission','','Dear {{student_name}} your admission is confirm in Class: {{class}} Section:  {{section}} for Session: {{current_session_name}} for more \r\ndetail\r\n contact\r\n System\r\n Admin\r\n {{class}} {{section}} {{admission_no}} {{roll_no}} {{admission_date}} {{mobileno}} {{email}} {{dob}} {{guardian_name}} {{guardian_relation}} {{guardian_phone}} {{father_name}} {{father_phone}} {{blood_group}} {{mother_name}} {{gender}} {{guardian_email}}','{{student_name}} {{class}}  {{section}}  {{admission_no}}  {{roll_no}}  {{admission_date}}   {{mobileno}}  {{email}}  {{dob}}  {{guardian_name}}  {{guardian_relation}}  {{guardian_phone}}  {{father_name}}  {{father_phone}}  {{blood_group}}  {{mother_name}}  {{gender}} {{guardian_email}} {{current_session_name}} ','2021-06-02 08:43:30'),(2,'exam_result','1','0',0,1,1,'Exam Result','','Dear {{student_name}} - {{exam_roll_no}}, your {{exam}} result has been published.','{{student_name}} {{exam_roll_no}} {{exam}}','2021-06-02 08:43:42'),(3,'fee_submission','1','0',0,1,1,'Fee Submission','','Dear parents, we have received Fees Amount {{fee_amount}} for  {{student_name}}  by Eshikshya English School\r\n{{class}} {{section}} .','{{student_name}} {{class}} {{section}} {{fine_type}} {{fine_percentage}} {{fine_amount}} {{fee_group_name}} {{type}} {{code}} {{email}} {{contact_no}} {{invoice_id}} {{sub_invoice_id}} {{due_date}} {{amount}} {{fee_amount}}','2022-11-14 04:53:50'),(4,'absent_attendence','1','0',0,1,1,'Absent Attendence','','Absent Notice :{{student_name}}  was absent on date {{date}} in period {{subject_name}} {{subject_code}} {{subject_type}} from Your School Name','{{student_name}} {{mobileno}} {{email}} {{father_name}} {{father_phone}} {{father_occupation}} {{mother_name}} {{mother_phone}} {{guardian_name}} {{guardian_phone}} {{guardian_occupation}} {{guardian_email}} {{date}} {{current_session_name}}             {{time_from}} {{time_to}} {{subject_name}} {{subject_code}} {{subject_type}}  ','2022-11-08 08:46:53'),(5,'login_credential','1','0',0,0,1,'Login Credential','','Hello {{display_name}} your login details for Url: {{url}} Username: {{username}}  Password: {{password}}','{{url}} {{display_name}} {{username}} {{password}}','2021-06-02 08:44:29'),(6,'homework','1','0',0,1,1,'Homework','','New Homework has been created for \r\n{{student_name}} at\r\n\r\n\r\n\r\n{{homework_date}} for the class {{class}} {{section}} {{subject}}. kindly submit your\r\n\r\n\r\n homework before {{submit_date}} .Thank you','{{homework_date}} {{submit_date}} {{class}} {{section}} {{subject}} {{student_name}}','2021-06-02 08:44:39'),(7,'fees_reminder','1','0',0,1,1,'Fees Reminder','','Dear parents, please pay fee amount Rs.{{due_amount}} of {{fee_type}} before {{due_date}} for {{student_name}}  from smart school (ignore if you already paid)','{{fee_type}}{{fee_code}}{{due_date}}{{student_name}}{{school_name}}{{fee_amount}}{{due_amount}}{{deposit_amount}} ','2021-06-02 08:44:54'),(8,'forgot_password','1','0',0,0,0,'Forgot Password','','Dear  {{name}} , \r\n    Recently a request was submitted to reset password for your account. If you didn\'t make the request, just ignore this email. Otherwise you can reset your password using this link <a href=\'{{resetPassLink}}\'>Click here to reset your password</a>,\r\nif you\'re having trouble clicking the password reset button, copy and paste the URL below into your web browser. your username {{username}}\r\n{{resetPassLink}}\r\n Regards,\r\n {{school_name}}','{{school_name}}{{name}}{{username}}{{resetPassLink}} ','2021-06-02 08:45:08'),(9,'online_examination_publish_exam','1','0',0,1,1,'Online Examination Publish Exam','','A new exam {{exam_title}} has been created for  duration: {{time_duration}} min, which will be available from:  {{exam_from}} to  {{exam_to}}.','{{exam_title}} {{exam_from}} {{exam_to}} {{time_duration}} {{attempt}} {{passing_percentage}}','2021-06-02 08:45:36'),(10,'online_examination_publish_result','1','0',0,1,1,'Online Examination Publish Result','','Exam {{exam_title}} result has been declared which was conducted between  {{exam_from}} to   {{exam_to}}, for more details, please check your student portal.','{{exam_title}} {{exam_from}} {{exam_to}} {{time_duration}} {{attempt}} {{passing_percentage}}','2021-06-02 08:45:58'),(11,'online_admission_form_submission','1','0',0,1,1,'Online Admission Form Submission','','Dear {{firstname}}  {{lastname}} your online admission form is Submitted successfully  on date {{date}}. Your Reference number is {{reference_no}}. Please remember your reference number for further process.',' {{firstname}} {{lastname}} {{date}} {{reference_no}}','2021-06-02 08:46:21'),(12,'online_admission_fees_submission','0','0',0,1,1,'Online Admission Fees Submission','','Dear {{firstname}}  {{lastname}} your online admission form is Submitted successfully and the payment of {{paid_amount}} has recieved successfully on date {{date}}. Your Reference number is {{reference_no}}. Please remember your reference number for further process.',' {{firstname}} {{lastname}} {{date}} {{paid_amount}} {{reference_no}}','2021-06-02 08:46:46');
/*!40000 ALTER TABLE `notification_setting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `online_admission_fields`
--

DROP TABLE IF EXISTS `online_admission_fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `online_admission_fields` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  `status` int DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `online_admission_fields`
--

LOCK TABLES `online_admission_fields` WRITE;
/*!40000 ALTER TABLE `online_admission_fields` DISABLE KEYS */;
INSERT INTO `online_admission_fields` VALUES (1,'middlename',1,'2022-11-08 09:37:59'),(2,'lastname',1,'2022-11-08 09:38:01'),(3,'category',0,'2022-09-22 05:08:22'),(4,'religion',0,'2022-09-22 06:11:28'),(5,'cast',0,'2022-09-22 06:11:30'),(6,'mobile_no',0,'2022-09-22 06:11:32'),(7,'admission_date',0,'2021-06-02 04:48:35'),(8,'student_photo',0,'2022-09-22 06:11:37'),(9,'is_student_house',0,'2022-09-22 06:11:41'),(10,'is_blood_group',0,'2022-09-22 06:11:43'),(11,'student_height',0,'2022-09-22 06:11:46'),(12,'student_weight',0,'2022-09-22 06:11:49'),(13,'father_name',0,'2022-09-22 06:11:53'),(14,'father_phone',0,'2022-09-22 06:11:56'),(15,'father_occupation',0,'2022-09-22 06:11:58'),(16,'father_pic',0,'2022-09-22 06:12:00'),(17,'mother_name',0,'2022-09-22 06:12:02'),(18,'mother_phone',0,'2022-09-22 06:12:05'),(19,'mother_occupation',0,'2022-09-22 06:12:07'),(20,'mother_pic',0,'2022-09-22 06:12:09'),(21,'guardian_name',0,'2022-11-08 09:11:21'),(22,'guardian_phone',0,'2022-11-08 09:11:21'),(23,'if_guardian_is',0,'2022-11-08 09:11:21'),(24,'guardian_relation',0,'2022-11-08 09:11:21'),(25,'guardian_email',0,'2022-11-08 09:11:28'),(26,'guardian_occupation',0,'2022-09-22 06:11:30'),(27,'guardian_address',0,'2022-09-22 06:12:27'),(28,'bank_account_no',0,'2022-07-12 09:35:58'),(29,'bank_name',0,'2021-06-02 04:48:35'),(30,'ifsc_code',0,'2021-06-02 04:48:35'),(31,'national_identification_no',0,'2021-06-02 04:48:35'),(32,'local_identification_no',0,'2021-06-02 04:48:35'),(33,'rte',0,'2021-06-02 04:48:35'),(34,'previous_school_details',0,'2022-09-22 06:12:48'),(35,'guardian_photo',0,'2022-11-08 09:11:30'),(36,'student_note',0,'2022-09-22 06:12:51'),(37,'measurement_date',0,'2022-09-22 06:11:51'),(38,'student_email',0,'2022-09-22 05:10:53'),(39,'current_address',0,'2022-09-22 06:11:55'),(40,'permanent_address',0,'2022-09-22 06:11:58');
/*!40000 ALTER TABLE `online_admission_fields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `online_admission_payment`
--

DROP TABLE IF EXISTS `online_admission_payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `online_admission_payment` (
  `id` int NOT NULL AUTO_INCREMENT,
  `admission_id` int NOT NULL,
  `paid_amount` float(10,2) NOT NULL,
  `payment_mode` varchar(50) NOT NULL,
  `payment_type` varchar(100) NOT NULL,
  `transaction_id` varchar(100) NOT NULL,
  `note` varchar(100) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `online_admission_payment`
--

LOCK TABLES `online_admission_payment` WRITE;
/*!40000 ALTER TABLE `online_admission_payment` DISABLE KEYS */;
/*!40000 ALTER TABLE `online_admission_payment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `online_admissions`
--

DROP TABLE IF EXISTS `online_admissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `online_admissions` (
  `id` int NOT NULL AUTO_INCREMENT,
  `admission_no` varchar(100) DEFAULT NULL,
  `roll_no` varchar(100) DEFAULT NULL,
  `reference_no` varchar(50) DEFAULT NULL,
  `admission_date` date DEFAULT NULL,
  `firstname` varchar(100) DEFAULT NULL,
  `middlename` varchar(255) DEFAULT NULL,
  `lastname` varchar(100) DEFAULT NULL,
  `rte` varchar(20) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL,
  `mobileno` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `pincode` varchar(100) DEFAULT NULL,
  `religion` varchar(100) DEFAULT NULL,
  `cast` varchar(50) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `gender` varchar(100) DEFAULT NULL,
  `current_address` text,
  `permanent_address` text,
  `category_id` int DEFAULT NULL,
  `class_section_id` int DEFAULT NULL,
  `route_id` int DEFAULT NULL,
  `school_house_id` int DEFAULT NULL,
  `blood_group` varchar(200) DEFAULT NULL,
  `vehroute_id` int DEFAULT NULL,
  `hostel_room_id` int DEFAULT NULL,
  `adhar_no` varchar(100) DEFAULT NULL,
  `samagra_id` varchar(100) DEFAULT NULL,
  `bank_account_no` varchar(100) DEFAULT NULL,
  `bank_name` varchar(100) DEFAULT NULL,
  `ifsc_code` varchar(100) DEFAULT NULL,
  `guardian_is` varchar(100) DEFAULT NULL,
  `father_name` varchar(100) DEFAULT NULL,
  `father_phone` varchar(100) DEFAULT NULL,
  `father_occupation` varchar(100) DEFAULT NULL,
  `mother_name` varchar(100) DEFAULT NULL,
  `mother_phone` varchar(100) DEFAULT NULL,
  `mother_occupation` varchar(100) DEFAULT NULL,
  `guardian_name` varchar(100) DEFAULT NULL,
  `guardian_relation` varchar(100) DEFAULT NULL,
  `guardian_phone` varchar(100) DEFAULT NULL,
  `guardian_occupation` varchar(150) DEFAULT NULL,
  `guardian_address` text,
  `guardian_email` varchar(100) DEFAULT NULL,
  `father_pic` varchar(200) DEFAULT NULL,
  `mother_pic` varchar(200) DEFAULT NULL,
  `guardian_pic` varchar(200) DEFAULT NULL,
  `is_enroll` int DEFAULT '0',
  `previous_school` text,
  `height` varchar(100) DEFAULT NULL,
  `weight` varchar(100) DEFAULT NULL,
  `note` varchar(200) DEFAULT NULL,
  `form_status` int DEFAULT '1',
  `paid_status` int DEFAULT NULL,
  `measurement_date` date DEFAULT NULL,
  `app_key` text,
  `document` text,
  `disable_at` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `class_section_id` (`class_section_id`),
  CONSTRAINT `online_admissions_ibfk_1` FOREIGN KEY (`class_section_id`) REFERENCES `class_sections` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `online_admissions`
--

LOCK TABLES `online_admissions` WRITE;
/*!40000 ALTER TABLE `online_admissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `online_admissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `onlineexam`
--

DROP TABLE IF EXISTS `onlineexam`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `onlineexam` (
  `id` int NOT NULL AUTO_INCREMENT,
  `exam` text,
  `attempt` int NOT NULL,
  `exam_from` datetime DEFAULT NULL,
  `exam_to` datetime DEFAULT NULL,
  `is_quiz` int NOT NULL DEFAULT '0',
  `auto_publish_date` datetime DEFAULT NULL,
  `time_from` time DEFAULT NULL,
  `time_to` time DEFAULT NULL,
  `duration` time NOT NULL,
  `passing_percentage` float NOT NULL DEFAULT '0',
  `description` text,
  `session_id` int DEFAULT NULL,
  `publish_result` int NOT NULL DEFAULT '0',
  `is_active` varchar(1) DEFAULT '0',
  `is_marks_display` int NOT NULL DEFAULT '0',
  `is_neg_marking` int NOT NULL DEFAULT '0',
  `is_random_question` int NOT NULL DEFAULT '0',
  `is_rank_generated` int NOT NULL DEFAULT '0',
  `publish_exam_notification` int NOT NULL,
  `publish_result_notification` int NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `session_id` (`session_id`),
  CONSTRAINT `onlineexam_ibfk_1` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `onlineexam`
--

LOCK TABLES `onlineexam` WRITE;
/*!40000 ALTER TABLE `onlineexam` DISABLE KEYS */;
INSERT INTO `onlineexam` VALUES (1,'1st Term',50,'2079-09-06 00:00:00','2079-09-06 15:00:00',1,NULL,NULL,NULL,'03:00:00',25,'<p>1st Term Examination</p>',30,0,'1',1,0,1,0,1,0,'2022-12-22 09:09:48',NULL);
/*!40000 ALTER TABLE `onlineexam` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `onlineexam_attempts`
--

DROP TABLE IF EXISTS `onlineexam_attempts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `onlineexam_attempts` (
  `id` int NOT NULL AUTO_INCREMENT,
  `onlineexam_student_id` int NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `onlineexam_student_id` (`onlineexam_student_id`),
  CONSTRAINT `onlineexam_attempts_ibfk_1` FOREIGN KEY (`onlineexam_student_id`) REFERENCES `onlineexam_students` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `onlineexam_attempts`
--

LOCK TABLES `onlineexam_attempts` WRITE;
/*!40000 ALTER TABLE `onlineexam_attempts` DISABLE KEYS */;
/*!40000 ALTER TABLE `onlineexam_attempts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `onlineexam_questions`
--

DROP TABLE IF EXISTS `onlineexam_questions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `onlineexam_questions` (
  `id` int NOT NULL AUTO_INCREMENT,
  `question_id` int DEFAULT NULL,
  `onlineexam_id` int DEFAULT NULL,
  `session_id` int DEFAULT NULL,
  `marks` float(10,2) NOT NULL DEFAULT '0.00',
  `neg_marks` float(10,2) DEFAULT '0.00',
  `is_active` varchar(1) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `onlineexam_id` (`onlineexam_id`),
  KEY `question_id` (`question_id`),
  KEY `session_id` (`session_id`),
  CONSTRAINT `onlineexam_questions_ibfk_1` FOREIGN KEY (`onlineexam_id`) REFERENCES `onlineexam` (`id`) ON DELETE CASCADE,
  CONSTRAINT `onlineexam_questions_ibfk_2` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `onlineexam_questions_ibfk_3` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `onlineexam_questions`
--

LOCK TABLES `onlineexam_questions` WRITE;
/*!40000 ALTER TABLE `onlineexam_questions` DISABLE KEYS */;
/*!40000 ALTER TABLE `onlineexam_questions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `onlineexam_student_results`
--

DROP TABLE IF EXISTS `onlineexam_student_results`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `onlineexam_student_results` (
  `id` int NOT NULL AUTO_INCREMENT,
  `onlineexam_student_id` int NOT NULL,
  `onlineexam_question_id` int NOT NULL,
  `select_option` longtext,
  `marks` float(10,2) NOT NULL DEFAULT '0.00',
  `remark` text,
  `attachment_name` text,
  `attachment_upload_name` varchar(250) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `onlineexam_student_id` (`onlineexam_student_id`),
  KEY `onlineexam_question_id` (`onlineexam_question_id`),
  CONSTRAINT `onlineexam_student_results_ibfk_1` FOREIGN KEY (`onlineexam_student_id`) REFERENCES `onlineexam_students` (`id`) ON DELETE CASCADE,
  CONSTRAINT `onlineexam_student_results_ibfk_2` FOREIGN KEY (`onlineexam_question_id`) REFERENCES `onlineexam_questions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `onlineexam_student_results`
--

LOCK TABLES `onlineexam_student_results` WRITE;
/*!40000 ALTER TABLE `onlineexam_student_results` DISABLE KEYS */;
/*!40000 ALTER TABLE `onlineexam_student_results` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `onlineexam_students`
--

DROP TABLE IF EXISTS `onlineexam_students`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `onlineexam_students` (
  `id` int NOT NULL AUTO_INCREMENT,
  `onlineexam_id` int DEFAULT NULL,
  `student_session_id` int DEFAULT NULL,
  `is_attempted` int NOT NULL DEFAULT '0',
  `rank` int DEFAULT '0',
  `quiz_attempted` int NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `onlineexam_id` (`onlineexam_id`),
  KEY `student_session_id` (`student_session_id`),
  CONSTRAINT `onlineexam_students_ibfk_1` FOREIGN KEY (`onlineexam_id`) REFERENCES `onlineexam` (`id`) ON DELETE CASCADE,
  CONSTRAINT `onlineexam_students_ibfk_2` FOREIGN KEY (`student_session_id`) REFERENCES `student_session` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `onlineexam_students`
--

LOCK TABLES `onlineexam_students` WRITE;
/*!40000 ALTER TABLE `onlineexam_students` DISABLE KEYS */;
/*!40000 ALTER TABLE `onlineexam_students` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment_settings`
--

DROP TABLE IF EXISTS `payment_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `payment_settings` (
  `id` int NOT NULL AUTO_INCREMENT,
  `payment_type` varchar(200) NOT NULL,
  `api_username` varchar(200) DEFAULT NULL,
  `api_secret_key` varchar(200) NOT NULL,
  `salt` varchar(200) NOT NULL,
  `api_publishable_key` varchar(200) NOT NULL,
  `api_password` varchar(200) DEFAULT NULL,
  `api_signature` varchar(200) DEFAULT NULL,
  `api_email` varchar(200) DEFAULT NULL,
  `paypal_demo` varchar(100) NOT NULL,
  `account_no` varchar(200) NOT NULL,
  `is_active` varchar(255) DEFAULT 'no',
  `gateway_mode` int NOT NULL COMMENT '0 Testing, 1 live',
  `paytm_website` varchar(255) NOT NULL,
  `paytm_industrytype` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment_settings`
--

LOCK TABLES `payment_settings` WRITE;
/*!40000 ALTER TABLE `payment_settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `payment_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payslip_allowance`
--

DROP TABLE IF EXISTS `payslip_allowance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `payslip_allowance` (
  `id` int NOT NULL AUTO_INCREMENT,
  `payslip_id` int NOT NULL,
  `allowance_type` varchar(200) NOT NULL,
  `amount` float NOT NULL,
  `staff_id` int NOT NULL,
  `cal_type` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payslip_allowance`
--

LOCK TABLES `payslip_allowance` WRITE;
/*!40000 ALTER TABLE `payslip_allowance` DISABLE KEYS */;
/*!40000 ALTER TABLE `payslip_allowance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_category`
--

DROP TABLE IF EXISTS `permission_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `permission_category` (
  `id` int NOT NULL AUTO_INCREMENT,
  `perm_group_id` int DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `short_code` varchar(100) DEFAULT NULL,
  `enable_view` int DEFAULT '0',
  `enable_add` int DEFAULT '0',
  `enable_edit` int DEFAULT '0',
  `enable_delete` int DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=267 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_category`
--

LOCK TABLES `permission_category` WRITE;
/*!40000 ALTER TABLE `permission_category` DISABLE KEYS */;
INSERT INTO `permission_category` VALUES (1,1,'Student','student',1,1,1,1,'2019-10-24 05:42:03'),(2,1,'Import Student','import_student',1,0,0,0,'2018-06-22 10:17:19'),(3,1,'Student Categories','student_categories',1,1,1,1,'2018-06-22 10:17:36'),(4,1,'Student Houses','student_houses',1,1,1,1,'2018-06-22 10:17:53'),(5,2,'Collect Fees','collect_fees',1,1,0,1,'2018-06-22 10:21:03'),(6,2,'Fees Carry Forward','fees_carry_forward',1,0,0,0,'2018-06-27 00:18:15'),(7,2,'Fees Master','fees_master',1,1,1,1,'2018-06-27 00:18:57'),(8,2,'Fees Group','fees_group',1,1,1,1,'2018-06-22 10:21:46'),(9,3,'Income','income',1,1,1,1,'2018-06-22 10:23:21'),(10,3,'Income Head','income_head',1,1,1,1,'2018-06-22 10:22:44'),(11,3,'Search Income','search_income',1,0,0,0,'2018-06-22 10:23:00'),(12,4,'Expense','expense',1,1,1,1,'2018-06-22 10:24:06'),(13,4,'Expense Head','expense_head',1,1,1,1,'2018-06-22 10:23:47'),(14,4,'Search Expense','search_expense',1,0,0,0,'2018-06-22 10:24:13'),(15,5,'Student / Period Attendance','student_attendance',1,1,1,0,'2019-11-29 01:19:05'),(20,6,'Marks Grade','marks_grade',1,1,1,1,'2018-06-22 10:25:25'),(21,7,'Class Timetable','class_timetable',1,1,1,0,'2022-02-09 19:25:27'),(23,7,'Subject','subject',1,1,1,1,'2018-06-22 10:32:17'),(24,7,'Class','class',1,1,1,1,'2018-06-22 10:32:35'),(25,7,'Section','section',1,1,1,1,'2018-06-22 10:31:10'),(26,7,'Promote Student','promote_student',1,0,0,0,'2018-06-22 10:32:47'),(27,8,'Upload Content','upload_content',1,1,0,1,'2018-06-22 10:33:19'),(28,9,'Books List','books',1,1,1,1,'2019-11-24 00:37:12'),(29,9,'Issue Return','issue_return',1,0,0,0,'2019-11-24 00:37:18'),(30,9,'Add Staff Member','add_staff_member',1,0,0,0,'2018-07-02 11:37:00'),(31,10,'Issue Item','issue_item',1,1,1,1,'2019-11-29 06:39:27'),(32,10,'Add Item Stock','item_stock',1,1,1,1,'2019-11-24 00:39:17'),(33,10,'Add Item','item',1,1,1,1,'2019-11-24 00:39:39'),(34,10,'Item Store','store',1,1,1,1,'2019-11-24 00:40:41'),(35,10,'Item Supplier','supplier',1,1,1,1,'2019-11-24 00:40:49'),(37,11,'Routes','routes',1,1,1,1,'2018-06-22 10:39:17'),(38,11,'Vehicle','vehicle',1,1,1,1,'2018-06-22 10:39:36'),(39,11,'Assign Vehicle','assign_vehicle',1,1,1,1,'2018-06-27 04:39:20'),(40,12,'Hostel','hostel',1,1,1,1,'2018-06-22 10:40:49'),(41,12,'Room Type','room_type',1,1,1,1,'2018-06-22 10:40:27'),(42,12,'Hostel Rooms','hostel_rooms',1,1,1,1,'2018-06-25 06:23:03'),(43,13,'Notice Board','notice_board',1,1,1,1,'2018-06-22 10:41:17'),(44,13,'Email','email',1,0,0,0,'2019-11-26 05:20:37'),(46,13,'Email / SMS Log','email_sms_log',1,0,0,0,'2018-06-22 10:41:23'),(53,15,'Languages','languages',0,1,0,1,'2021-01-23 07:09:32'),(54,15,'General Setting','general_setting',1,0,1,0,'2018-07-05 09:08:35'),(55,15,'Session Setting','session_setting',1,1,1,1,'2018-06-22 10:44:15'),(56,15,'Notification Setting','notification_setting',1,0,1,0,'2018-07-05 09:08:41'),(57,15,'SMS Setting','sms_setting',1,0,1,0,'2018-07-05 09:08:47'),(58,15,'Email Setting','email_setting',1,0,1,0,'2018-07-05 09:08:51'),(59,15,'Front CMS Setting','front_cms_setting',1,0,1,0,'2018-07-05 09:08:55'),(60,15,'Payment Methods','payment_methods',1,0,1,0,'2018-07-05 09:08:59'),(61,16,'Menus','menus',1,1,0,1,'2018-07-09 03:50:06'),(62,16,'Media Manager','media_manager',1,1,0,1,'2018-07-09 03:50:26'),(63,16,'Banner Images','banner_images',1,1,0,1,'2018-06-22 10:46:02'),(64,16,'Pages','pages',1,1,1,1,'2018-06-22 10:46:21'),(65,16,'Gallery','gallery',1,1,1,1,'2018-06-22 10:47:02'),(66,16,'Event','event',1,1,1,1,'2018-06-22 10:47:20'),(67,16,'News','notice',1,1,1,1,'2018-07-03 08:39:34'),(68,2,'Fees Group Assign','fees_group_assign',1,0,0,0,'2018-06-22 10:20:42'),(69,2,'Fees Type','fees_type',1,1,1,1,'2018-06-22 10:19:34'),(70,2,'Fees Discount','fees_discount',1,1,1,1,'2018-06-22 10:20:10'),(71,2,'Fees Discount Assign','fees_discount_assign',1,0,0,0,'2018-06-22 10:20:17'),(73,2,'Search Fees Payment','search_fees_payment',1,0,0,0,'2018-06-22 10:20:27'),(74,2,'Search Due Fees','search_due_fees',1,0,0,0,'2018-06-22 10:20:35'),(77,7,'Assign Class Teacher','assign_class_teacher',1,1,1,1,'2018-06-22 10:30:52'),(78,17,'Admission Enquiry','admission_enquiry',1,1,1,1,'2018-06-22 10:51:24'),(79,17,'Follow Up Admission Enquiry','follow_up_admission_enquiry',1,1,0,1,'2018-06-22 10:51:39'),(80,17,'Visitor Book','visitor_book',1,1,1,1,'2018-06-22 10:48:58'),(81,17,'Phone Call Log','phone_call_log',1,1,1,1,'2018-06-22 10:50:57'),(82,17,'Postal Dispatch','postal_dispatch',1,1,1,1,'2018-06-22 10:50:21'),(83,17,'Postal Receive','postal_receive',1,1,1,1,'2018-06-22 10:50:04'),(84,17,'Complain','complaint',1,1,1,1,'2018-07-03 08:40:55'),(85,17,'Setup Font Office','setup_font_office',1,1,1,1,'2018-06-22 10:49:24'),(86,18,'Staff','staff',1,1,1,1,'2018-06-22 10:53:31'),(87,18,'Disable Staff','disable_staff',1,0,0,0,'2018-06-22 10:53:12'),(88,18,'Staff Attendance','staff_attendance',1,1,1,0,'2018-06-22 10:53:10'),(90,18,'Staff Payroll','staff_payroll',1,1,0,1,'2018-06-22 10:52:51'),(93,19,'Homework','homework',1,1,1,1,'2018-06-22 10:53:50'),(94,19,'Homework Evaluation','homework_evaluation',1,1,0,0,'2018-06-27 03:07:21'),(96,20,'Student Certificate','student_certificate',1,1,1,1,'2018-07-06 10:41:07'),(97,20,'Generate Certificate','generate_certificate',1,0,0,0,'2018-07-06 10:37:16'),(98,20,'Student ID Card','student_id_card',1,1,1,1,'2018-07-06 10:41:28'),(99,20,'Generate ID Card','generate_id_card',1,0,0,0,'2018-07-06 10:41:49'),(102,21,'Calendar To Do List','calendar_to_do_list',1,1,1,1,'2018-06-22 10:54:41'),(104,10,'Item Category','item_category',1,1,1,1,'2018-06-22 10:34:33'),(106,22,'Quick Session Change','quick_session_change',1,0,0,0,'2018-06-22 10:54:45'),(107,1,'Disable Student','disable_student',1,0,0,0,'2018-06-25 06:21:34'),(108,18,' Approve Leave Request','approve_leave_request',1,0,1,1,'2020-10-05 08:56:27'),(109,18,'Apply Leave','apply_leave',1,1,0,0,'2019-11-28 23:47:46'),(110,18,'Leave Types ','leave_types',1,1,1,1,'2018-07-02 10:17:56'),(111,18,'Department','department',1,1,1,1,'2018-06-26 03:57:07'),(112,18,'Designation','designation',1,1,1,1,'2018-06-26 03:57:07'),(113,22,'Fees Collection And Expense Monthly Chart','fees_collection_and_expense_monthly_chart',1,0,0,0,'2018-07-03 07:08:15'),(114,22,'Fees Collection And Expense Yearly Chart','fees_collection_and_expense_yearly_chart',1,0,0,0,'2018-07-03 07:08:15'),(115,22,'Monthly Fees Collection Widget','Monthly fees_collection_widget',1,0,0,0,'2018-07-03 07:13:35'),(116,22,'Monthly Expense Widget','monthly_expense_widget',1,0,0,0,'2018-07-03 07:13:35'),(117,22,'Student Count Widget','student_count_widget',1,0,0,0,'2018-07-03 07:13:35'),(118,22,'Staff Role Count Widget','staff_role_count_widget',1,0,0,0,'2018-07-03 07:13:35'),(122,5,'Attendance By Date','attendance_by_date',1,0,0,0,'2018-07-03 08:42:29'),(123,9,'Add Student','add_student',1,0,0,0,'2018-07-03 08:42:29'),(126,15,'User Status','user_status',1,0,0,0,'2018-07-03 08:42:29'),(127,18,'Can See Other Users Profile','can_see_other_users_profile',1,0,0,0,'2018-07-03 08:42:29'),(128,1,'Student Timeline','student_timeline',0,1,0,1,'2018-07-05 08:08:52'),(129,18,'Staff Timeline','staff_timeline',0,1,0,1,'2018-07-05 08:08:52'),(130,15,'Backup','backup',1,1,0,1,'2018-07-09 04:17:17'),(131,15,'Restore','restore',1,0,0,0,'2018-07-09 04:17:17'),(134,1,'Disable Reason','disable_reason',1,1,1,1,'2019-11-27 06:39:21'),(135,2,'Fees Reminder','fees_reminder',1,0,1,0,'2019-10-25 00:39:49'),(136,5,'Approve Leave','approve_leave',1,0,0,0,'2019-10-25 00:46:44'),(137,6,'Exam Group','exam_group',1,1,1,1,'2019-10-25 01:02:34'),(141,6,'Design Admit Card','design_admit_card',1,1,1,1,'2019-10-25 01:06:59'),(142,6,'Print Admit Card','print_admit_card',1,0,0,0,'2019-11-23 23:57:51'),(143,6,'Design Marksheet','design_marksheet',1,1,1,1,'2019-10-25 01:10:25'),(144,6,'Print Marksheet','print_marksheet',1,0,0,0,'2019-10-25 01:11:02'),(145,7,'Teachers Timetable','teachers_time_table',1,0,0,0,'2019-11-30 02:52:21'),(146,14,'Student Report','student_report',1,0,0,0,'2019-10-25 01:27:00'),(147,14,'Guardian Report','guardian_report',1,0,0,0,'2019-10-25 01:30:27'),(148,14,'Student History','student_history',1,0,0,0,'2019-10-25 01:39:07'),(149,14,'Student Login Credential Report','student_login_credential_report',1,0,0,0,'2019-10-25 01:39:07'),(150,14,'Class Subject Report','class_subject_report',1,0,0,0,'2019-10-25 01:39:07'),(151,14,'Admission Report','admission_report',1,0,0,0,'2019-10-25 01:39:07'),(152,14,'Sibling Report','sibling_report',1,0,0,0,'2019-10-25 01:39:07'),(153,14,'Homework Evaluation Report','homehork_evaluation_report',1,0,0,0,'2019-11-24 01:04:24'),(154,14,'Student Profile','student_profile',1,0,0,0,'2019-10-25 01:39:07'),(155,14,'Fees Statement','fees_statement',1,0,0,0,'2019-10-25 01:55:52'),(156,14,'Balance Fees Report','balance_fees_report',1,0,0,0,'2019-10-25 01:55:52'),(157,14,'Fees Collection Report','fees_collection_report',1,0,0,0,'2019-10-25 01:55:52'),(158,14,'Online Fees Collection Report','online_fees_collection_report',1,0,0,0,'2019-10-25 01:55:52'),(159,14,'Income Report','income_report',1,0,0,0,'2019-10-25 01:55:52'),(160,14,'Expense Report','expense_report',1,0,0,0,'2019-10-25 01:55:52'),(161,14,'PayRoll Report','payroll_report',1,0,0,0,'2019-10-31 00:23:22'),(162,14,'Income Group Report','income_group_report',1,0,0,0,'2019-10-25 01:55:52'),(163,14,'Expense Group Report','expense_group_report',1,0,0,0,'2019-10-25 01:55:52'),(164,14,'Attendance Report','attendance_report',1,0,0,0,'2019-10-25 02:08:06'),(165,14,'Staff Attendance Report','staff_attendance_report',1,0,0,0,'2019-10-25 02:08:06'),(174,14,'Transport Report','transport_report',1,0,0,0,'2019-10-25 02:13:56'),(175,14,'Hostel Report','hostel_report',1,0,0,0,'2019-11-27 06:51:53'),(176,14,'Audit Trail Report','audit_trail_report',1,0,0,0,'2019-10-25 02:16:39'),(177,14,'User Log','user_log',1,0,0,0,'2019-10-25 02:19:27'),(178,14,'Book Issue Report','book_issue_report',1,0,0,0,'2019-10-25 02:29:04'),(179,14,'Book Due Report','book_due_report',1,0,0,0,'2019-10-25 02:29:04'),(180,14,'Book Inventory Report','book_inventory_report',1,0,0,0,'2019-10-25 02:29:04'),(181,14,'Stock Report','stock_report',1,0,0,0,'2019-10-25 02:31:28'),(182,14,'Add Item Report','add_item_report',1,0,0,0,'2019-10-25 02:31:28'),(183,14,'Issue Item Report','issue_item_report',1,0,0,0,'2019-11-29 03:48:06'),(185,23,'Online Examination','online_examination',1,1,1,1,'2019-11-23 23:54:50'),(186,23,'Question Bank','question_bank',1,1,1,1,'2019-11-23 23:55:18'),(187,6,'Exam Result','exam_result',1,0,0,0,'2019-11-23 23:58:50'),(188,7,'Subject Group','subject_group',1,1,1,1,'2019-11-24 00:34:32'),(189,18,'Teachers Rating','teachers_rating',1,0,1,1,'2019-11-24 03:12:54'),(190,22,'Fees Awaiting Payment Widegts','fees_awaiting_payment_widegts',1,0,0,0,'2019-11-24 00:52:51'),(191,22,'Conveted Leads Widegts','conveted_leads_widegts',1,0,0,0,'2019-11-24 00:58:24'),(192,22,'Fees Overview Widegts','fees_overview_widegts',1,0,0,0,'2019-11-24 00:57:41'),(193,22,'Enquiry Overview Widegts','enquiry_overview_widegts',1,0,0,0,'2019-12-02 05:06:09'),(194,22,'Library Overview Widegts','book_overview_widegts',1,0,0,0,'2019-12-01 01:13:04'),(195,22,'Student Today Attendance Widegts','today_attendance_widegts',1,0,0,0,'2019-12-03 04:57:45'),(196,6,'Marks Import','marks_import',1,0,0,0,'2019-11-24 01:02:11'),(197,14,'Student Attendance Type Report','student_attendance_type_report',1,0,0,0,'2019-11-24 01:06:32'),(198,14,'Exam Marks Report','exam_marks_report',1,0,0,0,'2019-11-24 01:11:15'),(200,14,'Online Exam Wise Report','online_exam_wise_report',1,0,0,0,'2019-11-24 01:18:14'),(201,14,'Online Exams Report','online_exams_report',1,0,0,0,'2019-11-29 02:48:05'),(202,14,'Online Exams Attempt Report','online_exams_attempt_report',1,0,0,0,'2019-11-29 02:46:24'),(203,14,'Online Exams Rank Report','online_exams_rank_report',1,0,0,0,'2019-11-24 01:22:25'),(204,14,'Staff Report','staff_report',1,0,0,0,'2019-11-24 01:25:27'),(205,6,'Exam','exam',1,1,1,1,'2019-11-24 04:55:48'),(207,6,'Exam Publish','exam_publish',1,0,0,0,'2019-11-24 05:15:04'),(208,6,'Link Exam','link_exam',1,0,1,0,'2019-11-24 05:15:04'),(210,6,'Assign / View student','exam_assign_view_student',1,0,1,0,'2019-11-24 05:15:04'),(211,6,'Exam Subject','exam_subject',1,0,1,0,'2019-11-24 05:15:04'),(212,6,'Exam Marks','exam_marks',1,0,1,0,'2019-11-24 05:15:04'),(213,15,'Language Switcher','language_switcher',1,0,0,0,'2019-11-24 05:17:11'),(214,23,'Add Questions in Exam ','add_questions_in_exam',1,0,1,0,'2019-11-28 01:38:57'),(215,15,'Custom Fields','custom_fields',1,0,0,0,'2019-11-29 04:08:35'),(216,15,'System Fields','system_fields',1,0,0,0,'2019-11-25 00:15:01'),(217,13,'SMS','sms',1,0,0,0,'2018-06-22 10:40:54'),(219,14,'Student / Period Attendance Report','student_period_attendance_report',1,0,0,0,'2019-11-29 02:19:31'),(220,14,'Biometric Attendance Log','biometric_attendance_log',1,0,0,0,'2019-11-27 05:59:16'),(221,14,'Book Issue Return Report','book_issue_return_report',1,0,0,0,'2019-11-27 06:30:23'),(222,23,'Assign / View Student','online_assign_view_student',1,0,1,0,'2019-11-28 04:20:22'),(223,14,'Rank Report','rank_report',1,0,0,0,'2019-11-29 02:30:21'),(224,25,'Chat','chat',1,0,0,0,'2019-11-29 04:10:28'),(226,22,'Income Donut Graph','income_donut_graph',1,0,0,0,'2019-11-29 05:00:33'),(227,22,'Expense Donut Graph','expense_donut_graph',1,0,0,0,'2019-11-29 05:01:10'),(228,9,'Import Book','import_book',1,0,0,0,'2019-11-29 06:21:01'),(229,22,'Staff Present Today Widegts','staff_present_today_widegts',1,0,0,0,'2019-11-29 06:48:00'),(230,22,'Student Present Today Widegts','student_present_today_widegts',1,0,0,0,'2019-11-29 06:47:42'),(231,26,'Multi Class Student','multi_class_student',1,1,1,1,'2020-10-05 08:56:27'),(232,27,'Online Admission','online_admission',1,0,1,1,'2019-12-02 06:11:10'),(233,15,'Print Header Footer','print_header_footer',1,0,0,0,'2020-02-12 02:02:02'),(234,28,'Manage Alumni','manage_alumni',1,1,1,1,'2020-06-02 03:15:46'),(235,28,'Events','events',1,1,1,1,'2020-05-28 21:48:52'),(236,29,'Manage Lesson Plan','manage_lesson_plan',1,1,1,0,'2020-05-28 22:17:37'),(237,29,'Manage Syllabus Status','manage_syllabus_status',1,0,1,0,'2020-05-28 22:20:11'),(238,29,'Lesson','lesson',1,1,1,1,'2020-05-28 22:20:11'),(239,29,'Topic','topic',1,1,1,1,'2020-05-28 22:20:11'),(240,14,'Syllabus Status Report','syllabus_status_report',1,0,0,0,'2020-05-28 23:17:54'),(241,14,'Teacher Syllabus Status Report','teacher_syllabus_status_report',1,0,0,0,'2020-05-28 23:17:54'),(242,14,'Alumni Report','alumni_report',1,0,0,0,'2020-06-07 23:59:54'),(243,15,'Student Profile Update','student_profile_update',1,0,0,0,'2020-08-21 05:36:33'),(244,14,'Student Gender Ratio Report','student_gender_ratio_report',1,0,0,0,'2020-08-22 12:37:51'),(245,14,'Student Teacher Ratio Report','student_teacher_ratio_report',1,0,0,0,'2020-08-22 12:42:27'),(246,14,'Daily Attendance Report','daily_attendance_report',1,0,0,0,'2020-08-22 12:43:16'),(247,23,'Import Question','import_question',1,0,0,0,'2019-11-23 18:25:18'),(248,20,'Staff ID Card','staff_id_card',1,1,1,1,'2018-07-06 10:41:28'),(249,20,'Generate Staff ID Card','generate_staff_id_card',1,0,0,0,'2018-07-06 10:41:49'),(250,30,'Account Category','account_category',1,1,1,1,'2022-09-16 09:30:22'),(251,30,'Chart of account','chart_of_account',1,1,1,1,'2022-09-16 09:30:22'),(252,30,'Approved Voucher','approved_voucher',1,0,0,0,'2022-09-16 09:30:22'),(253,30,'Unapproved Voucher','upapproved_voucher',1,0,1,1,'2022-09-16 09:30:22'),(254,30,'Rejected Voucher','rejected_voucher',1,0,0,0,'2022-09-16 09:30:22'),(255,30,'Journal Voucher','journal_voucher',1,0,0,0,'2022-09-16 09:30:22'),(256,30,'Payment Voucher','payment_voucher',1,0,0,0,'2022-09-16 09:30:22'),(257,30,'Receipt Voucher','receipt_voucher',1,0,0,0,'2022-09-16 09:30:22'),(258,30,'Account Reports','account_reports',1,0,0,0,'2022-09-16 09:30:22'),(259,30,'Trail Balance','trail_balance',1,0,0,0,'2022-09-16 09:30:22'),(260,30,'General Ledger','general_legdger',1,0,0,0,'2022-09-16 09:30:22'),(261,30,'Profit & Loss','profit_loss',1,0,0,0,'2022-09-16 09:30:22'),(262,30,'Balance Sheet','balance_sheet',1,0,0,0,'2022-09-16 09:30:22'),(263,30,'Day Book','day_book',1,0,0,0,'2022-09-16 09:30:22'),(264,30,'Day Sheet','day_sheet',1,0,0,0,'2022-09-16 09:30:22'),(265,30,'Daily Reconsile','daily_reconsile',1,0,0,0,'2022-09-16 09:30:22'),(266,2,'Guardian Fee','guardian_fee',1,1,1,1,'2022-09-16 09:30:22');
/*!40000 ALTER TABLE `permission_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_group`
--

DROP TABLE IF EXISTS `permission_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `permission_group` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `short_code` varchar(100) NOT NULL,
  `is_active` int DEFAULT '0',
  `system` int NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_group`
--

LOCK TABLES `permission_group` WRITE;
/*!40000 ALTER TABLE `permission_group` DISABLE KEYS */;
INSERT INTO `permission_group` VALUES (1,'Student Information','student_information',1,1,'2019-03-15 09:30:22'),(2,'Fees Collection','fees_collection',1,0,'2020-06-11 00:51:35'),(3,'Income','income',1,0,'2020-06-01 01:57:39'),(4,'Expense','expense',1,0,'2019-03-15 09:06:22'),(5,'Student Attendance','student_attendance',1,0,'2018-07-02 07:48:08'),(6,'Examination','examination',1,0,'2018-07-11 02:49:08'),(7,'Academics','academics',1,1,'2018-07-02 07:25:43'),(8,'Download Center','download_center',1,0,'2018-07-02 07:49:29'),(9,'Library','library',1,0,'2022-11-10 10:42:40'),(10,'Inventory','inventory',1,0,'2018-06-27 00:48:58'),(11,'Transport','transport',1,0,'2018-06-27 07:51:26'),(12,'Hostel','hostel',1,0,'2022-11-10 10:42:57'),(13,'Communicate','communicate',1,0,'2018-07-02 07:50:00'),(14,'Reports','reports',1,1,'2018-06-27 03:40:22'),(15,'System Settings','system_settings',1,1,'2018-06-27 03:40:28'),(16,'Front CMS','front_cms',1,0,'2018-07-10 05:16:54'),(17,'Front Office','front_office',1,0,'2018-06-27 03:45:30'),(18,'Human Resource','human_resource',1,1,'2018-06-27 03:41:02'),(19,'Homework','homework',1,0,'2018-06-27 00:49:38'),(20,'Certificate','certificate',1,0,'2018-06-27 07:51:29'),(21,'Calendar To Do List','calendar_to_do_list',1,0,'2019-03-15 09:06:25'),(22,'Dashboard and Widgets','dashboard_and_widgets',1,1,'2018-06-27 03:41:17'),(23,'Online Examination','online_examination',1,0,'2020-06-01 02:25:36'),(25,'Chat','chat',1,0,'2019-11-23 23:54:04'),(26,'Multi Class','multi_class',1,0,'2019-11-27 12:14:14'),(27,'Online Admission','online_admission',1,0,'2019-11-27 02:42:13'),(28,'Alumni','alumni',1,0,'2020-05-29 00:26:38'),(29,'Lesson Plan','lesson_plan',1,0,'2020-06-07 05:38:30'),(30,'Account','account',1,0,'2022-09-16 09:30:22');
/*!40000 ALTER TABLE `permission_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_student`
--

DROP TABLE IF EXISTS `permission_student`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `permission_student` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `short_code` varchar(100) NOT NULL,
  `system` int NOT NULL,
  `student` int NOT NULL,
  `parent` int NOT NULL,
  `group_id` int NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_student`
--

LOCK TABLES `permission_student` WRITE;
/*!40000 ALTER TABLE `permission_student` DISABLE KEYS */;
INSERT INTO `permission_student` VALUES (1,'Fees','fees',0,1,1,2,'2020-06-11 00:51:35'),(2,'Class Timetable','class_timetable',1,1,1,7,'2020-05-30 19:57:50'),(3,'Homework','homework',0,1,1,19,'2020-06-01 02:49:14'),(4,'Download Center','download_center',0,1,1,8,'2020-06-01 02:52:49'),(5,'Attendance','attendance',0,1,1,5,'2020-06-01 02:57:18'),(7,'Examinations','examinations',0,1,1,6,'2020-06-01 02:59:50'),(8,'Notice Board','notice_board',0,1,1,13,'2020-06-01 03:00:35'),(11,'Library','library',0,1,1,9,'2022-11-10 10:42:40'),(12,'Transport Routes','transport_routes',0,1,1,11,'2020-06-01 03:51:30'),(13,'Hostel Rooms','hostel_rooms',0,1,1,12,'2022-11-10 10:42:57'),(14,'Calendar To Do List','calendar_to_do_list',0,1,1,21,'2020-06-01 03:53:18'),(15,'Online Examination','online_examination',0,1,1,23,'2020-06-11 05:20:01'),(16,'Teachers Rating','teachers_rating',0,1,1,0,'2020-06-01 04:49:58'),(17,'Chat','chat',0,1,1,25,'2020-06-01 04:53:06'),(18,'Multi Class','multi_class',1,1,1,26,'2020-05-30 19:56:52'),(19,'Lesson Plan','lesson_plan',0,1,1,29,'2020-06-07 05:38:30'),(20,'Syllabus Status','syllabus_status',0,1,1,29,'2020-06-07 05:38:30'),(23,'Apply Leave','apply_leave',0,1,1,0,'2020-06-11 05:20:23');
/*!40000 ALTER TABLE `permission_student` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `print_headerfooter`
--

DROP TABLE IF EXISTS `print_headerfooter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `print_headerfooter` (
  `id` int NOT NULL AUTO_INCREMENT,
  `print_type` varchar(255) NOT NULL,
  `header_image` varchar(255) NOT NULL,
  `footer_content` text NOT NULL,
  `created_by` int NOT NULL,
  `entry_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `print_headerfooter`
--

LOCK TABLES `print_headerfooter` WRITE;
/*!40000 ALTER TABLE `print_headerfooter` DISABLE KEYS */;
INSERT INTO `print_headerfooter` VALUES (1,'staff_payslip','header_image.jpg','This payslip is computer generated hence no signature is required.',1,'2020-02-28 15:41:08'),(2,'student_receipt','header_image.jpg','This receipt is computer generated hence no signature is required.',1,'2020-02-28 15:40:58'),(3,'online_admission_receipt','header_image.jpg','This receipt is for online admission  computer ffffffff generated hence no signature is required.',1,'2021-05-27 12:50:24');
/*!40000 ALTER TABLE `print_headerfooter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `question_answers`
--

DROP TABLE IF EXISTS `question_answers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `question_answers` (
  `id` int NOT NULL AUTO_INCREMENT,
  `question_id` int NOT NULL,
  `option_id` int NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question_answers`
--

LOCK TABLES `question_answers` WRITE;
/*!40000 ALTER TABLE `question_answers` DISABLE KEYS */;
/*!40000 ALTER TABLE `question_answers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `question_options`
--

DROP TABLE IF EXISTS `question_options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `question_options` (
  `id` int NOT NULL AUTO_INCREMENT,
  `question_id` int NOT NULL,
  `option` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question_options`
--

LOCK TABLES `question_options` WRITE;
/*!40000 ALTER TABLE `question_options` DISABLE KEYS */;
/*!40000 ALTER TABLE `question_options` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questions`
--

DROP TABLE IF EXISTS `questions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `questions` (
  `id` int NOT NULL AUTO_INCREMENT,
  `staff_id` int DEFAULT NULL,
  `subject_id` int DEFAULT NULL,
  `question_type` varchar(100) NOT NULL,
  `level` varchar(10) NOT NULL,
  `class_id` int NOT NULL,
  `section_id` int NOT NULL,
  `class_section_id` int DEFAULT NULL,
  `question` text,
  `opt_a` text,
  `opt_b` text,
  `opt_c` text,
  `opt_d` text,
  `opt_e` text,
  `correct` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `subject_id` (`subject_id`),
  CONSTRAINT `questions_ibfk_1` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questions`
--

LOCK TABLES `questions` WRITE;
/*!40000 ALTER TABLE `questions` DISABLE KEYS */;
/*!40000 ALTER TABLE `questions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `read_notification`
--

DROP TABLE IF EXISTS `read_notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `read_notification` (
  `id` int NOT NULL AUTO_INCREMENT,
  `student_id` int DEFAULT NULL,
  `parent_id` int DEFAULT NULL,
  `staff_id` int DEFAULT NULL,
  `notification_id` int DEFAULT NULL,
  `is_active` varchar(255) DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `read_notification`
--

LOCK TABLES `read_notification` WRITE;
/*!40000 ALTER TABLE `read_notification` DISABLE KEYS */;
/*!40000 ALTER TABLE `read_notification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reference`
--

DROP TABLE IF EXISTS `reference`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `reference` (
  `id` int NOT NULL AUTO_INCREMENT,
  `reference` varchar(100) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reference`
--

LOCK TABLES `reference` WRITE;
/*!40000 ALTER TABLE `reference` DISABLE KEYS */;
/*!40000 ALTER TABLE `reference` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `roles` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `slug` varchar(150) DEFAULT NULL,
  `is_active` int DEFAULT '0',
  `is_system` int NOT NULL DEFAULT '0',
  `is_superadmin` int NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'Admin',NULL,0,1,0,'2018-06-30 15:39:11','0000-00-00'),(2,'Teacher',NULL,0,1,0,'2018-06-30 15:39:14','0000-00-00'),(3,'Accountant',NULL,0,1,0,'2018-06-30 15:39:17','0000-00-00'),(4,'Librarian',NULL,0,1,0,'2018-06-30 15:39:21','0000-00-00'),(6,'Receptionist',NULL,0,1,0,'2018-07-02 05:39:03','0000-00-00'),(7,'Super Admin',NULL,0,1,1,'2018-07-11 14:11:29','0000-00-00'),(8,'KITCHEN STAFF',NULL,0,0,0,'2022-12-22 06:15:46',NULL);
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles_permissions`
--

DROP TABLE IF EXISTS `roles_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `roles_permissions` (
  `id` int NOT NULL AUTO_INCREMENT,
  `role_id` int DEFAULT NULL,
  `perm_cat_id` int DEFAULT NULL,
  `can_view` int DEFAULT NULL,
  `can_add` int DEFAULT NULL,
  `can_edit` int DEFAULT NULL,
  `can_delete` int DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1498 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles_permissions`
--

LOCK TABLES `roles_permissions` WRITE;
/*!40000 ALTER TABLE `roles_permissions` DISABLE KEYS */;
INSERT INTO `roles_permissions` VALUES (10,1,17,1,1,1,1,'2018-07-06 09:48:56'),(11,1,78,1,1,1,1,'2018-07-03 00:49:43'),(23,1,12,1,1,1,1,'2018-07-06 09:45:38'),(24,1,13,1,1,1,1,'2018-07-06 09:48:28'),(26,1,15,1,1,1,0,'2019-11-27 23:47:28'),(28,1,19,1,1,1,0,'2018-07-02 11:31:10'),(30,1,76,1,1,1,0,'2018-07-02 11:31:10'),(31,1,21,1,1,1,0,'2022-02-09 19:25:28'),(32,1,22,1,1,1,1,'2018-07-02 11:32:05'),(34,1,24,1,1,1,1,'2019-11-28 06:35:20'),(43,1,32,1,1,1,1,'2018-07-06 10:22:05'),(44,1,33,1,1,1,1,'2018-07-06 10:22:29'),(45,1,34,1,1,1,1,'2018-07-06 10:23:59'),(46,1,35,1,1,1,1,'2018-07-06 10:24:34'),(47,1,104,1,1,1,1,'2018-07-06 10:23:08'),(48,1,37,1,1,1,1,'2018-07-06 10:25:30'),(49,1,38,1,1,1,1,'2018-07-09 05:15:27'),(58,1,52,1,1,0,1,'2018-07-09 03:19:43'),(61,1,55,1,1,1,1,'2018-07-02 09:24:16'),(67,1,61,1,1,0,1,'2018-07-09 05:59:19'),(68,1,62,1,1,0,1,'2018-07-09 05:59:19'),(69,1,63,1,1,0,1,'2018-07-09 03:51:38'),(70,1,64,1,1,1,1,'2018-07-09 03:02:19'),(71,1,65,1,1,1,1,'2018-07-09 03:11:21'),(72,1,66,1,1,1,1,'2018-07-09 03:13:09'),(73,1,67,1,1,1,1,'2018-07-09 03:14:47'),(74,1,79,1,1,0,1,'2019-11-30 01:32:51'),(75,1,80,1,1,1,1,'2018-07-06 09:41:23'),(76,1,81,1,1,1,1,'2018-07-06 09:41:23'),(78,1,83,1,1,1,1,'2018-07-06 09:41:23'),(79,1,84,1,1,1,1,'2018-07-06 09:41:23'),(80,1,85,1,1,1,1,'2018-07-12 00:16:00'),(87,1,92,1,1,1,1,'2018-06-26 03:33:43'),(94,1,82,1,1,1,1,'2018-07-06 09:41:23'),(120,1,39,1,1,1,1,'2018-07-06 10:26:28'),(156,1,9,1,1,1,1,'2019-11-27 23:45:46'),(157,1,10,1,1,1,1,'2019-11-27 23:45:46'),(159,1,40,1,1,1,1,'2019-11-30 00:49:39'),(160,1,41,1,1,1,1,'2019-12-02 05:43:41'),(161,1,42,1,1,1,1,'2019-11-30 00:49:39'),(169,1,27,1,1,0,1,'2019-11-29 06:15:37'),(178,1,54,1,0,1,0,'2018-07-05 09:09:22'),(179,1,56,1,0,1,0,'2019-11-30 00:49:54'),(180,1,57,1,0,1,0,'2019-11-30 01:32:51'),(181,1,58,1,0,1,0,'2019-11-30 01:32:51'),(182,1,59,1,0,1,0,'2019-11-30 01:32:51'),(183,1,60,1,0,1,0,'2019-11-30 00:59:57'),(190,1,105,1,0,0,0,'2018-07-02 11:13:25'),(199,1,75,1,0,0,0,'2018-07-02 11:19:46'),(201,1,14,1,0,0,0,'2018-07-02 11:22:03'),(203,1,16,1,0,0,0,'2018-07-02 11:24:21'),(204,1,26,1,0,0,0,'2018-07-02 11:32:05'),(206,1,29,1,0,0,0,'2018-07-02 11:43:54'),(207,1,30,1,0,0,0,'2018-07-02 11:43:54'),(208,1,31,1,1,1,1,'2019-11-30 01:32:51'),(215,1,50,1,0,0,0,'2018-07-02 12:04:53'),(216,1,51,1,0,0,0,'2018-07-02 12:04:53'),(222,1,1,1,1,1,1,'2019-11-27 22:55:06'),(227,1,91,1,0,0,0,'2018-07-03 01:49:27'),(230,10,53,0,1,0,0,'2018-07-03 03:52:55'),(231,10,54,0,0,1,0,'2018-07-03 03:52:55'),(232,10,55,1,1,1,1,'2018-07-03 03:58:42'),(233,10,56,0,0,1,0,'2018-07-03 03:52:55'),(235,10,58,0,0,1,0,'2018-07-03 03:52:55'),(236,10,59,0,0,1,0,'2018-07-03 03:52:55'),(239,10,1,1,1,1,1,'2018-07-03 04:16:43'),(241,10,3,1,0,0,0,'2018-07-03 04:23:56'),(242,10,2,1,0,0,0,'2018-07-03 04:24:39'),(243,10,4,1,0,1,1,'2018-07-03 04:31:24'),(245,10,107,1,0,0,0,'2018-07-03 04:36:41'),(246,10,5,1,1,0,1,'2018-07-03 04:38:18'),(247,10,7,1,1,1,1,'2018-07-03 04:42:07'),(248,10,68,1,0,0,0,'2018-07-03 04:42:53'),(249,10,69,1,1,1,1,'2018-07-03 04:49:46'),(250,10,70,1,0,0,1,'2018-07-03 04:52:40'),(251,10,72,1,0,0,0,'2018-07-03 04:56:46'),(252,10,73,1,0,0,0,'2018-07-03 04:56:46'),(253,10,74,1,0,0,0,'2018-07-03 04:58:34'),(254,10,75,1,0,0,0,'2018-07-03 04:58:34'),(255,10,9,1,1,1,1,'2018-07-03 05:02:22'),(256,10,10,1,1,1,1,'2018-07-03 05:03:09'),(257,10,11,1,0,0,0,'2018-07-03 05:03:09'),(258,10,12,1,1,1,1,'2018-07-03 05:08:40'),(259,10,13,1,1,1,1,'2018-07-03 05:08:40'),(260,10,14,1,0,0,0,'2018-07-03 05:08:53'),(261,10,15,1,1,1,0,'2018-07-03 05:11:28'),(262,10,16,1,0,0,0,'2018-07-03 05:12:12'),(263,10,17,1,1,1,1,'2018-07-03 05:14:30'),(264,10,19,1,1,1,0,'2018-07-03 05:15:45'),(265,10,20,1,1,1,1,'2018-07-03 05:18:51'),(266,10,76,1,0,0,0,'2018-07-03 05:21:21'),(267,10,21,1,1,1,0,'2018-07-03 05:22:45'),(268,10,22,1,1,1,1,'2018-07-03 05:25:00'),(269,10,23,1,1,1,1,'2018-07-03 05:27:16'),(270,10,24,1,1,1,1,'2018-07-03 05:27:49'),(271,10,25,1,1,1,1,'2018-07-03 05:27:49'),(272,10,26,1,0,0,0,'2018-07-03 05:28:25'),(273,10,77,1,1,1,1,'2018-07-03 05:29:57'),(274,10,27,1,1,0,1,'2018-07-03 05:30:36'),(275,10,28,1,1,1,1,'2018-07-03 05:33:09'),(276,10,29,1,0,0,0,'2018-07-03 05:34:03'),(277,10,30,1,0,0,0,'2018-07-03 05:34:03'),(278,10,31,1,0,0,0,'2018-07-03 05:34:03'),(279,10,32,1,1,1,1,'2018-07-03 05:35:42'),(280,10,33,1,1,1,1,'2018-07-03 05:36:32'),(281,10,34,1,1,1,1,'2018-07-03 05:38:03'),(282,10,35,1,1,1,1,'2018-07-03 05:38:41'),(283,10,104,1,1,1,1,'2018-07-03 05:40:43'),(284,10,37,1,1,1,1,'2018-07-03 05:42:42'),(285,10,38,1,1,1,1,'2018-07-03 05:43:56'),(286,10,39,1,1,1,1,'2018-07-03 05:45:39'),(287,10,40,1,1,1,1,'2018-07-03 05:47:22'),(288,10,41,1,1,1,1,'2018-07-03 05:48:54'),(289,10,42,1,1,1,1,'2018-07-03 05:49:31'),(290,10,43,1,1,1,1,'2018-07-03 05:51:15'),(291,10,44,1,0,0,0,'2018-07-03 05:52:06'),(292,10,46,1,0,0,0,'2018-07-03 05:52:06'),(293,10,50,1,0,0,0,'2018-07-03 05:52:59'),(294,10,51,1,0,0,0,'2018-07-03 05:52:59'),(295,10,60,0,0,1,0,'2018-07-03 05:55:05'),(296,10,61,1,1,1,1,'2018-07-03 05:56:52'),(297,10,62,1,1,1,1,'2018-07-03 05:58:53'),(298,10,63,1,1,0,0,'2018-07-03 05:59:37'),(299,10,64,1,1,1,1,'2018-07-03 06:00:27'),(300,10,65,1,1,1,1,'2018-07-03 06:02:51'),(301,10,66,1,1,1,1,'2018-07-03 06:02:51'),(302,10,67,1,0,0,0,'2018-07-03 06:02:51'),(303,10,78,1,1,1,1,'2018-07-04 04:10:04'),(307,1,126,1,0,0,0,'2018-07-03 09:26:13'),(310,1,119,1,0,0,0,'2018-07-03 10:15:00'),(311,1,120,1,0,0,0,'2018-07-03 10:15:00'),(315,1,123,1,0,0,0,'2018-07-03 10:27:03'),(317,1,124,1,0,0,0,'2018-07-03 10:29:14'),(320,1,47,1,0,0,0,'2018-07-03 11:01:12'),(321,1,121,1,0,0,0,'2018-07-03 11:01:12'),(369,1,102,1,1,1,1,'2019-12-02 05:02:15'),(372,10,79,1,1,0,0,'2018-07-04 04:10:04'),(373,10,80,1,1,1,1,'2018-07-04 04:23:09'),(374,10,81,1,1,1,1,'2018-07-04 04:23:50'),(375,10,82,1,1,1,1,'2018-07-04 04:26:54'),(376,10,83,1,1,1,1,'2018-07-04 04:27:55'),(377,10,84,1,1,1,1,'2018-07-04 04:30:26'),(378,10,85,1,1,1,1,'2018-07-04 04:32:54'),(379,10,86,1,1,1,1,'2018-07-04 04:46:18'),(380,10,87,1,0,0,0,'2018-07-04 04:49:49'),(381,10,88,1,1,1,0,'2018-07-04 04:51:20'),(382,10,89,1,0,0,0,'2018-07-04 04:51:51'),(383,10,90,1,1,0,1,'2018-07-04 04:55:01'),(384,10,91,1,0,0,0,'2018-07-04 04:55:01'),(385,10,108,1,1,1,1,'2018-07-04 04:57:46'),(386,10,109,1,1,1,1,'2018-07-04 04:58:26'),(387,10,110,1,1,1,1,'2018-07-04 05:02:43'),(388,10,111,1,1,1,1,'2018-07-04 05:03:21'),(389,10,112,1,1,1,1,'2018-07-04 05:05:06'),(390,10,127,1,0,0,0,'2018-07-04 05:05:06'),(391,10,93,1,1,1,1,'2018-07-04 05:07:14'),(392,10,94,1,1,0,0,'2018-07-04 05:08:02'),(394,10,95,1,0,0,0,'2018-07-04 05:08:44'),(395,10,102,1,1,1,1,'2018-07-04 05:11:02'),(396,10,106,1,0,0,0,'2018-07-04 05:11:39'),(397,10,113,1,0,0,0,'2018-07-04 05:12:37'),(398,10,114,1,0,0,0,'2018-07-04 05:12:37'),(399,10,115,1,0,0,0,'2018-07-04 05:18:45'),(400,10,116,1,0,0,0,'2018-07-04 05:18:45'),(401,10,117,1,0,0,0,'2018-07-04 05:19:43'),(402,10,118,1,0,0,0,'2018-07-04 05:19:43'),(434,1,125,1,0,0,0,'2018-07-06 09:59:26'),(435,1,96,1,1,1,1,'2018-07-09 01:03:54'),(445,1,48,1,0,0,0,'2018-07-06 11:49:35'),(446,1,49,1,0,0,0,'2018-07-06 11:49:35'),(461,1,97,1,0,0,0,'2018-07-09 01:00:16'),(462,1,95,1,0,0,0,'2018-07-09 01:18:41'),(464,1,86,1,1,1,1,'2019-11-28 06:39:19'),(474,1,130,1,1,0,1,'2018-07-09 10:56:36'),(476,1,131,1,0,0,0,'2018-07-09 04:53:32'),(479,2,47,1,0,0,0,'2018-07-10 06:47:12'),(480,2,105,1,0,0,0,'2018-07-10 06:47:12'),(482,2,119,1,0,0,0,'2018-07-10 06:47:12'),(483,2,120,1,0,0,0,'2018-07-10 06:47:12'),(486,2,16,1,0,0,0,'2018-07-10 06:47:12'),(493,2,22,1,0,0,0,'2018-07-12 00:20:27'),(504,2,95,1,0,0,0,'2018-07-10 06:47:12'),(513,3,72,1,0,0,0,'2018-07-10 07:07:30'),(517,3,75,1,0,0,0,'2018-07-10 07:10:38'),(527,3,89,1,0,0,0,'2018-07-10 07:18:44'),(529,3,91,1,0,0,0,'2018-07-10 07:18:44'),(549,3,124,1,0,0,0,'2018-07-10 07:22:17'),(557,6,82,1,1,1,1,'2019-12-01 01:48:28'),(558,6,83,1,1,1,1,'2019-12-01 01:49:08'),(559,6,84,1,1,1,1,'2019-12-01 01:49:59'),(575,6,44,1,0,0,0,'2018-07-10 07:35:33'),(576,6,46,1,0,0,0,'2018-07-10 07:35:33'),(578,6,102,1,1,1,1,'2019-12-01 01:52:27'),(594,3,125,1,0,0,0,'2018-07-10 07:58:12'),(595,3,48,1,0,0,0,'2018-07-10 07:58:12'),(596,3,49,1,0,0,0,'2018-07-10 07:58:12'),(617,2,17,1,1,1,1,'2018-07-11 06:55:14'),(618,2,19,1,1,1,0,'2018-07-11 06:55:14'),(620,2,76,1,1,1,0,'2018-07-11 06:55:14'),(622,2,121,1,0,0,0,'2018-07-11 06:56:27'),(625,1,28,1,1,1,1,'2019-11-29 06:19:18'),(628,6,22,1,0,0,0,'2018-07-12 00:23:47'),(634,4,102,1,1,1,1,'2019-12-01 01:03:00'),(662,1,138,1,0,0,0,'2019-11-01 02:28:24'),(663,1,139,1,1,1,1,'2019-11-01 02:28:24'),(664,1,140,1,1,1,1,'2019-11-01 02:28:24'),(669,1,145,1,0,0,0,'2019-11-26 04:51:15'),(677,1,153,1,0,0,0,'2019-11-01 02:28:24'),(690,1,166,1,0,0,0,'2019-11-01 02:28:24'),(691,1,167,1,0,0,0,'2019-11-01 02:28:24'),(692,1,168,1,0,0,0,'2019-11-01 02:28:24'),(693,1,170,1,0,0,0,'2019-11-01 02:28:24'),(694,1,172,1,0,0,0,'2019-11-01 02:28:24'),(695,1,173,1,0,0,0,'2019-11-01 02:28:24'),(720,1,216,1,0,0,0,'2019-11-26 05:24:12'),(728,1,185,1,1,1,1,'2019-11-28 02:50:33'),(729,1,186,1,1,1,1,'2019-11-28 02:49:07'),(730,1,214,1,0,1,0,'2019-11-28 01:47:53'),(732,1,198,1,0,0,0,'2019-11-26 05:24:30'),(733,1,199,1,0,0,0,'2019-11-26 05:24:30'),(734,1,200,1,0,0,0,'2019-11-26 05:24:30'),(735,1,201,1,0,0,0,'2019-11-26 05:24:30'),(736,1,202,1,0,0,0,'2019-11-26 05:24:30'),(737,1,203,1,0,0,0,'2019-11-26 05:24:30'),(739,1,218,1,0,0,0,'2019-11-27 06:36:31'),(743,1,218,1,0,0,0,'2019-11-27 06:36:32'),(747,1,2,1,0,0,0,'2019-11-27 22:56:08'),(748,1,3,1,1,1,1,'2019-11-27 22:56:32'),(749,1,4,1,1,1,1,'2019-11-27 22:56:48'),(751,1,128,0,1,0,1,'2019-11-27 22:57:01'),(752,1,132,1,0,1,1,'2019-11-27 23:02:23'),(754,1,134,1,1,1,1,'2019-11-27 23:18:21'),(755,1,5,1,1,0,1,'2019-11-27 23:35:07'),(756,1,6,1,0,0,0,'2019-11-27 23:35:25'),(757,1,7,1,1,1,1,'2019-11-27 23:36:35'),(758,1,8,1,1,1,1,'2019-11-27 23:37:27'),(760,1,68,1,0,0,0,'2019-11-27 23:38:06'),(761,1,69,1,1,1,1,'2019-11-27 23:39:06'),(762,1,70,1,1,1,1,'2019-11-27 23:39:41'),(763,1,71,1,0,0,0,'2019-11-27 23:39:59'),(764,1,72,1,0,0,0,'2019-11-27 23:40:11'),(765,1,73,1,0,0,0,'2019-11-27 23:43:15'),(766,1,74,1,0,0,0,'2019-11-27 23:43:55'),(768,1,11,1,0,0,0,'2019-11-27 23:45:46'),(769,1,122,1,0,0,0,'2019-11-27 23:52:43'),(771,1,136,1,0,0,0,'2019-11-27 23:55:36'),(772,1,20,1,1,1,1,'2019-11-28 04:06:44'),(773,1,137,1,1,1,1,'2019-11-28 00:46:14'),(774,1,141,1,1,1,1,'2019-11-28 00:59:42'),(775,1,142,1,0,0,0,'2019-11-27 23:56:12'),(776,1,143,1,1,1,1,'2019-11-28 00:59:42'),(777,1,144,1,0,0,0,'2019-11-27 23:56:12'),(778,1,187,1,0,0,0,'2019-11-27 23:56:12'),(779,1,196,1,0,0,0,'2019-11-27 23:56:12'),(781,1,207,1,0,0,0,'2019-11-27 23:56:12'),(782,1,208,1,0,1,0,'2019-11-28 00:10:22'),(783,1,210,1,0,1,0,'2019-11-28 00:34:40'),(784,1,211,1,0,1,0,'2019-11-28 00:38:23'),(785,1,212,1,0,1,0,'2019-11-28 00:42:15'),(786,1,205,1,1,1,1,'2019-11-28 00:42:15'),(787,1,222,1,0,1,0,'2019-11-28 01:36:36'),(788,1,77,1,1,1,1,'2019-11-28 06:22:10'),(789,1,188,1,1,1,1,'2019-11-28 06:26:16'),(790,1,23,1,1,1,1,'2019-11-28 06:34:20'),(791,1,25,1,1,1,1,'2019-11-28 06:36:20'),(792,1,127,1,0,0,0,'2019-11-28 06:41:25'),(794,1,88,1,1,1,0,'2019-11-28 06:43:04'),(795,1,90,1,1,0,1,'2019-11-28 06:46:22'),(796,1,108,1,0,1,1,'2021-01-23 07:09:32'),(797,1,109,1,1,0,0,'2019-11-28 23:38:11'),(798,1,110,1,1,1,1,'2019-11-28 23:49:29'),(799,1,111,1,1,1,1,'2019-11-28 23:49:57'),(800,1,112,1,1,1,1,'2019-11-28 23:49:57'),(801,1,129,0,1,0,1,'2019-11-28 23:49:57'),(802,1,189,1,0,1,1,'2019-11-28 23:59:22'),(806,2,133,1,0,1,0,'2019-11-29 00:34:35'),(810,2,1,1,1,1,1,'2019-11-30 02:54:16'),(813,1,133,1,0,1,0,'2019-11-29 00:39:57'),(817,1,93,1,1,1,1,'2019-11-29 00:56:14'),(825,1,87,1,0,0,0,'2019-11-29 00:56:14'),(829,1,94,1,1,0,0,'2019-11-29 00:57:57'),(836,1,146,1,0,0,0,'2019-11-29 01:13:28'),(837,1,147,1,0,0,0,'2019-11-29 01:13:28'),(838,1,148,1,0,0,0,'2019-11-29 01:13:28'),(839,1,149,1,0,0,0,'2019-11-29 01:13:28'),(840,1,150,1,0,0,0,'2019-11-29 01:13:28'),(841,1,151,1,0,0,0,'2019-11-29 01:13:28'),(842,1,152,1,0,0,0,'2019-11-29 01:13:28'),(843,1,154,1,0,0,0,'2019-11-29 01:13:28'),(862,1,155,1,0,0,0,'2019-11-29 02:07:30'),(863,1,156,1,0,0,0,'2019-11-29 02:07:52'),(864,1,157,1,0,0,0,'2019-11-29 02:08:05'),(874,1,158,1,0,0,0,'2019-11-29 02:14:03'),(875,1,159,1,0,0,0,'2019-11-29 02:14:31'),(876,1,160,1,0,0,0,'2019-11-29 02:14:44'),(878,1,162,1,0,0,0,'2019-11-29 02:15:58'),(879,1,163,1,0,0,0,'2019-11-29 02:16:19'),(882,1,164,1,0,0,0,'2019-11-29 02:25:17'),(884,1,165,1,0,0,0,'2019-11-29 02:25:30'),(886,1,197,1,0,0,0,'2019-11-29 02:25:48'),(887,1,219,1,0,0,0,'2019-11-29 02:26:05'),(889,1,220,1,0,0,0,'2019-11-29 02:26:22'),(932,1,204,1,0,0,0,'2019-11-29 03:43:27'),(933,1,221,1,0,0,0,'2019-11-29 03:45:04'),(934,1,178,1,0,0,0,'2019-11-29 03:45:16'),(935,1,179,1,0,0,0,'2019-11-29 03:45:33'),(936,1,161,1,0,0,0,'2019-11-29 03:45:48'),(937,1,180,1,0,0,0,'2019-11-29 03:45:48'),(938,1,181,1,0,0,0,'2019-11-29 03:49:33'),(939,1,182,1,0,0,0,'2019-11-29 03:49:45'),(940,1,183,1,0,0,0,'2019-11-29 03:49:56'),(941,1,174,1,0,0,0,'2019-11-29 03:50:53'),(943,1,176,1,0,0,0,'2019-11-29 03:52:10'),(944,1,177,1,0,0,0,'2019-11-29 03:52:22'),(945,1,53,0,1,0,1,'2021-01-23 07:09:32'),(946,1,215,1,0,0,0,'2019-11-29 04:01:37'),(947,1,213,1,0,0,0,'2019-11-29 04:07:45'),(974,1,224,1,0,0,0,'2019-11-29 04:32:52'),(979,1,225,1,0,0,0,'2019-11-29 04:45:30'),(982,2,225,1,0,0,0,'2019-11-29 04:47:19'),(1026,1,135,1,0,1,0,'2019-11-29 06:02:12'),(1031,1,228,1,0,0,0,'2019-11-29 06:21:16'),(1083,1,175,1,0,0,0,'2019-11-30 00:37:24'),(1086,1,43,1,1,1,1,'2019-11-30 00:49:39'),(1087,1,44,1,0,0,0,'2019-11-30 00:49:39'),(1088,1,46,1,0,0,0,'2019-11-30 00:49:39'),(1089,1,217,1,0,0,0,'2019-11-30 00:49:39'),(1090,1,98,1,1,1,1,'2019-11-30 01:32:51'),(1091,1,99,1,0,0,0,'2019-11-30 01:30:18'),(1092,1,223,1,0,0,0,'2019-11-30 01:32:51'),(1103,2,205,1,1,1,1,'2019-11-30 01:56:04'),(1105,2,23,1,0,0,0,'2019-11-30 01:56:04'),(1106,2,24,1,0,0,0,'2019-11-30 01:56:04'),(1107,2,25,1,0,0,0,'2019-11-30 01:56:04'),(1108,2,77,1,0,0,0,'2019-11-30 01:56:04'),(1119,2,117,1,0,0,0,'2019-11-30 01:56:04'),(1123,3,8,1,1,1,1,'2019-11-30 06:46:18'),(1125,3,69,1,1,1,1,'2019-11-30 07:00:49'),(1126,3,70,1,1,1,1,'2019-11-30 07:04:46'),(1130,3,9,1,1,1,1,'2019-11-30 07:14:54'),(1131,3,10,1,1,1,1,'2019-11-30 07:16:02'),(1134,3,35,1,1,1,1,'2019-11-30 07:25:04'),(1135,3,104,1,1,1,1,'2019-11-30 07:25:53'),(1140,3,41,1,1,1,1,'2019-11-30 07:37:13'),(1141,3,42,1,1,1,1,'2019-11-30 07:37:46'),(1142,3,43,1,1,1,1,'2019-11-30 07:42:06'),(1151,3,87,1,0,0,0,'2019-11-30 02:23:13'),(1152,3,88,1,1,1,0,'2019-11-30 02:23:13'),(1153,3,90,1,1,0,1,'2019-11-30 02:23:13'),(1154,3,108,1,0,1,0,'2019-11-30 02:23:13'),(1155,3,109,1,1,0,0,'2019-11-30 02:23:13'),(1156,3,110,1,1,1,1,'2019-11-30 02:23:13'),(1157,3,111,1,1,1,1,'2019-11-30 02:23:13'),(1158,3,112,1,1,1,1,'2019-11-30 02:23:13'),(1159,3,127,1,0,0,0,'2019-11-30 02:23:13'),(1160,3,129,0,1,0,1,'2019-11-30 02:23:13'),(1161,3,102,1,1,1,1,'2019-11-30 02:23:13'),(1162,3,106,1,0,0,0,'2019-11-30 02:23:13'),(1163,3,113,1,0,0,0,'2019-11-30 02:23:13'),(1164,3,114,1,0,0,0,'2019-11-30 02:23:13'),(1165,3,115,1,0,0,0,'2019-11-30 02:23:13'),(1166,3,116,1,0,0,0,'2019-11-30 02:23:13'),(1167,3,117,1,0,0,0,'2019-11-30 02:23:13'),(1168,3,118,1,0,0,0,'2019-11-30 02:23:13'),(1171,2,142,1,0,0,0,'2019-11-30 02:36:17'),(1172,2,144,1,0,0,0,'2019-11-30 02:36:17'),(1179,2,212,1,0,1,0,'2019-11-30 02:36:17'),(1183,2,148,1,0,0,0,'2019-11-30 02:36:17'),(1184,2,149,1,0,0,0,'2019-11-30 02:36:17'),(1185,2,150,1,0,0,0,'2019-11-30 02:36:17'),(1186,2,151,1,0,0,0,'2019-11-30 02:36:17'),(1187,2,152,1,0,0,0,'2019-11-30 02:36:17'),(1188,2,153,1,0,0,0,'2019-11-30 02:36:17'),(1189,2,154,1,0,0,0,'2019-11-30 02:36:17'),(1190,2,197,1,0,0,0,'2019-11-30 02:36:17'),(1191,2,198,1,0,0,0,'2019-11-30 02:36:17'),(1192,2,199,1,0,0,0,'2019-11-30 02:36:17'),(1193,2,200,1,0,0,0,'2019-11-30 02:36:17'),(1194,2,201,1,0,0,0,'2019-11-30 02:36:17'),(1195,2,202,1,0,0,0,'2019-11-30 02:36:17'),(1196,2,203,1,0,0,0,'2019-11-30 02:36:17'),(1197,2,219,1,0,0,0,'2019-11-30 02:36:17'),(1198,2,223,1,0,0,0,'2019-11-30 02:36:17'),(1199,2,213,1,0,0,0,'2019-11-30 02:36:17'),(1201,2,230,1,0,0,0,'2019-11-30 02:36:17'),(1204,2,214,1,0,1,0,'2019-11-30 02:36:17'),(1206,2,224,1,0,0,0,'2019-11-30 02:36:17'),(1208,2,2,1,0,0,0,'2019-11-30 02:55:45'),(1210,2,143,1,1,1,1,'2019-11-30 02:57:28'),(1211,2,145,1,0,0,0,'2019-11-30 02:57:28'),(1214,2,3,1,1,1,1,'2019-11-30 03:03:18'),(1216,2,4,1,1,1,1,'2019-11-30 03:32:56'),(1218,2,128,0,1,0,1,'2019-11-30 03:37:44'),(1220,3,135,1,0,1,0,'2019-11-30 07:08:56'),(1231,3,190,1,0,0,0,'2019-11-30 03:44:02'),(1232,3,192,1,0,0,0,'2019-11-30 03:44:02'),(1233,3,226,1,0,0,0,'2019-11-30 03:44:02'),(1234,3,227,1,0,0,0,'2019-11-30 03:44:02'),(1235,3,224,1,0,0,0,'2019-11-30 03:44:02'),(1236,2,15,1,1,1,0,'2019-11-30 03:54:25'),(1239,2,122,1,0,0,0,'2019-11-30 03:57:48'),(1240,2,136,1,0,0,0,'2019-11-30 03:57:48'),(1242,6,217,1,0,0,0,'2019-11-30 04:00:13'),(1243,6,224,1,0,0,0,'2019-11-30 04:00:13'),(1245,2,20,1,1,1,1,'2019-11-30 04:01:28'),(1246,2,137,1,1,1,1,'2019-11-30 04:02:40'),(1248,2,141,1,1,1,1,'2019-11-30 04:04:04'),(1250,2,187,1,0,0,0,'2019-11-30 04:11:19'),(1252,2,207,1,0,0,0,'2019-11-30 04:21:21'),(1253,2,208,1,0,1,0,'2019-11-30 04:22:00'),(1255,2,210,1,0,1,0,'2019-11-30 04:22:58'),(1256,2,211,1,0,1,0,'2019-11-30 04:24:03'),(1257,2,21,1,1,0,0,'2022-02-09 19:25:28'),(1259,2,188,1,0,0,0,'2019-11-30 04:34:35'),(1260,2,27,1,0,0,0,'2019-11-30 04:36:13'),(1262,2,43,1,1,1,1,'2019-11-30 04:39:42'),(1263,2,44,1,0,0,0,'2019-11-30 04:41:43'),(1264,2,46,1,0,0,0,'2019-11-30 04:41:43'),(1265,2,217,1,0,0,0,'2019-11-30 04:41:43'),(1266,2,146,1,0,0,0,'2019-11-30 04:46:35'),(1267,2,147,1,0,0,0,'2019-11-30 04:47:37'),(1269,2,164,1,0,0,0,'2019-11-30 04:51:04'),(1271,2,109,1,1,0,0,'2019-11-30 05:03:37'),(1272,2,93,1,1,1,1,'2019-11-30 05:07:25'),(1273,2,94,1,1,0,0,'2019-11-30 05:07:42'),(1275,2,102,1,1,1,1,'2019-11-30 05:11:22'),(1277,2,196,1,0,0,0,'2019-11-30 05:15:01'),(1278,2,195,1,0,0,0,'2019-11-30 05:19:08'),(1279,2,185,1,1,1,1,'2019-11-30 05:21:44'),(1280,2,186,1,1,1,1,'2019-11-30 05:22:43'),(1281,2,222,1,0,1,0,'2019-11-30 05:24:30'),(1283,3,5,1,1,0,1,'2019-11-30 06:43:04'),(1284,3,6,1,0,0,0,'2019-11-30 06:43:29'),(1285,3,7,1,1,1,1,'2019-11-30 06:44:39'),(1286,3,68,1,0,0,0,'2019-11-30 06:46:58'),(1287,3,71,1,0,0,0,'2019-11-30 07:05:41'),(1288,3,73,1,0,0,0,'2019-11-30 07:05:59'),(1289,3,74,1,0,0,0,'2019-11-30 07:06:08'),(1290,3,11,1,0,0,0,'2019-11-30 07:16:37'),(1291,3,12,1,1,1,1,'2019-11-30 07:19:29'),(1292,3,13,1,1,1,1,'2019-11-30 07:22:27'),(1294,3,14,1,0,0,0,'2019-11-30 07:22:55'),(1295,3,31,1,1,1,1,'2019-12-02 06:30:37'),(1297,3,37,1,1,1,1,'2019-11-30 07:28:09'),(1298,3,38,1,1,1,1,'2019-11-30 07:29:02'),(1299,3,39,1,1,1,1,'2019-11-30 07:30:07'),(1300,3,40,1,1,1,1,'2019-11-30 07:32:43'),(1301,3,44,1,0,0,0,'2019-11-30 07:44:09'),(1302,3,46,1,0,0,0,'2019-11-30 07:44:09'),(1303,3,217,1,0,0,0,'2019-11-30 07:44:09'),(1304,3,155,1,0,0,0,'2019-11-30 07:44:32'),(1305,3,156,1,0,0,0,'2019-11-30 07:45:18'),(1306,3,157,1,0,0,0,'2019-11-30 07:45:42'),(1307,3,158,1,0,0,0,'2019-11-30 07:46:07'),(1308,3,159,1,0,0,0,'2019-11-30 07:46:21'),(1309,3,160,1,0,0,0,'2019-11-30 07:46:33'),(1313,3,161,1,0,0,0,'2019-11-30 07:48:26'),(1314,3,162,1,0,0,0,'2019-11-30 07:48:48'),(1315,3,163,1,0,0,0,'2019-11-30 07:48:48'),(1316,3,164,1,0,0,0,'2019-11-30 07:49:47'),(1317,3,165,1,0,0,0,'2019-11-30 07:49:47'),(1318,3,174,1,0,0,0,'2019-11-30 07:49:47'),(1319,3,175,1,0,0,0,'2019-11-30 07:49:59'),(1320,3,181,1,0,0,0,'2019-11-30 07:50:08'),(1321,3,86,1,1,1,1,'2019-11-30 07:54:08'),(1322,4,28,1,1,1,1,'2019-12-01 00:52:39'),(1324,4,29,1,0,0,0,'2019-12-01 00:53:46'),(1325,4,30,1,0,0,0,'2019-12-01 00:53:59'),(1326,4,123,1,0,0,0,'2019-12-01 00:54:26'),(1327,4,228,1,0,0,0,'2019-12-01 00:54:39'),(1328,4,43,1,1,1,1,'2019-12-01 00:58:05'),(1332,4,44,1,0,0,0,'2019-12-01 00:59:16'),(1333,4,46,1,0,0,0,'2019-12-01 00:59:16'),(1334,4,217,1,0,0,0,'2019-12-01 00:59:16'),(1335,4,178,1,0,0,0,'2019-12-01 00:59:59'),(1336,4,179,1,0,0,0,'2019-12-01 01:00:11'),(1337,4,180,1,0,0,0,'2019-12-01 01:00:29'),(1338,4,221,1,0,0,0,'2019-12-01 01:00:46'),(1339,4,86,1,0,0,0,'2019-12-01 01:01:02'),(1341,4,106,1,0,0,0,'2019-12-01 01:05:21'),(1342,1,107,1,0,0,0,'2019-12-01 01:06:44'),(1343,4,117,1,0,0,0,'2019-12-01 01:10:20'),(1344,4,194,1,0,0,0,'2019-12-01 01:11:35'),(1348,4,230,1,0,0,0,'2019-12-01 01:19:15'),(1350,6,1,1,0,0,0,'2019-12-01 01:35:32'),(1351,6,21,1,0,0,0,'2019-12-01 01:36:29'),(1352,6,23,1,0,0,0,'2019-12-01 01:36:45'),(1353,6,24,1,0,0,0,'2019-12-01 01:37:05'),(1354,6,25,1,0,0,0,'2019-12-01 01:37:34'),(1355,6,77,1,0,0,0,'2019-12-01 01:38:08'),(1356,6,188,1,0,0,0,'2019-12-01 01:38:45'),(1357,6,43,1,1,1,1,'2019-12-01 01:40:44'),(1358,6,78,1,1,1,1,'2019-12-01 01:43:04'),(1360,6,79,1,1,0,1,'2019-12-01 01:44:39'),(1361,6,80,1,1,1,1,'2019-12-01 01:45:08'),(1362,6,81,1,1,1,1,'2019-12-01 01:47:50'),(1363,6,85,1,1,1,1,'2019-12-01 01:50:43'),(1364,6,86,1,0,0,0,'2019-12-01 01:51:10'),(1365,6,106,1,0,0,0,'2019-12-01 01:52:55'),(1366,6,117,1,0,0,0,'2019-12-01 01:53:08'),(1394,1,106,1,0,0,0,'2019-12-02 05:20:33'),(1395,1,113,1,0,0,0,'2019-12-02 05:20:59'),(1396,1,114,1,0,0,0,'2019-12-02 05:21:34'),(1397,1,115,1,0,0,0,'2019-12-02 05:21:34'),(1398,1,116,1,0,0,0,'2019-12-02 05:21:54'),(1399,1,117,1,0,0,0,'2019-12-02 05:22:04'),(1400,1,118,1,0,0,0,'2019-12-02 05:22:20'),(1402,1,191,1,0,0,0,'2019-12-02 05:23:34'),(1403,1,192,1,0,0,0,'2019-12-02 05:23:47'),(1404,1,193,1,0,0,0,'2019-12-02 05:23:58'),(1405,1,194,1,0,0,0,'2019-12-02 05:24:11'),(1406,1,195,1,0,0,0,'2019-12-02 05:24:20'),(1408,1,227,1,0,0,0,'2019-12-02 05:25:47'),(1410,1,226,1,0,0,0,'2019-12-02 05:31:41'),(1411,1,229,1,0,0,0,'2019-12-02 05:32:57'),(1412,1,230,1,0,0,0,'2019-12-02 05:32:57'),(1413,1,190,1,0,0,0,'2019-12-02 05:43:41'),(1414,2,174,1,0,0,0,'2019-12-02 05:54:37'),(1415,2,175,1,0,0,0,'2019-12-02 05:54:37'),(1418,2,232,1,0,1,1,'2019-12-02 06:11:27'),(1419,2,231,1,0,0,0,'2019-12-02 06:12:28'),(1420,1,231,1,1,1,1,'2021-01-23 07:09:32'),(1421,1,232,1,0,1,1,'2019-12-02 06:19:32'),(1422,3,32,1,1,1,1,'2019-12-02 06:30:37'),(1423,3,33,1,1,1,1,'2019-12-02 06:30:37'),(1424,3,34,1,1,1,1,'2019-12-02 06:30:37'),(1425,3,182,1,0,0,0,'2019-12-02 06:30:37'),(1426,3,183,1,0,0,0,'2019-12-02 06:30:37'),(1427,3,189,1,0,1,1,'2019-12-02 06:30:37'),(1428,3,229,1,0,0,0,'2019-12-02 06:30:37'),(1429,3,230,1,0,0,0,'2019-12-02 06:30:37'),(1430,4,213,1,0,0,0,'2019-12-02 06:32:14'),(1432,4,224,1,0,0,0,'2019-12-02 06:32:14'),(1433,4,195,1,0,0,0,'2019-12-03 04:57:53'),(1434,4,229,1,0,0,0,'2019-12-03 04:58:19'),(1436,6,213,1,0,0,0,'2019-12-03 05:10:11'),(1437,6,191,1,0,0,0,'2019-12-03 05:10:11'),(1438,6,193,1,0,0,0,'2019-12-03 05:10:11'),(1439,6,230,1,0,0,0,'2019-12-03 05:10:11'),(1440,2,106,1,0,0,0,'2020-01-25 04:21:36'),(1441,2,107,1,0,0,0,'2020-02-12 02:10:13'),(1442,2,134,1,1,1,1,'2020-02-12 02:12:36'),(1443,1,233,1,0,0,0,'2020-02-12 02:21:57'),(1444,2,86,1,0,0,0,'2020-02-12 02:22:33'),(1445,3,233,1,0,0,0,'2020-02-12 03:51:17'),(1446,1,234,1,1,1,1,'2020-06-01 21:51:09'),(1447,1,235,1,1,1,1,'2020-05-29 23:17:01'),(1448,1,236,1,1,1,0,'2020-05-29 23:17:52'),(1449,1,237,1,0,1,0,'2020-05-29 23:18:18'),(1450,1,238,1,1,1,1,'2020-05-29 23:19:52'),(1451,1,239,1,1,1,1,'2020-05-29 23:22:10'),(1452,2,236,1,1,1,0,'2020-05-29 23:40:33'),(1453,2,237,1,0,1,0,'2020-05-29 23:40:33'),(1454,2,238,1,1,1,1,'2020-05-29 23:40:33'),(1455,2,239,1,1,1,1,'2020-05-29 23:40:33'),(1456,2,240,1,0,0,0,'2020-05-28 20:51:18'),(1457,2,241,1,0,0,0,'2020-05-28 20:51:18'),(1458,1,240,1,0,0,0,'2020-06-07 18:30:42'),(1459,1,241,1,0,0,0,'2020-06-07 18:30:42'),(1460,1,242,1,0,0,0,'2020-06-07 18:30:42'),(1461,2,242,1,0,0,0,'2020-06-11 22:45:24'),(1462,3,242,1,0,0,0,'2020-06-14 22:46:54'),(1463,6,242,1,0,0,0,'2020-06-14 22:48:14'),(1464,1,243,1,0,0,0,'2020-09-12 06:05:45'),(1465,1,109,1,1,0,0,'2020-09-21 06:33:50'),(1466,1,108,1,1,1,1,'2020-09-21 06:50:36'),(1467,1,244,1,0,0,0,'2020-09-21 06:59:54'),(1468,1,245,1,0,0,0,'2020-09-21 06:59:54'),(1469,1,246,1,0,0,0,'2020-09-21 06:59:54'),(1470,1,247,1,0,0,0,'2021-01-07 06:12:14'),(1472,2,247,1,0,0,0,'2021-01-21 12:46:40'),(1473,1,248,1,1,1,1,'2021-05-19 12:52:49'),(1474,1,249,1,0,0,0,'2021-05-19 12:52:49'),(1475,2,248,1,1,1,1,'2021-05-28 13:11:52'),(1476,3,248,1,1,1,1,'2021-05-28 09:36:16'),(1477,3,249,1,0,0,0,'2021-05-28 09:36:16'),(1478,6,248,1,0,0,0,'2021-05-28 09:56:14'),(1479,6,249,1,0,0,0,'2021-05-28 09:56:14'),(1480,2,249,1,0,0,0,'2021-05-28 13:11:52'),(1481,3,250,1,1,1,1,'2022-09-22 08:28:50'),(1482,3,251,1,1,1,1,'2022-09-22 08:28:50'),(1483,3,252,1,0,0,0,'2022-09-22 08:28:50'),(1484,3,253,1,0,0,0,'2022-09-22 08:28:50'),(1485,3,254,1,0,0,0,'2022-09-22 08:28:50'),(1486,3,255,1,0,0,0,'2022-09-22 08:28:50'),(1487,3,256,1,0,0,0,'2022-09-22 08:28:50'),(1488,3,257,1,0,0,0,'2022-09-22 08:28:50'),(1489,3,258,1,0,0,0,'2022-09-22 08:28:50'),(1490,3,259,1,0,0,0,'2022-09-22 08:28:50'),(1491,3,260,1,0,0,0,'2022-09-22 08:28:50'),(1492,3,261,1,0,0,0,'2022-09-22 08:28:50'),(1493,3,262,1,0,0,0,'2022-09-22 08:28:50'),(1494,3,263,1,0,0,0,'2022-09-22 08:28:50'),(1495,3,264,1,0,0,0,'2022-09-22 08:28:50'),(1496,3,265,1,0,0,0,'2022-09-22 08:28:50'),(1497,1,266,1,1,1,1,'2022-12-09 04:34:35');
/*!40000 ALTER TABLE `roles_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `room_types`
--

DROP TABLE IF EXISTS `room_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `room_types` (
  `id` int NOT NULL AUTO_INCREMENT,
  `room_type` varchar(200) DEFAULT NULL,
  `description` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `room_types`
--

LOCK TABLES `room_types` WRITE;
/*!40000 ALTER TABLE `room_types` DISABLE KEYS */;
/*!40000 ALTER TABLE `room_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sch_settings`
--

DROP TABLE IF EXISTS `sch_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sch_settings` (
  `id` int NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `biometric` int DEFAULT '0',
  `biometric_device` text,
  `email` varchar(100) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `address` text,
  `lang_id` int DEFAULT NULL,
  `languages` varchar(500) NOT NULL,
  `dise_code` varchar(50) DEFAULT NULL,
  `date_format` varchar(50) NOT NULL,
  `time_format` varchar(255) NOT NULL,
  `currency` varchar(50) NOT NULL,
  `currency_symbol` varchar(50) NOT NULL,
  `is_rtl` varchar(10) DEFAULT 'disabled',
  `is_duplicate_fees_invoice` int DEFAULT '0',
  `timezone` varchar(30) DEFAULT 'UTC',
  `session_id` int DEFAULT NULL,
  `cron_secret_key` varchar(100) NOT NULL,
  `currency_place` varchar(50) NOT NULL DEFAULT 'before_number',
  `class_teacher` varchar(100) NOT NULL,
  `start_month` varchar(40) NOT NULL,
  `attendence_type` int NOT NULL DEFAULT '0',
  `image` varchar(100) DEFAULT NULL,
  `admin_logo` varchar(255) NOT NULL,
  `admin_small_logo` varchar(255) NOT NULL,
  `theme` varchar(200) NOT NULL DEFAULT 'default.jpg',
  `fee_due_days` int DEFAULT '0',
  `adm_auto_insert` int NOT NULL DEFAULT '1',
  `adm_prefix` varchar(50) NOT NULL DEFAULT 'ssadm19/20',
  `adm_start_from` varchar(11) NOT NULL,
  `adm_no_digit` int NOT NULL DEFAULT '6',
  `adm_update_status` int NOT NULL DEFAULT '0',
  `staffid_auto_insert` int NOT NULL DEFAULT '1',
  `staffid_prefix` varchar(100) NOT NULL DEFAULT 'staffss/19/20',
  `staffid_start_from` varchar(50) NOT NULL,
  `staffid_no_digit` int NOT NULL DEFAULT '6',
  `staffid_update_status` int NOT NULL DEFAULT '0',
  `is_active` varchar(255) DEFAULT 'no',
  `online_admission` int DEFAULT '0',
  `online_admission_payment` varchar(50) NOT NULL,
  `online_admission_amount` float NOT NULL,
  `online_admission_instruction` text NOT NULL,
  `online_admission_conditions` text NOT NULL,
  `is_blood_group` int NOT NULL DEFAULT '1',
  `is_student_house` int NOT NULL DEFAULT '1',
  `roll_no` int NOT NULL DEFAULT '1',
  `category` int NOT NULL,
  `religion` int NOT NULL DEFAULT '1',
  `cast` int NOT NULL DEFAULT '1',
  `mobile_no` int NOT NULL DEFAULT '1',
  `student_email` int NOT NULL DEFAULT '1',
  `admission_date` int NOT NULL DEFAULT '1',
  `lastname` int NOT NULL,
  `middlename` int NOT NULL DEFAULT '1',
  `student_photo` int NOT NULL DEFAULT '1',
  `student_height` int NOT NULL DEFAULT '1',
  `student_weight` int NOT NULL DEFAULT '1',
  `measurement_date` int NOT NULL DEFAULT '1',
  `father_name` int NOT NULL DEFAULT '1',
  `father_phone` int NOT NULL DEFAULT '1',
  `father_occupation` int NOT NULL DEFAULT '1',
  `father_pic` int NOT NULL DEFAULT '1',
  `mother_name` int NOT NULL DEFAULT '1',
  `mother_phone` int NOT NULL DEFAULT '1',
  `mother_occupation` int NOT NULL DEFAULT '1',
  `mother_pic` int NOT NULL DEFAULT '1',
  `guardian_name` int NOT NULL,
  `guardian_relation` int NOT NULL DEFAULT '1',
  `guardian_phone` int NOT NULL,
  `guardian_email` int NOT NULL DEFAULT '1',
  `guardian_pic` int NOT NULL DEFAULT '1',
  `guardian_occupation` int NOT NULL,
  `guardian_address` int NOT NULL DEFAULT '1',
  `current_address` int NOT NULL DEFAULT '1',
  `permanent_address` int NOT NULL DEFAULT '1',
  `route_list` int NOT NULL DEFAULT '1',
  `hostel_id` int NOT NULL DEFAULT '1',
  `bank_account_no` int NOT NULL DEFAULT '1',
  `ifsc_code` int NOT NULL,
  `bank_name` int NOT NULL,
  `national_identification_no` int NOT NULL DEFAULT '1',
  `local_identification_no` int NOT NULL DEFAULT '1',
  `rte` int NOT NULL DEFAULT '1',
  `previous_school_details` int NOT NULL DEFAULT '1',
  `student_note` int NOT NULL DEFAULT '1',
  `upload_documents` int NOT NULL DEFAULT '1',
  `staff_designation` int NOT NULL DEFAULT '1',
  `staff_department` int NOT NULL DEFAULT '1',
  `staff_last_name` int NOT NULL DEFAULT '1',
  `staff_father_name` int NOT NULL DEFAULT '1',
  `staff_mother_name` int NOT NULL DEFAULT '1',
  `staff_date_of_joining` int NOT NULL DEFAULT '1',
  `staff_phone` int NOT NULL DEFAULT '1',
  `staff_emergency_contact` int NOT NULL DEFAULT '1',
  `staff_marital_status` int NOT NULL DEFAULT '1',
  `staff_photo` int NOT NULL DEFAULT '1',
  `staff_current_address` int NOT NULL DEFAULT '1',
  `staff_permanent_address` int NOT NULL DEFAULT '1',
  `staff_qualification` int NOT NULL DEFAULT '1',
  `staff_work_experience` int NOT NULL DEFAULT '1',
  `staff_note` int NOT NULL DEFAULT '1',
  `staff_epf_no` int NOT NULL DEFAULT '1',
  `staff_basic_salary` int NOT NULL DEFAULT '1',
  `staff_contract_type` int NOT NULL DEFAULT '1',
  `staff_work_shift` int NOT NULL DEFAULT '1',
  `staff_work_location` int NOT NULL DEFAULT '1',
  `staff_leaves` int NOT NULL DEFAULT '1',
  `staff_account_details` int NOT NULL DEFAULT '1',
  `staff_social_media` int NOT NULL DEFAULT '1',
  `staff_upload_documents` int NOT NULL DEFAULT '1',
  `mobile_api_url` tinytext NOT NULL,
  `app_primary_color_code` varchar(20) DEFAULT NULL,
  `app_secondary_color_code` varchar(20) DEFAULT NULL,
  `app_logo` varchar(250) DEFAULT NULL,
  `student_profile_edit` int NOT NULL DEFAULT '0',
  `start_week` varchar(10) NOT NULL,
  `my_question` int NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  `calender` tinyint(1) NOT NULL,
  `ac_user` varchar(255) DEFAULT NULL,
  `ac_pwd` varchar(255) DEFAULT NULL,
  `display_admit` int DEFAULT '0',
  `display_marksheet` int DEFAULT '0',
  `fee_guardian` int DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `lang_id` (`lang_id`),
  KEY `session_id` (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sch_settings`
--

LOCK TABLES `sch_settings` WRITE;
/*!40000 ALTER TABLE `sch_settings` DISABLE KEYS */;
INSERT INTO `sch_settings` VALUES (1,'Eshikshya',0,'zkteco','info@eshikshya.com.np','01-4004615','Gairidhara Rd 1, Kathmandu 44600',4,'[\"4\",\"53\"]','eShikshya','Y/m/d','12-hour','NPR','NPR','disabled',1,'Asia/Kathmandu',30,'','after_number','no','1',0,'1.jpg','16577019371.png','16586376151.png','red.jpg',60,1,'Es','10',2,1,1,'staff','10',2,1,'no',1,'no',0,'','<p>&nbsp;Please enter your institution online admission terms &amp; conditions here.</p>\r\n',1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,'','#424242','#eeeeee','1.jpg',0,'Sunday',0,'2022-12-22 09:52:23',NULL,0,'user3','user123',0,1,NULL);
/*!40000 ALTER TABLE `sch_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `school_houses`
--

DROP TABLE IF EXISTS `school_houses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `school_houses` (
  `id` int NOT NULL AUTO_INCREMENT,
  `house_name` varchar(200) NOT NULL,
  `description` varchar(400) NOT NULL,
  `is_active` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `school_houses`
--

LOCK TABLES `school_houses` WRITE;
/*!40000 ALTER TABLE `school_houses` DISABLE KEYS */;
/*!40000 ALTER TABLE `school_houses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sections`
--

DROP TABLE IF EXISTS `sections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sections` (
  `id` int NOT NULL AUTO_INCREMENT,
  `section` varchar(60) DEFAULT NULL,
  `is_active` varchar(255) DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sections`
--

LOCK TABLES `sections` WRITE;
/*!40000 ALTER TABLE `sections` DISABLE KEYS */;
/*!40000 ALTER TABLE `sections` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `send_notification`
--

DROP TABLE IF EXISTS `send_notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `send_notification` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL,
  `publish_date` date DEFAULT NULL,
  `date` date DEFAULT NULL,
  `message` text,
  `visible_student` varchar(10) NOT NULL DEFAULT 'no',
  `visible_staff` varchar(10) NOT NULL DEFAULT 'no',
  `visible_parent` varchar(10) NOT NULL DEFAULT 'no',
  `created_by` varchar(60) DEFAULT NULL,
  `created_id` int DEFAULT NULL,
  `is_active` varchar(255) DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `send_notification`
--

LOCK TABLES `send_notification` WRITE;
/*!40000 ALTER TABLE `send_notification` DISABLE KEYS */;
INSERT INTO `send_notification` VALUES (1,'Notice of Public Holiday for Election','2079-07-29','2079-07-29','<p>We want to notify that we are providing holiday from Mangsir 2 to 5 as per government has alocated public holiday for election.</p><p>Thank You</p>','Yes','Yes','Yes','Super Admin',1,'no','2022-11-15 09:35:09',NULL),(2,'Notice of Public Holiday on the occasion of Christ','2079-09-10','2079-09-06','<p>We would like to notify that there will be public holiday on the occasion of Christmas day.</p><p>Thank you.</p>','Yes','Yes','Yes','Super Admin',1,'no','2022-12-21 05:06:30',NULL),(3,'Tamu Loshar','2079-09-15','2079-09-10','<p>Date: 2079/09/07</p><p><u>Subject: Holidays Notice</u></p><p>This is to notify all the Staff member, Student, that school will remain close on 2079/09/15 on the occasion of Tamu Loshar. </p><p>Thank you. <u><br></u></p><p><u><br></u></p>','No','Yes','No','Super Admin',1,'no','2022-12-22 06:01:20',NULL),(4,'Winter Holiday','2079-09-16','2079-09-07','<p>We would like to inform all the student and staff member that school will remain close from Jan 1st, 2022, to 22nd Jan 2022 for winter vacation holiday.</p><p>Thank You.</p>','Yes','Yes','Yes','Super Admin',1,'no','2022-12-22 09:08:19',NULL);
/*!40000 ALTER TABLE `send_notification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sessions` (
  `id` int NOT NULL AUTO_INCREMENT,
  `session` varchar(60) DEFAULT NULL,
  `is_active` varchar(255) DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sessions`
--

LOCK TABLES `sessions` WRITE;
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
INSERT INTO `sessions` VALUES (26,'2075-76','no','2022-07-13 09:09:08',NULL),(27,'2076-77','no','2022-07-13 09:09:21',NULL),(28,'2077-78','no','2022-07-13 09:10:24',NULL),(29,'2078-79','no','2022-07-13 09:10:37',NULL),(30,'2079-80','no','2022-07-13 09:07:48',NULL),(31,'2080-81','no','2022-07-13 09:10:52',NULL);
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sms_config`
--

DROP TABLE IF EXISTS `sms_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sms_config` (
  `id` int NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `api_id` varchar(100) NOT NULL,
  `authkey` varchar(100) NOT NULL,
  `senderid` varchar(100) NOT NULL,
  `contact` text,
  `username` varchar(150) DEFAULT NULL,
  `url` varchar(150) DEFAULT NULL,
  `password` varchar(150) DEFAULT NULL,
  `is_active` varchar(255) DEFAULT 'disabled',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sms_config`
--

LOCK TABLES `sms_config` WRITE;
/*!40000 ALTER TABLE `sms_config` DISABLE KEYS */;
/*!40000 ALTER TABLE `sms_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `source`
--

DROP TABLE IF EXISTS `source`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `source` (
  `id` int NOT NULL AUTO_INCREMENT,
  `source` varchar(100) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `source`
--

LOCK TABLES `source` WRITE;
/*!40000 ALTER TABLE `source` DISABLE KEYS */;
/*!40000 ALTER TABLE `source` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `staff`
--

DROP TABLE IF EXISTS `staff`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `staff` (
  `id` int NOT NULL AUTO_INCREMENT,
  `employee_id` varchar(200) NOT NULL,
  `lang_id` int NOT NULL,
  `department` int DEFAULT '0',
  `designation` int DEFAULT '0',
  `qualification` varchar(200) NOT NULL,
  `work_exp` varchar(200) NOT NULL,
  `name` varchar(200) NOT NULL,
  `surname` varchar(200) NOT NULL,
  `father_name` varchar(200) NOT NULL,
  `mother_name` varchar(200) NOT NULL,
  `contact_no` varchar(200) NOT NULL,
  `emergency_contact_no` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `dob` date NOT NULL,
  `marital_status` varchar(100) NOT NULL,
  `date_of_joining` date NOT NULL,
  `date_of_leaving` date NOT NULL,
  `local_address` varchar(300) NOT NULL,
  `permanent_address` varchar(200) NOT NULL,
  `note` varchar(200) NOT NULL,
  `image` varchar(200) NOT NULL,
  `password` varchar(250) NOT NULL,
  `gender` varchar(50) NOT NULL,
  `account_title` varchar(200) NOT NULL,
  `bank_account_no` varchar(200) NOT NULL,
  `bank_name` varchar(200) NOT NULL,
  `ifsc_code` varchar(200) NOT NULL,
  `bank_branch` varchar(100) NOT NULL,
  `payscale` varchar(200) NOT NULL,
  `basic_salary` varchar(200) NOT NULL,
  `epf_no` varchar(200) NOT NULL,
  `contract_type` varchar(100) NOT NULL,
  `shift` varchar(100) NOT NULL,
  `location` varchar(100) NOT NULL,
  `facebook` varchar(200) NOT NULL,
  `twitter` varchar(200) NOT NULL,
  `linkedin` varchar(200) NOT NULL,
  `instagram` varchar(200) NOT NULL,
  `resume` varchar(200) NOT NULL,
  `joining_letter` varchar(200) NOT NULL,
  `resignation_letter` varchar(200) NOT NULL,
  `other_document_name` varchar(200) NOT NULL,
  `other_document_file` varchar(200) NOT NULL,
  `user_id` int NOT NULL,
  `is_active` int NOT NULL,
  `verification_code` varchar(100) NOT NULL,
  `disable_at` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `employee_id` (`employee_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `staff`
--

LOCK TABLES `staff` WRITE;
/*!40000 ALTER TABLE `staff` DISABLE KEYS */;
INSERT INTO `staff` VALUES (1,'5555',0,0,1,'BBA','1','Creation Soft','surname','father_name','mother_name','contact_no','emergency_contact_no','creation.soft123@gmail.com','2054-12-18','marital_status','2054-12-18','2054-12-18','local_address','permanent_address','note','','$2y$10$hB/mzM1s6L9qGskM.Z5tKeDh9b2Jb7wng9xGSvNWcxxpH1mh.bSMy','male','ac_title','bank_no','bank_name','ifsc','bank_branch','payscale','basic_salary','epf_no','','','','','','','','','','','','',0,1,'','2054-12-18');
/*!40000 ALTER TABLE `staff` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `staff_attendance`
--

DROP TABLE IF EXISTS `staff_attendance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `staff_attendance` (
  `id` int NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `staff_id` int NOT NULL,
  `staff_attendance_type_id` int NOT NULL,
  `remark` varchar(200) NOT NULL,
  `is_active` int NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_staff_attendance_staff` (`staff_id`),
  KEY `FK_staff_attendance_staff_attendance_type` (`staff_attendance_type_id`),
  CONSTRAINT `FK_staff_attendance_staff` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_staff_attendance_staff_attendance_type` FOREIGN KEY (`staff_attendance_type_id`) REFERENCES `staff_attendance_type` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `staff_attendance`
--

LOCK TABLES `staff_attendance` WRITE;
/*!40000 ALTER TABLE `staff_attendance` DISABLE KEYS */;
/*!40000 ALTER TABLE `staff_attendance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `staff_attendance_type`
--

DROP TABLE IF EXISTS `staff_attendance_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `staff_attendance_type` (
  `id` int NOT NULL AUTO_INCREMENT,
  `type` varchar(200) NOT NULL,
  `key_value` varchar(200) NOT NULL,
  `is_active` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `staff_attendance_type`
--

LOCK TABLES `staff_attendance_type` WRITE;
/*!40000 ALTER TABLE `staff_attendance_type` DISABLE KEYS */;
INSERT INTO `staff_attendance_type` VALUES (1,'Present','<b class=\"text text-success\">P</b>','yes','0000-00-00 00:00:00','0000-00-00'),(2,'Late','<b class=\"text text-warning\">L</b>','yes','0000-00-00 00:00:00','0000-00-00'),(3,'Absent','<b class=\"text text-danger\">A</b>','yes','0000-00-00 00:00:00','0000-00-00'),(4,'Half Day','<b class=\"text text-warning\">F</b>','yes','2018-05-07 01:56:16','0000-00-00'),(5,'Holiday','H','yes','0000-00-00 00:00:00','0000-00-00');
/*!40000 ALTER TABLE `staff_attendance_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `staff_designation`
--

DROP TABLE IF EXISTS `staff_designation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `staff_designation` (
  `id` int NOT NULL AUTO_INCREMENT,
  `designation` varchar(200) NOT NULL,
  `is_active` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `staff_designation`
--

LOCK TABLES `staff_designation` WRITE;
/*!40000 ALTER TABLE `staff_designation` DISABLE KEYS */;
INSERT INTO `staff_designation` VALUES (1,'Administrator','yes'),(2,'Teacher','yes'),(8,'Staff','yes'),(9,'Accountant','yes'),(10,'Driver','yes'),(11,'KITCHEN HELPER','yes');
/*!40000 ALTER TABLE `staff_designation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `staff_id_card`
--

DROP TABLE IF EXISTS `staff_id_card`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `staff_id_card` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `school_name` varchar(255) NOT NULL,
  `school_address` varchar(255) NOT NULL,
  `background` varchar(100) NOT NULL,
  `logo` varchar(100) NOT NULL,
  `sign_image` varchar(100) NOT NULL,
  `header_color` varchar(100) NOT NULL,
  `enable_vertical_card` int NOT NULL DEFAULT '0',
  `enable_staff_role` tinyint(1) NOT NULL COMMENT '0=disable,1=enable',
  `enable_staff_id` tinyint(1) NOT NULL COMMENT '0=disable,1=enable',
  `enable_staff_department` tinyint(1) NOT NULL COMMENT '0=disable,1=enable',
  `enable_designation` tinyint(1) NOT NULL COMMENT '0=disable,1=enable',
  `enable_name` tinyint(1) NOT NULL COMMENT '0=disable,1=enable',
  `enable_fathers_name` tinyint(1) NOT NULL COMMENT '0=disable,1=enable',
  `enable_mothers_name` tinyint(1) NOT NULL COMMENT '0=disable,1=enable',
  `enable_date_of_joining` tinyint(1) NOT NULL COMMENT '0=disable,1=enable',
  `enable_permanent_address` tinyint(1) NOT NULL COMMENT '0=disable,1=enable',
  `enable_staff_dob` tinyint(1) NOT NULL COMMENT '0=disable,1=enable',
  `enable_staff_phone` tinyint(1) NOT NULL COMMENT '0=disable,1=enable',
  `status` tinyint(1) NOT NULL COMMENT '0=disable,1=enable',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `staff_id_card`
--

LOCK TABLES `staff_id_card` WRITE;
/*!40000 ALTER TABLE `staff_id_card` DISABLE KEYS */;
/*!40000 ALTER TABLE `staff_id_card` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `staff_leave_details`
--

DROP TABLE IF EXISTS `staff_leave_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `staff_leave_details` (
  `id` int NOT NULL AUTO_INCREMENT,
  `staff_id` int NOT NULL,
  `leave_type_id` int NOT NULL,
  `alloted_leave` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_staff_leave_details_staff` (`staff_id`),
  KEY `FK_staff_leave_details_leave_types` (`leave_type_id`),
  CONSTRAINT `FK_staff_leave_details_leave_types` FOREIGN KEY (`leave_type_id`) REFERENCES `leave_types` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_staff_leave_details_staff` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `staff_leave_details`
--

LOCK TABLES `staff_leave_details` WRITE;
/*!40000 ALTER TABLE `staff_leave_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `staff_leave_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `staff_leave_request`
--

DROP TABLE IF EXISTS `staff_leave_request`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `staff_leave_request` (
  `id` int NOT NULL AUTO_INCREMENT,
  `staff_id` int NOT NULL,
  `leave_type_id` int NOT NULL,
  `leave_from` date NOT NULL,
  `leave_to` date NOT NULL,
  `leave_days` int NOT NULL,
  `employee_remark` varchar(200) NOT NULL,
  `admin_remark` varchar(200) NOT NULL,
  `status` varchar(100) NOT NULL,
  `applied_by` varchar(200) NOT NULL,
  `document_file` varchar(200) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_staff_leave_request_staff` (`staff_id`),
  KEY `FK_staff_leave_request_leave_types` (`leave_type_id`),
  CONSTRAINT `FK_staff_leave_request_leave_types` FOREIGN KEY (`leave_type_id`) REFERENCES `leave_types` (`id`),
  CONSTRAINT `FK_staff_leave_request_staff` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `staff_leave_request`
--

LOCK TABLES `staff_leave_request` WRITE;
/*!40000 ALTER TABLE `staff_leave_request` DISABLE KEYS */;
/*!40000 ALTER TABLE `staff_leave_request` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `staff_payroll`
--

DROP TABLE IF EXISTS `staff_payroll`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `staff_payroll` (
  `id` int NOT NULL AUTO_INCREMENT,
  `basic_salary` int NOT NULL,
  `pay_scale` varchar(200) NOT NULL,
  `grade` varchar(50) NOT NULL,
  `is_active` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `staff_payroll`
--

LOCK TABLES `staff_payroll` WRITE;
/*!40000 ALTER TABLE `staff_payroll` DISABLE KEYS */;
/*!40000 ALTER TABLE `staff_payroll` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `staff_payslip`
--

DROP TABLE IF EXISTS `staff_payslip`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `staff_payslip` (
  `id` int NOT NULL AUTO_INCREMENT,
  `staff_id` int NOT NULL,
  `basic` float(10,2) NOT NULL,
  `total_allowance` float(10,2) NOT NULL,
  `total_deduction` float(10,2) NOT NULL,
  `leave_deduction` int NOT NULL,
  `tax` varchar(200) NOT NULL,
  `net_salary` float(10,2) NOT NULL,
  `status` varchar(100) NOT NULL,
  `month` varchar(200) NOT NULL,
  `year` varchar(200) NOT NULL,
  `payment_mode` varchar(200) NOT NULL,
  `payment_date` date NOT NULL,
  `remark` varchar(200) NOT NULL,
  `transid` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_staff_payslip_staff` (`staff_id`),
  CONSTRAINT `FK_staff_payslip_staff` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `staff_payslip`
--

LOCK TABLES `staff_payslip` WRITE;
/*!40000 ALTER TABLE `staff_payslip` DISABLE KEYS */;
/*!40000 ALTER TABLE `staff_payslip` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `staff_rating`
--

DROP TABLE IF EXISTS `staff_rating`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `staff_rating` (
  `id` int NOT NULL AUTO_INCREMENT,
  `staff_id` int NOT NULL,
  `comment` text NOT NULL,
  `rate` int NOT NULL,
  `user_id` int NOT NULL,
  `role` varchar(255) NOT NULL,
  `status` int NOT NULL COMMENT '0 decline, 1 Approve',
  `entrydt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_staff_rating_staff` (`staff_id`),
  CONSTRAINT `FK_staff_rating_staff` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `staff_rating`
--

LOCK TABLES `staff_rating` WRITE;
/*!40000 ALTER TABLE `staff_rating` DISABLE KEYS */;
/*!40000 ALTER TABLE `staff_rating` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `staff_roles`
--

DROP TABLE IF EXISTS `staff_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `staff_roles` (
  `id` int NOT NULL AUTO_INCREMENT,
  `role_id` int DEFAULT NULL,
  `staff_id` int DEFAULT NULL,
  `is_active` int DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `role_id` (`role_id`),
  KEY `staff_id` (`staff_id`),
  CONSTRAINT `FK_staff_roles_roles` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_staff_roles_staff` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `staff_roles`
--

LOCK TABLES `staff_roles` WRITE;
/*!40000 ALTER TABLE `staff_roles` DISABLE KEYS */;
INSERT INTO `staff_roles` VALUES (1,7,1,0,'2022-02-18 07:26:00','2022-02-18');
/*!40000 ALTER TABLE `staff_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `staff_timeline`
--

DROP TABLE IF EXISTS `staff_timeline`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `staff_timeline` (
  `id` int NOT NULL AUTO_INCREMENT,
  `staff_id` int NOT NULL,
  `title` varchar(200) NOT NULL,
  `timeline_date` date NOT NULL,
  `description` varchar(300) NOT NULL,
  `document` varchar(200) NOT NULL,
  `status` varchar(200) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_staff_timeline_staff` (`staff_id`),
  CONSTRAINT `FK_staff_timeline_staff` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `staff_timeline`
--

LOCK TABLES `staff_timeline` WRITE;
/*!40000 ALTER TABLE `staff_timeline` DISABLE KEYS */;
/*!40000 ALTER TABLE `staff_timeline` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student_applyleave`
--

DROP TABLE IF EXISTS `student_applyleave`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `student_applyleave` (
  `id` int NOT NULL AUTO_INCREMENT,
  `student_session_id` int NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `apply_date` date NOT NULL,
  `status` int NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `docs` text NOT NULL,
  `reason` text NOT NULL,
  `approve_by` int NOT NULL,
  `request_type` int NOT NULL COMMENT '0 student,1 staff',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student_applyleave`
--

LOCK TABLES `student_applyleave` WRITE;
/*!40000 ALTER TABLE `student_applyleave` DISABLE KEYS */;
/*!40000 ALTER TABLE `student_applyleave` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student_attendences`
--

DROP TABLE IF EXISTS `student_attendences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `student_attendences` (
  `id` int NOT NULL AUTO_INCREMENT,
  `student_session_id` int DEFAULT NULL,
  `biometric_attendence` int NOT NULL DEFAULT '0',
  `date` date DEFAULT NULL,
  `attendence_type_id` int DEFAULT NULL,
  `remark` varchar(200) NOT NULL,
  `biometric_device_data` text,
  `is_active` varchar(255) DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `student_session_id` (`student_session_id`),
  KEY `attendence_type_id` (`attendence_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=219 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student_attendences`
--

LOCK TABLES `student_attendences` WRITE;
/*!40000 ALTER TABLE `student_attendences` DISABLE KEYS */;
/*!40000 ALTER TABLE `student_attendences` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student_discount_applied`
--

DROP TABLE IF EXISTS `student_discount_applied`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `student_discount_applied` (
  `id` int NOT NULL AUTO_INCREMENT,
  `student_fees_master_id` int DEFAULT NULL,
  `fee_groups_feetype_id` int DEFAULT NULL,
  `amount_detail` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student_discount_applied`
--

LOCK TABLES `student_discount_applied` WRITE;
/*!40000 ALTER TABLE `student_discount_applied` DISABLE KEYS */;
/*!40000 ALTER TABLE `student_discount_applied` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student_doc`
--

DROP TABLE IF EXISTS `student_doc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `student_doc` (
  `id` int NOT NULL AUTO_INCREMENT,
  `student_id` int DEFAULT NULL,
  `title` varchar(200) DEFAULT NULL,
  `doc` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student_doc`
--

LOCK TABLES `student_doc` WRITE;
/*!40000 ALTER TABLE `student_doc` DISABLE KEYS */;
INSERT INTO `student_doc` VALUES (1,22,'','4a5c2f2a828314d79432bb91afeb3ef3.jpg'),(2,22,'','8875025219_fa4ab2ceb4_z.jpg'),(3,22,'','6b7ed698713c09ad9e6afc7dcb996a09.jpg'),(4,103,NULL,'5219cbd10ba91236de538665a59e767f.docx');
/*!40000 ALTER TABLE `student_doc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student_edit_fields`
--

DROP TABLE IF EXISTS `student_edit_fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `student_edit_fields` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  `status` int DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student_edit_fields`
--

LOCK TABLES `student_edit_fields` WRITE;
/*!40000 ALTER TABLE `student_edit_fields` DISABLE KEYS */;
/*!40000 ALTER TABLE `student_edit_fields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student_fees`
--

DROP TABLE IF EXISTS `student_fees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `student_fees` (
  `id` int NOT NULL AUTO_INCREMENT,
  `student_session_id` int DEFAULT NULL,
  `feemaster_id` int DEFAULT NULL,
  `amount` float(10,2) DEFAULT NULL,
  `amount_discount` float(10,2) NOT NULL,
  `amount_fine` float(10,2) NOT NULL DEFAULT '0.00',
  `description` text,
  `date` date DEFAULT NULL,
  `payment_mode` varchar(50) NOT NULL,
  `is_active` varchar(255) DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student_fees`
--

LOCK TABLES `student_fees` WRITE;
/*!40000 ALTER TABLE `student_fees` DISABLE KEYS */;
/*!40000 ALTER TABLE `student_fees` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student_fees_deposite`
--

DROP TABLE IF EXISTS `student_fees_deposite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `student_fees_deposite` (
  `id` int NOT NULL AUTO_INCREMENT,
  `student_fees_master_id` int DEFAULT NULL,
  `fee_groups_feetype_id` int DEFAULT NULL,
  `amount_detail` text,
  `is_active` varchar(10) NOT NULL DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `transid` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `student_fees_master_id` (`student_fees_master_id`),
  KEY `fee_groups_feetype_id` (`fee_groups_feetype_id`)
) ENGINE=InnoDB AUTO_INCREMENT=98 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student_fees_deposite`
--

LOCK TABLES `student_fees_deposite` WRITE;
/*!40000 ALTER TABLE `student_fees_deposite` DISABLE KEYS */;
/*!40000 ALTER TABLE `student_fees_deposite` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student_fees_discounts`
--

DROP TABLE IF EXISTS `student_fees_discounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `student_fees_discounts` (
  `id` int NOT NULL AUTO_INCREMENT,
  `student_session_id` int DEFAULT NULL,
  `fees_discount_id` int DEFAULT NULL,
  `status` varchar(20) DEFAULT 'assigned',
  `payment_id` varchar(50) DEFAULT NULL,
  `description` text,
  `is_active` varchar(10) NOT NULL DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `student_session_id` (`student_session_id`),
  KEY `fees_discount_id` (`fees_discount_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student_fees_discounts`
--

LOCK TABLES `student_fees_discounts` WRITE;
/*!40000 ALTER TABLE `student_fees_discounts` DISABLE KEYS */;
/*!40000 ALTER TABLE `student_fees_discounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student_fees_master`
--

DROP TABLE IF EXISTS `student_fees_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `student_fees_master` (
  `id` int NOT NULL AUTO_INCREMENT,
  `is_system` int NOT NULL DEFAULT '0',
  `student_session_id` int DEFAULT NULL,
  `fee_session_group_id` int DEFAULT NULL,
  `amount` float(10,2) DEFAULT '0.00',
  `is_active` varchar(10) NOT NULL DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `month` int DEFAULT NULL,
  `months` int DEFAULT NULL,
  `transid` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `student_session_id` (`student_session_id`),
  KEY `fee_session_group_id` (`fee_session_group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2012 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student_fees_master`
--

LOCK TABLES `student_fees_master` WRITE;
/*!40000 ALTER TABLE `student_fees_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `student_fees_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student_present`
--

DROP TABLE IF EXISTS `student_present`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `student_present` (
  `id` int NOT NULL AUTO_INCREMENT,
  `student_id` int NOT NULL,
  `present_days` int DEFAULT NULL,
  `exam_group_class_batch_exam_id` int NOT NULL,
  `student_session_id` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student_present`
--

LOCK TABLES `student_present` WRITE;
/*!40000 ALTER TABLE `student_present` DISABLE KEYS */;
/*!40000 ALTER TABLE `student_present` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student_session`
--

DROP TABLE IF EXISTS `student_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `student_session` (
  `id` int NOT NULL AUTO_INCREMENT,
  `session_id` int DEFAULT NULL,
  `student_id` int DEFAULT NULL,
  `class_id` int DEFAULT NULL,
  `section_id` int DEFAULT NULL,
  `route_id` int NOT NULL,
  `hostel_room_id` int NOT NULL,
  `vehroute_id` int DEFAULT NULL,
  `transport_fees` float(10,2) NOT NULL DEFAULT '0.00',
  `fees_discount` float(10,2) NOT NULL DEFAULT '0.00',
  `is_active` varchar(255) DEFAULT 'no',
  `is_alumni` int NOT NULL,
  `default_login` int NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `session_id` (`session_id`),
  KEY `student_id` (`student_id`),
  KEY `class_id` (`class_id`),
  KEY `section_id` (`section_id`),
  CONSTRAINT `student_session_ibfk_1` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `student_session_ibfk_2` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`) ON DELETE CASCADE,
  CONSTRAINT `student_session_ibfk_3` FOREIGN KEY (`class_id`) REFERENCES `classes` (`id`) ON DELETE CASCADE,
  CONSTRAINT `student_session_ibfk_4` FOREIGN KEY (`section_id`) REFERENCES `sections` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=203 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student_session`
--

LOCK TABLES `student_session` WRITE;
/*!40000 ALTER TABLE `student_session` DISABLE KEYS */;
/*!40000 ALTER TABLE `student_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student_sibling`
--

DROP TABLE IF EXISTS `student_sibling`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `student_sibling` (
  `id` int NOT NULL AUTO_INCREMENT,
  `student_id` int DEFAULT NULL,
  `sibling_student_id` int DEFAULT NULL,
  `is_active` varchar(255) DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student_sibling`
--

LOCK TABLES `student_sibling` WRITE;
/*!40000 ALTER TABLE `student_sibling` DISABLE KEYS */;
/*!40000 ALTER TABLE `student_sibling` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student_subject_attendances`
--

DROP TABLE IF EXISTS `student_subject_attendances`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `student_subject_attendances` (
  `id` int NOT NULL AUTO_INCREMENT,
  `student_session_id` int DEFAULT NULL,
  `subject_timetable_id` int DEFAULT NULL,
  `attendence_type_id` int DEFAULT NULL,
  `date` date DEFAULT NULL,
  `remark` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `attendence_type_id` (`attendence_type_id`),
  KEY `student_session_id` (`student_session_id`),
  KEY `subject_timetable_id` (`subject_timetable_id`),
  CONSTRAINT `student_subject_attendances_ibfk_1` FOREIGN KEY (`attendence_type_id`) REFERENCES `attendence_type` (`id`) ON DELETE CASCADE,
  CONSTRAINT `student_subject_attendances_ibfk_2` FOREIGN KEY (`student_session_id`) REFERENCES `student_session` (`id`) ON DELETE CASCADE,
  CONSTRAINT `student_subject_attendances_ibfk_3` FOREIGN KEY (`subject_timetable_id`) REFERENCES `subject_timetable` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student_subject_attendances`
--

LOCK TABLES `student_subject_attendances` WRITE;
/*!40000 ALTER TABLE `student_subject_attendances` DISABLE KEYS */;
/*!40000 ALTER TABLE `student_subject_attendances` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student_timeline`
--

DROP TABLE IF EXISTS `student_timeline`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `student_timeline` (
  `id` int NOT NULL AUTO_INCREMENT,
  `student_id` int NOT NULL,
  `title` varchar(200) NOT NULL,
  `timeline_date` date NOT NULL,
  `description` varchar(200) NOT NULL,
  `document` varchar(200) NOT NULL,
  `status` varchar(200) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student_timeline`
--

LOCK TABLES `student_timeline` WRITE;
/*!40000 ALTER TABLE `student_timeline` DISABLE KEYS */;
/*!40000 ALTER TABLE `student_timeline` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `students`
--

DROP TABLE IF EXISTS `students`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `students` (
  `id` int NOT NULL AUTO_INCREMENT,
  `parent_id` int NOT NULL,
  `admission_no` varchar(100) DEFAULT NULL,
  `roll_no` varchar(100) DEFAULT NULL,
  `admission_date` date DEFAULT NULL,
  `firstname` varchar(100) DEFAULT NULL,
  `middlename` varchar(255) DEFAULT NULL,
  `lastname` varchar(100) DEFAULT NULL,
  `rte` varchar(20) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL,
  `mobileno` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `pincode` varchar(100) DEFAULT NULL,
  `religion` varchar(100) DEFAULT NULL,
  `cast` varchar(50) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `gender` varchar(100) DEFAULT NULL,
  `current_address` text,
  `permanent_address` text,
  `category_id` varchar(100) DEFAULT NULL,
  `route_id` int NOT NULL,
  `school_house_id` int NOT NULL,
  `blood_group` varchar(200) NOT NULL,
  `vehroute_id` int NOT NULL,
  `hostel_room_id` int NOT NULL,
  `adhar_no` varchar(100) DEFAULT NULL,
  `samagra_id` varchar(100) DEFAULT NULL,
  `bank_account_no` varchar(100) DEFAULT NULL,
  `bank_name` varchar(100) DEFAULT NULL,
  `ifsc_code` varchar(100) DEFAULT NULL,
  `guardian_is` varchar(100) NOT NULL,
  `father_name` varchar(100) DEFAULT NULL,
  `father_phone` varchar(100) DEFAULT NULL,
  `father_occupation` varchar(100) DEFAULT NULL,
  `mother_name` varchar(100) DEFAULT NULL,
  `mother_phone` varchar(100) DEFAULT NULL,
  `mother_occupation` varchar(100) DEFAULT NULL,
  `guardian_name` varchar(100) DEFAULT NULL,
  `guardian_relation` varchar(100) DEFAULT NULL,
  `guardian_phone` varchar(100) DEFAULT NULL,
  `guardian_occupation` varchar(150) NOT NULL,
  `guardian_address` text,
  `guardian_email` varchar(100) DEFAULT NULL,
  `father_pic` varchar(200) NOT NULL,
  `mother_pic` varchar(200) NOT NULL,
  `guardian_pic` varchar(200) NOT NULL,
  `is_active` varchar(255) DEFAULT 'yes',
  `previous_school` text,
  `height` varchar(100) NOT NULL,
  `weight` varchar(100) NOT NULL,
  `measurement_date` date DEFAULT NULL,
  `dis_reason` int NOT NULL,
  `note` varchar(200) DEFAULT NULL,
  `dis_note` text NOT NULL,
  `app_key` text,
  `parent_app_key` text,
  `disable_at` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=195 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `students`
--

LOCK TABLES `students` WRITE;
/*!40000 ALTER TABLE `students` DISABLE KEYS */;
/*!40000 ALTER TABLE `students` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subject_group_class_sections`
--

DROP TABLE IF EXISTS `subject_group_class_sections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `subject_group_class_sections` (
  `id` int NOT NULL AUTO_INCREMENT,
  `subject_group_id` int DEFAULT NULL,
  `class_section_id` int DEFAULT NULL,
  `session_id` int DEFAULT NULL,
  `description` text,
  `is_active` int DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `class_section_id` (`class_section_id`),
  KEY `subject_group_id` (`subject_group_id`),
  KEY `session_id` (`session_id`),
  CONSTRAINT `subject_group_class_sections_ibfk_1` FOREIGN KEY (`class_section_id`) REFERENCES `class_sections` (`id`) ON DELETE CASCADE,
  CONSTRAINT `subject_group_class_sections_ibfk_2` FOREIGN KEY (`subject_group_id`) REFERENCES `subject_groups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `subject_group_class_sections_ibfk_3` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subject_group_class_sections`
--

LOCK TABLES `subject_group_class_sections` WRITE;
/*!40000 ALTER TABLE `subject_group_class_sections` DISABLE KEYS */;
/*!40000 ALTER TABLE `subject_group_class_sections` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subject_group_subjects`
--

DROP TABLE IF EXISTS `subject_group_subjects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `subject_group_subjects` (
  `id` int NOT NULL AUTO_INCREMENT,
  `subject_group_id` int DEFAULT NULL,
  `session_id` int DEFAULT NULL,
  `subject_id` int DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `subject_group_id` (`subject_group_id`),
  KEY `session_id` (`session_id`),
  KEY `subject_id` (`subject_id`),
  CONSTRAINT `subject_group_subjects_ibfk_1` FOREIGN KEY (`subject_group_id`) REFERENCES `subject_groups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `subject_group_subjects_ibfk_2` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `subject_group_subjects_ibfk_3` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subject_group_subjects`
--

LOCK TABLES `subject_group_subjects` WRITE;
/*!40000 ALTER TABLE `subject_group_subjects` DISABLE KEYS */;
/*!40000 ALTER TABLE `subject_group_subjects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subject_groups`
--

DROP TABLE IF EXISTS `subject_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `subject_groups` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  `description` text,
  `session_id` int DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `session_id` (`session_id`),
  CONSTRAINT `subject_groups_ibfk_1` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subject_groups`
--

LOCK TABLES `subject_groups` WRITE;
/*!40000 ALTER TABLE `subject_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `subject_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subject_syllabus`
--

DROP TABLE IF EXISTS `subject_syllabus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `subject_syllabus` (
  `id` int NOT NULL AUTO_INCREMENT,
  `topic_id` int NOT NULL,
  `session_id` int NOT NULL,
  `created_by` int NOT NULL,
  `created_for` int NOT NULL,
  `date` date NOT NULL,
  `time_from` varchar(255) NOT NULL,
  `time_to` varchar(255) NOT NULL,
  `presentation` text NOT NULL,
  `attachment` text NOT NULL,
  `lacture_youtube_url` varchar(255) NOT NULL,
  `lacture_video` varchar(255) NOT NULL,
  `sub_topic` text NOT NULL,
  `teaching_method` text NOT NULL,
  `general_objectives` text NOT NULL,
  `previous_knowledge` text NOT NULL,
  `comprehensive_questions` text NOT NULL,
  `status` int NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `topic_id` (`topic_id`),
  KEY `session_id` (`session_id`),
  KEY `created_by` (`created_by`),
  KEY `created_for` (`created_for`),
  CONSTRAINT `subject_syllabus_ibfk_1` FOREIGN KEY (`topic_id`) REFERENCES `topic` (`id`) ON DELETE CASCADE,
  CONSTRAINT `subject_syllabus_ibfk_2` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `subject_syllabus_ibfk_3` FOREIGN KEY (`created_by`) REFERENCES `staff` (`id`) ON DELETE CASCADE,
  CONSTRAINT `subject_syllabus_ibfk_4` FOREIGN KEY (`created_for`) REFERENCES `staff` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subject_syllabus`
--

LOCK TABLES `subject_syllabus` WRITE;
/*!40000 ALTER TABLE `subject_syllabus` DISABLE KEYS */;
/*!40000 ALTER TABLE `subject_syllabus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subject_timetable`
--

DROP TABLE IF EXISTS `subject_timetable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `subject_timetable` (
  `id` int NOT NULL AUTO_INCREMENT,
  `day` varchar(20) DEFAULT NULL,
  `class_id` int DEFAULT NULL,
  `section_id` int DEFAULT NULL,
  `subject_group_id` int DEFAULT NULL,
  `subject_group_subject_id` int DEFAULT NULL,
  `staff_id` int DEFAULT NULL,
  `time_from` varchar(20) DEFAULT NULL,
  `time_to` varchar(20) DEFAULT NULL,
  `start_time` time DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  `room_no` varchar(20) DEFAULT NULL,
  `session_id` int DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `class_id` (`class_id`),
  KEY `section_id` (`section_id`),
  KEY `subject_group_id` (`subject_group_id`),
  KEY `subject_group_subject_id` (`subject_group_subject_id`),
  KEY `staff_id` (`staff_id`),
  KEY `session_id` (`session_id`),
  CONSTRAINT `subject_timetable_ibfk_1` FOREIGN KEY (`class_id`) REFERENCES `classes` (`id`) ON DELETE CASCADE,
  CONSTRAINT `subject_timetable_ibfk_2` FOREIGN KEY (`section_id`) REFERENCES `sections` (`id`) ON DELETE CASCADE,
  CONSTRAINT `subject_timetable_ibfk_3` FOREIGN KEY (`subject_group_id`) REFERENCES `subject_groups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `subject_timetable_ibfk_4` FOREIGN KEY (`subject_group_subject_id`) REFERENCES `subject_group_subjects` (`id`) ON DELETE CASCADE,
  CONSTRAINT `subject_timetable_ibfk_5` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`id`) ON DELETE CASCADE,
  CONSTRAINT `subject_timetable_ibfk_6` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subject_timetable`
--

LOCK TABLES `subject_timetable` WRITE;
/*!40000 ALTER TABLE `subject_timetable` DISABLE KEYS */;
/*!40000 ALTER TABLE `subject_timetable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subjects`
--

DROP TABLE IF EXISTS `subjects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `subjects` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `code` varchar(100) NOT NULL,
  `type` varchar(100) NOT NULL,
  `is_active` varchar(255) DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subjects`
--

LOCK TABLES `subjects` WRITE;
/*!40000 ALTER TABLE `subjects` DISABLE KEYS */;
/*!40000 ALTER TABLE `subjects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `submit_assignment`
--

DROP TABLE IF EXISTS `submit_assignment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `submit_assignment` (
  `id` int NOT NULL AUTO_INCREMENT,
  `homework_id` int NOT NULL,
  `student_id` int NOT NULL,
  `message` text NOT NULL,
  `docs` varchar(225) NOT NULL,
  `file_name` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `submit_assignment`
--

LOCK TABLES `submit_assignment` WRITE;
/*!40000 ALTER TABLE `submit_assignment` DISABLE KEYS */;
/*!40000 ALTER TABLE `submit_assignment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teacher_subjects`
--

DROP TABLE IF EXISTS `teacher_subjects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `teacher_subjects` (
  `id` int NOT NULL AUTO_INCREMENT,
  `session_id` int DEFAULT NULL,
  `class_section_id` int DEFAULT NULL,
  `subject_id` int DEFAULT NULL,
  `teacher_id` int DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `is_active` varchar(255) DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `class_section_id` (`class_section_id`),
  KEY `session_id` (`session_id`),
  KEY `subject_id` (`subject_id`),
  KEY `teacher_id` (`teacher_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teacher_subjects`
--

LOCK TABLES `teacher_subjects` WRITE;
/*!40000 ALTER TABLE `teacher_subjects` DISABLE KEYS */;
/*!40000 ALTER TABLE `teacher_subjects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `template_admitcards`
--

DROP TABLE IF EXISTS `template_admitcards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `template_admitcards` (
  `id` int NOT NULL AUTO_INCREMENT,
  `template` varchar(250) DEFAULT NULL,
  `heading` text,
  `title` text,
  `left_logo` varchar(200) DEFAULT NULL,
  `right_logo` varchar(200) DEFAULT NULL,
  `exam_name` varchar(200) DEFAULT NULL,
  `school_name` varchar(200) DEFAULT NULL,
  `exam_center` varchar(200) DEFAULT NULL,
  `sign` varchar(200) DEFAULT NULL,
  `background_img` varchar(200) DEFAULT NULL,
  `is_name` int NOT NULL DEFAULT '1',
  `is_father_name` int NOT NULL DEFAULT '1',
  `is_mother_name` int NOT NULL DEFAULT '1',
  `is_dob` int NOT NULL DEFAULT '1',
  `is_admission_no` int NOT NULL DEFAULT '1',
  `is_roll_no` int NOT NULL DEFAULT '1',
  `is_address` int NOT NULL DEFAULT '1',
  `is_gender` int NOT NULL DEFAULT '1',
  `is_photo` int NOT NULL,
  `is_class` int NOT NULL DEFAULT '0',
  `is_section` int NOT NULL DEFAULT '0',
  `content_footer` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  `simple_template` int DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `template_admitcards`
--

LOCK TABLES `template_admitcards` WRITE;
/*!40000 ALTER TABLE `template_admitcards` DISABLE KEYS */;
/*!40000 ALTER TABLE `template_admitcards` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `template_marksheets`
--

DROP TABLE IF EXISTS `template_marksheets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `template_marksheets` (
  `id` int NOT NULL AUTO_INCREMENT,
  `template` varchar(200) DEFAULT NULL,
  `heading` text,
  `title` text,
  `left_logo` varchar(200) DEFAULT NULL,
  `right_logo` varchar(200) DEFAULT NULL,
  `exam_name` varchar(200) DEFAULT NULL,
  `school_name` varchar(200) DEFAULT NULL,
  `exam_center` varchar(200) DEFAULT NULL,
  `left_sign` varchar(200) DEFAULT NULL,
  `middle_sign` varchar(200) DEFAULT NULL,
  `right_sign` varchar(200) DEFAULT NULL,
  `exam_session` int DEFAULT '1',
  `is_name` int DEFAULT '1',
  `is_father_name` int DEFAULT '1',
  `is_mother_name` int DEFAULT '1',
  `is_dob` int DEFAULT '1',
  `is_admission_no` int DEFAULT '1',
  `is_roll_no` int DEFAULT '1',
  `is_photo` int DEFAULT '1',
  `is_division` int NOT NULL DEFAULT '1',
  `is_customfield` int NOT NULL,
  `background_img` varchar(200) DEFAULT NULL,
  `date` varchar(20) DEFAULT NULL,
  `is_class` int NOT NULL DEFAULT '0',
  `is_teacher_remark` int NOT NULL DEFAULT '1',
  `is_section` int NOT NULL DEFAULT '0',
  `content` text,
  `content_footer` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `template_marksheets`
--

LOCK TABLES `template_marksheets` WRITE;
/*!40000 ALTER TABLE `template_marksheets` DISABLE KEYS */;
/*!40000 ALTER TABLE `template_marksheets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `timetables`
--

DROP TABLE IF EXISTS `timetables`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `timetables` (
  `id` int NOT NULL AUTO_INCREMENT,
  `teacher_subject_id` int DEFAULT NULL,
  `day_name` varchar(50) DEFAULT NULL,
  `start_time` varchar(50) DEFAULT NULL,
  `end_time` varchar(50) DEFAULT NULL,
  `room_no` varchar(50) DEFAULT NULL,
  `is_active` varchar(255) DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `timetables`
--

LOCK TABLES `timetables` WRITE;
/*!40000 ALTER TABLE `timetables` DISABLE KEYS */;
/*!40000 ALTER TABLE `timetables` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `topic`
--

DROP TABLE IF EXISTS `topic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `topic` (
  `id` int NOT NULL AUTO_INCREMENT,
  `session_id` int NOT NULL,
  `lesson_id` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` int NOT NULL,
  `complete_date` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `session_id` (`session_id`),
  KEY `lesson_id` (`lesson_id`),
  CONSTRAINT `topic_ibfk_1` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `topic_ibfk_2` FOREIGN KEY (`lesson_id`) REFERENCES `lesson` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `topic`
--

LOCK TABLES `topic` WRITE;
/*!40000 ALTER TABLE `topic` DISABLE KEYS */;
/*!40000 ALTER TABLE `topic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transport_route`
--

DROP TABLE IF EXISTS `transport_route`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `transport_route` (
  `id` int NOT NULL AUTO_INCREMENT,
  `route_title` varchar(100) DEFAULT NULL,
  `no_of_vehicle` int DEFAULT NULL,
  `fare` float(10,2) DEFAULT NULL,
  `note` text,
  `is_active` varchar(255) DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transport_route`
--

LOCK TABLES `transport_route` WRITE;
/*!40000 ALTER TABLE `transport_route` DISABLE KEYS */;
/*!40000 ALTER TABLE `transport_route` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userlog`
--

DROP TABLE IF EXISTS `userlog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `userlog` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user` varchar(100) DEFAULT NULL,
  `role` varchar(100) DEFAULT NULL,
  `class_section_id` int DEFAULT NULL,
  `ipaddress` varchar(100) DEFAULT NULL,
  `user_agent` varchar(500) DEFAULT NULL,
  `login_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=166 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userlog`
--

LOCK TABLES `userlog` WRITE;
/*!40000 ALTER TABLE `userlog` DISABLE KEYS */;
/*!40000 ALTER TABLE `userlog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `childs` text NOT NULL,
  `role` varchar(30) NOT NULL,
  `verification_code` varchar(200) NOT NULL,
  `lang_id` int NOT NULL,
  `is_active` varchar(255) DEFAULT 'yes',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=569 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_authentication`
--

DROP TABLE IF EXISTS `users_authentication`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users_authentication` (
  `id` int NOT NULL AUTO_INCREMENT,
  `users_id` int NOT NULL,
  `token` varchar(255) NOT NULL,
  `expired_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_authentication`
--

LOCK TABLES `users_authentication` WRITE;
/*!40000 ALTER TABLE `users_authentication` DISABLE KEYS */;
/*!40000 ALTER TABLE `users_authentication` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vehicle_routes`
--

DROP TABLE IF EXISTS `vehicle_routes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vehicle_routes` (
  `id` int NOT NULL AUTO_INCREMENT,
  `route_id` int DEFAULT NULL,
  `vehicle_id` int DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vehicle_routes`
--

LOCK TABLES `vehicle_routes` WRITE;
/*!40000 ALTER TABLE `vehicle_routes` DISABLE KEYS */;
/*!40000 ALTER TABLE `vehicle_routes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vehicles`
--

DROP TABLE IF EXISTS `vehicles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vehicles` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `vehicle_no` varchar(20) DEFAULT NULL,
  `vehicle_model` varchar(100) NOT NULL DEFAULT 'None',
  `manufacture_year` varchar(4) DEFAULT NULL,
  `driver_name` varchar(50) DEFAULT NULL,
  `driver_licence` varchar(50) NOT NULL DEFAULT 'None',
  `driver_contact` varchar(20) DEFAULT NULL,
  `note` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vehicles`
--

LOCK TABLES `vehicles` WRITE;
/*!40000 ALTER TABLE `vehicles` DISABLE KEYS */;
/*!40000 ALTER TABLE `vehicles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `visitors_book`
--

DROP TABLE IF EXISTS `visitors_book`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `visitors_book` (
  `id` int NOT NULL AUTO_INCREMENT,
  `source` varchar(100) DEFAULT NULL,
  `purpose` varchar(255) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `contact` varchar(12) DEFAULT NULL,
  `id_proof` varchar(50) DEFAULT NULL,
  `no_of_pepple` int DEFAULT NULL,
  `date` date NOT NULL,
  `in_time` varchar(20) DEFAULT NULL,
  `out_time` varchar(20) DEFAULT NULL,
  `note` text NOT NULL,
  `image` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `visitors_book`
--

LOCK TABLES `visitors_book` WRITE;
/*!40000 ALTER TABLE `visitors_book` DISABLE KEYS */;
/*!40000 ALTER TABLE `visitors_book` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `visitors_purpose`
--

DROP TABLE IF EXISTS `visitors_purpose`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `visitors_purpose` (
  `id` int NOT NULL AUTO_INCREMENT,
  `visitors_purpose` varchar(100) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `visitors_purpose`
--

LOCK TABLES `visitors_purpose` WRITE;
/*!40000 ALTER TABLE `visitors_purpose` DISABLE KEYS */;
/*!40000 ALTER TABLE `visitors_purpose` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-12-28 15:01:20
