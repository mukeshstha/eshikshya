<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Changedata extends Admin_Controller
{
	public function __construct()
    {

        parent::__construct();
        $this->CI =& get_instance();

    }

	public function change(){
		// $feetype = "alter table feetype add glname varchar(255) null";
  //       $this->CI->db->query($feetype);
        $studentfee = "alter table student_fees_deposite add transid varchar(255)";
        $this->CI->db->query($studentfee);
	}
    public function change1(){
        $sql = "alter table fees_discounts add discount_percent decimal(10,2)";
        
        $this->CI->db->query($sql);
    }
    public function change2(){
        $sql1 = "alter table fees_discounts add feetype_id int(11)";
        $this->CI->db->query($sql1);
    }
    public function change3(){
        $sql1 = "alter table exam_group_class_batch_exam_subjects add practical_max float(10,2) null";
        
        $this->CI->db->query($sql1);
    }
    public function change4(){
        $sql2 = "alter table exam_group_class_batch_exam_subjects add practical_min float(10,2) null";
        $this->CI->db->query($sql2);
    }
    public function change5(){
        $sql2 = "alter table exam_group_exam_results add get_marks_practical float(10,2) null";
        $this->CI->db->query($sql2);
    }

    public function change6(){
        $sql = "alter table sch_settings add ac_user varchar(255) null";
        $this->CI->db->query($sql);
        $sql2 = "alter table sch_settings add ac_pwd varchar(255) null";
        $this->CI->db->query($sql2);
    }

    public function change7(){
        $sql = "INSERT INTO `permission_group`(`id`, `name`, `short_code`, `is_active`, `system`, `created_at`) VALUES ('30','Account','account','1','0','2022-9-16 15:15:22')";
        $this->CI->db->query($sql); 
    } 

    public function change8(){
        $sql = "INSERT INTO `permission_category`(`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES ('250','30','Account Category','account_category','1','1','1','1','2022-9-16 15:15:22')";
        $this->CI->db->query($sql); 
        $sql1 = "INSERT INTO `permission_category`(`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES ('251','30','Chart of account','chart_of_account','1','1','1','1','2022-9-16 15:15:22')";
        $this->CI->db->query($sql1);
        $sql2 = "INSERT INTO `permission_category`(`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES ('252','30','Approved Voucher','approved_voucher','1','0','0','0','2022-9-16 15:15:22')";
        $this->CI->db->query($sql2); 
        $sql3 = "INSERT INTO `permission_category`(`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES ('253','30','Unapproved Voucher','upapproved_voucher','1','0','1','1','2022-9-16 15:15:22')";
        $this->CI->db->query($sql3);
        $sql4 = "INSERT INTO `permission_category`(`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES ('254','30','Rejected Voucher','rejected_voucher','1','0','0','0','2022-9-16 15:15:22')";
        $this->CI->db->query($sql4);
        $sql5 = "INSERT INTO `permission_category`(`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES ('255','30','Journal Voucher','journal_voucher','1','0','0','0','2022-9-16 15:15:22')";
        $this->CI->db->query($sql5);
        $sql6 = "INSERT INTO `permission_category`(`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES ('256','30','Payment Voucher','payment_voucher','1','0','0','0','2022-9-16 15:15:22')";
        $this->CI->db->query($sql6);
        $sql7 = "INSERT INTO `permission_category`(`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES ('257','30','Receipt Voucher','receipt_voucher','1','0','0','0','2022-9-16 15:15:22')";
        $this->CI->db->query($sql7);
        $sql8 = "INSERT INTO `permission_category`(`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES ('258','30','Account Reports','account_reports','1','0','0','0','2022-9-16 15:15:22')";
        $this->CI->db->query($sql8);
        $sql9 = "INSERT INTO `permission_category`(`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES ('259','30','Trail Balance','trail_balance','1','0','0','0','2022-9-16 15:15:22')";
        $this->CI->db->query($sql9);
        $sql10 = "INSERT INTO `permission_category`(`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES ('260','30','General Ledger','general_legdger','1','0','0','0','2022-9-16 15:15:22')";
        $this->CI->db->query($sql10);
        $sql11 = "INSERT INTO `permission_category`(`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES ('261','30','Profit & Loss','profit_loss','1','0','0','0','2022-9-16 15:15:22')";
        $this->CI->db->query($sql11);
        $sql12 = "INSERT INTO `permission_category`(`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES ('262','30','Balance Sheet','balance_sheet','1','0','0','0','2022-9-16 15:15:22')";
        $this->CI->db->query($sql12);
        $sql13 = "INSERT INTO `permission_category`(`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES ('263','30','Day Book','day_book','1','0','0','0','2022-9-16 15:15:22')";
        $this->CI->db->query($sql13);
        $sql14 = "INSERT INTO `permission_category`(`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES ('264','30','Day Sheet','day_sheet','1','0','0','0','2022-9-16 15:15:22')";
        $this->CI->db->query($sql14);
        $sql15 = "INSERT INTO `permission_category`(`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES ('265','30','Daily Reconsile','daily_reconsile','1','0','0','0','2022-9-16 15:15:22')";
        $this->CI->db->query($sql15);
    }   

    public function change9(){
        $sql = "ALTER TABLE messages MODIFY template_id varchar(100) NULL";
        $this->CI->db->query($sql);
    }

    public function change10(){
        $sql = "ALTER TABLE student_applyleave MODIFY docs text NULL";
        $this->CI->db->query($sql);
    }
    public function change11(){
        $sql = "ALTER TABLE student_applyleave MODIFY approve_by int(11) NULL";
        $this->CI->db->query($sql);
    }
    public function change12(){
        $sql = "CREATE TABLE payment_methods (id int NOT NULL PRIMARY KEY AUTO_INCREMENT,name varchar(255),glcode varchar(255),created_at timestamp DEFAULT CURRENT_TIMESTAMP)";
        $this->CI->db->query($sql);
    }
    public function change13(){
        $sql = "INSERT INTO `front_cms_pages`(`id`, `page_type`, `is_homepage`, `title`, `url`, `type`, `slug`, `publish`,`is_active`, `created_at`) VALUES ('5','default','0','Online Admission','online_admission','page','online-admission','1','no','2022-9-16 15:15:22')";
        $this->CI->db->query($sql);
    }

    public function change14(){
        $sql = "'SET @@global.max_allowed_packet = ' . strlen( $sql ) + 1024";
        $this->CI->db->query($sql);
    }

    public function change15(){
        $sql = "ALTER TABLE chat_messages MODIFY ip varchar(30) NULL";
        $this->CI->db->query($sql);
    }
    public function change16(){
        $sql = "ALTER TABLE chat_messages MODIFY time int(11) NULL";
        $this->CI->db->query($sql);
    }
    public function change17(){
        $sql = "CREATE TABLE student_present (id int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,student_id int(11) NOT NULL,present_days int(11) NULL,exam_group_class_batch_exam_id int(11) NOT NULL,student_session_id int(11) NOT NULL)";
        $this->CI->db->query($sql);
    }

    public function change18(){
        $sql = "ALTER table feetype ADD submission_type int(11) NULL";
        $this->CI->db->query($sql);
    }

    public function change19(){
        $sql = "ALTER table student_fees_master ADD months int(11) NULL";
        $this->CI->db->query($sql);
    }

    public function change20(){
        $sql = "ALTER TABLE student_fees_master ADD transid varchar(255)";
        $this->CI->db->query($sql);
    }

    public function change21(){
        $sql = "ALTER TABLE online_admissions MODIFY reference_no varchar(50) NULL,MODIFY middlename varchar(255) NULL,MODIFY rte varchar(20) NULL,MODIFY cast varchar(50) NULL,MODIFY route_id int(11) NULL,MODIFY blood_group varchar(200) NULL,MODIFY vehroute_id int(11) NULL,MODIFY hostel_room_id int(11) NULL,MODIFY guardian_is varchar(100) NULL,MODIFY guardian_occupation varchar(150) NULL,MODIFY guardian_email varchar(100) NULL,MODIFY father_pic varchar(200) NULL,MODIFY mother_pic varchar(200) NULL,MODIFY guardian_pic varchar(200) NULL,MODIFY height varchar(100) NULL,MODIFY weight varchar(100) NULL,MODIFY note varchar(200) NULL,MODIFY disable_at date NULL,MODIFY paid_status int(11) NULL,MODIFY form_status int(11) DEFAULT '1'";
        $this->CI->db->query($sql);
    }

    public function change22(){
        $sql = "ALTER TABLE complaint MODIFY action_taken varchar(200) NULL,MODIFY assigned varchar(50) NULL,MODIFY note text NULL,MODIFY image varchar(100) NULL";
        $this->CI->db->query($sql);
    }

    public function change23(){
        $sql = "ALTER TABLE visitors_book MODIFY contact varchar(12) NULL,MODIFY id_proof varchar(50) NULL,MODIFY no_of_pepple int(11) NULL,MODIFY in_time varchar(20) NULL,MODIFY out_time varchar(20) NULL,MODIFY image varchar(100) NULL";
        $this->CI->db->query($sql);
    }

    public function change24(){
        $sql = "ALTER TABLE sch_settings ADD display_admit int(11) DEFAULT '0', ADD display_marksheet int(11) DEFAULT '0'";
        $this->CI->db->query($sql);
    }

    public function change25(){
        $sql = "ALTER TABLE staff_payslip ADD transid varchar(255) NULL";
        $this->CI->db->query($sql);
    }

    public function change26(){
        $sql = "ALTER TABLE sch_settings ADD fee_guardian int(11) DEFAULT '0'";
        $this->CI->db->query($sql);
        $sql1 = "INSERT INTO `permission_category`(`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES ('266','2','Guardian Fee','guardian_fee','1','1','1','1','2022-9-16 15:15:22')";
        $this->CI->db->query($sql1);
    }

    public function change27(){
        $sql = "ALTER TABLE template_admitcards ADD simple_template int(11) DEFAULT '0'";
        $this->CI->db->query($sql);
    }
}
