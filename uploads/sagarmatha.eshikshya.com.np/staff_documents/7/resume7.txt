Your Password is - 

Before updating the password on Outlook Desktop Application, We have to change it by logging in to the outlook web interface. 
To do so, Please follow the given steps:

1. Open your preffered browser i.e., chrome, edge, mozilla, brave, etc.
2. Go to https://prodsys.prabhupay.com
3. Enter your email id and given password in the respective field and click Sign In. It will then redirect you to change the password.
4. Fill in your desired password and click OK.

Password criteria -
 i.	Minimum Length - 8 characters recommended
ii.	Minimum complexity - No dictionary words included. Passwords should use these types of characters:
      Password should contain Lowercase, Uppercase, Numbers and Special characters such as !@#$%^&*(){}[]


Thank You.
