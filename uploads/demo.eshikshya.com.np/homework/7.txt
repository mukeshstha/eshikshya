 <div class="form-group">
                                                <label for="exampleInputEmail1"><?php echo $this->lang->line('date_of_birth'); ?></label><small class="req"> *</small>
                                                <input id="dob" placeholder="<?php echo $this->lang->line('placeholder_std_dob') ?>"  type="text" class="form-control date" value="<?php echo set_value('dob'); ?>"  />
                                                <span class="text-danger"><?php echo form_error('dob'); ?></span>
                                                <label for="exampleInputEmail1" id="dob1">Date of Birth(AD)</label>
                                                <input type="text" name="dob" id ="dob_english" placeholder="Click here to get AD date" class="form-control date"> 
                                                 <script type="text/javascript">
                                                document.getElementById("dob1").style.display = "none";
                                                document.getElementById("dob_english").style.display = "none";
                                                    window.onload = function() {
                                                 var mainInput = document.getElementById("dob");
                                                 mainInput.nepaliDatePicker({
                                                onChange: function() {
                                                var inputVal = document.getElementById("dob").value;
                                                inputobj=NepaliFunctions.ConvertToDateObject(inputVal, "YYYY-MM-DD")
                                                 var englishdate=NepaliFunctions.BS2AD(inputobj);
                                                 document.getElementById("dob_english").value =englishdate["year"]+"-"+englishdate["month"]+"-"+englishdate["day"];
 }
});       
}
                                                    </script>
                                            </div>