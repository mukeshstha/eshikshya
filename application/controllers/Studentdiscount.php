<?php 

class Studentdiscount extends Admin_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
		$this->load->database();
		$this->load->model('student_discount_applied_model');
    }

	public function getDiscount(){
		 $this->form_validation->set_rules('created_at', 'created_at', 'trim|required|xss_clean');
		 $this->form_validation->set_rules('amount_detail', 'Amount Detail', 'trim|required|xss_clean');
		$collected_by    = $this->customlib->getAdminSessionUserName();
		$amount_discount = $this->input->post('amount_discount');
		$total_discount = $this->input->post('amt_discount');
		if($amount_discount > 0){
			$discount_given = $amount_discount;
		}else{
			$discount_given = $total_discount;
		}
	        $data = array(
	            'student_fees_master_id'   => $this->input->post('student_fees_master_id'),
	            'fee_groups_feetype_id'    => $this->input->post('fee_groups_feetype_id'),
	            'amount_detail'            => $discount_given,
	            'created_by'               => $collected_by,
	            'created_at'               => date('Y-m-d', $this->customlib->datetostrtotime($this->input->post('collected_date'))),
	            'name'                     => $this->input->post('discount_name'),
	        );
	        
	        $this->student_discount_applied_model->discount_applied($data);
	        $this->session->set_flashdata('msg', '<div class="alert alert-success text-left">' . $this->lang->line('success_message') . '</div>');
        redirect($_SERVER['HTTP_REFERER']);
	}
	public function delete(){
		$value = $this->input->post('discount_id');
        $this->student_discount_applied_model->remove($value);
        $this->session->set_flashdata('msg', '<div class="alert alert-success text-left">' . $this->lang->line('delete_message') . '</div>');
        redirect($_SERVER['HTTP_REFERER']);
	}
}