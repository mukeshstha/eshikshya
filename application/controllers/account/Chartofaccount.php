<?php

class Chartofaccount extends Admin_Controller {

	public function __construct()
    {
        parent::__construct();
    }
	public function view()
	{
		$this->session->set_userdata('top_menu', 'Account getcategory');
        $this->session->set_userdata('sub_menu', 'account/chartofaccount');

        $token = $this->input->cookie('Token');
        if(isset($token)){
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => 'http://actm.prabhumanagement.com/api/ChartOfAccount/GetCOACategory',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
          CURLOPT_HTTPHEADER => array(
            'Authorization: Bearer ' .$token
          ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        $data["text"]=json_decode($response, true);
      }else{
         redirect($_SERVER['HTTP_REFERER']);
      }

		$this->load->view('layout/header');
        $this->load->view('account/chartofaccount/view',$data);
        $this->load->view('layout/footer');
	}

	public function dynamicselect()
	{
		// $this->load->model('account_maincategory_model');
		$category = $this->input->get('category');
        $giventoken = $_COOKIE["Token"];
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => 'http://actm.prabhumanagement.com/api/ChartOfAccount/GetChildLedgerByGLCode?GLCode='. $category,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
          CURLOPT_HTTPHEADER => array(
            'Authorization: Bearer ' . $giventoken
          ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        $data= json_decode($response, true);
        return $data;
	}

	
	public function create($id)
	{
		$data["id"]=$id;

		$this->load->view('layout/header');
    $this->load->view('account/chartofaccount/create',$data);
    $this->load->view('layout/footer');
	}

	public function create_subsidiary(){
    $this->form_validation->set_rules('name', 'name', 'required');
      $this->form_validation->set_rules('subcategory2', 'subcategory2', 'required');
    if ($this->form_validation->run() == FALSE) {

    }else{
	
    $giventoken = $_COOKIE["Token"];
    $curl = curl_init();
    $ip = getenv('HTTP_CLIENT_IP')?:
            getenv('HTTP_X_FORWARDED_FOR')?:
            getenv('HTTP_X_FORWARDED')?:
            getenv('HTTP_FORWARDED_FOR')?:
            getenv('HTTP_FORWARDED')?:
            getenv('REMOTE_ADDR');
    $data = 'GLCode=' . $this->input->post('subcategory2') . '&SubsidiaryName='.$this->input->post('name').'&CreatedBy='.$this->customlib->getAdminSessionUserName().'&CreatedDate='.date("Y-m-d").'&TransId=925446CA-C2FB-4266-BD1A-F9CE8758342B&IpAddress='.$ip;
    curl_setopt_array($curl, array(
      CURLOPT_URL => 'http://actm.prabhumanagement.com/api/ChartOfAccount/CreateGLSubsidiary',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'POST',
      CURLOPT_POSTFIELDS => $data,
      CURLOPT_HTTPHEADER => array(
        'Authorization: Bearer ' . $giventoken,
        'Content-Type: application/x-www-form-urlencoded'
      ),
    ));

    $response = curl_exec($curl);

    curl_close($curl);

    $result = json_decode($response,true);
    if($result['StatusCode']==00){
      $this->session->set_flashdata('msg', '<div class="alert alert-success text-left">' . $result['Message'] . '</div>');
    }else{
      $this->session->set_flashdata('msg', '<div class="alert alert-danger text-left">' . $result['Message'] . '</div>');
    }

    redirect($_SERVER['HTTP_REFERER']);
    }
	}

	public function delete()
	{

	}
}

?>