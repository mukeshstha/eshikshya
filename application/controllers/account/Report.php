<?php

class Report extends Admin_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->model("account_category_model");
    }

	public function trailbalance()
	{
		$this->session->set_userdata('top_menu', 'Account getcategory');
        $this->session->set_userdata('mid_menu','Account/trailbalance');
        $this->session->set_userdata('sub_menu', 'account/report');

		$token = $this->input->cookie('Token');

		$fromdate = date("Y-m-d", strtotime($this->input->get("FromDate")));  
    	$todate = date("Y-m-d", strtotime($this->input->get("ToDate"))); 
		
		if(isset($fromdate) && isset($todate)){
            if($this->customlib->getcalender()->calender=="0"){
                $newFromDate = $this->customlib->englishdate(date("Y-m-d", strtotime($fromdate)));
                $newtoDate = $this->customlib->englishdate(date("Y-m-d", strtotime($todate)));
            }else{
                $newFromDate = date("Y-m-d", strtotime($fromdate));  
                $newtoDate = date("Y-m-d", strtotime($todate)); 
            }
            
        }else{
            $newFromDate = date("Y-m-d", strtotime($fromdate));  
                $newtoDate = date("Y-m-d", strtotime($todate)); 
        }

		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => 'http://actm.prabhumanagement.com/api/Voucher/GetTrialBalance?FromDate='.$newFromDate.'&ToDate='.$newtoDate.'&ReportType=Summary',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'GET',
			CURLOPT_POSTFIELDS => 'FromDate='.$newFromDate.'&ToDate='.$newtoDate.'&ReportType=Summary',
			CURLOPT_HTTPHEADER => array(
				'Authorization: Bearer '.$token
				),
			));

		$response = curl_exec($curl);

		curl_close($curl);

		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => 'http://actm.prabhumanagement.com/api/Voucher/GetTrialBalance?FromDate='.$newFromDate.'&ToDate='.$newtoDate.'&ReportType=Details',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'GET',
			CURLOPT_POSTFIELDS => 'FromDate='.$newFromDate.'&ToDate='.$newtoDate.'&ReportType=Details',
			CURLOPT_HTTPHEADER => array(
				'Authorization: Bearer '.$token
				),
			));

		$detailedResponse = curl_exec($curl);

		curl_close($curl);
		
		$text['list']= json_decode($response, true);
		$text['detail']= json_decode($detailedResponse, true);
        $this->load->view('layout/header');
        $this->load->view('account/report/trailbalance',$text);
        $this->load->view('layout/footer');

	}


	public function profitloss()
	{
		$this->session->set_userdata('top_menu', 'Account getcategory');
        $this->session->set_userdata('mid_menu','Account/profitandloss');
        $this->session->set_userdata('sub_menu', 'account/report');

		$token = $this->input->cookie('Token');

        $fromdate = date("Y-m-d", strtotime($this->input->get("FromDate")));  
    	$todate = date("Y-m-d", strtotime($this->input->get("ToDate"))); 
		
		if(isset($fromdate) && isset($todate)){
            if($this->customlib->getcalender()->calender=="0"){
                $newFromDate = $this->customlib->englishdate(date("Y-m-d", strtotime($fromdate)));
                $newtoDate = $this->customlib->englishdate(date("Y-m-d", strtotime($todate)));
            }else{
                $newFromDate = date("Y-m-d", strtotime($fromdate));  
                $newtoDate = date("Y-m-d", strtotime($todate)); 
            }
            
        }else{
            $newFromDate = date("Y-m-d", strtotime($fromdate));  
            $newtoDate = date("Y-m-d", strtotime($todate)); 
        }

		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => 'http://actm.prabhumanagement.com/api/TrialBalance/GetProfitAndLoss?FromDate='.$newFromDate.'&ToDate='.$newtoDate.'&ReportType=Summary',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'GET',
			CURLOPT_POSTFIELDS => 'FromDate='.$newFromDate.'&ToDate='.$newtoDate.'&ReportType=Summary',
			CURLOPT_HTTPHEADER => array(
				'Authorization: Bearer '.$token
				),
			));

		$response = curl_exec($curl);

		curl_close($curl);

		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => 'http://actm.prabhumanagement.com/api/TrialBalance/GetProfitAndLoss?FromDate='.$newFromDate.'&ToDate='.$newtoDate.'&ReportType=Details',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'GET',
			CURLOPT_POSTFIELDS => 'FromDate='.$newFromDate.'&ToDate='.$newtoDate.'&ReportType=Details',
			CURLOPT_HTTPHEADER => array(
				'Authorization: Bearer '.$token
				),
			));

		$detailedResponse = curl_exec($curl);

		curl_close($curl);
		
		$text['list']= json_decode($response, true);
		$text['detail']= json_decode($detailedResponse, true);
        $this->load->view('layout/header');
        $this->load->view('account/report/profitloss',$text);
        $this->load->view('layout/footer');

	}

	public function balancesheet()
	{
		$this->session->set_userdata('top_menu', 'Account getcategory');
        $this->session->set_userdata('mid_menu','Account/balancesheet');
        $this->session->set_userdata('sub_menu', 'account/report');

		$token = $this->input->cookie('Token');
		$fromdate = date("Y-m-d", strtotime($this->input->get("FromDate")));  
    	$todate = date("Y-m-d", strtotime($this->input->get("ToDate"))); 
		
		if(isset($fromdate) && isset($todate)){
            if($this->customlib->getcalender()->calender=="0"){
                $newFromDate = $this->customlib->englishdate(date("Y-m-d", strtotime($fromdate)));
                $newtoDate = $this->customlib->englishdate(date("Y-m-d", strtotime($todate)));
            }else{
                $newFromDate = date("Y-m-d", strtotime($fromdate));  
                $newtoDate = date("Y-m-d", strtotime($todate)); 
            }
            
        }else{
            $newFromDate = date("Y-m-d", strtotime($fromdate));  
                $newtoDate = date("Y-m-d", strtotime($todate)); 
        }

		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => 'http://actm.prabhumanagement.com/api/TrialBalance/BalanceSheet?FromDate='.$newFromDate.'&ToDate='.$newtoDate.'&ReportType=Summary',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'GET',
			CURLOPT_POSTFIELDS => 'FromDate='.$newFromDate.'&ToDate='.$newtoDate.'&ReportType=Summary',
			CURLOPT_HTTPHEADER => array(
				'Authorization: Bearer '.$token
				),
			));

		$response = curl_exec($curl);

		curl_close($curl);

		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => 'http://actm.prabhumanagement.com/api/TrialBalance/BalanceSheet?FromDate='.$newFromDate.'&ToDate='.$newtoDate.'&ReportType=Details',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'GET',
			CURLOPT_POSTFIELDS => 'FromDate='.$newFromDate.'&ToDate='.$newtoDate.'&ReportType=Details',
			CURLOPT_HTTPHEADER => array(
				'Authorization: Bearer '.$token
				),
			));

		$detailedResponse = curl_exec($curl);

		curl_close($curl);
		
		$text['list']= json_decode($response, true);
		$text['detail']= json_decode($detailedResponse, true);
        $this->load->view('layout/header');
        $this->load->view('account/report/balancesheet',$text);
        $this->load->view('layout/footer');

	}


	public function daybook()
	{
		$this->session->set_userdata('top_menu', 'Account getcategory');
        $this->session->set_userdata('mid_menu','Account/daybook');
        $this->session->set_userdata('sub_menu', 'account/report');

        
		$token = $this->input->cookie('Token');

		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => 'http://actm.prabhumanagement.com/api/ChartOfAccount/GetSubsidiaryListByFlag?Flag=CH',
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'GET',
		  CURLOPT_HTTPHEADER => array(
		    'Authorization: Bearer '.$token
		  ),
		));
		$response = curl_exec($curl);
		curl_close($curl);
		$text['cash']= json_decode($response, true);



		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => 'http://actm.prabhumanagement.com/api/ChartOfAccount/GetSubsidiaryListByFlag?Flag=BK',
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'GET',
		  CURLOPT_HTTPHEADER => array(
		    'Authorization: Bearer '.$token
		  ),
		));
		$response = curl_exec($curl);
		curl_close($curl);
		$text['bank']= json_decode($response, true);

		if($this->customlib->getcalender()->calender=="0"){
			$newFromDate = $this->customlib->englishdate(date("Y-m-d", strtotime($this->input->get("FromDate"))));  
        	$newtoDate = $this->customlib->englishdate(date("Y-m-d", strtotime($this->input->get("ToDate")))); 
		}else{
			$newFromDate = date("Y-m-d", strtotime($this->input->get("FromDate")));  
        	$newtoDate = date("Y-m-d", strtotime($this->input->get("ToDate"))); 
		}

			$cashledger = $this->input->get("cashledger");
			$bankledger = $this->input->get("bankledger");
			$text['fromdate'] = date($this->customlib->getSchoolDateFormat(), strtotime($newFromDate));
			$text['todate'] = date($this->customlib->getSchoolDateFormat(), strtotime($newtoDate));
			if(isset($cashledger)){
				$curl = curl_init();
				curl_setopt_array($curl, array(
				  	CURLOPT_URL => 'http://actm.prabhumanagement.com/api/DayBook/DayBook?FromDate='.$newFromDate.'&ToDate='.$newtoDate.'&CashFlag=true&BankFlag=false&CashGLId='.$cashledger.'&BankGLId=0',
				  	CURLOPT_RETURNTRANSFER => true,
				  	CURLOPT_ENCODING => '',
				  	CURLOPT_MAXREDIRS => 10,
				  	CURLOPT_TIMEOUT => 0,
				  	CURLOPT_FOLLOWLOCATION => true,
				  	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  	CURLOPT_CUSTOMREQUEST => 'GET',
				  	CURLOPT_HTTPHEADER => array(
				    	'Authorization: Bearer '.$token
				  	),
				));
				$response = curl_exec($curl);
				curl_close($curl);
				$text['cashdata']= json_decode($response, true);

			}else{
				$curl = curl_init();
				curl_setopt_array($curl, array(
				  	CURLOPT_URL => 'http://actm.prabhumanagement.com/api/DayBook/DayBook?FromDate='.$newFromDate.'&ToDate='.$newtoDate.'&CashFlag=false&BankFlag=true&CashGLId=0&BankGLId='.$bankledger,
				  	CURLOPT_RETURNTRANSFER => true,
				  	CURLOPT_ENCODING => '',
				  	CURLOPT_MAXREDIRS => 10,
				  	CURLOPT_TIMEOUT => 0,
				  	CURLOPT_FOLLOWLOCATION => true,
				  	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  	CURLOPT_CUSTOMREQUEST => 'GET',
				  	CURLOPT_HTTPHEADER => array(
				    	'Authorization: Bearer '.$token
				  	),
				));
				$response = curl_exec($curl);
				curl_close($curl);
				$text['bankdata']= json_decode($response, true);
			}
		$text['daybook']=$this->account_category_model->getvoucherdetails("12257","JV0000425");
		$this->load->view('layout/header');
        $this->load->view('account/report/daybook',$text);
        $this->load->view('layout/footer');
	}

	public function generalledger(){
		$this->session->set_userdata('top_menu', 'Account getcategory');
        $this->session->set_userdata('mid_menu','Account/generalledger');
        $this->session->set_userdata('sub_menu', 'account/report');

        $token = $this->input->cookie('Token');
        $fromdate = $this->input->get("FromDate");
        $todate = $this->input->get("ToDate");
        if(isset($fromdate) && isset($todate)){
        	if($this->customlib->getcalender()->calender=="0"){
                $newFromDate = $this->customlib->englishdate(date("Y-m-d", strtotime($fromdate)));
                $newtoDate = $this->customlib->englishdate(date("Y-m-d", strtotime($todate)));
            }else{
                $newFromDate = date("Y-m-d", strtotime($fromdate));  
                $newtoDate = date("Y-m-d", strtotime($todate)); 
            }
	    }else{
            $newFromDate = date("Y-m-d", strtotime($fromdate));  
            $newtoDate = date("Y-m-d", strtotime($todate)); 
        }
        $glname = $this->input->get('glname');


        $curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => 'https://actm.prabhumanagement.com/api/Voucher/GetGLDetails?GLId='.$glname.'&FromDate='.$newFromDate.'&ToDate='.$newtoDate,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'GET',
		  CURLOPT_HTTPHEADER => array(
		    'Authorization: Bearer ' . $token
		  ),
		));

		$response = curl_exec($curl);

		curl_close($curl);
        $data['list']= json_decode($response, true);
		// print_r($text['value']);
		// die();

		$this->load->view('layout/header');
		$this->load->view('account/report/generalledger',$data);
		$this->load->view('layout/footer');


	}

	public function getvoucher(){
		$data                    = array();
        $voucher_id              = $this->input->post('voucher_id');
        $voucher_no				 = $this->input->post('voucher_no');
        $data['voucherdetails']  = $this->account_category_model->getvoucherdetails($voucher_id,$voucher_no);
        $data['sendingdetails']  = $this->load->view('account/report/_getvoucherdetails', $data, true);
        echo json_encode($data); 
	}

	

	
}

