<?php


class Journal extends Admin_Controller
{
	public function __construct()
    {
        parent::__construct();
    }

	public function index(){
        $this->session->set_userdata('top_menu', 'Account getcategory');
        $this->session->set_userdata('sub_menu', 'account/journal');

    	$this->load->view('layout/header');
        $this->load->view('admin/journal/list');
        $this->load->view('layout/footer');
	}

	public function create(){
		$data['category'] = $this->Journal_model->category();
		$this->load->view('layout/header');
        $this->load->view('admin/journal/create',$data);
        $this->load->view('layout/footer');
	}



    public function getapproved(){
        $this->session->set_userdata('top_menu', 'Account getcategory');
        $this->session->set_userdata('sub_menu', 'account/journal');
        $this->session->set_userdata('mid_menu','Account/approved');

        $fromdate=$this->input->get("FromDate");
        $todate= $this->input->get("ToDate");
        if(isset($fromdate) && isset($todate)){
            if($this->customlib->getcalender()->calender=="0"){
                $newFromDate = $this->customlib->englishdate(date("Y-m-d", strtotime($fromdate)));
                $newtoDate = $this->customlib->englishdate(date("Y-m-d", strtotime($todate)));
            }else{
                $newFromDate = date("Y-m-d", strtotime($fromdate));  
                $newtoDate = date("Y-m-d", strtotime($todate)); 
            }
            
        }else{
            $newFromDate = date("Y-m-d", strtotime($fromdate));  
                $newtoDate = date("Y-m-d", strtotime($todate)); 
        }
        $token = $this->input->cookie('Token');
        if (isset($token)) {
            $curl = curl_init();
            curl_setopt_array($curl, array(
            CURLOPT_URL => 'http://actm.prabhumanagement.com/api/Voucher/GETAPPROVEDVOUCHERLIST?FromDate='.$newFromDate.'&ToDate='.$newtoDate,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 0,
              CURLOPT_FOLLOWLOCATION => true,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => 'GET',
              CURLOPT_HTTPHEADER => array(
                'Authorization: Bearer '.$token
              ),
            ));

            $response = curl_exec($curl);

            curl_close($curl);

            $text['list']= json_decode($response, true);
        }else{
            redirect($_SERVER['HTTP_REFERER']);
        }

        $this->load->view('layout/header');
        $this->load->view('account/voucher/approvedvoucher',$text);
        $this->load->view('layout/footer');
    }

    public function getupapproved(){
        $this->session->set_userdata('top_menu', 'Account getcategory');
        $this->session->set_userdata('sub_menu', 'account/journal');
        $this->session->set_userdata('mid_menu','Account/getupapproved');

        // $token = $this->load->view('account/init_curl');

        $token = $this->input->cookie('Token');
        $fromdate=$this->input->get("FromDate");
        $todate= $this->input->get("ToDate");
        if(isset($fromdate) && isset($todate)){
            if($this->customlib->getcalender()->calender=="0"){
                $newFromDate = $this->customlib->englishdate(date("Y-m-d", strtotime($fromdate)));
                $newtoDate = $this->customlib->englishdate(date("Y-m-d", strtotime($todate)));
            }else{
                $newFromDate = date("Y-m-d", strtotime($fromdate));  
                $newtoDate = date("Y-m-d", strtotime($todate)); 
            }
        }else{
            $newFromDate = date("Y-m-d", strtotime($fromdate));  
            $newtoDate = date("Y-m-d", strtotime($todate)); 
        }
        if (isset($token)){
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => 'http://actm.prabhumanagement.com/api/Voucher/GETUNAPPROVEDVOUCHERLIST?FromDate='.$newFromDate.'&ToDate='.$newtoDate,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_HTTPHEADER => array(
            'Authorization: Bearer '.$token
          ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        $text['list']= json_decode($response, true);
        }else{
            redirect($_SERVER['HTTP_REFERER']);
        }
        $this->load->view('layout/header');
        $this->load->view('account/voucher/journallist',$text);
        $this->load->view('layout/footer');
    }

    public function rejected(){
        $this->session->set_userdata('top_menu', 'Account getcategory');
        $this->session->set_userdata('mid_menu','Account/rejected');
        $this->session->set_userdata('sub_menu', 'account/journal');
        $token = $this->input->cookie('Token');
        $fromdate=$this->input->get("FromDate");
        $todate= $this->input->get("ToDate");
        if(isset($fromdate) && isset($todate)){
            if($this->customlib->getcalender()->calender=="0"){
                $newFromDate = $this->customlib->englishdate(date("Y-m-d", strtotime($fromdate)));
                $newtoDate = $this->customlib->englishdate(date("Y-m-d", strtotime($todate)));
            }else{
                $newFromDate = date("Y-m-d", strtotime($fromdate));  
                $newtoDate = date("Y-m-d", strtotime($todate)); 
            }
            
        }else{
            $newFromDate = date("Y-m-d", strtotime($fromdate));  
            $newtoDate = date("Y-m-d", strtotime($todate)); 
        }
        
        if(isset($token)){
            $curl = curl_init();
            curl_setopt_array($curl, array(
            CURLOPT_URL => 'http://actm.prabhumanagement.com/api/Voucher/GETREJECTEDVOUCHERLIST?FromDate='.$newFromDate.'&ToDate='.$newtoDate,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Authorization: Bearer '.$token
              ),
            ));

            $response = curl_exec($curl);

            curl_close($curl);
            $text['list']= json_decode($response, true);
        }else{
            redirect($_SERVER['HTTP_REFERER']);
        }
        

        
        $this->load->view('layout/header');
        $this->load->view('account/voucher/rejectedvoucher',$text);
        $this->load->view('layout/footer');
    }

    public function viewjournal($narration,$id,$vouchernumber,$transid){
        $token = $this->input->cookie('Token');
        $text['transid'] = $transid; 
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => 'http://actm.prabhumanagement.com/api/Voucher/GetVoucherDetails?Id='.$id.'&VoucherNo='.$vouchernumber,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
          CURLOPT_HTTPHEADER => array(
            'Authorization: Bearer '.$token
          ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        $text['id']= $id;
        $text['vouchernumber'] = $vouchernumber;
        $text['list']= json_decode($response, true);
        $this->load->view('layout/header');
        $this->load->view('account/voucher/viewjournal',$text);
        $this->load->view('layout/footer');
    }

    public function viewunapprovedjournal($id,$vouchernumber){
        $token = $this->input->cookie('Token');
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => 'http://actm.prabhumanagement.com/api/Voucher/GetVoucherDetails?Id='.$id.'&VoucherNo='.$vouchernumber,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
          CURLOPT_HTTPHEADER => array(
            'Authorization: Bearer '.$token
          ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        $text['list']= json_decode($response, true);
        $this->load->view('layout/header');
        $this->load->view('account/voucher/viewjournal',$text);
        $this->load->view('layout/footer');
    }

    public function eomvoucher(){
        $this->session->set_userdata('top_menu', 'Account getcategory');
        $this->session->set_userdata('mid_menu','Account/eom');
        $this->session->set_userdata('sub_menu', 'account/journal');
        $token = $this->input->cookie('Token');
        $newFromDate = date("Y-m-d", strtotime($this->input->get("FromDate")));  
        $newtoDate = date("Y-m-d", strtotime($this->input->get("ToDate"))); 
        $curl = curl_init();


        curl_setopt_array($curl, array(
        CURLOPT_URL => 'http://actm.prabhumanagement.com/api/Voucher/EMVoucherList?FromDate='.$newFromDate.'&ToDate='.$newtoDate,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
          CURLOPT_HTTPHEADER => array(
            'Authorization: Bearer '.$token
          ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        $text['list']= json_decode($response, true);
        $this->load->view('layout/header');
        $this->load->view('account/voucher/eomvoucher',$text);
        $this->load->view('layout/footer');
    }


    public function approve($id,$voucherno){
        $token = $this->input->cookie('Token');
    $url = 'http://actm.prabhumanagement.com/api/Voucher/VoucherApproval';
   
        $ch = curl_init($url);
        $ip = getenv('HTTP_CLIENT_IP')?:
        getenv('HTTP_X_FORWARDED_FOR')?:
        getenv('HTTP_X_FORWARDED')?:
        getenv('HTTP_FORWARDED_FOR')?:
        getenv('HTTP_FORWARDED')?:
        getenv('REMOTE_ADDR');

        $data = array(
            'VoucherNo'=>$voucherno,
            'Id'=>$id,
            'Status'=>'00',
            'ApprovedBy'=>$this->customlib->getAdminSessionUserName(),
            'ApprovedDate'=>date("Y-m-d"),
            );

        $object=json_encode($data);
    
        $response=curl_setopt($ch, CURLOPT_POSTFIELDS, $object);
            
        /* set the content type json */
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    "X-Custom-Header: value",
                    "Content-Type: application/json",
                    'Authorization: Bearer '.$token
                ));
            
        /* set return type json */
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            
        /* execute request */
        curl_exec($ch);
        curl_close($ch);
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function reject($refno,$id,$voucherno,$transid){
        $token = $this->input->cookie('Token');
        $url = 'http://actm.prabhumanagement.com/api/Voucher/VoucherApproval';
   
        $ch = curl_init($url);
        $ip = getenv('HTTP_CLIENT_IP')?:
        getenv('HTTP_X_FORWARDED_FOR')?:
        getenv('HTTP_X_FORWARDED')?:
        getenv('HTTP_FORWARDED_FOR')?:
        getenv('HTTP_FORWARDED')?:
        getenv('REMOTE_ADDR');

        $data = array(
            'VoucherNo'=>$voucherno,
            'Id'=>$id,
            'Status'=>'Rj',
            'ApprovedBy'=>$this->customlib->getAdminSessionUserName(),
            'ApprovedDate'=>date("Y-m-d"),
            );

        $object=json_encode($data);
        $response=curl_setopt($ch, CURLOPT_POSTFIELDS, $object);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "X-Custom-Header: value",
            "Content-Type: application/json",
            'Authorization: Bearer '.$token
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        $text['list']= json_decode($response, true);
        if($text['list']['StatusCode']=="00"){
            if($refno=="11"){
                $this->deletetransaction($transid);
            }elseif ($refno=="00") {
                $this->deletetransactionassigned($transid);
            }elseif ($refno == "22"){
                $this->deletesalary($transid);
            }
        }
        redirect($_SERVER['HTTP_REFERER']);
        // echo $response;
    }

    public function deletetransaction($transid)
    {
        $data['title'] = 'studentfee List';
        $this->studentfee_model->removetrans($transid);
    }

    public function deletetransactionassigned($transid){
        $this->studentfee_model->removetransassigned($transid);
    }

    public function deletesalary($transid){
        $this->payroll_model->removesalary($transid);
    }

}


