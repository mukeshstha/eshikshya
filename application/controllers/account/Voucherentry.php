<?php

class Voucherentry extends Admin_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('setting_model');
    }

    public function journalentry()
    {
        $this->session->set_userdata('top_menu', 'Account getcategory');
        $this->session->set_userdata('mid_menu','Account/journalentry');
        $this->session->set_userdata('sub_menu', 'account/entry');

        $this->load->view('layout/header');
        $this->load->view('account/voucher_entry/journalvoucher_entry');
        $this->load->view('layout/footer');

    }
    public function journalentrypost()
    {
        $this->session->set_userdata('top_menu', 'Account getcategory');
        $this->session->set_userdata('mid_menu','Account/journalentry');
        $this->session->set_userdata('sub_menu', 'account/entry');

        $token = $this->input->cookie('Token');
        $this->load->library('form_validation');
        $createddate=$this->input->post("entry_date");
        if($this->customlib->getcalender()->calender=="0"){
            $newFromDate = $this->customlib->englishdate(date("Y-m-d", strtotime($createddate))); 
        }else{
            $newFromDate = date("Y-m-d", strtotime($createddate)); 
        }
        
        $this->form_validation->set_rules('debittotal', 'Debit Total', 'required');
        $this->form_validation->set_rules('credittotal', 'Credit Total', 'matches[debittotal]'); 
        if ($this->form_validation->run() == FALSE)
        {
            $this->journalentry();
        }
        else{
            if(isset($_POST['debit']))
            {
                $count=count($_POST["debit"]);
                for($i=0;$i<$count;$i++){

                    $subdata[]= array(
                        'Debit'=> $this->input->post("debit[$i]"),
                        'Credit' => $this->input->post("credit[$i]"),
                        'Remarks'=>  $this->input->post("remark[$i]"),
                        'GLId' =>  $this->input->post("glname[$i]"),

                        );              
                }
                $object=json_encode($subdata);

                $url = 'http://actm.prabhumanagement.com/api/Voucher/CreateJV';
                $ch = curl_init($url);
                $ip = getenv('HTTP_CLIENT_IP')?:
                getenv('HTTP_X_FORWARDED_FOR')?:
                getenv('HTTP_X_FORWARDED')?:
                getenv('HTTP_FORWARDED_FOR')?:
                getenv('HTTP_FORWARDED')?:
                getenv('REMOTE_ADDR');

                $data = array(
                    'CreatedBy'=>$this->customlib->getAdminSessionUserName(),
                    'FiscalYear' => "2078",
                    'Narration'=> $this->input->post("narration"),
                    'CreatedDate' => $newFromDate,
                    'RefNumber' => '99',
                    'TransId'=>'925446CA-C2FB-4266-BD1A-F9CE87583424',
                    'JVVoucherDetails'=>$subdata
                    );

                $final=json_encode($data);

                $response=curl_setopt($ch, CURLOPT_POSTFIELDS, $final);

                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    "X-Custom-Header: value",
                    "Content-Type: application/json",
                    "Authorization: token ".$token,
                    ));

                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                $message=curl_exec($ch);
                curl_close($ch);
                $result = json_decode($message,true);

                if($result['StatusCode']==00){
                  $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissible text-left"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' . $result['Message'] . '</div>');
                }else{
                  $this->session->set_flashdata('msg', '<div class="alert alert-danger alert-dismissible text-left"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' . $result['Message'] . '</div>');
                }
                redirect($_SERVER['HTTP_REFERER']);
            }
        }
    }

    public function paymentVoucher(){
        $this->session->set_userdata('top_menu', 'Account getcategory');
        $this->session->set_userdata('mid_menu','Account/paymententry');
        $this->session->set_userdata('sub_menu', 'account/entry');

        $this->load->view('layout/header');
        $this->load->view('account/voucher_entry/paymentvoucher');
        $this->load->view('layout/footer');
    }

    public function paymentVoucherPost(){
        $token = $this->input->cookie('Token');
        

        $createddate=$this->input->post("entry_date_bs");
        $newFromDate = date("Y-m-d", strtotime($createddate));


        if(isset($_POST['GLId']))
        {
            $count=count($_POST["GLId"]);
            for($i=0;$i<$count;$i++){

                $subdata[]= array(
                    'GLId'=> $this->input->post("GLId[$i]"),
                    'Amount' => $this->input->post("amount[$i]"),
                    'Remarks'=>  $this->input->post("remark[$i]"),
                    );              
            }
            $object=json_encode($subdata);
            
            $url = 'http://actm.prabhumanagement.com/api/Voucher/CreatePV';
            $ch = curl_init($url);
            $ip = getenv('HTTP_CLIENT_IP')?:
            getenv('HTTP_X_FORWARDED_FOR')?:
            getenv('HTTP_X_FORWARDED')?:
            getenv('HTTP_FORWARDED_FOR')?:
            getenv('HTTP_FORWARDED')?:
            getenv('REMOTE_ADDR');
            $data = array(
                'CreatedBy'=>$this->customlib->getAdminSessionUserName(), 
                'FiscalYear' => "2078",
                'Narration'=> $this->input->post("narration"),
                'CreatedDate' => $newFromDate,
                'TransId'=>'925446CA-C2FB-4266-BD1A-F9CE87583424',
                'CashGlId'=>$this->input->post("CashGlId"),
                'RefNumber'=>$this->input->post("reference_no"),
                'PVVoucherDetails'=>$subdata
                );

            $final=json_encode($data);

            $response=curl_setopt($ch, CURLOPT_POSTFIELDS, $final);

            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                "X-Custom-Header: value",
                "Content-Type: application/json",
                "Authorization: token ".$token,
                ));

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $message=curl_exec($ch);
            curl_close($ch);
            $result = json_decode($message,true);

            if($result['StatusCode']==00){
                  $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissible text-left"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' . $result['Message'] . '</div>');
                }else{
                  $this->session->set_flashdata('msg', '<div class="alert alert-danger alert-dismissible text-left"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' . $result['Message'] . '</div>');
                }
            redirect($_SERVER['HTTP_REFERER']);



        }
    }


    public function receiptVoucher(){

        $this->session->set_userdata('top_menu', 'Account getcategory');
        $this->session->set_userdata('mid_menu','Account/receiptentry');
        $this->session->set_userdata('sub_menu', 'account/entry');

        $this->load->view('layout/header');
        $this->load->view('account/voucher_entry/receiptvoucher');
        $this->load->view('layout/footer');
    }

    public function receiptVoucherPost(){
        $token = $this->input->cookie('Token');
        

        $createddate=$this->input->post("entry_date_bs");
        $newFromDate = date("Y-m-d", strtotime($createddate));


        if(isset($_POST['GLId']))
        {
            $count=count($_POST["GLId"]);
            for($i=0;$i<$count;$i++){

                $subdata[]= array(
                    'GLId'=> $this->input->post("GLId[$i]"),
                    'Amount' => $this->input->post("amount[$i]"),
                    'Remarks'=>  $this->input->post("remark[$i]"),
                    );              
            }
            $object=json_encode($subdata);
            
            $url = 'http://actm.prabhumanagement.com/api/Voucher/CreateRV';
            $ch = curl_init($url);
            $ip = getenv('HTTP_CLIENT_IP')?:
            getenv('HTTP_X_FORWARDED_FOR')?:
            getenv('HTTP_X_FORWARDED')?:
            getenv('HTTP_FORWARDED_FOR')?:
            getenv('HTTP_FORWARDED')?:
            getenv('REMOTE_ADDR');

            $data = array(
                'CreatedBy'=>$this->customlib->getAdminSessionUserName(),
                'FiscalYear' => "2078",
                'Narration'=> $this->input->post("narration"),
                'CreatedDate' => $newFromDate,
                'TransId'=>'925446CA-C2FB-4266-BD1A-F9CE87583424',
                'CashGlId'=>$this->input->post("CashGlId"),
                'RefNumber'=>$this->input->post("reference_no"),
                'RVDetails'=>$subdata
                );

            $final=json_encode($data);

            $response=curl_setopt($ch, CURLOPT_POSTFIELDS, $final);

            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                "X-Custom-Header: value",
                "Content-Type: application/json",
                "Authorization: token ".$token,
                ));

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $message = curl_exec($ch);
            curl_close($ch);
            $result = json_decode($message,true);

            if($result['StatusCode']==00){
                  $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissible text-left"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' . $result['Message'] . '</div>');
                }else{
                  $this->session->set_flashdata('msg', '<div class="alert alert-danger alert-dismissible text-left"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' . $result['Message'] . '</div>');
                }
            redirect($_SERVER['HTTP_REFERER']);
        }
    }


    public function eomentry()
    {


        $this->session->set_userdata('top_menu', 'Account getcategory');
        $this->session->set_userdata('mid_menu','Account/eomentry');
        $this->session->set_userdata('sub_menu', 'account/entry');

        $this->load->view('layout/header');
        $this->load->view('account/voucher_entry/eomvoucher');
        $this->load->view('layout/footer');

    }
    public function eomentrypost()
    {
        $token = $this->input->cookie('Token');
        $createddate=$this->input->post("entry_date_bs");
        $newFromDate = date("Y-m-d", strtotime($createddate)); 
        $transaction=$this->input->post("transdate");
        $transdate = date("Y-m-d", strtotime($transaction));
        $this->form_validation->set_rules('debittotal', 'Debit Total', 'required');
        $this->form_validation->set_rules('credittotal', 'Credit Total', 'matches[debittotal]'); 
        if ($this->form_validation->run() == FALSE)
        {
            $this->eomentry();
        }
        else{
            if(isset($_POST['debit']))
            {
                $count=count($_POST["debit"]);
                for($i=0;$i<$count;$i++){

                    $subdata[]= array(
                        'Debit'=> $this->input->post("debit[$i]"),
                        'Credit' => $this->input->post("credit[$i]"),
                        'Remarks'=>  $this->input->post("remark[$i]"),
                        'GLId' =>  $this->input->post("glname[$i]")
                        );              
                }
                $object=json_encode($subdata);

                $url = 'http://actm.prabhumanagement.com/api/Voucher/CreateEMV';
                $ch = curl_init($url);
                $ip = getenv('HTTP_CLIENT_IP')?:
                getenv('HTTP_X_FORWARDED_FOR')?:
                getenv('HTTP_X_FORWARDED')?:
                getenv('HTTP_FORWARDED_FOR')?:
                getenv('HTTP_FORWARDED')?:
                getenv('REMOTE_ADDR');

                $data = array(
                    'CreatedBy'=>$this->customlib->getAdminSessionUserName(),
                    'FiscalYear' => $this->setting_model->getCurrentSession(),
                    'Narration'=> $this->input->post("narration"),
                    'CreatedDate' => $newFromDate,
                    'RefNumber'=>$this->input->post("reference_no"),
                    'TransDate'=>$transdate,
                    'TransId'=>'925446CA-C2FB-4266-BD1A-F9CE87583424',
                    'EMVoucherDetailsDtoLst'=>$subdata
                    );
                // print_r($data);
                // die();
                $final=json_encode($data);
                $response=curl_setopt($ch, CURLOPT_POSTFIELDS, $final);

                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    "X-Custom-Header: value",
                    "Content-Type: application/json",
                    "Authorization: token ".$token,
                    ));

                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                $message = curl_exec($ch);
                curl_close($ch);
                $result = json_decode($message,true);

                if($result['StatusCode']==00){
                  $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissible text-left"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' . $result['Message'] . '</div>');
                }else{
                  $this->session->set_flashdata('msg', '<div class="alert alert-danger alert-dismissible text-left"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' . $result['Message'] . '</div>');
                }
                redirect($_SERVER['HTTP_REFERER']);
            }
        }
    }

}
?>