<?php

class Category extends Admin_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->database();
    $this->load->model('account_maincategory_model');
    $this->load->model('account_category_model'); 
  }

  public function addCategory()
  {
    $this->session->set_userdata('top_menu', 'Account getcategory');
    $this->session->set_userdata('sub_menu', 'account/getcategory');

    $token = $this->input->cookie('Token');
    if($token!==""){
      $curl = curl_init();

      curl_setopt_array($curl, array(
        CURLOPT_URL => 'http://actm.prabhumanagement.com/api/ChartOfAccount/GetCOACategory',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_HTTPHEADER => array(
          'Authorization: Bearer ' .$token
        ),
      ));

      $response = curl_exec($curl);

      curl_close($curl);

      $data["text"]=json_decode($response, true);
    }else{
      redirect($_SERVER['HTTP_REFERER']);
    }
    
    $this->load->view('layout/header');
    $this->load->view('account/category/addcategory',$data);
    $this->load->view('layout/footer');
  }
  public function createCategory()
  {
    $this->form_validation->set_rules('title', 'title', 'required');
    $this->form_validation->set_rules('GLCode', 'Parent Category', 'required');
    if ($this->form_validation->run() == FALSE) {
      redirect('/account/category/addCategory');
    }else{
      $token = $this->input->cookie('Token');
      $uniqueid = $this->input->post('trans_id');
      $ac_type = $this->input->post('category_type');
      if(isset($token)){
        $ip = getenv('HTTP_CLIENT_IP')?:
        getenv('HTTP_X_FORWARDED_FOR')?:
        getenv('HTTP_X_FORWARDED')?:
        getenv('HTTP_FORWARDED_FOR')?:
        getenv('HTTP_FORWARDED')?:
        getenv('REMOTE_ADDR');
        if($ac_type=="group"){
            $data = 'CreatedBy='.$this->customlib->getAdminSessionUserName().'&LedgerHead='.$this->input->post("title").'&CreatedDate='.date("Y-m-d").'&GLCode='.$this->input->post("GLCode").'&TransId='.$uniqueid.'&IpAddress='.$ip;

          $curl = curl_init();

          curl_setopt_array($curl, array(
            CURLOPT_URL => 'http://actm.prabhumanagement.com/api/ChartOfAccount/CreateLedgerHead',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_HTTPHEADER => array(
              'Authorization: Bearer ' . $token,
              'Content-Type: application/x-www-form-urlencoded'
            ),
          ));

          $response = curl_exec($curl);
          redirect($_SERVER['HTTP_REFERER']);
        }else{
            $ip = getenv('HTTP_CLIENT_IP')?:
                    getenv('HTTP_X_FORWARDED_FOR')?:
                    getenv('HTTP_X_FORWARDED')?:
                    getenv('HTTP_FORWARDED_FOR')?:
                    getenv('HTTP_FORWARDED')?:
                    getenv('REMOTE_ADDR');
                    $SubsidiaryName = urlencode($this->input->post('title'));
                    $createdby = urlencode($this->customlib->getAdminSessionUserName());
            $value = 'GLCode=' . $this->input->post("GLCode") . '&SubsidiaryName='.$SubsidiaryName.'&CreatedBy='.$createdby.'&CreatedDate='.date("Y-m-d").'&TransId='.$uniqueid;

            
            $curl = curl_init();

            curl_setopt_array($curl, array(
              CURLOPT_URL => 'http://actm.prabhumanagement.com/api/ChartOfAccount/CreateGLSubsidiary',
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => '',
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 0,
              CURLOPT_FOLLOWLOCATION => true,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => 'POST',
              CURLOPT_POSTFIELDS => $value,
              CURLOPT_HTTPHEADER => array(
                'Authorization: Bearer '.$token,
                'Content-Type: application/x-www-form-urlencoded'
              ),
            ));

            $response = curl_exec($curl);

            curl_close($curl);
            redirect($_SERVER['HTTP_REFERER']);
        }
        
         $result = json_decode($response,true);
         // print_r($result);
         // die();
          if($result['StatusCode']==00){
            $this->session->set_flashdata('msg', '<div class="alert alert-success text-left">' . $result['Message'] . '</div>');
          }else{
            $this->session->set_flashdata('msg', '<div class="alert alert-danger text-left">' . $result['Message'] . '</div>');
          }
          $this->addCategory();
      }else{
        redirect($_SERVER['HTTP_REFERER']);
      }
    }

    

    
  }

  public function getcategory()
  {
    $this->session->set_userdata('top_menu', 'Account getcategory');
    $this->session->set_userdata('sub_menu', 'account/getcategory');

    
    $token=$this->input->cookie('Token');

    if(isset($token)){
      $curl = curl_init();

      curl_setopt_array($curl, array(
        CURLOPT_URL => 'http://actm.prabhumanagement.com/api/ChartOfAccount/GetCOACategory',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_HTTPHEADER => array(
          'Authorization: Bearer ' .$token
        ),
      ));

      $response = curl_exec($curl);

      curl_close($curl);

      $data["text"]=json_decode($response, true);
    }else{
      redirect($_SERVER['HTTP_REFERER']);
    }
    $this->load->view('layout/header');
    $this->load->view('account/category/categorylist',$data);
    $this->load->view('layout/footer');
  }

  public function getsubsidiary($id){
    $parentGl = $id;
    $curl = curl_init();
    $giventoken=$_COOKIE["Token"];
    curl_setopt_array($curl, array(
      CURLOPT_URL => 'http://actm.prabhumanagement.com/api/ChartOfAccount/GetChildLedgerByGLCode?GLCode='. $parentGl,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'GET',
      CURLOPT_HTTPHEADER => array(
        'Authorization: Bearer ' . $giventoken
      ),
    ));

    $response = curl_exec($curl);

    curl_close($curl);
    $subtext= json_decode($response, true);
    return $subtext;
  }

  public function getsubcategory(){
        // POST data
    $postData = $this->input->post();
    $data['category'] = $postData;
    $this->load->view('account/chartofaccount/dynamicselect',$data);
  }

  public function getsubchildcategory(){
        // POST data
    $postData = $this->input->post();
    $data['category'] = $postData;
    $this->load->view('account/chartofaccount/dynamicselect',$data);
  }

  public function editcategory(){
    $glname = $this->input->post('glname');
    $glcode = $this->input->post('glcode');
    $token = $_COOKIE["Token"];
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => 'https://actm.prabhumanagement.com/api/ChartOfAccount/UpdateGlName',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'POST',
      CURLOPT_POSTFIELDS => 'GlName='.$glname.'&updatedBy='.$this->customlib->getAdminSessionUserName().'&updatedDate='.date('Y-m-d').'&GlCode='.$glcode,
      CURLOPT_HTTPHEADER => array(
        'Authorization: Bearer '.$token,
        'Content-Type: application/x-www-form-urlencoded'
      ),
    ));

    $response = curl_exec($curl);

    curl_close($curl);
    // $subtext= json_decode($response, true);
    $array = array('status' => 'success', 'error' => '', 'message' => $this->lang->line('success_message'));
    echo json_encode($array);
    redirect($_SERVER['HTTP_REFERER']);
  }

}