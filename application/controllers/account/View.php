<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class View extends Admin_Controller
{
 public function category()
    {
    	$this->load->view('layout/header');
        $this->load->view('account/static/category');
        $this->load->view('layout/footer');
    }

    public function createcategory()
    {
    	$this->load->view('layout/header');
        $this->load->view('account/static/createcategory');
        $this->load->view('layout/footer');
    }

    public function editcategory()
    {
    	$this->load->view('layout/header');
        $this->load->view('account/static/editcategory');
        $this->load->view('layout/footer');
    }

    public function chartofaccount()
    {
    	$this->load->view('layout/header');
        $this->load->view('account/static/chartofaccount');
        $this->load->view('layout/footer');
    }
    public function createchart()
    {
    	$this->load->view('layout/header');
        $this->load->view('account/static/createchart');
        $this->load->view('layout/footer');
    }
    public function journal()
    {
    	$this->load->view('layout/header');
        $this->load->view('account/static/journal');
        $this->load->view('layout/footer');
    }
    public function createjournal()
    {
    	$this->load->view('layout/header');
        $this->load->view('account/static/addjournal');
        $this->load->view('layout/footer');
    }
    public function viewjournal()
    {
    	$this->load->view('layout/header');
        $this->load->view('account/static/viewjournal');
        $this->load->view('layout/footer');
    }
    public function paymentVoucher()
    {
    	$this->load->view('layout/header');
        $this->load->view('account/static/paymentVoucher');
        $this->load->view('layout/footer');
    }

    public function receiptVoucher()
    {
    	$this->load->view('layout/header');
        $this->load->view('account/static/receiptVoucher');
        $this->load->view('layout/footer');
    }
    public function trailbalance()
    {
    	$this->load->view('layout/header');
        $this->load->view('account/static/trailbalance');
        $this->load->view('layout/footer');
    }

    public function generalledger()
    {
    	$this->load->view('layout/header');
        $this->load->view('account/static/generalLedger');
        $this->load->view('layout/footer');
    }

    public function daybook()
    {
    	$this->load->view('layout/header');
        $this->load->view('account/static/daybook');
        $this->load->view('layout/footer');
    }
    public function daysheet()
    {
    	$this->load->view('layout/header');
        $this->load->view('account/static/daysheet');
        $this->load->view('layout/footer');
    }
    public function dailyreconsile()
    {
    	$this->load->view('layout/header');
        $this->load->view('account/static/dailyreconsile');
        $this->load->view('layout/footer');
    }
}

?>