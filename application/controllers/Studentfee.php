
<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Studentfee extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('smsgateway');
        $this->load->library('mailsmsconf');
        $this->search_type        = $this->config->item('search_type');
        $this->sch_setting_detail = $this->setting_model->getSetting();
        $this->load->model('student_discount_applied_model');
    }

    public function index()
    {
        if (!$this->rbac->hasPrivilege('collect_fees', 'can_view')) {
            access_denied();
        }

        $this->session->set_userdata('top_menu', $this->lang->line('fees_collection'));
        $this->session->set_userdata('sub_menu', 'studentfee/index');
        $data['sch_setting'] = $this->sch_setting_detail;
        $data['title']       = 'student fees';
        $class               = $this->class_model->get();
        $data['classlist']   = $class;
        $this->load->view('layout/header', $data);
        $this->load->view('studentfee/studentfeeSearch', $data);
        $this->load->view('layout/footer', $data);
    }

    public function collection_report()
    {
        if (!$this->rbac->hasPrivilege('collect_fees', 'can_view')) {
            access_denied();
        }

        $data['collect_by'] = $this->studentfeemaster_model->get_feesreceived_by();
        $data['searchlist'] = $this->customlib->get_searchtype();
        $data['group_by']   = $this->customlib->get_groupby();
        $feetype = $this->feetype_model->get();
        $data['feetypeList'] = $feetype;
        $this->session->set_userdata('top_menu', 'Reports');
        $this->session->set_userdata('sub_menu', 'Reports/finance');
        $this->session->set_userdata('subsub_menu', 'Reports/finance/collection_report');
        $subtotal=false;
        if (isset($_POST['search_type']) && $_POST['search_type'] != '') {
            $dates               = $this->customlib->get_betweendate($_POST['search_type']);
            $data['search_type'] = $_POST['search_type'];
        } else {
            $dates               = $this->customlib->get_betweendate('this_year');
            $data['search_type'] = '';
        }

        if (isset($_POST['collect_by']) && $_POST['collect_by'] != '') {
            $data['received_by'] = $received_by = $_POST['collect_by'];

        } else {
            $data['received_by'] = $received_by = '';
        }

        if (isset($_POST['feetype_id']) && $_POST['feetype_id'] != '') {
            $feetype_id = $_POST['feetype_id'];

        } else {
            $feetype_id="";
        }

        if (isset($_POST['group']) && $_POST['group'] != '') {
            $data['group_byid'] = $group = $_POST['group'];
            $subtotal=true;
        } else {
            $data['group_byid'] = $group = '';
        }

        $collect_by          = array();
        $collection          = array();
        $start_date          = date('Y-m-d', strtotime($dates['from_date']));
        $end_date            = date('Y-m-d', strtotime($dates['to_date']));


        $this->form_validation->set_rules('search_type', $this->lang->line('search') . " " . $this->lang->line('type'), 'trim|required|xss_clean');


        if ($this->form_validation->run() == false) {
            $data['results'] = array();
        } else {
            $data['results'] = $this->studentfeemaster_model->getFeeCollectionReport($start_date, $end_date,$feetype_id, $received_by, $group);

            if ($group != '') {

                if ($group == 'class') {

                    $group_by = 'class_id';
                } elseif ($group == 'collection') {

                    $group_by = 'received_by';
                } elseif ($group == 'mode') {

                    $group_by = 'payment_mode';
                }

                foreach ($data['results'] as $key => $value) {

                    $collection[$value[$group_by]][] = $value;
                }
            } else {

                $s = 0;
                foreach ($data['results'] as $key => $value) {

                    $collection[$s++] = array($value);
                }
            }

            $data['results'] = $collection;
        }
        $data['subtotal']=$subtotal;
        $data['sch_setting'] = $this->sch_setting_detail;
        $this->load->view('layout/header', $data);
        $this->load->view('studentfee/collection_report', $data);
        $this->load->view('layout/footer', $data);
    }

    public function pdf()
    {
        $this->load->helper('pdf_helper');
    }

    public function search()
    {
        $search_type = $this->input->post('search_type');
        if ($search_type == "class_search") {
            $this->form_validation->set_rules('class_id', $this->lang->line('class'), 'required|trim|xss_clean');
        } elseif ($search_type == "keyword_search") {
            $this->form_validation->set_rules('search_text', 'keyword --r', 'required|trim|xss_clean');
            $data = array('search_text' => 'dummy');

            $this->form_validation->set_data($data);
        }
        if ($this->form_validation->run() == false) {
            $error = array();
            if ($search_type == "class_search") {
                $error['class_id'] = form_error('class_id');
            } elseif ($search_type == "keyword_search") {
                $error['search_text'] = form_error('search_text');
            }

            $array = array('status' => 0, 'error' => $error);
            echo json_encode($array);
        } else {
            $search_type = $this->input->post('search_type');
            $search_text = $this->input->post('search_text');
            $class_id    = $this->input->post('class_id');
            $section_id  = $this->input->post('section_id');
            $params      = array('class_id' => $class_id, 'section_id' => $section_id, 'search_type' => $search_type, 'search_text' => $search_text);
            $array       = array('status' => 1, 'error' => '', 'params' => $params);
            echo json_encode($array);
        }
    }

    public function ajaxSearch()
    {
        $class       = $this->input->post('class_id');
        $section     = $this->input->post('section_id');
        $search_text = $this->input->post('search_text');
        $search_type = $this->input->post('search_type');
        if ($search_type == "class_search") {
            $students = $this->student_model->getDatatableByClassSection($class, $section);
        } elseif ($search_type == "keyword_search") {
            $students = $this->student_model->getDatatableByFullTextSearch($search_text);
        }
        $sch_setting = $this->sch_setting_detail;
        $students = json_decode($students);
        $dt_data  = array();
        if (!empty($students->data)) {
            foreach ($students->data as $student_key => $student) {
                $row   = array();
                $row[] = $student->class;
                $row[] = $student->section;
                $row[] = $student->admission_no;
                $row[] = "<a href='" . base_url() . "student/view/" . $student->id . "'>" . $this->customlib->getFullName($student->firstname, $student->middlename, $student->lastname, $sch_setting->middlename, $sch_setting->lastname) . "</a>";              
                $sch_setting = $this->sch_setting_detail;     
                if ($sch_setting->father_name) {  
                    $row[] = $student->father_name;
                }
                $row[] = $this->customlib->dateformat($student->dob);
                $row[] = $student->guardian_phone;
                $row[] = "<a href=" . site_url('studentfee/addfee/' . $student->student_session_id) . "  class='btn btn-info btn-xs'>" . $this->lang->line('collect_fees') . "</a>";

                $dt_data[] = $row;
            }

        }
        $json_data = array(
            "draw"            => intval($students->draw),
            "recordsTotal"    => intval($students->recordsTotal),
            "recordsFiltered" => intval($students->recordsFiltered),
            "data"            => $dt_data,
        );
        echo json_encode($json_data);

    }

    public function ajaxSearchguardian()
    {
        $class       = $this->input->post('class_id');
        $section     = $this->input->post('section_id');
        $search_text = $this->input->post('search_text');
        $search_type = $this->input->post('search_type');
        if ($search_type == "class_search") {
            $students = $this->student_model->getDatatableByClassSection($class, $section);
        } elseif ($search_type == "keyword_search") {
            $students = $this->student_model->getDatatableByFullTextSearch($search_text);
        }
        $sch_setting = $this->sch_setting_detail;
        $students = json_decode($students);
        $dt_data  = array();
        if (!empty($students->data)) {
            foreach ($students->data as $student_key => $student) {
                $row   = array();
                $row[] = "<a href='" . base_url() . "student/view/" . $student->id . "'>" . $student->guardian_name . "</a>";              
                $sch_setting = $this->sch_setting_detail; 
                $row[] = $student->guardian_phone;
                $row[] = "<a href=" . site_url('studentfee/guardian_fees/' . $student->student_session_id) . "  class='btn btn-info' style='padding:10px'>" . "View Fee Detail" . "</a>";

                $dt_data[] = $row;
            }

        }
        $json_data = array(
            "draw"            => intval($students->draw),
            "recordsTotal"    => intval($students->recordsTotal),
            "recordsFiltered" => intval($students->recordsFiltered),
            "data"            => $dt_data,
        );
        echo json_encode($json_data);

    }

    public function feesearch()
    {
        if (!$this->rbac->hasPrivilege('search_due_fees', 'can_view')) {
            access_denied();
        }

        $this->session->set_userdata('top_menu', 'Fees Collection');
        $this->session->set_userdata('sub_menu', 'studentfee/feesearch');
        $data['title']       = 'student fees';
        $class               = $this->class_model->get();
        $data['classlist']   = $class;
        $data['sch_setting'] = $this->sch_setting_detail;
        $feesessiongroup     = $this->feesessiongroup_model->getFeesByGroup();

        $data['feesessiongrouplist'] = $feesessiongroup;
        $data['fees_group']          = "";
        if (isset($_POST['feegroup_id']) && $_POST['feegroup_id'] != '') {
            $data['fees_group'] = $_POST['feegroup_id'];
        }

        $this->form_validation->set_rules('feegroup_id', $this->lang->line('fee_group'), 'trim|required|xss_clean');

        if ($this->form_validation->run() == false) {
            $this->load->view('layout/header', $data);
            $this->load->view('studentfee/studentSearchFee', $data);
            $this->load->view('layout/footer', $data);
        } else {
            $data['student_due_fee'] = array();
            $feegroup_id             = $this->input->post('feegroup_id');
            $feegroup                = explode("-", $feegroup_id);
            $feegroup_id             = $feegroup[0];
            $fee_groups_feetype_id   = $feegroup[1];
            $class_id                = $this->input->post('class_id');
            $section_id              = $this->input->post('section_id');
            $student_due_fee         = $this->studentfee_model->getDueStudentFees($feegroup_id, $fee_groups_feetype_id, $class_id, $section_id);
            if (!empty($student_due_fee)) {
                foreach ($student_due_fee as $student_due_fee_key => $student_due_fee_value) {
                    $amt_due                                                  = $student_due_fee_value['amount'];
                    $student_due_fee[$student_due_fee_key]['amount_discount'] = 0;
                    $student_due_fee[$student_due_fee_key]['amount_fine']     = 0;
                    $a                                                        = json_decode($student_due_fee_value['amount_detail']);
                    if (!empty($a)) {
                        $amount          = 0;
                        $amount_discount = 0;
                        $amount_fine     = 0;

                        foreach ($a as $a_key => $a_value) {
                            $amount          = $amount + $a_value->amount;
                            $amount_discount = $amount_discount + $a_value->amount_discount;
                            $amount_fine     = $amount_fine + $a_value->amount_fine;
                        }
                        if ($amt_due <= $amount) {
                            unset($student_due_fee[$student_due_fee_key]);
                        } else {

                            $student_due_fee[$student_due_fee_key]['amount_detail']   = $amount;
                            $student_due_fee[$student_due_fee_key]['amount_discount'] = $amount_discount;
                            $student_due_fee[$student_due_fee_key]['amount_fine']     = $amount_fine;
                        }
                    }
                }
            }

            $data['student_due_fee'] = $student_due_fee;
            $this->load->view('layout/header', $data);
            $this->load->view('studentfee/studentSearchFee', $data);
            $this->load->view('layout/footer', $data);
        }
    }

    public function reportbyname()
    {
        if (!$this->rbac->hasPrivilege('fees_statement', 'can_view')) {
            access_denied();
        }
        $this->session->set_userdata('top_menu', 'Reports');
        $this->session->set_userdata('sub_menu', 'Reports/finance');
        $this->session->set_userdata('subsub_menu', 'Reports/finance/reportbyname');
        $data['title']       = 'student fees';
        $data['title']       = 'student fees';
        $class               = $this->class_model->get();
        $data['classlist']   = $class;
        $data['sch_setting'] = $this->sch_setting_detail;

        if ($this->input->server('REQUEST_METHOD') == "GET") {

            $this->load->view('layout/header', $data);
            $this->load->view('studentfee/reportByName', $data);
            $this->load->view('layout/footer', $data);
        } else {           
            {

                $data['student_due_fee'] = array();
                $class_id                = $this->input->post('class_id');
                $section_id              = $this->input->post('section_id');
                $student_id              = $this->input->post('student_id');               
                $student_due_fee              = $this->studentfeemaster_model->getStudentFeesByClassSectionStudent($class_id,$section_id,$student_id);      
                $data['student_due_fee']      = $student_due_fee;
                $data['class_id']             = $class_id;
                $data['section_id']           = $section_id;
                $data['student_id']           = $student_id;
                $category                     = $this->category_model->get();
                $data['categorylist']         = $category;
                $this->load->view('layout/header', $data);
                $this->load->view('studentfee/reportByName', $data);
                $this->load->view('layout/footer', $data);
            }
        }
    }

    public function reportbyclass()
    {
        $data['title']     = 'student fees';
        $data['title']     = 'student fees';
        $class             = $this->class_model->get();
        $data['classlist'] = $class;
        if ($this->input->server('REQUEST_METHOD') == "GET") {
            $this->load->view('layout/header', $data);
            $this->load->view('studentfee/reportByClass', $data);
            $this->load->view('layout/footer', $data);
        } else {
            $student_fees_array      = array();
            $class_id                = $this->input->post('class_id');
            $section_id              = $this->input->post('section_id');
            $student_result          = $this->student_model->searchByClassSection($class_id, $section_id);
            $data['student_due_fee'] = array();
            if (!empty($student_result)) {
                foreach ($student_result as $key => $student) {
                    $student_array                      = array();
                    $student_array['student_detail']    = $student;
                    $student_session_id                 = $student['student_session_id'];
                    $student_id                         = $student['id'];
                    $student_due_fee                    = $this->studentfee_model->getDueFeeBystudentSection($class_id, $section_id, $student_session_id);
                    $student_array['fee_detail']        = $student_due_fee;
                    $student_fees_array[$student['id']] = $student_array;
                }
            }
            $data['class_id']           = $class_id;
            $data['section_id']         = $section_id;
            $data['student_fees_array'] = $student_fees_array;
            $this->load->view('layout/header', $data);
            $this->load->view('studentfee/reportByClass', $data);
            $this->load->view('layout/footer', $data);
        }
    }

    public function view($id)
    {
        if (!$this->rbac->hasPrivilege('collect_fees', 'can_view')) {
            access_denied();
        }
        $data['title']      = 'studentfee List';
        $studentfee         = $this->studentfee_model->get($id);
        $data['studentfee'] = $studentfee;
        $this->load->view('layout/header', $data);
        $this->load->view('studentfee/studentfeeShow', $data);
        $this->load->view('layout/footer', $data);
    }

    public function deleteFee()
    {

        if (!$this->rbac->hasPrivilege('collect_fees', 'can_delete')) {
            access_denied();
        }
        $invoice_id  = $this->input->post('main_invoice');
        $sub_invoice = $this->input->post('sub_invoice');
        if (!empty($invoice_id)) {
            $this->studentfee_model->remove($invoice_id, $sub_invoice);
        }
        $array = array('status' => 'success', 'result' => 'success');
        echo json_encode($array);
    }

    public function deleteDiscount()
    {

        if (!$this->rbac->hasPrivilege('collect_fees', 'can_delete')) {
            access_denied();
        }
        $id  = $this->input->post('discount_id');
        if (!empty($id)) {
            $this->feediscount_model->deleteDiscounts($id);
        }
        else{
            echo "aayena";

        }

        $array = array('status' => 'success', 'result' => 'success');
        echo json_encode($array);
    }

    public function deleteStudentDiscount()
    {

        $discount_id = $this->input->post('discount_id');
        if (!empty($discount_id)) {
            $data = array('id' => $discount_id, 'status' => 'assigned', 'payment_id' => "");
            $this->feediscount_model->updateStudentDiscount($data);
        }
        $array = array('status' => 'success', 'result' => 'success');
        echo json_encode($array);
    }

    public function getcollectfee()
    {
        $setting_result      = $this->setting_model->get();
        $data['settinglist'] = $setting_result;
        $record              = $this->input->post('data');
        $record_array        = json_decode($record);

        $fees_array = array();
        foreach ($record_array as $key => $value) {
            $fee_groups_feetype_id = $value->fee_groups_feetype_id;
            $fee_master_id         = $value->fee_master_id;
            $fee_session_group_id  = $value->fee_session_group_id;
            $feeList               = $this->studentfeemaster_model->getDueFeeByFeeSessionGroupFeetype($fee_session_group_id, $fee_master_id, $fee_groups_feetype_id);
            $fees_array[]          = $feeList;
        }
        $data['feearray'] = $fees_array;
        $result           = array(
            'view' => $this->load->view('studentfee/getcollectfee', $data, true),
        );

        $this->output->set_output(json_encode($result));
    }

    public function addfee($id)
    {
        if (!$this->rbac->hasPrivilege('collect_fees', 'can_add')) {
            access_denied();
        }
        $data['sch_setting']  = $this->sch_setting_detail;
        $data['title']        = 'Student Detail';
        $student              = $this->student_model->getByStudentSession($id);
        $data['student']      = $student;
        $student_due_fee      = $this->studentfeemaster_model->getStudentFees($id);
        $student_discount_fee = $this->feediscount_model->getStudentFeesDiscount($id);

        $data['student_discount_fee'] = $student_discount_fee;
        $data['student_due_fee']      = $student_due_fee;
        $category                     = $this->category_model->get();
        $data['categorylist']         = $category;
        $class_section                = $this->student_model->getClassSection($student["class_id"]);
        $data["class_section"]        = $class_section;
        $session                      = $this->setting_model->getCurrentSession();
        $studentlistbysection         = $this->student_model->getStudentClassSection($student["class_id"], $session);
        $data["studentlistbysection"] = $studentlistbysection;

        $this->load->view('layout/header', $data); 
        $this->load->view('studentfee/studentAddfee', $data);
        $this->load->view('layout/footer', $data);
    }

    public function deleteTransportFee()
    {
        $id = $this->input->post('feeid');
        $this->studenttransportfee_model->remove($id);
        $array = array('status' => 'success', 'result' => 'success');
        echo json_encode($array);
    }

    public function delete($id)
    {
        $data['title'] = 'studentfee List';
        $this->studentfee_model->remove($id);
        redirect('studentfee/index');
    }

    public function create()
    {
        if (!$this->rbac->hasPrivilege('collect_fees', 'can_view')) {
            access_denied();
        }
        $data['title'] = 'Add studentfee';
        $this->form_validation->set_rules('category', $this->lang->line('category'), 'trim|required|xss_clean');
        if ($this->form_validation->run() == false) {
            $this->load->view('layout/header', $data);
            $this->load->view('studentfee/studentfeeCreate', $data);
            $this->load->view('layout/footer', $data);
        } else {
            $data = array(
                'category' => $this->input->post('category'),
            );
            $this->studentfee_model->add($data);
            $this->session->set_flashdata('msg', '<div studentfee="alert alert-success text-center">' . $this->lang->line('success_message') . '</div>');
            redirect('studentfee/index');
        }
    }

    public function edit($id)
    {
        if (!$this->rbac->hasPrivilege('collect_fees', 'can_edit')) {
            access_denied();
        }
        $data['title']      = 'Edit studentfees';
        $data['id']         = $id;
        $studentfee         = $this->studentfee_model->get($id);
        $data['studentfee'] = $studentfee;
        $this->form_validation->set_rules('category', $this->lang->line('category'), 'trim|required|xss_clean');
        if ($this->form_validation->run() == false) {
            $this->load->view('layout/header', $data);
            $this->load->view('studentfee/studentfeeEdit', $data);
            $this->load->view('layout/footer', $data);
        } else {
            $data = array(
                'id'       => $id,
                'category' => $this->input->post('category'),
            );
            $this->studentfee_model->add($data);
            $this->session->set_flashdata('msg', '<div studentfee="alert alert-success text-center">' . $this->lang->line('update_message') . '</div>');
            redirect('studentfee/index');
        }
    }

    public function addstudentfee()
    {

        $this->form_validation->set_rules('student_fees_master_id', $this->lang->line('fee_master'), 'required|trim|xss_clean');
        $this->form_validation->set_rules('fee_groups_feetype_id', $this->lang->line('student'), 'required|trim|xss_clean');
        $this->form_validation->set_rules('amount', $this->lang->line('amount'), 'required|trim|xss_clean|numeric|callback_check_deposit');
        $this->form_validation->set_rules('amount_discount', $this->lang->line('discount'), 'required|numeric|trim|xss_clean');
        $this->form_validation->set_rules('amount_fine', $this->lang->line('fine'), 'required|trim|numeric|xss_clean');
        $this->form_validation->set_rules('payment_mode', $this->lang->line('payment_mode'), 'required|trim|xss_clean');

        if ($this->form_validation->run() == false) {
            $data = array(
                'amount'                 => form_error('amount'),
                'student_fees_master_id' => form_error('student_fees_master_id'),
                'fee_groups_feetype_id'  => form_error('fee_groups_feetype_id'),
                'amount_discount'        => form_error('amount_discount'),
                'amount_fine'            => form_error('amount_fine'),
                'payment_mode'           => form_error('payment_mode'),
            );
            $array = array('status' => 'fail', 'error' => $data);
            echo json_encode($array);
        }else{
            if($this->customlib->getcalender()->calender=="0"){
                $newFromDate = $this->customlib->englishdate(date('Y-m-d', $this->customlib->datetostrtotime($this->input->post('date'))));
            }else{
                $newFromDate = date('Y-m-d', $this->customlib->datetostrtotime($this->input->post('date')));
            }

            $staff_record = $this->staff_model->get($this->customlib->getStaffID());

            $collected_by             = $this->customlib->getAdminSessionUserName() . "(" . $staff_record['employee_id'] . ")";
            $student_fees_discount_id = $this->input->post('student_fees_discount_id');
            $json_array               = array(
                'amount'          => $this->input->post('amount'),
                'date'            => date('Y-m-d', $this->customlib->datetostrtotime($this->input->post('date'))),
                'amount_discount' => $this->input->post('amount_discount'),
                'amount_fine'     => $this->input->post('amount_fine'),
                'description'     => $this->input->post('description'),
                'collected_by'    => $collected_by,
                'payment_mode'    => $this->input->post('payment_mode'),
                'received_by'     => $staff_record['id'],
            );
            $data = array(
                'student_fees_master_id' => $this->input->post('student_fees_master_id'),
                'fee_groups_feetype_id'  => $this->input->post('fee_groups_feetype_id'),
                'amount_detail'          => $json_array,
            );

            $action             = $this->input->post('action');
            $send_to            = $this->input->post('guardian_phone');
            $email              = $this->input->post('guardian_email');
            $parent_app_key     = $this->input->post('parent_app_key');
            $student_session_id = $this->input->post('student_session_id');
            $inserted_id        = $this->studentfeemaster_model->fee_deposit($data, $send_to, $student_fees_discount_id);


            $token = $this->input->cookie('Token');
            if($this->input->post('amount')>0 || $this->input->post('amount')!==""){
                $account_subdata[]= array(

                    'Debit'=> $this->input->post('amount'),
                    'Credit' => "0",
                    'Remarks'=>  $this->input->post('title'),
                    'GLId' =>  $this->input->post('glname'),
                );
                $account_subdata[]= array(
                    'Debit'=> "0",
                    'Credit' => $this->input->post('amount'),
                    'Remarks'=>  $this->input->post('payment_mode'),
                    'GLId' =>  "16789",
                );
            }
            $subfinal=json_encode($account_subdata);

            $url = 'http://actm.prabhumanagement.com/api/Voucher/CreateJV';
            $ch = curl_init($url);
            $ip = getenv('HTTP_CLIENT_IP')?:
            getenv('HTTP_X_FORWARDED_FOR')?:
            getenv('HTTP_X_FORWARDED')?:
            getenv('HTTP_FORWARDED_FOR')?:
            getenv('HTTP_FORWARDED')?:
            getenv('REMOTE_ADDR');

            $data = array(
                'CreatedBy'=>$this->customlib->getAdminSessionUserName(),
                'FiscalYear' => $this->setting_model->getCurrentSessionName(),
                'Narration'=> "Fee Collection",
                'CreatedDate' => $newFromDate,
                'TransId'=>'925446CA-C2FB-4266-BD1A-F9CE87583424',
                'JVVoucherDetails'=>$account_subdata
            );
            $final=json_encode($data);

            $response=curl_setopt($ch, CURLOPT_POSTFIELDS, $final);

            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                "X-Custom-Header: value",
                "Content-Type: application/json",
                "Authorization: token ".$token,
            ));

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $response = curl_exec($ch);
            curl_close($ch);
                    // echo $response;

            $mailsms_array      = $this->feegrouptype_model->getFeeGroupByID($this->input->post('fee_groups_feetype_id'));

            $print_record       = array();
            if ($action == "print") {
                $receipt_data           = json_decode($inserted_id);
                $data['sch_setting']    = $this->sch_setting_detail;
                $fee_record             = $this->studentfeemaster_model->getFeeByInvoice($receipt_data->invoice_id, $receipt_data->sub_invoice_id);
                $student                = $this->studentsession_model->searchStudentsBySession($student_session_id);
                $data['student']        = $student;
                $data['sub_invoice_id'] = $receipt_data->sub_invoice_id;
                $data['feeList']        = $fee_record;
                $print_record           = $this->load->view('print/printFeesByName', $data, true);
            }
            $mailsms_array->invoice        = $inserted_id;
            $mailsms_array->contact_no     = $send_to;
            $mailsms_array->email          = $email;
            $mailsms_array->parent_app_key = $parent_app_key;

            $this->mailsmsconf->mailsms('fee_submission', $mailsms_array);

            $array = array('status' => 'success', 'error' => '', 'print' => $print_record);
            echo json_encode($array);

            // redirect($_SERVER['HTTP_REFERER']);

        }
    }


    public function printFeesByName()
    {
        $data                   = array('payment' => "0");
        $record                 = $this->input->post('data');
        $invoice_id             = $this->input->post('main_invoice');
        $sub_invoice_id         = $this->input->post('sub_invoice');
        $student_session_id     = $this->input->post('student_session_id');
        $setting_result         = $this->setting_model->get();
        $data['settinglist']    = $setting_result;
        $student                = $this->studentsession_model->searchStudentsBySession($student_session_id);
        $fee_record             = $this->studentfeemaster_model->getFeeByInvoice($invoice_id, $sub_invoice_id);
        $data['student']        = $student;
        $data['sub_invoice_id'] = $sub_invoice_id;
        $data['feeList']        = $fee_record;
        $data['sch_setting']    = $this->sch_setting_detail;
        $this->load->view('print/printFeesByName', $data);
    }

    public function printFeesByGroup()
    {
        $fee_groups_feetype_id = $this->input->post('fee_groups_feetype_id');
        $fee_master_id         = $this->input->post('fee_master_id');
        $fee_session_group_id  = $this->input->post('fee_session_group_id');
        $setting_result        = $this->setting_model->get();
        $data['settinglist']   = $setting_result;
        $data['feeList']       = $this->studentfeemaster_model->getDueFeeByFeeSessionGroupFeetype($fee_session_group_id, $fee_master_id, $fee_groups_feetype_id);
        $data['sch_setting']   = $this->sch_setting_detail;
        $this->load->view('print/printFeesByGroup', $data);
    }

    public function printFeesByGroupArray()
    {
        $data['sch_setting'] = $this->sch_setting_detail;
        $record              = $this->input->post('data');
        $record_array        = json_decode($record);
        $fees_array          = array();
        foreach ($record_array as $key => $value) {
            $fee_groups_feetype_id = $value->fee_groups_feetype_id;
            $fee_master_id         = $value->fee_master_id;
            $fee_session_group_id  = $value->fee_session_group_id;
            $feeList               = $this->studentfeemaster_model->getDueFeeByFeeSessionGroupFeetype($fee_session_group_id, $fee_master_id, $fee_groups_feetype_id);
            $fees_array[]          = $feeList;
        }
        $data['feearray'] = $fees_array;
        $this->load->view('print/printFeesByGroupArray', $data);
    }

    public function printMonthlybill()
    {
        $data['sch_setting'] = $this->sch_setting_detail;
        $record              = $this->input->post('data');
        print_r($record);
        $record_array        = json_decode($record);
        $fees_array          = array();
        foreach ($record_array as $key => $value) {
            $fee_groups_feetype_id = $value->fee_groups_feetype_id;
            $fee_master_id         = $value->fee_master_id;
            $fee_session_group_id  = $value->fee_session_group_id;
            $feeList               = $this->studentfeemaster_model->getDueFeeByFeeSessionGroupFeetype($fee_session_group_id, $fee_master_id, $fee_groups_feetype_id);
            $fees_array[]          = $feeList;
        }
        $data['feearray'] = $fees_array;
        $data['record'] = $record;
        $this->load->view('print/printMonthlybill', $data);
    }

    public function printToday()
    {
        $data['sch_setting'] = $this->sch_setting_detail;
        $record              = $this->input->post('data');
        $record_array        = json_decode($record);
        $fees_array          = array();
        foreach ($record_array as $key => $value) {
            $fee_groups_feetype_id = $value->fee_groups_feetype_id;
            $fee_master_id         = $value->fee_master_id;
            $fee_session_group_id  = $value->fee_session_group_id;
            $feeList               = $this->studentfeemaster_model->getDueFeeByFeeSessionGroupFeetype($fee_session_group_id, $fee_master_id, $fee_groups_feetype_id);
            $fees_array[]          = $feeList;
        }
        $data['feearray'] = $fees_array;
        $data['record'] = $record;
        $this->load->view('print/printToday', $data);
    }

    public function searchpayment()
    {
        if (!$this->rbac->hasPrivilege('search_fees_payment', 'can_view')) {
            access_denied();
        }
        $this->session->set_userdata('top_menu', 'Fees Collection');
        $this->session->set_userdata('sub_menu', 'studentfee/searchpayment');
        $data['title'] = 'Edit studentfees';

        $this->form_validation->set_rules('paymentid', $this->lang->line('payment_id'), 'trim|required|xss_clean');
        if ($this->form_validation->run() == false) {

        } else {
            $paymentid = $this->input->post('paymentid');
            $invoice   = explode("/", $paymentid);

            if (array_key_exists(0, $invoice) && array_key_exists(1, $invoice)) {
                $invoice_id             = $invoice[0];
                $sub_invoice_id         = $invoice[1];
                $feeList                = $this->studentfeemaster_model->getFeeByInvoice($invoice_id, $sub_invoice_id);
                $data['feeList']        = $feeList;
                $data['sub_invoice_id'] = $sub_invoice_id;
            } else {
                $data['feeList'] = array();
            }
        }
        $data['sch_setting'] = $this->sch_setting_detail;
        $this->load->view('layout/header', $data);
        $this->load->view('studentfee/searchpayment', $data);
        $this->load->view('layout/footer', $data);
    }

    public function addfeegroup()
    {
        $this->form_validation->set_rules('fee_session_groups', $this->lang->line('fee_group'), 'required|trim|xss_clean');

        if ($this->form_validation->run() == false) {
            $data = array(
                'fee_session_groups' => form_error('fee_session_groups'),
            );
            $array = array('status' => 'fail', 'error' => $data);
            echo json_encode($array);
        } else {
            $student_session_id     = $this->input->post('student_session_id');
            $fee_session_groups     = $this->input->post('fee_session_groups');
            $student_sesssion_array = isset($student_session_id) ? $student_session_id : array();
            $student_ids            = $this->input->post('student_ids');
            $delete_student         = array_diff($student_ids, $student_sesssion_array);
            $fees_allocated         = $this->input->post('totalfee');
            $months                 = $this->input->post('month_id');
            $transid                = $this->input->post('transid');
            $submissiontype         = $this->input->post('submissiontype');
            $fee_group_feetype_discount= $this->input->post('fee_group_typeinput');
            $discount_session       = $this->input->post('discount_session_id');

            $fee_discount           = isset($discount_session) ? $discount_session : array();
            $amount_detail          = $this->input->post('amount_discount');
            $created_by             = $this->input->post('collected_by');
            $name                   = $this->input->post('discount_name');


            $total_assigned = 0;
            $preserve_record = array();
            if (!empty($student_sesssion_array)) {
                foreach ($student_sesssion_array as $key => $value) {
                    $insert_array = array(
                        'student_session_id'   => $value,
                        'fee_session_group_id' => $fee_session_groups,
                        'months'               => $months,
                        'transid'              => $transid,
                    );

                    if($this->studentfeemaster_model->add($insert_array)){
                        $inserted_id = $this->studentfeemaster_model->add($insert_array);
                        $preserve_record[$value] = $inserted_id;
                    }  
                    $total_assigned = $total_assigned + $fees_allocated;
                }

                if (!empty($fee_discount)) {
                    foreach($fee_discount as $discount_value){
                        $disamount = "amount_discount_" . $discount_value;
                        $disname = "discount_name_" . $discount_value;
                        $discount_amt = $this->input->post('amount_discount_'.$discount_value);
                        $discountname = $this->input->post('discount_name_'.$discount_value);
                        $discount_insert = array(
                            'student_fees_master_id'   => $preserve_record[$discount_value],
                            'fee_groups_feetype_id'    => $fee_group_feetype_discount,
                            'amount_detail'            => $discount_amt,
                            'created_by'               => $created_by,
                            'created_at'               => date('Y-m-d', $this->customlib->datetostrtotime($this->input->post('today'))),
                            'name'                     => $discountname,
                        );
                        $this->student_discount_applied_model->discount_applied($discount_insert);
                    }
                }
                $this->create_voucher($total_assigned,$transid);
            }



            if (!empty($delete_student)) {
                if($submissiontype=="1"){
                    $this->studentfeemaster_model->deleteannual($fee_session_groups, $delete_student);
                }else{
                    $this->studentfeemaster_model->delete($fee_session_groups, $delete_student,$months);
                }

            }

            $array = array('status' => 'Success', 'error' => '', 'message' => $this->lang->line('success_message'));

            echo json_encode($array);


        }
    }

    public function applydiscount(){
        $student_session_id     = $this->input->post('discount_student');
        $fee_session_groups     = $this->input->post('fee_session_groups');
        $student_sesssion_array = isset($student_session_id) ? $student_session_id : array();
        $student_fees_master_id = $this->input->post('student_fees_master_id');
        $discount_amount        = $this->input->post('amount_discount');
        $fee_group_typeinput    = $this->input->post('fee_group_typeinput');
        $collected_by           = $this->input->post('collected_by');
        $discount_name          = $this->input->post('discount_name');
        if($this->customlib->getcalender()->calender=="0"){
            $currentdate    = $this->customlib->currentdate();
        }else{
            $currentdate    = date('y-m-d');
        }

        if (!empty($student_sesssion_array)) {
            foreach ($student_sesssion_array as $key => $value) {
                $insert_array = array(
                    'student_fees_master_id'   => $student_fees_master_id,
                    'fee_groups_feetype_id' => $fee_group_typeinput,
                    'amount_detail'         => $discount_amount,
                    'created_at'            => $currentdate,
                    'created_by'            => $collected_by,
                    'name'                  => $discount_name,
                );

                $this->student_discount_applied_model->insert($insert_array);
            }
        }
    }

    public function geBalanceFee()
    {
        $this->form_validation->set_rules('fee_groups_feetype_id', $this->lang->line('fee_groups_feetype_id'), 'required|trim|xss_clean');
        $this->form_validation->set_rules('student_fees_master_id', 'student_fees_master_id', 'required|trim|xss_clean');
        $this->form_validation->set_rules('student_session_id', 'student_session_id', 'required|trim|xss_clean');

        if ($this->form_validation->run() == false) {
            $data = array(
                'fee_groups_feetype_id'  => form_error('fee_groups_feetype_id'),
                'student_fees_master_id' => form_error('student_fees_master_id'),
            );
            $array = array('status' => 'fail', 'error' => $data);
            echo json_encode($array);
        } else {
            $data                   = array();
            $student_session_id     = $this->input->post('student_session_id');
            $fee_groups_feetype_id  = $this->input->post('fee_groups_feetype_id');
            $student_fees_master_id = $this->input->post('student_fees_master_id');
            $remain_amount_object   = $this->getStuFeetypeBalance($fee_groups_feetype_id, $student_fees_master_id);
            $discount_not_applied   = $this->getNotAppliedDiscount($student_session_id);
            $remain_amount          = json_decode($remain_amount_object)->balance;
            $remain_amount_fine     = json_decode($remain_amount_object)->fine_amount;


            $array = array('status' => 'success', 'error' => '', 'balance' => $remain_amount, 'discount_not_applied' => $discount_not_applied, 'remain_amount_fine' => $remain_amount_fine);
            echo json_encode($array);
        }
    }

    public function getStuFeetypeBalance($fee_groups_feetype_id, $student_fees_master_id)
    {
        $data                           = array();
        $data['fee_groups_feetype_id']  = $fee_groups_feetype_id;
        $data['student_fees_master_id'] = $student_fees_master_id;
        $result                         = $this->studentfeemaster_model->studentDeposit($data);
        $amount_balance                 = 0;
        $amount                         = 0;
        $amount_fine                    = 0;
        $amount_discount                = 0;
        $fine_amount                    = 0;
        $fee_fine_amount                = 0;
        $due_amt                        = $result->amount;
        if (strtotime($result->due_date) < strtotime(date('Y-m-d'))) {
            $fee_fine_amount = $result->fine_amount;
        }

        if ($result->is_system) {
            $due_amt = $result->student_fees_master_amount;
        }

        $amount_detail = json_decode($result->amount_detail);
        if (is_object($amount_detail)) {

            foreach ($amount_detail as $amount_detail_key => $amount_detail_value) {
                $amount          = $amount + $amount_detail_value->amount;
                $amount_discount = $amount_discount + $amount_detail_value->amount_discount;
                $amount_fine     = $amount_fine + $amount_detail_value->amount_fine;
            }
        }

        $amount_balance = $due_amt - ($amount + $amount_discount);
        $fine_amount    = abs($amount_fine - $fee_fine_amount);
        $array          = array('status' => 'success', 'error' => '', 'balance' => $amount_balance, 'fine_amount' => $fine_amount);
        return json_encode($array);
    }

    public function check_deposit($amount)
    {
        if (is_numeric($this->input->post('amount')) && is_numeric($this->input->post('amount_discount'))) {
            if ($this->input->post('amount') != "" && $this->input->post('amount_discount') != "") {
                if ($this->input->post('amount') < 0) {
                    $this->form_validation->set_message('check_deposit', $this->lang->line('deposit_amount_can_not_be_less_than_zero'));
                    return false;
                } else {
                    $student_fees_master_id = $this->input->post('student_fees_master_id');
                    $fee_groups_feetype_id  = $this->input->post('fee_groups_feetype_id');
                    $deposit_amount         = $this->input->post('amount') + $this->input->post('amount_discount');
                    $remain_amount          = $this->getStuFeetypeBalance($fee_groups_feetype_id, $student_fees_master_id);
                    $remain_amount          = json_decode($remain_amount)->balance;
                    if ($remain_amount < $deposit_amount) {
                        $this->form_validation->set_message('check_deposit', $this->lang->line('deposit_amount_can_not_be_greater_than_remaining'));
                        return false;
                    } else {
                        return true;
                    }
                }
                return true;
            }
        } elseif (!is_numeric($this->input->post('amount'))) {

            $this->form_validation->set_message('check_deposit', $this->lang->line('amount') . " " . $this->lang->line('field_must_contain_only_numbers'));

            return false;
        } elseif (!is_numeric($this->input->post('amount_discount'))) {

            return true;
        }

        return true;
    }

    public function getNotAppliedDiscount($student_session_id)
    {
        return $this->feediscount_model->getDiscountNotApplied($student_session_id);
    }

    public function addfeegrp()
    {
        $staff_record = $this->session->userdata('admin');
        $this->form_validation->set_error_delimiters('', '');
        $this->form_validation->set_rules('row_counter[]', $this->lang->line('fees_list'), 'required|trim|xss_clean');
        $this->form_validation->set_rules('collected_date', $this->lang->line('date'), 'required|trim|xss_clean');
        $this->form_validation->set_rules('totalpay', $this->lang->line('date'), 'required');

        if ($this->form_validation->run() == false) {
            $data = array(
                'row_counter'    => form_error('row_counter'),
                'collected_date' => form_error('collected_date'),
                'totalpay'      => form_error('totalpay'),
            );
            $array = array('status' => 0, 'error' => $data);
            echo json_encode($array);
        } else {
            $collected_array = array();
            $collected_by    = $this->customlib->getAdminSessionUserName();

            $send_to            = $this->input->post('guardian_phone');
            $email              = $this->input->post('guardian_email');
            $parent_app_key     = $this->input->post('parent_app_key');
            $student_session_id = $this->input->post('student_session_id');
            $total_row = $this->input->post('row_counter');
            $totalpay = $this->input->post('totalpaid');
            $remainain_amount= $this->input->post('totalpaid');
            $paid_amount = $this->input->post('totalpaid');
            $transid = $this->input->post('trans_id');

            $account_subdata=array();
            if($totalpay!==0){
                $account_subdata[] =array(
                    'Credit' => $totalpay,
                    'Debit' => 0,
                    'Remarks' => $this->input->post('title_' . $total_row_value),
                    'GLId' => '17797',
                );
                if($payment_mode_fee="Cash"){
                    $account_subdata[] =array(
                        'Credit' => 0,
                        'Debit' => $totalpay,
                        'Remarks' => $payment_mode_fee,
                        'GLId' => '17799',
                    );
                }elseif ($payment_mode_fee="Cheque") {
                    $account_subdata[] =array(
                        'Credit' => 0,
                        'Debit' => $totalpay,
                        'Remarks' => $payment_mode_fee,
                        'GLId' => '17802',
                    );
                }elseif ($payment_mode_fee="bank_transfer") {
                    $account_subdata[] =array(
                        'Credit' => 0,
                        'Debit' => $totalpay,
                        'Remarks' => $payment_mode_fee,
                        'GLId' => '17803',
                    );
                }
                elseif ($payment_mode_fee="card") {
                    $account_subdata[] =array(
                        'Credit' => 0,
                        'Debit' => $totalpay,
                        'Remarks' => $payment_mode_fee,
                        'GLId' => '17804',
                    );
                }elseif ($payment_mode_fee="prabhu_pay") {
                    $account_subdata[] =array(
                        'Credit' => 0,
                        'Debit' => $totalpay,
                        'Remarks' => $payment_mode_fee,
                        'GLId' => '17806',
                    );
                }
                $account_data = array(
                    'CreatedBy'=>$this->customlib->getAdminSessionUserName(),
                    'FiscalYear' => $this->setting_model->getCurrentSessionName(),
                    'Narration'=> 'fee payment',
                    'CreatedDate' => $this->customlib->englishdate(date('Y-m-d', $this->customlib->datetostrtotime($this->input->post('collected_date')))),
                    'TransId'=>$transid,
                    'RefNumber' => '11',
                    'JVVoucherDetails'=>$account_subdata
                );
            }

            $final=json_encode($account_data);
            $token = $this->input->cookie('Token');
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => 'http://actm.prabhumanagement.com/api/Voucher/CreateJV',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS =>$final,
                CURLOPT_HTTPHEADER => array(
                    'Authorization: Bearer ' . $token,
                    'Content-Type: application/json'
                ),
            ));
            $response = curl_exec($curl);
            $result = json_decode($response,true);
            if($result['StatusCode']==00){
            // echo "test";
                foreach ($total_row as $total_row_key => $total_row_value) {
                    $this->input->post('student_fees_master_id_' . $total_row_value);
                    $this->input->post('fee_groups_feetype_id_' . $total_row_value);

                    if($paid_amount > 0){

                        $payable_amount = $this->input->post('fee_amount_' . $total_row_value);
                        if($paid_amount > $payable_amount){
                            $json_array = array(
                                'amount'          => $payable_amount,
                                'date'            => date('Y-m-d', $this->customlib->datetostrtotime($this->input->post('collected_date'))),
                                'description'     => $this->input->post('fee_gupcollected_note'),
                                'collected_by'      =>$collected_by,
                                'amount_discount' => 0,
                                'amount_fine'     => $this->input->post('fee_groups_feetype_fine_amount_' . $total_row_value),
                                'payment_mode'    => $this->input->post('payment_mode_fee'),
                                'received_by'     => $staff_record['id'],
                            );
                        }elseif($paid_amount!==0 && $paid_amount < $payable_amount){
                            $payable_amount = $paid_amount;
                            $json_array = array(
                                'amount'          => $paid_amount,
                                'date'            => date('Y-m-d', $this->customlib->datetostrtotime($this->input->post('collected_date'))),
                                'description'     => $this->input->post('fee_gupcollected_note'),
                                'collected_by'      =>$collected_by,
                                'amount_discount' => 0,
                                'amount_fine'     => $this->input->post('fee_groups_feetype_fine_amount_' . $total_row_value),
                                'payment_mode'    => $this->input->post('payment_mode_fee'),
                                'received_by'     => $staff_record['id'],
                            );
                        }elseif ($paid_amount!==0 && $paid_amount==$payable_amount) {
                            $json_array = array(
                                'amount'          => $payable_amount,
                                'date'            => date('Y-m-d', $this->customlib->datetostrtotime($this->input->post('collected_date'))),
                                'description'     => $this->input->post('fee_gupcollected_note'),
                                'collected_by'      =>$collected_by,
                                'amount_discount' => 0,
                                'amount_fine'     => $this->input->post('fee_groups_feetype_fine_amount_' . $total_row_value),
                                'payment_mode'    => $this->input->post('payment_mode_fee'),
                                'received_by'     => $staff_record['id'],
                            );
                        }
                        $paid_amount = $paid_amount - $payable_amount;
                    }else{
                        break;
                    }

                    if(isset($json_array)){
                        $collected_array[] = array(
                            'student_fees_master_id' => $this->input->post('student_fees_master_id_' . $total_row_value),
                            'fee_groups_feetype_id'  => $this->input->post('fee_groups_feetype_id_' . $total_row_value),
                            'transid'                  => $transid,
                            'amount_detail'          => $json_array,
                        );
                    }
                }
                $payment_mode_fee = $this->input->post('payment_mode_fee');
                $deposited_fees = $this->studentfeemaster_model->fee_deposit_collections($collected_array);
                $fees_record    = json_decode($deposited_fees);
                if(count($deposited_fees)>1){
                    foreach ($total_row as $total_row_key => $total_row_value) {
                        $mailsms_array                 = $this->feegrouptype_model->getFeeGroupByID($this->input->post('fee_groups_feetype_id_' . $total_row_value));
                        $mailsms_array->invoice        = json_encode($fees_record[$total_row_key]);
                        $mailsms_array->contact_no     = $send_to;
                        $mailsms_array->email          = $email;
                        $mailsms_array->parent_app_key = $parent_app_key;
                        $this->mailsmsconf->mailsms('fee_submission', $mailsms_array);
                    }
                }else{
                    $mailsms_array      = $this->feegrouptype_model->getFeeGroupByID($this->input->post('fee_groups_feetype_id'));
                    $mailsms_array->invoice        = $inserted_id;
                    $mailsms_array->contact_no     = $send_to;
                    $mailsms_array->email          = $email;
                    $mailsms_array->parent_app_key = $parent_app_key;

                    $this->mailsmsconf->mailsms('fee_submission', $mailsms_array);
                }

                $array = array('status' => 1, 'error' => '');
                echo json_encode($array);

            // $array = array('status' => 'Success', 'error' => '', 'message' => $this->lang->line('success_message'));
            // echo json_encode($array);
            }

        }
    }







    // public function getdiscountapplied(){
    //     $this->form_validation->set_rules('created_at', 'errormessage', 'trim|required|xss_clean');
    //     $this->form_validation->set_rules('amount_detail', 'errormessage', 'trim|required|xss_clean');
    //     $staff_record = $this->session->userdata('admin');
    //         $collected_by    = $this->customlib->getAdminSessionUserName();

    //         $send_to            = $this->input->post('guardian_phone');
    //         $email              = $this->input->post('guardian_email');
    //         $parent_app_key     = $this->input->post('parent_app_key');
    //         $student_session_id = $this->input->post('student_session_id');



    //         $data = array(
    //             'student_fees_master_id'   => $this->input->post('student_fees_master_id'),
    //             'fee_groups_feetype_id'    => $this->input->post('fee_groups_feetype_id'),
    //             'amount_detail'            => $this->input->post('amount_discount'),
    //             'created_by'               => $collected_by,
    //             'created_at'               => date('Y-m-d', $this->customlib->datetostrtotime($this->input->post('collected_date'))),
    //             'name'                     => $this->input->post('discount_name'),
    //         );

    //         $this->student_discount_applied_model->discount_applied($data);
    //     }
    //     // $this->form_validation->set_error_delimiters('', '');
    //     // $this->form_validation->set_rules('created_at', $this->lang->line('date'), 'required|trim|xss_clean');
    //     // $this->form_validation->set_rules('amount_detail',$this->lang->line('discount_amount'),"required|trim|xss_clean");

    //     // if ($this->form_validation->run() == false) {
    //     //     $data = array(
    //     //         // 'created_at' => form_error('collected_date'),
    //     //         // 'amount_detail' => form_error('amount_detail'),
    //     //     );
    //     //     $array = array('status' => 0, 'error' => $data);
    //     //     echo json_encode($array);
    //     // } else {
    //         // $discount = array();

    //     // $staff_record = $this->session->userdata('admin');
    //     //     $collected_by    = $this->customlib->getAdminSessionUserName();

    //     //     $send_to            = $this->input->post('guardian_phone');
    //     //     $email              = $this->input->post('guardian_email');
    //     //     $parent_app_key     = $this->input->post('parent_app_key');
    //     //     $student_session_id = $this->input->post('student_session_id');



    //     //     $data = array(
    //     //         'student_fees_master_id'   => $this->input->post('student_fees_master_id'),
    //     //         'fee_groups_feetype_id'    => $this->input->post('fee_groups_feetype_id'),
    //     //         'amount_detail'            => $this->input->post('amount_discount'),
    //     //         'created_by'               => $collected_by,
    //     //         'created_at'               => date('Y-m-d', $this->customlib->datetostrtotime($this->                           input->post('collected_date'))),
    //     //         'name'                     => $this->input->post('discount_name'),
    //     //     );
    //     //     // print_r ($data);
    //     //     // die();
    //     //     $discount_applied = $this->student_discount_applied_model->discount_applied($data);
    //     //     // $student_discount = json_decode($discount_applied);
    //     //     $this->session->set_flashdata('msg', '<div class="alert alert-success text-left">' . $this->lang->line('success_message') . '</div>');
    //     //     redirect('admin/book/index');
    //     //     echo $student_discount;
    //     //     // foreach ($total_row as $total_row_key => $total_row_value) {
    //     //     //     $mailsms_array                 = $this->feegrouptype_model->getFeeGroupByID($this->input->post('fee_groups_feetype_id_' . $total_row_value));
    //     //     //     $mailsms_array->invoice        = json_encode($fees_record[]);
    //     //     //     $mailsms_array->contact_no     = $send_to;
    //     //     //     $mailsms_array->email          = $email;
    //     //     //     $mailsms_array->parent_app_key = $parent_app_key;
    //     //     //     $this->mailsmsconf->mailsms('fee_submission', $mailsms_array);
    //     //     // }
    //     //     // $array = array('status' => 1, 'error' => '');
    //     //     // echo json_encode($discount);
    //     // // }
    // // }












    public function reportdailycollection()
    {
        if (!$this->rbac->hasPrivilege('fees_statement', 'can_view')) {
            access_denied();
        }
        $this->session->set_userdata('top_menu', 'Reports');
        $this->session->set_userdata('sub_menu', 'Reports/finance');
        $this->session->set_userdata('subsub_menu', 'Reports/finance/reportdailycollection');
        $data          = array();
        $data['title'] = 'Daily Collection Report';
        $this->form_validation->set_rules('date_from', $this->lang->line('date_from'), 'trim|required|xss_clean');
        $this->form_validation->set_rules('date_to', $this->lang->line('date_to'), 'trim|required|xss_clean');

        if ($this->form_validation->run() == true) {

            $date_from          = $this->input->post('date_from');
            $date_to            = $this->input->post('date_to');
            $formated_date_from = strtotime($this->customlib->dateFormatToYYYYMMDD($date_from));
            $formated_date_to   = strtotime($this->customlib->dateFormatToYYYYMMDD($date_to));
            $st_fees            = $this->studentfeemaster_model->getCurrentSessionStudentFees();
            $fees_data=array();

            for ($i=$formated_date_from; $i <= $formated_date_to ; $i+=86400) {
                $fees_data[$i]['amt']=0;
                $fees_data[$i]['count']=0;
                $fees_data[$i]['student_fees_deposite_ids']=array();

            }

            if (!empty($st_fees)) {
                foreach ($st_fees as $fee_key => $fee_value) {
                    if (isJSON($fee_value->amount_detail)) {
                        $fees_details = (json_decode($fee_value->amount_detail));
                        if (!empty($fees_details)) {
                            foreach ($fees_details as $fees_detail_key => $fees_detail_value) {
                                $date = strtotime($fees_detail_value->date);
                                if ($date >= $formated_date_from && $date <= $formated_date_to) {
                                    if (array_key_exists($date,$fees_data)){
                                        $fees_data[$date]['amt']+=$fees_detail_value->amount+$fees_detail_value->amount_fine; 
                                        $fees_data[$date]['count']+=1; 
                                        $fees_data[$date]['student_fees_deposite_ids'][]=$fee_value->student_fees_deposite_id; 
                                    }else{
                                        $fees_data[$date]['amt']=$fees_detail_value->amount+$fees_detail_value->amount_fine; 
                                        $fees_data[$date]['count']=1; 
                                        $fees_data[$date]['student_fees_deposite_ids'][]=$fee_value->student_fees_deposite_id; 
                                    }                                 
                                }                            
                            }
                        }
                    }
                }
            }
            $data['fees_data']=$fees_data;           

        }

        $this->load->view('layout/header', $data);
        $this->load->view('reports/reportdailycollection', $data);
        $this->load->view('layout/footer', $data);
    }

    public function reportduefees()
    {
        if (!$this->rbac->hasPrivilege('fees_statement', 'can_view')) {
            access_denied();
        }
        $this->session->set_userdata('top_menu', 'Reports');
        $this->session->set_userdata('sub_menu', 'Reports/finance');
        $this->session->set_userdata('subsub_menu', 'Reports/finance/reportduefees');
        $data                = array();
        $data['title']       = 'student fees';
        $class               = $this->class_model->get();
        $data['classlist']   = $class;
        $data['sch_setting'] = $this->sch_setting_detail;
        if ($this->input->server('REQUEST_METHOD') == "POST") {
            $date               = date('Y-m-d');
            $class_id           = $this->input->post('class_id');
            $section_id         = $this->input->post('section_id');
            $data['class_id']   = $class_id;
            $data['section_id'] = $section_id;
            $fees_dues          = $this->studentfeemaster_model->getStudentDueFeeTypesByDate($date, $class_id, $section_id);
            $students_list      = array();

            if (!empty($fees_dues)) {
                foreach ($fees_dues as $fee_due_key => $fee_due_value) {
                    // echo "<br/>";
                    // print_r($fee_due_value);

                    $amount_paid = 0;

                    if (isJSON($fee_due_value->amount_detail)) {
                        $student_fees_array = json_decode($fee_due_value->amount_detail);
                        foreach ($student_fees_array as $fee_paid_key => $fee_paid_value) {
                            $amount_paid += ($fee_paid_value->amount + $fee_paid_value->amount_discount);
                        }
                    }
                    if ($amount_paid < $fee_due_value->fee_amount || ($amount_paid < $fee_due_value->amount && $fee_due_value->is_system)) {
                        $students_list[$fee_due_value->student_session_id]['admission_no']             = $fee_due_value->admission_no;
                        $students_list[$fee_due_value->student_session_id]['roll_no']                  = $fee_due_value->roll_no;
                        $students_list[$fee_due_value->student_session_id]['admission_date']           = $fee_due_value->admission_date;
                        $students_list[$fee_due_value->student_session_id]['firstname']                = $fee_due_value->firstname;
                        $students_list[$fee_due_value->student_session_id]['middlename']               = $fee_due_value->middlename;
                        $students_list[$fee_due_value->student_session_id]['lastname']                 = $fee_due_value->lastname;
                        $students_list[$fee_due_value->student_session_id]['father_name']              = $fee_due_value->father_name;
                        $students_list[$fee_due_value->student_session_id]['image']                    = $fee_due_value->image;
                        $students_list[$fee_due_value->student_session_id]['mobileno']                 = $fee_due_value->mobileno;
                        $students_list[$fee_due_value->student_session_id]['email']                    = $fee_due_value->email;
                        $students_list[$fee_due_value->student_session_id]['state']                    = $fee_due_value->state;
                        $students_list[$fee_due_value->student_session_id]['city']                     = $fee_due_value->city;
                        $students_list[$fee_due_value->student_session_id]['pincode']                  = $fee_due_value->pincode;
                        $students_list[$fee_due_value->student_session_id]['class']                    = $fee_due_value->class;
                        $students_list[$fee_due_value->student_session_id]['section']                  = $fee_due_value->section;
                        $students_list[$fee_due_value->student_session_id]['fee_groups_feetype_ids'][] = $fee_due_value->fee_groups_feetype_id;
                    }

                }
                // exit();
            }

            if (!empty($students_list)) {
                foreach ($students_list as $student_key => $student_value) {
                    $students_list[$student_key]['fees_list'] = $this->studentfeemaster_model->studentDepositByFeeGroupFeeTypeArray($student_key, $student_value['fee_groups_feetype_ids']);

                }
            }

            $data['student_due_fee'] = $students_list;
        }

        $this->load->view('layout/header', $data);
        $this->load->view('reports/reportduefees', $data);
        $this->load->view('layout/footer', $data);
    }

    public function printreportduefees()
    {

        $data                = array();
        $data['title']       = 'student fees';
        $class               = $this->class_model->get();
        $data['classlist']   = $class;
        $data['sch_setting'] = $this->sch_setting_detail;
        $date                = date('Y-m-d');
        $class_id            = $this->input->post('class_id');
        $section_id          = $this->input->post('section_id');
        $data['class_id']    = $class_id;
        $data['section_id']  = $section_id;
        $fees_dues           = $this->studentfeemaster_model->getStudentDueFeeTypesByDate($date, $class_id, $section_id);
        $students_list       = array();

        if (!empty($fees_dues)) {
            foreach ($fees_dues as $fee_due_key => $fee_due_value) {
                $amount_paid = 0;

                if (isJSON($fee_due_value->amount_detail)) {
                    $student_fees_array = json_decode($fee_due_value->amount_detail);
                    foreach ($student_fees_array as $fee_paid_key => $fee_paid_value) {
                        $amount_paid += ($fee_paid_value->amount + $fee_paid_value->amount_discount);
                    }
                }
                if ($amount_paid < $fee_due_value->fee_amount) {
                    $students_list[$fee_due_value->student_session_id]['admission_no']             = $fee_due_value->admission_no;
                    $students_list[$fee_due_value->student_session_id]['roll_no']                  = $fee_due_value->roll_no;
                    $students_list[$fee_due_value->student_session_id]['admission_date']           = $fee_due_value->admission_date;
                    $students_list[$fee_due_value->student_session_id]['firstname']                = $fee_due_value->firstname;
                    $students_list[$fee_due_value->student_session_id]['middlename']               = $fee_due_value->middlename;
                    $students_list[$fee_due_value->student_session_id]['lastname']                 = $fee_due_value->lastname;
                    $students_list[$fee_due_value->student_session_id]['father_name']              = $fee_due_value->father_name;
                    $students_list[$fee_due_value->student_session_id]['image']                    = $fee_due_value->image;
                    $students_list[$fee_due_value->student_session_id]['mobileno']                 = $fee_due_value->mobileno;
                    $students_list[$fee_due_value->student_session_id]['email']                    = $fee_due_value->email;
                    $students_list[$fee_due_value->student_session_id]['state']                    = $fee_due_value->state;
                    $students_list[$fee_due_value->student_session_id]['city']                     = $fee_due_value->city;
                    $students_list[$fee_due_value->student_session_id]['pincode']                  = $fee_due_value->pincode;
                    $students_list[$fee_due_value->student_session_id]['class']                    = $fee_due_value->class;
                    $students_list[$fee_due_value->student_session_id]['section']                  = $fee_due_value->section;
                    $students_list[$fee_due_value->student_session_id]['fee_groups_feetype_ids'][] = $fee_due_value->fee_groups_feetype_id;
                }
            }
        }

        if (!empty($students_list)) {
            foreach ($students_list as $student_key => $student_value) {
                $students_list[$student_key]['fees_list'] = $this->studentfeemaster_model->studentDepositByFeeGroupFeeTypeArray($student_key, $student_value['fee_groups_feetype_ids']);

            }
        }
        $data['student_due_fee'] = $students_list;
        $page                    = $this->load->view('reports/_printreportduefees', $data, true);
        echo json_encode(array('status' => 1, 'page' => $page));
    }

    public function feeCollectionStudentDeposit()
    {
        $data=array();
        $date=$this->input->post('date');
        $fees_id=$this->input->post('fees_id');
        $fees_id_array=explode(',', $fees_id);    
        $fees_list=$this->studentfeemaster_model->getFeesDepositeByIdArray($fees_id_array);
        $data['student_list']=$fees_list;
        $data['date']=$date;
        $data['sch_setting']  = $this->sch_setting_detail;
        $page = $this->load->view('reports/_feeCollectionStudentDeposit', $data, true);
        echo json_encode(array('status' => 1, 'page' => $page));
    }

    public function printmonthlyfee(){
        $this->session->set_userdata('top_menu', 'Fees Collection');
        $this->session->set_userdata('sub_menu', 'studentfee/printMonthly');

        $class = $this->class_model->get();
        $data['classlist'] = $class;

        $class_id = $this->input->post('class_id');
        $section_id = $this->input->post('section_id');

        $data['studentList'] = $this->student_model->searchByClassSection($class_id,$section_id);

        $this->load->view('layout/header');
        $this->load->view('admin/monthlyfee/index',$data);
        $this->load->view('layout/footer');
    }

    public function printmonthlyfeeinvoice(){
        $this->form_validation->set_error_delimiters('', '');
        $data = array();
        $data['sch_setting'] = $this->sch_setting_detail;
        $data['students_value'] = $this->input->post('student_class_id[]');
        $student_exam_page = $this->load->view('print/printgroupmonthlyfee', $data, true); 
        $array = array('status' => '1', 'error' => '', 'page' => $student_exam_page);
        echo json_encode($array);
    }


    public function create_voucher($total_assigned,$transid){
        $account_subdata[] =array(
            'Credit' => $total_assigned,
            'Debit' => "0",
            'Remarks' => "Fees Assigned",
            'GLId' => $this->input->post('glname'),
        );

        $account_subdata[] =array(
            'Credit' => "0",
            'Debit' => $total_assigned,
            'Remarks' => "Fees Assigned",
            'GLId' => '17797',
        );

        $account_data = array(
            'CreatedBy'=>$this->customlib->getAdminSessionUserName(),
            'FiscalYear' => $this->setting_model->getCurrentSessionName(),
            'Narration'=> 'Fees Assigned',
            'CreatedDate' => date("Y-m-d"),
            'TransId'=> $transid,
            'RefNumber' => '00',
            'JVVoucherDetails'=>$account_subdata
        );

        $final=json_encode($account_data);

        $token = $this->input->cookie('Token');
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'http://actm.prabhumanagement.com/api/Voucher/CreateJV',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS =>$final,
            CURLOPT_HTTPHEADER => array(
                'Authorization: Bearer ' . $token,
                'Content-Type: application/json'
            ),
        ));

        $response = curl_exec($curl);


        curl_close($curl);
    }


    public function guardian_fee(){
        $this->session->set_userdata('top_menu', 'Fees Collection');
        $this->session->set_userdata('sub_menu', 'admin/guardian_fee');

        if (!$this->rbac->hasPrivilege('guardian_fee', 'can_view')) {
            access_denied();
        }

        $data['sch_setting'] = $this->sch_setting_detail;
        $data['title']       = 'student fees';
        $class               = $this->class_model->get();
        $data['classlist']   = $class;
        $this->load->view('layout/header', $data);
        $this->load->view('studentfee/guardianfeeSearch', $data); 
        $this->load->view('layout/footer', $data);
    }

    public function guardian_fees($id){
        if (!$this->rbac->hasPrivilege('guardian_fee', 'can_add')) {
            access_denied();
        }
        $data['sch_setting']  = $this->sch_setting_detail;
        $data['title']        = 'Student Detail';
        $student              = $this->student_model->getByStudentSession($id);
        $data['student']      = $student;
        $student_due_fee      = $this->studentfeemaster_model->getStudentFees($id);
        $student_discount_fee = $this->feediscount_model->getStudentFeesDiscount($id);

        $data['student_discount_fee'] = $student_discount_fee;
        $data['student_due_fee']      = $student_due_fee;
        $category                     = $this->category_model->get();
        $data['categorylist']         = $category;
        $class_section                = $this->student_model->getClassSection($student["class_id"]);
        $data["class_section"]        = $class_section;
        $session                      = $this->setting_model->getCurrentSession();
        $studentlistbysection         = $this->student_model->getStudentClassSection($student["class_id"], $session);
        $data["studentlistbysection"] = $studentlistbysection;

        $this->load->view('layout/header', $data); 
        $this->load->view('studentfee/guardianAddfee', $data);
        $this->load->view('layout/footer', $data);
    }




}
