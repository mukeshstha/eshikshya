<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class texslab{


	public function single($annual){
			if($annual < 500001){
				return 1;
			}elseif($annual > 500000 && $annual < 700001){
				return 10;
			}elseif($annual > 700000 && $annual < 1000001){
				return 20;
			}elseif($annual > 1000000 && $annual < 2000001){
				return 30;
			}else{
				return 36;
			} 
		
	}

	public function married($annual){
		if($annual < 600001){
			return 1;
		}elseif($annual > 600000 && $annual < 800001){
			return 10;
		}elseif($annual > 800000 && $annual < 1100001){
			return 20;
		}elseif($annual > 1100000 && $annual < 2000001){
			return 30;
		}else{
			return 36;
		}
	}
}