<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
  
class Creationsms {

    private $_CI;
    var $api_key; //your senderId here
    function __construct($params) { 
    	$this->api_key=$params['api_key'];
        $this->_CI = & get_instance();
        $this->session_name = $this->_CI->setting_model->getCurrentSessionName();
    } 
   
    public function sendSMS($to, $message) {
        $curl = curl_init();
            $data = array(
            'token'=>$this->api_key,
            'to' => $to,
            'content'=> $message
        );



        $final=json_encode($data);
        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://smsml.creationsoftnepal.com/SendBulkV1/send',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS =>$final,
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;
    }

    public function sendSingle($to,$message){
        $token = $this->api_key;
        $string = urlencode($message);
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://smsml.creationsoftnepal.com/send?to=977'.$to.'&content='.$string.'&token='.$token,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);
        
        curl_close($curl);
    }



}
?>