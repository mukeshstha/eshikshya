<?php

// if($this->customlib->getcalender()->calender=="1"){
//     if (isset($_POST['date_from']) && !empty($_POST['date_from'])) {
//         $date_from = date($this->customlib->getSchoolDateFormat(), $this->customlib->datetostrtotime($_POST['date_from']));
//     } else {
//         $date_from = date($this->customlib->getSchoolDateFormat());
//     }
//     if (isset($_POST['date_to']) && !empty($_POST['date_to'])) {
//         $date_to = date($this->customlib->getSchoolDateFormat(), $this->customlib->datetostrtotime($_POST['date_to']));
//     } else {
//         $date_to = date($this->customlib->getSchoolDateFormat());
//     }
// }else{
//     if (isset($_POST['date_from']) && !empty($_POST['date_from'])) {
//         $date_from = date($this->customlib->getSchoolDateFormat($this->customlib->datetostrtotime($_POST['date_from'])));
//     } else{
//         $date_from = date($this->customlib->currentdate());
//     }
//     if (isset($_POST['date_to']) && !empty($_POST['date_to'])) {
//         $date_to = date($this->customlib->getSchoolDateFormat($this->customlib->datetostrtotime($_POST['date_to'])));
//     } else {
//         $date_to = date($this->customlib->currentdate());
//     }
// }

?>
<div class="col-sm-6 col-md-3">
    <div class="form-group">
        <label><?php echo $this->lang->line('date_from'); ?></label>
        <input name="date_from" id="date_from" placeholder="" type="text" class="form-control datechanger" value="<?php echo $date_from; ?>"  />
        <span class="text-danger"><?php echo form_error('date_from'); ?></span>
    </div>
</div> 

<div class="col-sm-6 col-md-3">
    <div class="form-group">
        <label><?php echo $this->lang->line('date_to'); ?></label>
        <input  name="date_to" id="date_to" placeholder="" type="text" class="form-control datechanger" value="<?php echo $date_to; ?>"  />
        <span class="text-danger"><?php echo form_error('date_to'); ?></span>
    </div>
</div> 


<?php $calender= $this->customlib->getcalender(); ?>
<?php if($calender->calender=="0") : ?>
    <script type="text/javascript">
        var date_format_nepali = '<?php echo $result = strtr($this->customlib->getSchoolDateFormat(), ['d' => 'DD', 'm' => 'MM', 'Y' => 'YYYY']) ?>';

        $(function() {
            $(".datechanger").nepaliDatePicker({
                language:"english",
                dateFormat: date_format_nepali,
                ndpYear: true,
                ndpMonth: true,
            });
        }); 
    </script>
<?php else : ?>
    <script type="text/javascript">
    $(document).ready(function () {
        moment.lang('en', {
          week: { dow: start_week }
        });
        $("body").delegate(".datechanger", "focusin", function () {
            $(this).datepicker({
                todayHighlight: false,
                format: date_format,
                autoclose: true,
                weekStart : start_week,
                language: '<?php echo $language_name ?>'
            });
        });
    });
    </script>
<?php endif; ?>