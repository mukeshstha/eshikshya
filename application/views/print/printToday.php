<?php $currency_symbol = $this->customlib->getSchoolCurrencyFormat(); ?>
<style type="text/css">
    @media print {
        .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12 {
            float: left;
        }
        .col-sm-12 {
            width: 100%;
        }
        .col-sm-11 {
            width: 91.66666667%;
        }
        .col-sm-10 {
            width: 83.33333333%;
        }
        .col-sm-9 {
            width: 75%;
        }
        .col-sm-8 {
            width: 66.66666667%;
        }
        .col-sm-7 {
            width: 58.33333333%;
        }
        .col-sm-6 {
            width: 50%;
        }
        .col-sm-5 {
            width: 41.66666667%;
        }
        .col-sm-4 {
            width: 33.33333333%;
        }
        .col-sm-3 {
            width: 25%;
        }
        .col-sm-2 {
            width: 16.66666667%;
        }
        .col-sm-1 {
            width: 8.33333333%;
        }
        .col-sm-pull-12 {
            right: 100%;
        }
        .col-sm-pull-11 {
            right: 91.66666667%;
        }
        .col-sm-pull-10 {
            right: 83.33333333%;
        }
        .col-sm-pull-9 {
            right: 75%;
        }
        .col-sm-pull-8 {
            right: 66.66666667%;
        }
        .col-sm-pull-7 {
            right: 58.33333333%;
        }
        .col-sm-pull-6 {
            right: 50%;
        }
        .col-sm-pull-5 {
            right: 41.66666667%;
        }
        .col-sm-pull-4 {
            right: 33.33333333%;
        }
        .col-sm-pull-3 {
            right: 25%;
        }
        .col-sm-pull-2 {
            right: 16.66666667%;
        }
        .col-sm-pull-1 {
            right: 8.33333333%;
        }
        .col-sm-pull-0 {
            right: auto;
        }
        .col-sm-push-12 {
            left: 100%;
        }
        .col-sm-push-11 {
            left: 91.66666667%;
        }
        .col-sm-push-10 {
            left: 83.33333333%;
        }
        .col-sm-push-9 {
            left: 75%;
        }
        .col-sm-push-8 {
            left: 66.66666667%;
        }
        .col-sm-push-7 {
            left: 58.33333333%;
        }
        .col-sm-push-6 {
            left: 50%;
        }
        .col-sm-push-5 {
            left: 41.66666667%;
        }
        .col-sm-push-4 {
            left: 33.33333333%;
        }
        .col-sm-push-3 {
            left: 25%;
        }
        .col-sm-push-2 {
            left: 16.66666667%;
        }
        .col-sm-push-1 {
            left: 8.33333333%;
        }
        .col-sm-push-0 {
            left: auto;
        }
        .col-sm-offset-12 {
            margin-left: 100%;
        }
        .col-sm-offset-11 {
            margin-left: 91.66666667%;
        }
        .col-sm-offset-10 {
            margin-left: 83.33333333%;
        }
        .col-sm-offset-9 {
            margin-left: 75%;
        }
        .col-sm-offset-8 {
            margin-left: 66.66666667%;
        }
        .col-sm-offset-7 {
            margin-left: 58.33333333%;
        }
        .col-sm-offset-6 {
            margin-left: 50%;
        }
        .col-sm-offset-5 {
            margin-left: 41.66666667%;
        }
        .col-sm-offset-4 {
            margin-left: 33.33333333%;
        }
        .col-sm-offset-3 {
            margin-left: 25%;
        }
        .col-sm-offset-2 {
            margin-left: 16.66666667%;
        }
        .col-sm-offset-1 {
            margin-left: 8.33333333%;
        }
        .col-sm-offset-0 {
            margin-left: 0%;
        }
        .visible-xs {
            display: none !important;
        }
        .hidden-xs {
            display: block !important;
        }
        table.hidden-xs {
            display: table;
        }
        tr.hidden-xs {
            display: table-row !important;
        }
        th.hidden-xs,
        td.hidden-xs {
            display: table-cell !important;
        }
        .hidden-xs.hidden-print {
            display: none !important;
        }
        .hidden-sm {
            display: none !important;
        }
        .visible-sm {
            display: block !important;
        }
        table.visible-sm {
            display: table;
        }
        tr.visible-sm {
            display: table-row !important;
        }
        th.visible-sm,
        td.visible-sm {
            display: table-cell !important;
        }
        .print-table th,.print-table tr.back-color td{
                    background-color: #a6a6a6 !important;
                    -webkit-print-color-adjust: exact; 
                }
                .back-color{
                    background-color: #a6a6a6 !important;
                    margin-top: 10px;
                    padding: 5px;
                    -webkit-print-color-adjust: exact; 
                }
                .grey-back{
                    background-color: #f5f5f5 !important;
                    -webkit-print-color-adjust: exact; 
                }
                .print-table td,.print-table th{
                    border: 1px solid #000;
                }

.clearfix::after {
  content: "";
  clear: both;
  display: table;
}
                
        
    }
    .page-break{page-break-after: always !important; clear: both;}
</style>

        <div class="container"> 
            <div class="row">
                <div id="content" class="col-lg-12 col-sm-12 ">
                    <div class="invoice">
                        <div class="row header">
                            <div class="col-sm-12">
                                <div class="col-sm-2">
                                    <img src="<?php echo base_url(); ?>uploads/<?php echo $_SERVER['SERVER_NAME']; ?>/school_content/admin_logo/<?php $this->setting_model->getAdminlogo();?>" alt="<?php echo $this->customlib->getAppName() ?>" style="width: 100px;height: auto;margin-bottom: 0px"/> 
                                </div>
                                <div class="col-sm-9 text-center">
                                    <span style="font-weight: bold;font-size: 18px"><?php echo $this->setting_model->getCurrentSchoolName(); ?></span><br>
                                    <p style="font-weight: bold;margin:0px;font-size: 15px;"><?php echo $this->setting_model->get_address() ?>,&nbsp;<?php echo $this->lang->line('phone') ?> : <?php echo $this->setting_model->get_phone() ?></p>
                                    <p style="font-weight: bold;margin:0px;font-size: 15px;"><?php echo $this->lang->line('email') ?> : <?php echo $this->setting_model->get_email() ?></p>
                                </div>

                            </div> 
                            <div class="row">
                                <div class="col-sm-12 back-color" style="text-align: center;font-weight: bold; background: #000;font-size: 15px;">
                                    <?php echo "Payment Receipt"; ?>
                                </div>
                            </div>
                        <div class="row grey-back">                           
                            <div class="col-xs-8 text-left" style="font-size:15px !important;font-weight: bold">
                                        <strong><?php echo $this->lang->line('name'); ?>: &nbsp;<?php echo $this->customlib->getFullName($feearray[0]->firstname, $feearray[0]->middlename,$feearray[0]->lastname,$sch_setting->middlename,$sch_setting->lastname);

                              ?></strong><?php echo " (".$feearray[0]->admission_no.")"; ?> <br>

                                        <?php echo $this->lang->line('father_name'); ?>: <?php echo $feearray[0]->father_name; ?><br>
                                        <?php echo $this->lang->line('class'); ?>: <?php echo $feearray[0]->class . " (" . $feearray[0]->section . ")"; ?>
                            </div>
                            <div class="col-xs-4 text-right">
                                    <strong style="font-size:15px">Date :
                                        <?php if($this->customlib->getcalender()->calender=="0") : ?>
                                            <?php echo $this->customlib->currentdate(); ?>

                                        <?php else : ?>
                                        <?php
                                        $date = date('d-m-Y');

                                        echo date($this->customlib->getSchoolDateFormat(), $this->customlib->dateyyyymmddTodateformat($date));
                                        ?></strong><br/>
                                    <?php endif; ?>                           
                            </div>
                        </div>
                        <hr style="margin-top: 0px;margin-bottom: 0px;" />
                        <div class="row">
                         <?php
                                if (!empty($feearray)) {
                                    ?>

                                    <table class="table table-striped table-responsive print-table" style="font-size: 12pt;">
                                        <thead>
                                        <th style="width:80%"><?php echo $this->lang->line('fees') . "" . $this->lang->line('description'); ?></th>

                                        <th style="width:20%" class="text text-right"><?php echo $this->lang->line('amount'); ?></th>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $total_amount = 0;
                                            $total_deposite_amount = 0;
                                            $total_fine_amount = 0;
                                            $total_discount_amount = 0;
                                            $total_balance_amount = 0;
                                            $alot_fee_discount = 0;
                                            $total_paid= 0;
                                            if (empty($feearray)) {
                                                ?>
                                                <tr>
                                                    <td colspan="11" class="text-danger text-center">
                                                        <?php echo $this->lang->line('no_transaction_found'); ?>
                                                    </td>
                                                </tr>
                                                <?php
                                            } else {



                                                foreach ($feearray as $fee_key => $feeList) {
                                                    if ($feeList->is_system) {
                                                        $feeList->amount = $feeList->student_fees_master_amount;
                                                    }

                                                    $fee_discount = 0;
                                                    $fee_paid = 0;
                                                    $fee_fine = 0;
                                                    if (!empty($feeList->amount_detail)) {
                                                        $fee_deposits = json_decode(($feeList->amount_detail));

                                                        foreach ($fee_deposits as $fee_deposits_key => $fee_deposits_value) {
                                                            $fee_paid = $fee_paid + $fee_deposits_value->amount;
                                                            $fee_discount = $fee_discount + $fee_deposits_value->amount_discount;
                                                            $fee_fine = $fee_fine + $fee_deposits_value->amount_fine;
                                                        }
                                                    }
                                                    $feetype_balance = $feeList->amount - ($fee_paid + $fee_discount);
                                                    $total_amount = $total_amount + $feeList->amount;
                                                    $total_discount_amount = $total_discount_amount + $fee_discount;
                                                    $total_fine_amount = $total_fine_amount + $fee_fine;
                                                    $total_deposite_amount = $total_deposite_amount + $fee_paid;
                                                    $total_balance_amount = $total_balance_amount + $feetype_balance;
                                                    ?>



                                                    	<?php
                    if (!empty($feeList->amount_detail)) {
                        $fee_deposits = json_decode(($feeList->amount_detail));
                        
                        foreach ($fee_deposits as $fee_deposits_key => $fee_deposits_value) {

                        	if(date($this->customlib->getSchoolDateFormat(), $this->customlib->dateyyyymmddTodateformat($fee_deposits_value->date)) == $this->customlib->currentdate()){
                        	
                        ?>


                            <tr class="white-td deposit-description">
                            	<td style="width: 80%;" height="20px"><?php echo $feeList->name . " (" . $feeList->student_fees_deposite_id . "/" . $fee_deposits_value->inv_no . ")";?>&nbsp;<?php if($feeList->submission_type!=="1") : ?><?php if(isset($feeList->months)) : ?><?php echo "(" . $this->customlib->getMonthdata($feeList->months) . ")"; ?><?php endif; ?><?php endif; ?></td>
                                <td class="text text-right" height="20px"><?php echo (number_format($fee_deposits_value->amount, 2, '.', '')); ?></td>
                            </tr>


                        <?php
                        	$total_paid = $total_paid + $fee_deposits_value->amount;
                        }
                        
                        }
                    } ?>









                                                            <?php
                                                }
                                                ?>
                                                <?php
                                            }
                                            ?>
                                            <tr class="success back-color">

                                                <td align="left" class="text text-left" height="20px">
                                                    <b><?php echo $this->lang->line('total'); ?></b>
                                                </td>

                                                <td class="text text-right" height="20px"> <b>  <?php
                                                        echo ($currency_symbol . "&nbsp;" . number_format($total_paid, 2, '.', ''));
                                                        ?></b>
                                                </td> 
                                            </tr>
                                            <tr>
                                                <td colspan="2" style="text-transform: capitalize" height="20px">
                                                    <strong>
                                                    <?php echo $this->lang->line('in_words'); ?> : 
                                                    <?php echo $this->customlib->getNepaliCurrency($total_paid); ?>
                                                    </strong>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <?php
                                }
                                ?>
                                <div class="row header ">
                                    <div class="col-sm-12">  
                                        <div class="col-sm-6" style="padding-top:40px;font-size: 15px;">
                                            <?php echo $this->lang->line('thank_you'); ?>
                                        </div>     
                                        <div class="col-sm-6 text-center" style="padding-top:40px">
                                            <hr style="margin-bottom: 0px;border-color: #000;">
                                            <span style="font-size:15px;"><?php echo $this->lang->line('account_section'); ?></span>
                                        </div>                
                                    </div>
                                </div> 

                        </div>
                    </div>
                </div>
            </div>
            <div class="page-break"></div>
            <div class="clearfix"></div>
            <?php
            if ($sch_setting->is_duplicate_fees_invoice) {
                ?>
                <div class="row">
                    <div id="content" class="col-lg-12 col-sm-12 ">
                    <div class="invoice">
                        <div class="row header ">
                            <div class="col-sm-12">
                                <div class="col-sm-2">
                                    <img src="<?php echo base_url(); ?>uploads/<?php echo $_SERVER['SERVER_NAME']; ?>/school_content/admin_logo/<?php $this->setting_model->getAdminlogo();?>" alt="<?php echo $this->customlib->getAppName() ?>" style="width: 100px;height: auto;margin-bottom: 0px"/> 
                                </div>
                                <div class="col-sm-9 text-center">
                                    <span style="font-weight: bold;font-size: 18px"><?php echo $this->setting_model->getCurrentSchoolName(); ?></span><br>
                                    <p style="margin:0px;font-size: 15px;"><?php echo $this->setting_model->get_address() ?>,&nbsp;<?php echo $this->lang->line('phone') ?> : <?php echo $this->setting_model->get_phone() ?></p>
                                    <p style="margin:0px;font-size: 15px;"><?php echo $this->lang->line('email') ?> : <?php echo $this->setting_model->get_email() ?></p>
                                </div>

                            </div> 
                            <div class="row">
                                <div class="col-sm-12 back-color" style="text-align: center;font-weight: bold; background: #000;font-size: 15px;">
                                    <?php echo "Payment Receipt"; ?>
                                </div>
                            </div>
                        <div class="row grey-back">                           
                            <div class="col-xs-8 text-left" style="font-size:15px !important;">
                                        <strong><?php echo $this->lang->line('name'); ?>: &nbsp;<?php echo $this->customlib->getFullName($feearray[0]->firstname, $feearray[0]->middlename,$feearray[0]->lastname,$sch_setting->middlename,$sch_setting->lastname);

                              ?></strong><?php echo " (".$feearray[0]->admission_no.")"; ?> <br>

                                        <?php echo $this->lang->line('father_name'); ?>: <?php echo $feearray[0]->father_name; ?><br>
                                        <?php echo $this->lang->line('class'); ?>: <?php echo $feearray[0]->class . " (" . $feearray[0]->section . ")"; ?>
                            </div>
                            <div class="col-xs-4 text-right">
                                    <strong style="font-size:15px">Date :
                                        <?php if($this->customlib->getcalender()->calender=="0") : ?>
                                            <?php echo $this->customlib->currentdate(); ?>

                                        <?php else : ?>
                                        <?php
                                        $date = date('d-m-Y');

                                        echo date($this->customlib->getSchoolDateFormat(), $this->customlib->dateyyyymmddTodateformat($date));
                                        ?></strong><br/>
                                    <?php endif; ?>                           
                            </div>
                        </div>
                        <hr style="margin-top: 0px;margin-bottom: 0px;" />
                        <div class="row">
                         <?php
                                if (!empty($feearray)) {
                                    ?>

                                    <table class="table table-striped table-responsive print-table" style="font-size: 12pt;">
                                        <thead>
                                        <th style="width:80%"><?php echo $this->lang->line('fees') . "" . $this->lang->line('description'); ?></th>

                                        <th style="width:20%" class="text text-right"><?php echo $this->lang->line('amount'); ?></th>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $total_amount = 0;
                                            $total_deposite_amount = 0;
                                            $total_fine_amount = 0;
                                            $total_discount_amount = 0;
                                            $total_balance_amount = 0;
                                            $alot_fee_discount = 0;
                                            $total_paid= 0;
                                            if (empty($feearray)) {
                                                ?>
                                                <tr>
                                                    <td colspan="11" class="text-danger text-center">
                                                        <?php echo $this->lang->line('no_transaction_found'); ?>
                                                    </td>
                                                </tr>
                                                <?php
                                            } else {



                                                foreach ($feearray as $fee_key => $feeList) {
                                                    if ($feeList->is_system) {
                                                        $feeList->amount = $feeList->student_fees_master_amount;
                                                    }

                                                    $fee_discount = 0;
                                                    $fee_paid = 0;
                                                    $fee_fine = 0;
                                                    if (!empty($feeList->amount_detail)) {
                                                        $fee_deposits = json_decode(($feeList->amount_detail));

                                                        foreach ($fee_deposits as $fee_deposits_key => $fee_deposits_value) {
                                                            $fee_paid = $fee_paid + $fee_deposits_value->amount;
                                                            $fee_discount = $fee_discount + $fee_deposits_value->amount_discount;
                                                            $fee_fine = $fee_fine + $fee_deposits_value->amount_fine;
                                                        }
                                                    }
                                                    $feetype_balance = $feeList->amount - ($fee_paid + $fee_discount);
                                                    $total_amount = $total_amount + $feeList->amount;
                                                    $total_discount_amount = $total_discount_amount + $fee_discount;
                                                    $total_fine_amount = $total_fine_amount + $fee_fine;
                                                    $total_deposite_amount = $total_deposite_amount + $fee_paid;
                                                    $total_balance_amount = $total_balance_amount + $feetype_balance;
                                                    ?>



                                                    	<?php
                    if (!empty($feeList->amount_detail)) {
                        $fee_deposits = json_decode(($feeList->amount_detail));
                        
                        foreach ($fee_deposits as $fee_deposits_key => $fee_deposits_value) {

                        	if(date($this->customlib->getSchoolDateFormat(), $this->customlib->dateyyyymmddTodateformat($fee_deposits_value->date)) == $this->customlib->currentdate()){
                        	
                        ?>


                            <tr class="white-td deposit-description">
                            	<td style="width: 80%;"><?php echo $feeList->name . " (" . $feeList->student_fees_deposite_id . "/" . $fee_deposits_value->inv_no . ")";?></td>
                                <td class="text text-right"><?php echo (number_format($fee_deposits_value->amount, 2, '.', '')); ?></td>
                            </tr>


                        <?php
                        	$total_paid = $total_paid + $fee_deposits_value->amount;
                        }
                        
                        }
                    }
                ?>









                                                            <?php
                                                }
                                                ?>
                                                <?php
                                            }
                                            ?>
                                            <tr class="success back-color">

                                                <td align="left" class="text text-left">
                                                    <b><?php echo $this->lang->line('total'); ?></b>
                                                </td>

                                                <td class="text text-right"> <b>  <?php
                                                        echo ($currency_symbol . "&nbsp;" . number_format($total_paid, 2, '.', ''));
                                                        ?></b>
                                                </td> 
                                            </tr>
                                            <tr>
                                                <td colspan="2" style="text-transform: capitalize">
                                                    <strong>
                                                    <?php echo $this->lang->line('in_words'); ?> : 
                                                    <?php echo $this->customlib->getNepaliCurrency($total_paid); ?>
                                                    </strong>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <?php
                                }
                                ?>
                                <div class="row header ">
                                    <div class="col-sm-12">  
                                        <div class="col-sm-6" style="padding-top:40px;font-size: 15px;">
                                            <?php echo $this->lang->line('thank_you'); ?>
                                        </div>     
                                        <div class="col-sm-6 text-center" style="padding-top:40px">
                                            <hr style="margin-bottom: 0px;border-color: #000;">
                                            <span style="font-size:15px;"><?php echo $this->lang->line('account_section'); ?></span>
                                        </div>                
                                    </div>
                                </div> 

                        </div>
                    </div>
                </div>
            </div>
                </div>
                <?php
            }
            ?>
        </div>
