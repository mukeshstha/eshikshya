<?php $currency_symbol = $this->customlib->getSchoolCurrencyFormat(); ?>
<style type="text/css">
    @media print {
        .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12 {
            float: left;
        }
        .col-sm-12 {
            width: 100%;
        }
        .col-sm-11 {
            width: 91.66666667%;
        }
        .col-sm-10 {
            width: 83.33333333%;
        }
        .col-sm-9 {
            width: 75%;
        }
        .col-sm-8 {
            width: 66.66666667%;
        }
        .col-sm-7 {
            width: 58.33333333%;
        }
        .col-sm-6 {
            width: 50%;
        }
        .col-sm-5 {
            width: 41.66666667%;
        }
        .col-sm-4 {
            width: 33.33333333%;
        }
        .col-sm-3 {
            width: 25%;
        }
        .col-sm-2 {
            width: 16.66666667%;
        }
        .col-sm-1 {
            width: 8.33333333%;
        }
        .col-sm-pull-12 {
            right: 100%;
        }
        .col-sm-pull-11 {
            right: 91.66666667%;
        }
        .col-sm-pull-10 {
            right: 83.33333333%;
        }
        .col-sm-pull-9 {
            right: 75%;
        }
        .col-sm-pull-8 {
            right: 66.66666667%;
        }
        .col-sm-pull-7 {
            right: 58.33333333%;
        }
        .col-sm-pull-6 {
            right: 50%;
        }
        .col-sm-pull-5 {
            right: 41.66666667%;
        }
        .col-sm-pull-4 {
            right: 33.33333333%;
        }
        .col-sm-pull-3 {
            right: 25%;
        }
        .col-sm-pull-2 {
            right: 16.66666667%;
        }
        .col-sm-pull-1 {
            right: 8.33333333%;
        }
        .col-sm-pull-0 {
            right: auto;
        }
        .col-sm-push-12 {
            left: 100%;
        }
        .col-sm-push-11 {
            left: 91.66666667%;
        }
        .col-sm-push-10 {
            left: 83.33333333%;
        }
        .col-sm-push-9 {
            left: 75%;
        }
        .col-sm-push-8 {
            left: 66.66666667%;
        }
        .col-sm-push-7 {
            left: 58.33333333%;
        }
        .col-sm-push-6 {
            left: 50%;
        }
        .col-sm-push-5 {
            left: 41.66666667%;
        }
        .col-sm-push-4 {
            left: 33.33333333%;
        }
        .col-sm-push-3 {
            left: 25%;
        }
        .col-sm-push-2 {
            left: 16.66666667%;
        }
        .col-sm-push-1 {
            left: 8.33333333%;
        }
        .col-sm-push-0 {
            left: auto;
        }
        .col-sm-offset-12 {
            margin-left: 100%;
        }
        .col-sm-offset-11 {
            margin-left: 91.66666667%;
        }
        .col-sm-offset-10 {
            margin-left: 83.33333333%;
        }
        .col-sm-offset-9 {
            margin-left: 75%;
        }
        .col-sm-offset-8 {
            margin-left: 66.66666667%;
        }
        .col-sm-offset-7 {
            margin-left: 58.33333333%;
        }
        .col-sm-offset-6 {
            margin-left: 50%;
        }
        .col-sm-offset-5 {
            margin-left: 41.66666667%;
        }
        .col-sm-offset-4 {
            margin-left: 33.33333333%;
        }
        .col-sm-offset-3 {
            margin-left: 25%;
        }
        .col-sm-offset-2 {
            margin-left: 16.66666667%;
        }
        .col-sm-offset-1 {
            margin-left: 8.33333333%;
        }
        .col-sm-offset-0 {
            margin-left: 0%;
        }
        .visible-xs {
            display: none !important;
        }
        .hidden-xs {
            display: block !important;
        }
        table.hidden-xs {
            display: table;
        }
        tr.hidden-xs {
            display: table-row !important;
        }
        th.hidden-xs,
        td.hidden-xs {
            display: table-cell !important;
        }
        .hidden-xs.hidden-print {
            display: none !important;
        }
        .hidden-sm {
            display: none !important;
        }
        .visible-sm {
            display: block !important;
        }
        table.visible-sm {
            display: table;
        }
        tr.visible-sm {
            display: table-row !important;
        }
        th.visible-sm,
        td.visible-sm {
            display: table-cell !important;
        }
        .print-table th,.print-table tr.back-color td{
            background-color: #a6a6a6 !important;
            -webkit-print-color-adjust: exact; 
        }
        .back-color{
            background-color: #a6a6a6 !important;
            margin-top: 10px;
            padding: 5px;
            -webkit-print-color-adjust: exact; 
        }
        .grey-back{
            background-color: #f5f5f5 !important;
            -webkit-print-color-adjust: exact; 
        }
        .print-table td,.print-table th{
            border: 1px solid #000;
        }
        .back-color{
            background-color: #a7a7a7 !important;
            color: #fff;
            -webkit-print-color-adjust: exact; 
        }
        .page-break{page-break-after: always !important; clear: both;}
    }
    

</style>
<link rel="stylesheet" href="<?php echo base_url(); ?>backend/bootstrap/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/css/AdminLTE.min.css">
        <div class="container"> 
            <?php foreach ($students_value as $key => $value) : ?>
                <?php
                $studentvalue = $this->studentsession_model->searchStudentsBySession($value);
                $student_due_fee      = $this->studentfeemaster_model->getStudentFees($value);
                ?>
                <?php if (!empty($student_due_fee)) { ?>
            <div class="row" style="padding: 15px;margin-top: 15px;margin-bottom: 15px">
                <div id="content" class="col-lg-12 col-sm-12 ">
                    <div class="invoice">
                        <div class="row header ">
                            <div class="col-sm-12">
                                <div class="col-sm-2" style="text-align: right">
                                    <img src="<?php echo base_url(); ?>uploads/<?php echo $_SERVER['SERVER_NAME']; ?>/school_content/admin_logo/<?php $this->setting_model->getAdminlogo();?>" alt="<?php echo $this->customlib->getAppName() ?>" style="width: 90px;height: auto;margin-bottom: 0px"/> 
                                </div>
                                <div class="col-sm-8 text-center">
                                    <span style="font-weight: bold;font-size: 18px! important"><?php echo $this->setting_model->getCurrentSchoolName(); ?></span><br>
                                    <p style="margin:0px;font-size: 15px;"><?php echo $this->setting_model->get_address() ?>,&nbsp;<br><?php echo $this->lang->line('phone') ?> : <?php echo $this->setting_model->get_phone() ?></p>
                                    <p style="margin:0px;font-size: 15px;"><?php echo $this->lang->line('email') ?> : <?php echo $this->setting_model->get_email() ?></p>
                                </div>
                                <div class="col-sm-1"></div>

                            </div> 
                            <div class="col-sm-12 text-right">
                                <strong style="font-size:15px">Date :
                                        <?php if($this->customlib->getcalender()->calender=="0") : ?>
                                            <?php echo $this->customlib->currentdate(); ?>

                                        <?php else : ?>
                                        <?php
                                        $date = date('d-m-Y');

                                        echo date($this->customlib->getSchoolDateFormat(), $this->customlib->dateyyyymmddTodateformat($date));
                                        ?>  
                                        <?php endif; ?>
                                        </strong>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 back-color" style="text-align: center;font-weight: bold; background: #000;font-size: 14px;">
                                    <?php echo $this->lang->line('monthly_bill'); ?>
                                </div>
                            </div>
                        <div class="row grey-back">                           
                            <div class="col-xs-8 text-left" style="font-size:15px !important;">
                                        <strong><?php echo $this->lang->line('name'); ?>: &nbsp;<?php echo $studentvalue['firstname'] . " " . $studentvalue['middlename'] . " " . $studentvalue['lastname'];

                              ?></strong><?php echo " (".$studentvalue['admission_no'].")"; ?> <br>

                                        <?php echo $this->lang->line('father_name'); ?>: <?php echo $studentvalue['father_name']; ?><br>
                            </div>
                                    
                            <div class="col-xs-4 text-right">
                                      
                                           <?php $section=$this->section_model->getsectionname($studentvalue['section_id']);
                                    ?>
                            
                                        <?php echo $this->lang->line('class'); ?>: <?php echo $this->class_model->getclassname($studentvalue['class_id'])['class'] . " (" . $this->section_model->getsectionname($studentvalue['section_id'])['section'] . ")"; ?>                  
                            </div>
                        </div>
                        <hr style="margin-top: 0px;margin-bottom: 0px;" />
                        <div class="row">
                         

                                    <table class="table table-striped table-responsive print-table" style="font-size: 11pt;">
                                        <thead>
                                        <th style="width:80%"><?php echo $this->lang->line('fees') . " " . $this->lang->line('description'); ?></th>

                                        <th style="width:20%" class="text text-right"><?php echo $this->lang->line('amount'); ?></th>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $total_amount = 0;
                                            $total_deposite_amount = 0;
                                            $total_fine_amount = 0;
                                            $total_discount_amount = 0;
                                            $total_balance_amount = 0;
                                            $alot_fee_discount = 0;
                                            $total_single_discount   = 0;

                                            if (empty($student_due_fee)) {
                                                ?>
                                                <tr>
                                                    <td colspan="11" class="text-danger text-center">
                                                        <?php echo "No Remaining Due" ?>
                                                    </td>
                                                </tr>
                                                <?php
                                            } else { 
                                                $totalamount = 0;
                                                $count = 0;
                                                ?>
                                                   
                                                <?php foreach($student_due_fee as $key => $fee) : ?>
                                                        <?php foreach ($fee->fees as $fee_key => $fee_value) { 
                                                            $paid_amount = 0;
                                                            $discount_given = 0;
                                                            $single_discount=0;
                                                            
                                                            $fee_session_group = $fee_value->fee_groups_feetype_id;
                                                            $student_fee_master = $fee_value->id;
                                                            $fee_deposite = $this->studentfee_model->getpayment($student_fee_master,$fee_session_group);
                                                            $discount_applied = $this->studentfee_model->getdiscountapplied($student_fee_master,$fee_session_group);
                                                            ?>
                                                            <?php
                                                             if(isset($fee_deposite)){ 
                                                                $fee_deposits = json_decode($fee_deposite['amount_detail']);
                                                                ?>

                                                                <?php foreach ($fee_deposits as $key => $value) { 
                                                                    $paid_amount = $paid_amount + $value->amount; 
                                                                    $single_discount = $single_discount + $value->amount_discount;
                                                                }
                                                            }
                                                            if(isset($discount_applied)){
                                                                $discount_given = $discount_applied['amount_detail'];
                                                            }
                                                            $remainaing_single = $fee_value->amount - $paid_amount - $discount_given - $single_discount;


                                                            if($remainaing_single != '0'){
                                                                
                                                ?>
                                                        <tr>
                                                            <td><?php echo $fee_value->name ?><?php if($fee_value->submission_type!=="1") : ?><?php if(isset($fee_value->months)) : ?>&nbsp;<?php echo "(" . $this->customlib->getMonthdata($fee_value->months) . ")"; ?><?php endif; ?><?php endif; ?></td>
                                                            <td class="text-right"><?php echo ($currency_symbol . "&nbsp;" . number_format($remainaing_single, 2, '.', ''))?></td>
                                                        </tr>
                                                        
                                                        
                                                        <?php }}
                                                        $count++;
                                                        $totalamount = $totalamount + $fee_value->amount - $paid_amount - $discount_given - $single_discount;
                                                         ?>
                                                        <?php endforeach; ?>
                                                        <?php 
                                                        $req = 5 - $count;
                                                        for($i = 1; $i <= $req; $i++){
                                                        ?>
                                                        <tr>
                                                            <td></td>
                                                            <td>&nbsp;</td>
                                                        </tr>

                                                            <?php }
                                                }
                                                ?>

                                                <tr class="success back-color">

                                                    <td align="left" class="text text-left">
                                                        <b><?php echo $this->lang->line('total'); ?></b>
                                                    </td>

                                                    <td class="text text-right"> <b>  <?php
                                                            echo ($currency_symbol . "&nbsp;" . number_format($totalamount, 2, '.', ''));
                                                            ?></b>
                                                    </td> 
                                                </tr>
                                                <tr>
                                                    <td colspan="2" style="text-transform: capitalize">
                                                        <strong>
                                                            <?php echo $this->lang->line('in_words'); ?> : 
                                                            <?php echo $this->customlib->getNepaliCurrency($totalamount). " " . "Only"; ?>
                                                       </strong>
                                                    </td>
                                                </tr>
                                            
                                        </tbody>
                                    </table>
                                <div class="row header ">
                                    <div class="col-sm-12">   
                                        <!-- <span style="font-size:10px;font-style: italic;font-weight: normal;"><?php $this->setting_model->get_receiptfooter(); ?></span> -->  
                                        <div class="col-sm-8" style="padding-top:40px;font-size: 16px;padding-left: 25%">
                                            <strong><?php echo $this->lang->line('thank_you'); ?></strong>
                                        </div>     
                                        <div class="col-sm-4 text-center" style="padding-top:40px">
                                            <hr style="margin-bottom: 0px;border-color: #000;">
                                            <span style="font-size:17px;font-weight: bold"><?php echo $this->lang->line('account_section'); ?></span>
                                        </div>                
                                    </div>
                                </div> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
                                    <?php
                                }else{ ?>
                                    <div class="container" style="display: none"></div>
                             <?php   }
                                ?>
            <div class="page-break"></div>
            <?php endforeach; ?>
        
        
