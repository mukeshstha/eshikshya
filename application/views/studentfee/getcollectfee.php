<?php
$currency_symbol = $this->customlib->getSchoolCurrencyFormat();
?>
<style type="text/css">
    .collect_grp_fees{
      font-size: 15px;
    font-weight: 600;
    padding-bottom: 15px;
    }

    .fees-list {
        list-style: none;
        margin: 0;
        padding: 0;
    }
    .fees-list>.item {
        border-radius: 3px;
        -webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
        box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
        padding: 10px 0;
        background: #fff;
    }
    .fees-list>.item:before, .fees-list>.item:after {
        content: " ";
        display: table;
    }
    .fees-list>.item:after {
        clear: both;
    }
    .fees-list .product-img {
        float: left; 
    }
    .fees-list .product-img img {
        width: 50px;
        height: 50px;
    }
    .fees-list .product-info {
        margin-left: 0px;
    }
    .fees-list .product-title {
        font-weight: 600;
        font-size: 15px;
        display: inline;
    }
    .fees-list .product-title span{

        font-size: 15px;
        display: inline;
        font-weight: 100 !important;
    }
    .fees-list .product-description {
        display: block;
        color: #999;
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
    }
    .fees-list-in-box>.item {
        -webkit-box-shadow: none;
        box-shadow: none;
        border-radius: 0;
        border-bottom: 1px solid #f4f4f4;
    }
    .fees-list-in-box>.item:last-of-type {
        border-bottom-width: 0;
    }

.fees-footer {
    border-top-color: #f4f4f4;
}
.fees-footer {
    padding: 15px 0px 0px 0px;
    text-align: right;
    border-top: 1px solid #e5e5e5;
}
</style>
<div class="box-body">
<div class="row">
  <div class="col-lg-12">
    <div class="form-horizontal">
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-3 control-label"><?php echo $this->lang->line('date'); ?> <small class="req"> *</small></label>
            <div class="col-sm-9">
                <?php if($this->customlib->getcalender()->calender=="0") : ?>
                <input id="nepalidate" name="collected_date" placeholder="" type="text" class="form-control" value="<?php echo date($this->customlib->currentdate()); ?>" readonly="readonly" autocomplete="off">

                <?php else : ?>
                <input id="date" name="collected_date" placeholder="" type="text" class="form-control date_fee" value="" readonly="readonly" autocomplete="off">
            <?php endif; ?>
                <span id="form_collection_collected_date_error" class="text text-danger"></span>
            </div>
        </div>


        <div class="form-group">
            <label for="inputPassword3" class="col-sm-3 control-label"> <?php echo $this->lang->line('payment') . " " . $this->lang->line('mode'); ?></label>
            <div class="col-sm-9">
                <label class="radio-inline">
                    <input type="radio" name="payment_mode_fee" value="Cash" checked="checked"> <?php echo $this->lang->line('cash'); ?></label>
                <label class="radio-inline">
                    <input type="radio" name="payment_mode_fee" value="Cheque"> <?php echo $this->lang->line('cheque'); ?></label>
                <label class="radio-inline" style="display: none;">
                    <input type="radio" name="payment_mode_fee" value="DD"><?php echo $this->lang->line('dd'); ?></label>
                <label class="radio-inline">
                    <input type="radio" name="payment_mode_fee" value="bank_transfer"><?php echo $this->lang->line('bank_transfer'); ?>
                </label>
                <label class="radio-inline" style="display: none;">
                    <input type="radio" name="payment_mode_fee" value="upi"><?php echo $this->lang->line('upi'); ?>
                </label>
                <label class="radio-inline">
                    <input type="radio" name="payment_mode_fee" value="card"><?php echo $this->lang->line('card'); ?>
                </label>
                <span class="text-danger" id="payment_mode_error"></span>
            </div>
            <span id="form_collection_payment_mode_fee_error" class="text text-danger"></span>
        </div>
        <div class="form-group">
            <label for="inputPassword3" class="col-sm-3 control-label"><?php echo $this->lang->line('amount'); ?><small class="req"> *</small></label>
            <div class="col-sm-9">

                <input type="text" autofocus="" class="form-control modal_amount" id="amount" value="0" name="totalpaid" >

                <span class="text-danger" id="amount_error"></span>
            </div>
        </div>
        <div class="form-group" style="display: none;">
            <label for="inputPassword3" class="col-sm-3 control-label"> <?php echo $this->lang->line('discount'); ?> <?php echo $this->lang->line('group'); ?></label>
            <div class="col-sm-9">
                <select class="form-control modal_discount_group" id="discount_group">
                    <option value=""><?php echo $this->lang->line('select'); ?></option>
                </select>

                <span class="text-danger" id="amount_error"></span>
            </div>
        </div>
        <div class="form-group"  style="display: none;">
            <label for="inputPassword3" class="col-sm-3 control-label"><?php echo $this->lang->line('discount'); ?><small class="req"> *</small></label>
            <div class="col-sm-9">
                <div class="row">
                    <div class="col-md-5 col-sm-5">
                        <div class="">
                            <input type="text" class="form-control" id="amount_discount"  value="0">

                            <span class="text-danger" id="amount_discount_error"></span></div>
                    </div>
                    <div class="col-md-2 col-sm-2 ltextright">

                        <label for="inputPassword3" class="control-label"><?php echo $this->lang->line('fine'); ?><small class="req">*</small></label>
                    </div>
                    <div class="col-md-5 col-sm-5">
                        <div class="">
                            <input type="text" class="form-control" id="amount_fine" value="0">

                            <span class="text-danger" id="amount_fine_error"></span>
                        </div>
                    </div>
                </div>
            </div><!--./col-sm-9-->
        </div>
        <div class="form-group">
            <label for="inputPassword3" class="col-sm-3 control-label"> <?php echo $this->lang->line('note') ?></label>

            <div class="col-sm-9">
                <textarea class="form-control" rows="3" name="fee_gupcollected_note" id="description" placeholder=""></textarea>
                <span id="form_collection_fee_gupcollected_note_error" class="text text-danger"></span>
            </div>
        </div>

    </div>

<ul class="fees-list fees-list-in-box">
    <?php
    $row_counter = 1;
    $total_amount = 0;
    $total_discount = 0;
    foreach ($feearray as $fee_key => $fee_value) {
        $CI =& get_instance();
        $CI->load->model('Feediscount_model');
        $amount_prev_paid = 0;
        $fees_fine_amount = 0;
        $fine_amount_paid = 0;
        $fine_amount_status = false;
        $amount_to_be_pay = $fee_value->amount;
        $student_sess = $fee_value->id;
        $total_result = count($CI->feediscount_model->discount_active($student_sess));
        $data = $CI->feediscount_model->discount_active($student_sess);

        if ($fee_value->is_system) {
            $amount_to_be_pay = $fee_value->student_fees_master_amount;
        }
        if($total_result>0){
            foreach($data as $value){
                $discountallocated=$value['amount_detail'];
            }
            $total_discount = $total_discount + $discountallocated;
        }
        

    
        if (is_string(($fee_value->amount_detail)) && is_array(json_decode(($fee_value->amount_detail), true))) {

            $amount_data = json_decode($fee_value->amount_detail);

            foreach ($amount_data as $amount_data_key => $amount_data_value) {
                      $fine_amount_paid+=$amount_data_value->amount_fine;
                $amount_prev_paid = $amount_prev_paid + ($amount_data_value->amount + $amount_data_value->amount_discount);
            }

            if ($fee_value->is_system) {
                $amount_to_be_pay = $fee_value->student_fees_master_amount - $amount_prev_paid;
            } else {

                $amount_to_be_pay = $fee_value->amount - $amount_prev_paid;
            }
        }

    if (($fee_value->due_date != "0000-00-00" && $fee_value->due_date != NULL) && (strtotime($fee_value->due_date) < strtotime(date('Y-m-d'))) && $amount_to_be_pay > 0) {
         $fees_fine_amount=$fee_value->fine_amount-$fine_amount_paid;
         $total_amount=$total_amount+$fees_fine_amount;
         $fine_amount_status=true;
        }


        $total_amount = $total_amount + $amount_to_be_pay;
        if ($amount_to_be_pay > 0) {
            ?>
            <?php if($total_result>0){
                foreach($data as $value){
                    $discount_applied=$value['amount_detail']; 
                }
                if ($amount_to_be_pay - $discount_applied > 0) {?>
            <li class="item">
                <input name="row_counter[]" type="hidden" value="<?php echo $row_counter; ?>">
                <input name="student_fees_master_id_<?php echo $row_counter; ?>" type="hidden" value="<?php echo $fee_value->id; ?>">
                <input name="fee_groups_feetype_id_<?php echo $row_counter; ?>" type="hidden" value="<?php echo $fee_value->fee_groups_feetype_id; ?>">
                <input type="hidden" name="glname_<?php echo $row_counter; ?>" value="<?php echo $fee_value->glname ?>">
                <input type="hidden" name="title_<?php echo $row_counter; ?>" value="<?php echo $fee_value->name ?>">
                 <input name="fee_groups_feetype_fine_amount_<?php echo $row_counter; ?>" type="hidden" value="<?php echo $fees_fine_amount; ?>">
                <input name="fee_amount_<?php echo $row_counter; ?>" type="hidden" value="<?php echo $amount_to_be_pay-$discount_applied; ?>">

                <div class="product-info">
                    <a href="#"  onclick="return false;" class="product-title"><?php echo $fee_value->name; ?><?php if($fee_value->submission_type !== "1") : ?>&nbsp;
                        <?php echo "(" . $this->customlib->getMonthdata($fee_value->months) . ")"; ?><?php endif; ?>
                        <span class="pull-right"><?php echo  $currency_symbol.number_format((float) $amount_to_be_pay-$discount_applied, 2, '.', ''); ?></span>
                        
                    </a>
                         <span class="product-description" style="display: none;">
                        <?php echo $fee_value->code; ?>
                        </span>
                        <?php if($fine_amount_status){ ?>
                       <a href="#"  onclick="return false;" class="product-title text text-danger"><?php echo $this->lang->line('fine'); ?>
                        <span class="pull-right">
                            <?php echo  $currency_symbol.number_format((float) $fees_fine_amount, 2, '.', ''); ?>                                
                        </span>
                    </a>

                    <?php } ?>
                </div>
            </li>
            <?php   
                    }    }else{ ?>

                            <li class="item">
                <input name="row_counter[]" type="hidden" value="<?php echo $row_counter; ?>">
                <input name="student_fees_master_id_<?php echo $row_counter; ?>" type="hidden" value="<?php echo $fee_value->id; ?>">
                <input name="fee_groups_feetype_id_<?php echo $row_counter; ?>" type="hidden" value="<?php echo $fee_value->fee_groups_feetype_id; ?>">
                <input type="hidden" name="glname_<?php echo $row_counter; ?>" value="<?php echo $fee_value->glname ?>">
                <input type="hidden" name="title_<?php echo $row_counter; ?>" value="<?php echo $fee_value->name ?>">
                 <input name="fee_groups_feetype_fine_amount_<?php echo $row_counter; ?>" type="hidden" value="<?php echo $fees_fine_amount; ?>">
                <input name="fee_amount_<?php echo $row_counter; ?>" type="hidden" value="<?php echo $amount_to_be_pay; ?>">

                <div class="product-info">
                    <a href="#"  onclick="return false;" class="product-title"><?php echo $fee_value->name; ?><?php if($fee_value->submission_type !== "1") : ?>&nbsp;<?php if(isset($fee_value->months)) : ?>
                        <?php echo "(" . $this->customlib->getMonthdata($fee_value->months) . ")"; ?><?php endif; ?><?php endif; ?>
                        <span class="pull-right"><?php echo  $currency_symbol.number_format((float) $amount_to_be_pay, 2, '.', ''); ?></span>
                        
                    </a>
                         <span class="product-description" style="display: none;">
                        <?php echo $fee_value->code; ?>
                        </span>
                        <?php if($fine_amount_status){ ?>
                       <a href="#"  onclick="return false;" class="product-title text text-danger"><?php echo $this->lang->line('fine'); ?>
                        <span class="pull-right">
                            <?php echo  $currency_symbol.number_format((float) $fees_fine_amount, 2, '.', ''); ?>                                
                        </span>
                    </a>

                    <?php } ?>
                </div>
            </li>
                        <?php } ?>

            <?php
        }

        $row_counter++;
    }
    ?>
</ul>
</div>

</div>
</div>
<?php if ($total_amount > 0) { ?>
<div class="row collect_grp_fees">
    <div class="col-md-8">
        <span class="pull-right">
            <?php echo $this->lang->line('total') . " " . $this->lang->line('pay'); ?>
        </span>
    </div>
    <div class="col-md-4">
        <span class="pull-right">
            <?php echo $currency_symbol.number_format((float) $total_amount-$total_discount, 2, '.', ''); ?>
            <input type="hidden" name="totalpay" value="<?php echo number_format((float) $total_amount-$total_discount, 2, '.', ''); ?>">
        </span>

    </div>
</div>
<div class="row fees-footer">
    <div class="col-md-12">
          <button type="submit" class="btn btn-primary pull-right payment_collect" data-loading-text="<i class='fa fa-spinner fa-spin '></i><?php echo $this->lang->line('processing')?>"><i class="fa fa-money"></i> <?php echo $this->lang->line('pay'); ?></button>

    </div>
</div>
<?php }else{ ?>
    <div class="row">
    <div class="col-md-12">
<div class="alert alert-info">
    <?php echo $this->lang->line('no_fees_found'); ?>
</div>
</div>
    <?php
}

 ?>


    <?php $calender= $this->customlib->getcalender(); ?>
    <?php if($calender->calender=="0") : ?>
        <script type="text/javascript">
            var date_format_nepali = '<?php echo $result = strtr($this->customlib->getSchoolDateFormat(), ['d' => 'DD', 'm' => 'MM', 'Y' => 'YYYY']) ?>';
            var currentdate = '<?php echo $today= $this->customlib->currentdate() ?>';
            $(function() {
                $("#nepalidate").nepaliDatePicker({
                    language:"english",
                    dateFormat: date_format_nepali,
                    container:'#collect_fee_group',
                    disableAfter: currentdate,
                });
            });
        </script>
    <?php endif; ?>