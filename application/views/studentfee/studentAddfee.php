<style type="text/css">
    .checkbox-inline+.checkbox-inline, .radio-inline+.radio-inline {
    margin-left: 8px;}
@media print {
    .no-print{
        display: none !important;
    }
}

.discount_row td{
    background: #abe7a0 !important;
    font-size: 12px !important;
}

.white-td {
    background: #a5d7d4 !important;
    border-top: 1px solid #f4f4f4;
    font-size: 12px !important;
}
</style>
<?php
$currency_symbol = $this->customlib->getSchoolCurrencyFormat();
$language        = $this->customlib->getLanguage();
$language_name   = $language["short_code"];
$subdomain         = $_SERVER['SERVER_NAME'];
?>
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-12">
            <section class="content-header">
                <h1>
                    <i class="fa fa-money"></i> <?php echo $this->lang->line('fees_collection'); ?><small><?php echo $this->lang->line('student_fee'); ?></small></h1>
            </section>

        </div>
        <div>
            <a id="sidebarCollapse" class="studentsideopen"><i class="fa fa-navicon"></i></a>
            <aside class="studentsidebar">
                <div class="stutop" id="">
                    <!-- Create the tabs -->
                    <div class="studentsidetopfixed">
                        <input type="hidden" class="date" name="">
                        <p class="classtap"><?php echo $student["class"]; ?> <a href="#" data-toggle="control-sidebar" class="studentsideclose"><i class="fa fa-times"></i></a></p>
                        <ul class="nav nav-justified studenttaps">
                            <?php foreach ($class_section as $skey => $svalue) {
                            ?>
                            <li <?php
                                if ($student["section_id"] == $svalue["section_id"]) {
                                        echo "class='active'";
                                    }
                                    ?> ><a href="#section<?php echo $svalue["section_id"] ?>" data-toggle="tab"><?php print_r($svalue["section"]);?></a></li>
                                <?php }?>
                        </ul>
                    </div>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <?php foreach ($class_section as $skey => $snvalue) {
                        ?>
                            <div class="tab-pane <?php
                                if ($student["section_id"] == $snvalue["section_id"]) {
                                        echo "active";
                                    }
                                    ?>" id="section<?php echo $snvalue["section_id"]; ?>">
                                <?php
                                    foreach ($studentlistbysection as $stkey => $stvalue) {
                                        if ($stvalue['section_id'] == $snvalue["section_id"]) {
                                    ?>
                                        <div class="studentname">
                                            <a class="" href="<?php echo base_url() . "studentfee/addfee/" . $this->studentsession_model->findstudentsession($stvalue["id"] ) ?>">
                                                <?php if(isset($stvalue["image"])) : ?>
                                                <div class="icon"><img src="<?php echo base_url() . $stvalue["image"]; ?>" alt="User"></div>
                                                <?php else : ?>
                                                    <?php if($stvalue['gender']=="Male") : ?>
                                                        <div class="icon"><img src="<?php echo base_url() . "/uploads/".$subdomain."/student_images/default_male.jpg"; ?>" alt="User"></div>
                                                    <?php else : ?>
                                                        <div class="icon"><img src="<?php echo base_url() . "/uploads/".$subdomain."/student_images/default_female.jpg"; ?>" alt="User"></div>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                                <div class="student-tittle"><?php echo $stvalue["firstname"] . " " . $stvalue["lastname"]; ?></div></a>
                                        </div>
                                        <?php
                                        }
                                    }
                                ?>
                            </div>
                        <?php } ?>
                        <div class="tab-pane" id="sectionB">
                            <h3 class="control-sidebar-heading">Recent Activity 2</h3>
                        </div>

                        <div class="tab-pane" id="sectionC">
                            <h3 class="control-sidebar-heading">Recent Activity 3</h3>
                        </div>
                        <div class="tab-pane" id="sectionD">
                            <h3 class="control-sidebar-heading">Recent Activity 3</h3>
                        </div>
                        <!-- /.tab-pane -->
                    </div>
                </div>
            </aside>
        </div>
    </div>
    <!-- /.control-sidebar -->


    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header" style="padding:5px">
                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-md-4">
                                <h3 class="box-title"><?php echo $this->lang->line('student_fees'); ?></h3>
                            </div>
                            <div class="col-md-8">
                                <div class="btn-group pull-right">
                                    <a href="<?php echo base_url() ?>studentfee" type="button" class="btn btn-primary btn-xs" style="padding:10px">
                                        <i class="fa fa-arrow-left"></i> <?php echo $this->lang->line('back'); ?></a>
                                </div>
                            </div>

                        </div>
                    </div><!--./box-header-->
                    <div class="box-body" style="padding-top:0;">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="sfborder" style="padding:5px; border-radius: 0px">
                                    <div class="col-md-4">
                                        <div class="recipe-card recipe-card-blue">

                                            <article>
                                                <div class="text-center">
                                                    
                                                
                                                <img width="115" height="115" class="round5" src="<?php
                                                if (!empty($student['image'])) {
                                                    echo base_url() . $student['image'];
                                                } else {
                                                    echo base_url() . "uploads/". $subdomain."/student_images/no_image.png";
                                                }
                                                ?>" alt="No Image">
                                                </div>
                                                <h2><?php echo $this->lang->line('name'); ?>: <?php echo $this->customlib->getFullName($student['firstname'], $student['middlename'], $student['lastname'], $sch_setting->middlename, $sch_setting->lastname); ?></h2>
                                                <h3><?php echo $this->lang->line('class_section'); ?>: <?php echo $student['class'] . " (" . $student['section'] . ")" ?></h3>
                                            </article>
                                        </div>
                                        
                                    </div>

                                    <div class="col-md-8">
                                        <div style="display: none;">
                                            <?php $siblings = $this->student_model->getMySiblings($student['parent_id'],$student['id']); ?>
                                            <?php echo "<pre>"; print_r($siblings); echo "</pre>"; ?>
                                        
                                        </div>
                                        <ul class="list-group" style="padding:5px">
                                              <li class="list-group-item"><b><?php echo $this->lang->line('guardian_name'); ?>:</b> <?php echo $student['guardian_name']; ?> &nbsp; <?php echo $student['guardian_phone']; ?></li>
                                              <li class="list-group-item"><b><?php echo $this->lang->line('admission_no'); ?>:</b> <?php echo $student['admission_no']; ?></li>
                                              <li class="list-group-item"><b><?php echo $this->lang->line('mobile_no'); ?>:</b> <?php echo $student['mobileno']; ?></li>
                                              <li class="list-group-item"><b><?php echo $this->lang->line('roll_no'); ?>:</b> <?php echo $student['roll_no']; ?></li>
                                              <li class="list-group-item" style="display: none;"><b><?php echo $this->lang->line('category'); ?>:</b> 
                                                 <?php
                                                    foreach ($categorylist as $value) {
                                                        if ($student['category_id'] == $value['id']) {
                                                            echo $value['category'];
                                                        }
                                                    }
                                                ?>
                                              </li>
                                              <?php if ($sch_setting->rte) {?>
                                                <li class="list-group-item" style="display:none;"><b><?php echo $this->lang->line('rte'); ?></b> <b class="text-danger"> <?php echo $student['rte']; ?> </li>
                                                
                                            <?php }?>
                                            <li class="list-group-item"> <b style="text-decoration: underline;font-weight: bold;vertical-align: top;margin-right: 15px">Siblings:</b>
                                            <?php foreach ($siblings as $key => $value) { 
                                                $session_id = $this->studentsession_model->getstudentsessionid($value->session_id,$value->id);
                                            ?>
                                            <a class="siblingss btn-danger" style="padding:5px;margin-right: 10px;display: inline-block;border-radius: 5px" type="button" href="<?php echo base_url() . "studentfee/addfee/" . "" . $session_id->id; ?>"><?php echo $value->firstname . " " . $value->middlename . " " . $value->lastname; ?></a>
                                        <?php } ?></li>
                                              
                                        </ul>
                                        
                                        <div class="row" style="display: none;">
                                            <table class="table table-striped mb0 font13">
                                                <tbody>
                                                    <!-- <tr>
                                                        <th class="bozero"><?php echo $this->lang->line('name'); ?></th>
                                                        <td class="bozero"><?php echo $this->customlib->getFullName($student['firstname'], $student['middlename'], $student['lastname'], $sch_setting->middlename, $sch_setting->lastname); ?></td>

                                                        <th class="bozero"><?php echo $this->lang->line('class_section'); ?></th>
                                                        <td class="bozero"><?php echo $student['class'] . " (" . $student['section'] . ")" ?> </td>
                                                    </tr> -->
                                                    <tr>
                                                        <th><?php echo $this->lang->line('father_name'); ?></th>
                                                        <td><?php echo $student['father_name']; ?></td>
                                                        <th><?php echo $this->lang->line('admission_no'); ?></th>
                                                        <td><?php echo $student['admission_no']; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <th><?php echo $this->lang->line('mobile_no'); ?></th>
                                                        <td><?php echo $student['mobileno']; ?></td>
                                                        <th><?php echo $this->lang->line('roll_no'); ?></th>
                                                        <td> <?php echo $student['roll_no']; ?>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th><?php echo $this->lang->line('category'); ?></th>
                                                        <td>
                                                            <?php
                                                            foreach ($categorylist as $value) {
                                                                if ($student['category_id'] == $value['id']) {
                                                                    echo $value['category'];
                                                                }
                                                            }
                                                            ?>
                                                        </td>
                                                        <?php if ($sch_setting->rte) {?>
                                                            <th><?php echo $this->lang->line('rte'); ?></th>
                                                            <td><b class="text-danger"> <?php echo $student['rte']; ?> </b>
                                                            </td>
                                                        <?php }?>
                                                    </tr>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                   
                                </div>
                            </div>
                        </div>
                        <div class="row no-print">
                            <div class="col-md-12 mDMb10">
                                <a href="#" class="btn btn-sm btn-warning printSelected" style="padding: 8px;"><i class="fa fa-print"></i> <?php echo $this->lang->line('print_selected'); ?> </a>

                                <button type="button" class="btn btn-sm btn-success collectSelected" id="load" data-loading-text="<i class='fa fa-spinner fa-spin '></i> <?php echo $this->lang->line('please_wait') ?>" style="padding: 8px;"><i class="fa fa-money"></i> <?php echo $this->lang->line('pay') . " " . $this->lang->line('fees') ?></button>
                                <a href="#" class="btn btn-sm btn-info printMonthlybill" style="display: none;"><i class="fa fa-print"></i> <?php echo $this->lang->line('print_monthly_bill'); ?> </a>
                                <a href="#" class="btn btn-sm btn-info printToday" style="padding: 8px;"><i class="fa fa-print"></i> <?php echo "Print Today"; ?> </a>
                                <button class="btn btn-sm btn-danger showall pull-right" style="padding: 8px;"></button>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <div class="download_label "><?php echo $this->lang->line('student_fees') . ": " . $student['firstname'] . " " . $student['lastname'] ?> </div>
                            <table class="table table-striped table-bordered table-hover example table-fixed-header">
                                <thead class="header">
                                    <tr>
                                        <th class="no-print" style="width: 10px"><input type="checkbox" id="select_all"/></th>
                                        <th align="left"><?php echo $this->lang->line('fees_group'); ?></th>
                                        <th align="left" class="text text-left no-print"><?php echo $this->lang->line('due_date'); ?></th>
                                        <th align="left" class="text text-left"><?php echo $this->lang->line('status'); ?></th>
                                        <th class="text text-right"><?php echo $this->lang->line('amount') ?> <span><?php echo "(" . $currency_symbol . ")"; ?></span></th>
                                        <th  class="text text-left"><?php echo $this->lang->line('date'); ?></th>
                                        <th class="text text-right" ><?php echo $this->lang->line('discount'); ?> <span><?php echo "(" . $currency_symbol . ")"; ?></span></th>
                                        <th class="text text-right"><?php echo $this->lang->line('fine'); ?> <span><?php echo "(" . $currency_symbol . ")"; ?></span></th>
                                        <th class="text text-right"><?php echo $this->lang->line('paid'); ?> <span><?php echo "(" . $currency_symbol . ")"; ?></span></th>
                                        <th class="text text-right"><?php echo $this->lang->line('balance'); ?> <span><?php echo "(" . $currency_symbol . ")"; ?></span></th>
                                        <th class="text text-right no-print"><?php echo $this->lang->line('action'); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
<?php
    $total_amount           = 0;
    $total_deposite_amount  = 0;
    $total_fine_amount      = 0;
    $total_fees_fine_amount = 0;

    $total_discount_amount = 0;
    $total_balance_amount  = 0;
    $alot_fee_discount     = 0;

    foreach ($student_due_fee as $key => $fee) {
    	$CI =& get_instance();
        $CI->load->model('Feediscount_model');
        $student_sess = $fee->id;
        $total_result = count($CI->feediscount_model->discount_active($student_sess));
        $data = $CI->feediscount_model->discount_active($student_sess);

        foreach ($fee->fees as $fee_key => $fee_value) {
            $fee_paid         = 0;
            $fee_discount     = 0;
            $fee_fine         = 0;
            $fees_fine_amount = 0;
            $discount_applied = 0;
            $total_discount_paid = 0;
            
            if (!empty($fee_value->amount_detail)) {
                $fee_deposits = json_decode(($fee_value->amount_detail));

                foreach ($fee_deposits as $fee_deposits_key => $fee_deposits_value) {
                    $fee_paid     = $fee_paid + $fee_deposits_value->amount;
                    $fee_discount = $fee_discount + $fee_deposits_value->amount_discount;
                    $fee_fine     = $fee_fine + $fee_deposits_value->amount_fine;
                }
            }

            if (($fee_value->due_date != "0000-00-00" && $fee_value->due_date != null) && (strtotime($fee_value->due_date) < strtotime(date('Y-m-d')))) {
                $fees_fine_amount       = $fee_value->fine_amount;
                $total_fees_fine_amount = $total_fees_fine_amount + $fee_value->fine_amount;
            }
            if($total_result>0){
                foreach($data as $value){
                    $discount_applied=$value['amount_detail'];
                }
            }

            $total_amount          = $total_amount + $fee_value->amount;
            $total_discount_amount = $total_discount_amount + $fee_discount + $discount_applied;
            $total_deposite_amount = $total_deposite_amount + $fee_paid;
            $total_fine_amount     = $total_fine_amount + $fee_fine;
            $feetype_balance       = $fee_value->amount - ($fee_paid + $fee_discount)-$discount_applied;
            $total_balance_amount  = $total_balance_amount + $feetype_balance;
        
        ?>
        <?php
            if ($feetype_balance > 0 && strtotime($fee_value->due_date) < strtotime(date('Y-m-d'))) {

        ?>
                <tr class="danger font12 <?php if ($feetype_balance == 0) { echo "hide-display"; } ?>">
            <?php } else { ?>

                <tr class="dark-gray <?php if ($feetype_balance == 0) { echo "hide-display"; } ?>">
            <?php
                }
            ?>
                    <td class="no-print"><input class="checkbox" type="checkbox" name="fee_checkbox" data-fee_master_id="<?php echo $fee_value->id ?>" data-fee_session_group_id="<?php echo $fee_value->fee_session_group_id ?>" data-fee_groups_feetype_id="<?php echo $fee_value->fee_groups_feetype_id ?>"></td>
                    <td align="left"><?php echo $fee_value->name; ?><?php if($fee_value->submission_type !== "1") : ?> &nbsp;<?php if(isset($fee->months)) : ?>
                        <?php echo "(" . $this->customlib->getMonthdata($fee->months) . ")"; ?><?php endif; ?><?php endif; ?></td>
                    <td align="left" class="text text-left no-print">
                        <?php
                            if ($fee_value->due_date == "0000-00-00") {
                            } else {
                                echo date($this->customlib->getSchoolDateFormat(), $this->customlib->dateyyyymmddTodateformat($fee_value->due_date));
                            }
                            ?>
                    </td>
                    <td align="left" class="text text-left width85">
                        <?php
                            if ($feetype_balance == 0) {
                                        ?><span class="label label-success"><?php echo $this->lang->line('paid'); ?></span><?php
                            } else if (!empty($fee_value->amount_detail)) {
                                        ?><span class="label label-warning"><?php echo $this->lang->line('partial'); ?></span><?php
                            } else {
                                        ?><span class="label label-danger"><?php echo $this->lang->line('unpaid'); ?></span><?php
                            }
                        ?>
                    </td>
                    <td class="text text-right">
                        <?php echo $fee_value->amount;
                            if (($fee_value->due_date != "0000-00-00" && $fee_value->due_date != null) && (strtotime($fee_value->due_date) < strtotime(date('Y-m-d')))) {
                        ?>
                        <span class="text text-danger"><?php echo " + " . ($fee_value->fine_amount); ?></span>
                        <?php
                            }
                        ?>
                    </td>

                    <td class="text text-left"></td>
                    <td class="text text-right"><?php echo (number_format($fee_discount+ $discount_applied, 2, '.', '')); ?></td>
                    <td class="text text-right"><?php echo (number_format($fee_fine, 2, '.', '')); ?></td>
                    <td class="text text-right"><?php echo (number_format($fee_paid, 2, '.', '')); ?></td>
                    <td class="text text-right">
                        <?php 
                        $display_none = "ss-none";
                        if ($feetype_balance > 0) {
                            $display_none = "";

                            echo (number_format($feetype_balance, 2, '.', ''));
                        }
                        ?>
                    </td>

                    <td class="no-print" width="100">
                        <div class="btn-group">
                            <div class="pull-right">
                                <button type="button" data-student_session_id="<?php echo $fee->student_session_id; ?>" data-glname="<?php echo $fee_value->glname; ?>" data-student_fees_master_id="<?php echo $fee->id; ?>" data-fee_groups_feetype_id="<?php echo $fee_value->fee_groups_feetype_id; ?>" data-group="<?php echo $fee_value->name; ?>" data-type="<?php echo $fee_value->type; ?>" data-remaining="<?php echo (number_format($feetype_balance, 2, '.', '')); ?>" class="btn btn-xs btn-default myCollectFeeBtn <?php echo $display_none; ?>" title="<?php echo $this->lang->line('add_fees'); ?>" data-toggle="modal" data-target="#myFeesModal"><i class="fa fa-plus"></i></button> 

                                <button  style="display: none;" class="btn btn-xs btn-default printInv" data-fee_master_id="<?php echo $fee_value->id ?>" data-fee_session_group_id="<?php echo $fee_value->fee_session_group_id ?>" data-fee_groups_feetype_id="<?php echo $fee_value->fee_groups_feetype_id ?>" title="<?php echo $this->lang->line('print'); ?>"><i class="fa fa-print"></i> </button>

                                <button style="display: none;" class="view-btn btn btn-xs btn-default"><i class="fa fa-eye"></i></button>
                            </div>
                        </div>
                    </td>
                </tr>

                <?php
                    if (!empty($fee_value->amount_detail)) {
                        $fee_deposits = json_decode(($fee_value->amount_detail));

                        foreach ($fee_deposits as $fee_deposits_key => $fee_deposits_value) {
                        ?>

                            <tr class="white-td deposit-description <?php if ($feetype_balance == 0) { echo "hide-display"; } ?>">
                                <td class="no-print" align="left"></td>
                                <td class="text-right"><img src="<?php echo base_url(); ?>backend/images/table-arrow.png" alt="" /></td>
                                <td class="text text-left">
                                    <a href="#" data-toggle="popover" class="detail_popover" > <?php echo $fee_value->student_fees_deposite_id . "/" . $fee_deposits_value->inv_no; ?></a>
                                    <div class="fee_detail_popover">
                                        <?php
                                            if ($fee_deposits_value->description == "") {
                                        ?>
                                            <p class="text text-danger"><?php echo $this->lang->line('no_description'); ?></p>
                                            <?php
                                            } else {
                                            ?>
                                            <p class="text text-info"><?php echo $fee_deposits_value->description; ?></p>
                                            <?php
                                            }
                                            ?>
                                    </div>
                                </td>
                                <td class="text">
                                    <?php if($fee_deposits_value->amount==0) : ?>
                                        <?php echo "Discount Given"; ?>
                                    <?php else : ?>
                                        <?php echo $this->lang->line(strtolower($fee_deposits_value->payment_mode)); ?>
                                    <?php endif; ?>
                                        
                                </td>
                                <td></td>
                                <td class="text text-left">
                                    <?php echo date($this->customlib->getSchoolDateFormat(), $this->customlib->dateyyyymmddTodateformat($fee_deposits_value->date)); ?>
                                </td>
                                <td class="text text-right"><?php echo (number_format($fee_deposits_value->amount_discount, 2, '.', '')); ?></td>
                                <td class="text text-right"><?php echo (number_format($fee_deposits_value->amount_fine, 2, '.', '')); ?></td>
                                <td class="text text-right"><?php echo (number_format($fee_deposits_value->amount, 2, '.', '')); ?></td>
                                <td></td>
                                <td class="text text-right no-print">
                                    <div class="btn-group ">
                                       <div class="pull-right">
                                        <?php if ($this->rbac->hasPrivilege('collect_fees', 'can_delete')) {?>
                                            <!-- title="<?php echo $this->lang->line('revert'); ?>" -->
                                            <!-- <button class="btn btn-default btn-xs" data-invoiceno="<?php echo $fee_value->student_fees_deposite_id . "/" . $fee_deposits_value->inv_no; ?>" data-main_invoice="<?php echo $fee_value->student_fees_deposite_id; ?>" data-sub_invoice="<?php echo $fee_deposits_value->inv_no; ?>" data-toggle="modal" data-target="#confirm-delete" >
                                                <i class="fa fa-undo"> </i>
                                            </button> -->
                                        <?php }?>
                                        <button  class="btn btn-xs btn-default printDoc" data-main_invoice="<?php echo $fee_value->student_fees_deposite_id; ?>" data-sub_invoice="<?php echo $fee_deposits_value->inv_no; ?>"  title="<?php echo $this->lang->line('print'); ?>"><i class="fa fa-print"></i> </button>
                                    </div>
                                  </div>
                                </td>
                            </tr>
                        <?php
                        }
                    }
                ?>
<?php if($total_result>0) : ?>
        <?php foreach($data as $value) : ?>
            <tr class="discount_row deposit-description <?php if ($feetype_balance == 0) { echo "hide-display"; } ?>">
            	<td></td>
                <td><?php echo $value['name']; ?></td>
                <td>Discount Applied</td>
                <td></td>
                <td></td>
                <td></td>
                <td class="text text-right"><?php echo $value['amount_detail']; ?></td>
                <td></td>
                <td></td>
                <td></td>
                <td><button class="btn btn-default btn-xs" data-id="<?php echo $value['id']; ?>" data-toggle="modal" data-target="#discount_revert" >
                    <i class="fa fa-undo"> </i>
                    </button></td>
            </tr>
        <?php endforeach; ?>
    


    <?php else : ?>

                <?php

                    if (!empty($student_discount_fee)) {
        
                        foreach ($student_discount_fee as $discount_key => $discount_value) { ?>
                            <?php if($discount_value['feetype_id']==$fee_value->feetype_id) : ?>
                                <tr class="discount_row deposit-description <?php if ($feetype_balance == 0) { echo "hide-display"; } ?>">
                                	<td></td>
                                    <td align="left"> <?php echo $discount_value['name']; ?> (<?php echo $discount_value['code']; ?>) </td>
                                    <td>Discount Not Applied</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td class="text-right">
                                        <?php if($discount_value['amount'] >0 && $discount_value['amount']!== "") {
                                            echo $discount_value['amount'];
                                        }else{
                                            echo $discount_value['discount_percent']. "%";
                                        }
                                        ?>
                                    </td>
                                    <td></td>
                                    <td></td>
                                    <td></td>

                                    <td>
                                    	<?php if ($feetype_balance> 0) : ?>
                                        <button type="button" data-student_session_id="<?php echo $fee->student_session_id; ?>" data-student_fees_master_id="<?php echo $fee->id; ?>" data-fee_groups_feetype_id="<?php echo $fee_value->fee_groups_feetype_id; ?>" data-group="<?php echo $fee_value->name; ?>" data-amount="<?php echo $fee_value->amount; ?>" data-discount="<?php echo $discount_value['name']; ?>" data-type="<?php echo $fee_value->type; ?>" data-percent="<?php echo $discount_value['discount_percent']; ?>" data-amt_discount="<?php echo$discount_value['amount'] ?>" class="btn btn-xs btn-default discount_collect" title="<?php echo $this->lang->line('add_discount'); ?>" data-toggle="modal" data-target="#feediscountmodal"><i class="fa fa-plus"></i></button>
                                    <?php endif; ?>
                                    </td>
                                </tr>
                                        <?php endif; ?>
                                            <?php
                                            }
                                            }
                                        

                                    ?>
        <?php endif; ?>


                                            <?php
                                    }
                                }
 
                                    ?>


                                <tr class="box box-solid total-bg">
                                    <td align="left" ></td>
                                    <td  align="left" class="text text-left" ><?php echo $this->lang->line('grand_total'); ?></td>
                                    <td></td>
                                    <td></td>
                                    <td class="text text-right">
                                        <?php echo number_format($total_amount, 2, '.', '') . "<span class='text text-danger'>+" . number_format($total_fees_fine_amount, 2, '.', '') . "</span>";?>
                                    </td>
                                    <td class="text text-left"></td>

                                    <td class="text text-right"><?php echo (number_format($total_discount_amount, 2, '.', '')); ?></td>
                                    <td class="text text-right"><?php echo (number_format($total_fine_amount, 2, '.', '')); ?></td>
                                    <td class="text text-right"><?php echo (number_format($total_deposite_amount, 2, '.', '')); ?></td>
                                    <td class="text text-right"><?php echo (number_format($total_balance_amount - $alot_fee_discount, 2, '.', '')); ?></td>  
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
        <!--/.col (left) -->
    </div>
</section>
</div>


<div class="modal fade" id="myFeesModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title title text-center fees_title"></h4>
                
            </div>
            <div class="modal-body pb0">
                <div class="form-horizontal balanceformpopup">
                    <div class="box-body">

                        <input  type="hidden" class="form-control" id="std_id" value="<?php echo $student["student_session_id"]; ?>" readonly="readonly"/>
                        <input  type="hidden" class="form-control" id="parent_app_key" value="<?php echo $student['parent_app_key'] ?>" readonly="readonly"/>
                        <input  type="hidden" class="form-control" id="guardian_phone" value="<?php echo $student['guardian_phone'] ?>" readonly="readonly"/>
                        <input  type="hidden" class="form-control" id="guardian_email" value="<?php echo $student['guardian_email'] ?>" readonly="readonly"/>
                        <input  type="hidden" class="form-control" id="student_fees_master_id" value="0" readonly="readonly"/>
                        <input  type="hidden" class="form-control" id="fee_groups_feetype_id" value="0" readonly="readonly"/>
                        <input type="hidden" name="glname" id="glnameid">
                        <input type="hidden" name="title" class="title-input">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label"><?php echo $this->lang->line('date'); ?></label>
                            <div class="col-sm-9">
                                <?php if($this->customlib->getcalender()->calender=="0") : ?>
                                    <input id="date" name="admission_date" placeholder="<?php echo $this->lang->line('placeholder_fee_collection_date') ?>" type="text" class="form-control modeldate"  value="<?php echo date($this->customlib->currentdate()); ?>" readonly="readonly"/>

                                <?php else : ?>
                                <input  id="date" name="admission_date" placeholder="<?php echo $this->lang->line('placeholder_fee_collection_date') ?>" type="text" class="form-control date_fee"  value="<?php echo date($this->customlib->getSchoolDateFormat()); ?>" readonly="readonly"/>
                            <?php endif; ?>
                            </div>
                        </div>
                        <div class="form-group" style="display: none">
                            <label for="inputPassword3" class="col-sm-3 control-label"><?php echo $this->lang->line('amount'); ?><small class="req"> *</small></label>
                            <div class="col-sm-9">

                                <input type="text" autofocus="" class="form-control modal_amount" id="amount" value="0" name="totalpay" >

                                <span class="text-danger" id="amount_error"></span>
                            </div>
                        </div>
                        <div class="form-group" style="display: none">
                            <label for="inputPassword3" class="col-sm-3 control-label"> <?php echo $this->lang->line('discount'); ?> <?php echo $this->lang->line('group'); ?></label>
                            <div class="col-sm-9">
                                <select class="form-control modal_discount_group" id="discount_group">
                                    <option value=""><?php echo $this->lang->line('select'); ?></option>
                                </select>

                                <span class="text-danger" id="amount_error"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-3 control-label"><?php echo $this->lang->line('discount'); ?><small class="req"> *</small></label>
                            <div class="col-sm-9">
                                <div class="row">
                                    <div class="col-md-5 col-sm-5">
                                        <div class="">
                                            <input type="text" class="form-control" id="amount_discount"  value="0">

                                            <span class="text-danger" id="amount_discount_error"></span></div>
                                    </div>
                                    <div class="col-md-2 col-sm-2 ltextright" style="display: none">

                                        <label for="inputPassword3" class="control-label"><?php echo $this->lang->line('fine'); ?><small class="req">*</small></label>
                                    </div>
                                    <div class="col-md-5 col-sm-5" style="display: none">
                                        <div class="">
                                            <input type="text" class="form-control" id="amount_fine" value="0">

                                            <span class="text-danger" id="amount_fine_error"></span>
                                        </div>
                                    </div>
                                </div>
                            </div><!--./col-sm-9-->
                        </div>
                        <div class="form-group" style="display: none">
                            <label for="inputPassword3" class="col-sm-3 control-label"><?php echo $this->lang->line('payment'); ?> <?php echo $this->lang->line('mode'); ?></label>
                            <div class="col-sm-9">
                                <label class="radio-inline">
                                    <input type="radio" name="payment_mode_fee" value="Cash" checked="checked"><?php echo $this->lang->line('cash'); ?>
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="payment_mode_fee" value="Cheque"><?php echo $this->lang->line('cheque'); ?>
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="payment_mode_fee" value="DD"><?php echo $this->lang->line('dd'); ?>
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="payment_mode_fee" value="bank_transfer"><?php echo $this->lang->line('bank_transfer'); ?>
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="payment_mode_fee" value="upi"><?php echo $this->lang->line('upi'); ?>
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="payment_mode_fee" value="card"><?php echo $this->lang->line('card'); ?>
                                </label>
                                <span class="text-danger" id="payment_mode_error"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-3 control-label"><?php echo $this->lang->line('note'); ?></label>

                            <div class="col-sm-9">
                                <textarea class="form-control" rows="3" id="description" placeholder="<?php echo $this->lang->line('placeholder_fee_collection_note') ?>" required></textarea>
                                <span class="text-danger" id="description"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><?php echo $this->lang->line('cancel'); ?></button>
                <button type="button" class="btn cfees save_button" id="load" data-action="collect" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing"><?php echo $this->lang->line('save'); ?> </button>
                <button style="display: none" type="button" class="btn cfees save_button" id="load" data-action="print" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing"> <?php echo $currency_symbol; ?> <?php echo $this->lang->line('collect') . " & " . $this->lang->line('print') ?></button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="myDisApplyModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title title text-center discount_title"></h4>
            </div>
            <div class="modal-body pb0">
                <div class="form-horizontal">
                    <div class="box-body">
                        <input  type="hidden" class="form-control" id="student_fees_discount_id"  value=""/>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-3 control-label"><?php echo $this->lang->line('payment_id'); ?> <small class="req">*</small></label>
                            <div class="col-sm-9">

                                <input type="text" class="form-control" id="discount_payment_id" >

                                <span class="text-danger" id="discount_payment_id_error"></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-3 control-label"><?php echo $this->lang->line('description'); ?></label>

                            <div class="col-sm-9">
                                <textarea class="form-control" rows="3" id="dis_description" placeholder=""></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><?php echo $this->lang->line('cancel'); ?></button>
                <button type="button" class="btn cfees dis_apply_button" id="load" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing"> <?php echo $this->lang->line('apply_discount'); ?></button>
            </div>
        </div>
    </div>
</div>

<div class="delmodal modal fade" id="confirm-discountdelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"><?php echo $this->lang->line('confirmation'); ?></h4>
            </div>
            <div class="modal-body">
                <p><?php echo $this->lang->line('are_you_sure_want_to_revert'); ?> <b class="discount_title"></b> <?php echo $this->lang->line('discount_this_action_is_irreversible'); ?></p>
                <p><?php echo $this->lang->line('do_you_want_to_proceed') ?></p>
                <p class="debug-url"></p>
                <input type="hidden" name="discount_id"  id="discount_id" value="">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $this->lang->line('cancel'); ?></button>
                <a class="btn btn-danger btn-discountdel"><?php echo $this->lang->line('revert'); ?></a>
            </div>
        </div>
    </div>
</div>

<div class="delmodal modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"><?php echo $this->lang->line('confirmation'); ?></h4>
            </div>
            <div class="modal-body">

                <p><?php echo $this->lang->line('are_you_sure_want_to_delete'); ?> <b class="invoice_no"></b> <?php echo $this->lang->line('invoice_this_action_is_irreversible') ?></p>
                 <p><?php echo $this->lang->line('do_you_want_to_proceed') ?></p>
                <p class="debug-url"></p>
                <input type="hidden" name="main_invoice"  id="main_invoice" value="">
                <input type="hidden" name="sub_invoice" id="sub_invoice"  value="">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $this->lang->line('cancel'); ?></button>
                <a class="btn btn-danger btn-ok"><?php echo $this->lang->line('revert'); ?></a>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="discount_revert">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="delete_form" method="post" action="<?php echo base_url() ?>studentdiscount/delete">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"><?php echo $this->lang->line('confirmation'); ?></h4>
            </div>
            <div class="modal-body">

                <p><?php echo $this->lang->line('are_you_sure_want_to_revert_discount'); ?> <?php echo $this->lang->line('this_action_is_irreversible') ?></p>
                 <p><?php echo $this->lang->line('do_you_want_to_proceed') ?></p>
                <input type="hidden" name="discount_id"  id="discount_id">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $this->lang->line('cancel'); ?></button>
                <button class="btn btn-danger btn-ok-discount" type="submit"><?php echo $this->lang->line('revert'); ?></button>
            </div>
        </form>
        </div>
    </div>
</div>

<div class="norecord modal fade" id="confirm-norecord" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <p><?php echo $this->lang->line('no_record_found'); ?></p>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $this->lang->line('cancel'); ?></button>
            </div>
        </div>
    </div>
</div>

<div id="listCollectionModal" class="modal fade">
    <div class="modal-dialog">
        <form action="<?php echo site_url('studentfee/addfeegrp'); ?>" method="POST" id="collect_fee_group">
            <div class="modal-content">
<!-- //================ -->
 <input  type="hidden" class="form-control" id="group_std_id" name="student_session_id" value="<?php echo $student["student_session_id"]; ?>" readonly="readonly"/>
<input  type="hidden" class="form-control" id="group_parent_app_key" name="parent_app_key" value="<?php echo $student['parent_app_key'] ?>" readonly="readonly"/>
<input  type="hidden" class="form-control" id="group_guardian_phone" name="guardian_phone" value="<?php echo $student['guardian_phone'] ?>" readonly="readonly"/>
<input  type="hidden" class="form-control" id="group_guardian_email" name="guardian_email" value="<?php echo $student['guardian_email'] ?>" readonly="readonly"/>
<input type="hidden" name="trans_id" value="<?php echo $this->customlib->GUID(); ?>">
<!-- //================ -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><?php echo $this->lang->line('collect') . " " . $this->lang->line('fees'); ?></h4>
                </div>
                <div class="modal-body">

                </div>
               <!--  <div class="modal-footer">
                    <button type="submit" class="btn btn-primary payment_collect" data-loading-text="<i class='fa fa-spinner fa-spin '></i><?php //echo $this->lang->line('processing')?>"><i class="fa fa-money"></i> <?php //echo $this->lang->line('pay'); ?></button>
                </div> -->
            </div>
        </form>
    </div>
</div>





<div class="modal fade" id="feediscountmodal" role="dialog">
    <div class="modal-dialog">
        <form action="<?php echo base_url(); ?>studentdiscount/getDiscount" method="post">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title title text-center fees_title"></h4>
                </div>
                <div class="modal-body pb0">
                    <div class="form-horizontal balanceformpopup">
                    	<p>Are You sure You want to assign Discount?</p>
                        <div class="box-body" style="display: none;">

                            <!-- <input  type="text" class="form-control" id="std_id" value="<?php echo $student["student_session_id"]; ?>" readonly="readonly"/>
                            <input  type="text" class="form-control" id="parent_app_key" value="<?php echo $student['parent_app_key'] ?>" readonly="readonly"/>
                            <input  type="text" class="form-control" id="guardian_phone" value="<?php echo $student['guardian_phone'] ?>" readonly="readonly"/>
                            <input  type="text" class="form-control" id="guardian_email" value="<?php echo $student['guardian_email'] ?>" readonly="readonly"/> -->
                            <input  type="text" class="form-control" id="discount_student_fees_master_id" name="student_fees_master_id" value="0" readonly="readonly"/>
                            <input  type="text" class="form-control" id="discount_fee_groups_feetype_id" name="fee_groups_feetype_id" value="0" readonly="readonly"/>



                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label"><?php echo $this->lang->line('date'); ?></label>
                                <div class="col-sm-9">
                                    <?php if($this->customlib->getcalender()->calender=="0") : ?>
                                        <input id="date" name="collected_date" placeholder="<?php echo $this->lang->line('placeholder_fee_collection_date') ?>" type="text" class="form-control modeldate"  value="<?php echo date($this->customlib->currentdate()); ?>" readonly="readonly"/>

                                    <?php else : ?>
                                    <input  id="date" name="collected_date" placeholder="<?php echo $this->lang->line('placeholder_fee_collection_date') ?>" type="text" class="form-control date_fee"  value="<?php echo date($this->customlib->getSchoolDateFormat()); ?>" readonly="readonly"/>
                                <?php endif; ?>
                                </div>
                            </div>

                            <input type="text" name="glname" value="0">
                            <input type="text" autofocus="" class="form-control modal_amount" id="discount_amount" name="amt_discount" value="0"><br>
                            <input type="text" name="" class="discount_group"><br>
                            <input type="text" name="discount_name" class="discount_name">
                            <input type="text" name="" class="total-amount" value="0">
                            <input type="text" name="" class="discount_percent" value="0">
                            <input type="text" class="total_discount" name="amount_discount" value="0"><br>
                            <textarea class="form-control" rows="3" id="description" placeholder="<?php echo $this->lang->line('placeholder_fee_collection_note') ?>"></textarea>



                            
                        </div>
                    </div>
                </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><?php echo $this->lang->line('cancel'); ?></button>
                    <button class="discount_apply btn btn-success" type="submit">Apply</button>
                </div>
            </div>
        </form>
    </div>
</div>



<script type="text/javascript">
    $("#feediscountmodal").on('shown.bs.modal', function (e) {
        e.stopPropagation();
        var data = $(e.relatedTarget).data();
        var modal = $(this);
        var type = data.type;
        var amount = data.amount;
        var group = data.group;
        var fee_groups_feetype_id = data.fee_groups_feetype_id;
        var student_fees_master_id = data.student_fees_master_id;
        var student_session_id = data.student_session_id;
        var discount = data.discount;
        var percent = data.percent;
        var discount_group = data.type;
        var amt_discount = data.amt_discount;
        $('.fees_title').html("");
        $('.fees_title').html("<b>" + group + ":</b> " + type);
        $('.title').html("<b>" + group + ":</b> " + type);

        $('#discount_fee_groups_feetype_id').val(fee_groups_feetype_id);
        $('#discount_student_fees_master_id').val(student_fees_master_id);
        $('.total-amount').val(amount);
        $('.discount_percent').val(percent);
        $('.discount_group').val(discount_group);
        $('#description').val(discount);
        $('.discount_name').val(discount);
        $('#discount_amount').val(amt_discount);
        var discount_amount = (amount * percent)/100;
        
        	$('.total_discount').val(discount_amount);
        
    });

</script>
<script type="text/javascript">
    $(document).ready(function () {
            $(document).on('click', '.printDoc', function () {
            var main_invoice = $(this).data('main_invoice');
            var sub_invoice = $(this).data('sub_invoice');
            var student_session_id = '<?php echo $student['student_session_id'] ?>';
            $.ajax({
                url: '<?php echo site_url("studentfee/printFeesByName") ?>',
                type: 'post',
                data: {'student_session_id': student_session_id, 'main_invoice': main_invoice, 'sub_invoice': sub_invoice},
                success: function (response) {
                    Popup(response);
                }
            });
        });
        $(document).on('click', '.printInv', function () {
            var fee_master_id = $(this).data('fee_master_id');
            var fee_session_group_id = $(this).data('fee_session_group_id');
            var fee_groups_feetype_id = $(this).data('fee_groups_feetype_id');
            $.ajax({
                url: '<?php echo site_url("studentfee/printFeesByGroup") ?>',
                type: 'post',
                data: {'fee_groups_feetype_id': fee_groups_feetype_id, 'fee_master_id': fee_master_id, 'fee_session_group_id': fee_session_group_id},
                success: function (response) {
                    Popup(response);
                }
            });
        });
    });
</script>
<script type="text/javascript">
    $(document).on('click', '.save_button', function (e) {
        var $this = $(this);
        var action = $this.data('action');
        $this.button('loading');
        var form = $(this).attr('frm');
        var feetype = $('#feetype_').val();
        var date = $('#date').val();
        var student_session_id = $('#std_id').val();
        var amount = $('#amount').val();
        var amount_discount = $('#amount_discount').val();
        var amount_fine = $('#amount_fine').val();
        var description = $('#description').val();
        var parent_app_key = $('#parent_app_key').val();
        var guardian_phone = $('#guardian_phone').val();
        var guardian_email = $('#guardian_email').val();
        var student_fees_master_id = $('#student_fees_master_id').val();
        var fee_groups_feetype_id = $('#fee_groups_feetype_id').val();
        var payment_mode = $('input[name="payment_mode_fee"]:checked').val();
        var student_fees_discount_id = $('#discount_group').val();
        var glname = $('#glnameid').val();
        var title = $('.title-input').val();
        $.ajax({
            url: '<?php echo site_url("studentfee/addstudentfee") ?>', 
            type: 'post',
            data: {action: action, student_session_id: student_session_id, date: date, type: feetype, amount: amount, amount_discount: amount_discount, amount_fine: amount_fine, description: description, student_fees_master_id: student_fees_master_id, fee_groups_feetype_id: fee_groups_feetype_id, payment_mode: payment_mode, guardian_phone: guardian_phone, guardian_email: guardian_email, student_fees_discount_id: student_fees_discount_id, parent_app_key: parent_app_key,glname:glname,title:title},
            dataType: 'json',
            success: function (response) {
                $this.button('reset');
                if (response.status === "success") {
                    if (action === "collect") {
                        location.reload(true);
                    } else if (action === "print") {
                        Popup(response.print, true);
                    }
                } else if (response.status === "fail") {
                    $.each(response.error, function (index, value) {
                        var errorDiv = '#' + index + '_error';
                        $(errorDiv).empty().append(value);
                    });
                }
            }
        });
    });
</script>
<script>
    var base_url = '<?php echo base_url() ?>';

    function Popup(data, winload = false)
    {
        var frame1 = $('<iframe />').attr("id", "printDiv");
        frame1[0].name = "frame1";
        frame1.css({"position": "absolute", "top": "-1000000px"});
        $("body").append(frame1);
        var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
        frameDoc.document.open();
        //Create a new HTML document.
        frameDoc.document.write('<html>');
        frameDoc.document.write('<head>');
        frameDoc.document.write('<title></title>');
        frameDoc.document.write('<link rel="stylesheet" href="' + base_url + 'backend/bootstrap/css/bootstrap.min.css">');
        frameDoc.document.write('<link rel="stylesheet" href="' + base_url + 'backend/dist/css/font-awesome.min.css">');
        frameDoc.document.write('<link rel="stylesheet" href="' + base_url + 'backend/dist/css/ionicons.min.css">');
        frameDoc.document.write('<link rel="stylesheet" href="' + base_url + 'backend/dist/css/AdminLTE.min.css">');
        frameDoc.document.write('<link rel="stylesheet" href="' + base_url + 'backend/dist/css/skins/_all-skins.min.css">');
        frameDoc.document.write('<link rel="stylesheet" href="' + base_url + 'backend/plugins/iCheck/flat/blue.css">');
        frameDoc.document.write('<link rel="stylesheet" href="' + base_url + 'backend/plugins/morris/morris.css">');
        frameDoc.document.write('<link rel="stylesheet" href="' + base_url + 'backend/plugins/jvectormap/jquery-jvectormap-1.2.2.css">');
        frameDoc.document.write('<link rel="stylesheet" href="' + base_url + 'backend/plugins/datepicker/datepicker3.css">');
        frameDoc.document.write('<link rel="stylesheet" href="' + base_url + 'backend/plugins/daterangepicker/daterangepicker-bs3.css">');
        frameDoc.document.write('</head>');
        frameDoc.document.write('<body>');
        frameDoc.document.write(data);
        frameDoc.document.write('</body>');
        frameDoc.document.write('</html>');
        frameDoc.document.close();
        setTimeout(function () {
        document.getElementById('printDiv').contentWindow.focus();
        document.getElementById('printDiv').contentWindow.print();
        $("#printDiv", top.document).remove();
            // frame1.remove();
            if (winload) {
                window.location.reload(true);
            }
        }, 500);

        return true;
    }
    $(document).ready(function () {
        $('.delmodal').modal({
            backdrop: 'static',
            keyboard: false,
            show: false
        });
        $('#listCollectionModal').modal({
            backdrop: 'static',
            keyboard: false,
            show: false
        });

        $('#confirm-delete').on('show.bs.modal', function (e) {
            $('.invoice_no', this).text("");
            $('#main_invoice', this).val("");
            $('#sub_invoice', this).val("");
            $('.invoice_no', this).text($(e.relatedTarget).data('invoiceno'));
            $('#main_invoice', this).val($(e.relatedTarget).data('main_invoice'));
            $('#sub_invoice', this).val($(e.relatedTarget).data('sub_invoice'));
        });

        $('#confirm-discountdelete').on('show.bs.modal', function (e) {
            $('.discount_title', this).text("");
            $('#discount_id', this).val("");
            $('.discount_title', this).text($(e.relatedTarget).data('discounttitle'));
            $('#discount_id', this).val($(e.relatedTarget).data('discountid'));
        });
        $('#discount_revert').on('show.bs.modal', function (e) {
            $('#discount_id', this).val("");
            $('#discount_id', this).val($(e.relatedTarget).data('id'));
        });

        $('#confirm-delete').on('click', '.btn-ok', function (e) {
            var $modalDiv = $(e.delegateTarget);
            var main_invoice = $('#main_invoice').val();
            var sub_invoice = $('#sub_invoice').val();

            $modalDiv.addClass('modalloading');
            $.ajax({
                type: "post",
                url: '<?php echo site_url("studentfee/deleteFee") ?>',
                dataType: 'JSON',
                data: {'main_invoice': main_invoice, 'sub_invoice': sub_invoice},
                success: function (data) {
                    $modalDiv.modal('hide').removeClass('modalloading');
                    location.reload(true);
                }
            });
        });

        $('#confirm-discountdelete').on('click', '.btn-discountdel', function (e) {
            var $modalDiv = $(e.delegateTarget);
            var discount_id = $('#discount_id').val();

            $modalDiv.addClass('modalloading');
            $.ajax({
                type: "post",
                url: '<?php echo site_url("studentfee/deleteStudentDiscount") ?>',
                dataType: 'JSON',
                data: {'discount_id': discount_id},
                success: function (data) {
                    $modalDiv.modal('hide').removeClass('modalloading');
                    location.reload(true);
                }
            });
        });


        $(document).on('click', '.btn-ok', function (e) {
            var $modalDiv = $(e.delegateTarget);
            var main_invoice = $('#main_invoice').val();
            var sub_invoice = $('#sub_invoice').val();

            $modalDiv.addClass('modalloading');
            $.ajax({
                type: "post",
                url: '<?php echo site_url("studentfee/deleteFee") ?>',
                dataType: 'JSON',
                data: {'main_invoice': main_invoice, 'sub_invoice': sub_invoice},
                success: function (data) {
                    $modalDiv.modal('hide').removeClass('modalloading');
                    location.reload(true);
                }
            });

        });
        $('.detail_popover').popover({
            placement: 'right',
            title: '',
            trigger: 'hover',
            container: 'body',
            html: true,
            content: function () {
                return $(this).closest('td').find('.fee_detail_popover').html();
            }
        });
    });
    var fee_amount = 0;
</script>
<script type="text/javascript">
    $("#myFeesModal").on('shown.bs.modal', function (e) {
        e.stopPropagation();
        var discount_group_dropdown = '<option value=""><?php echo $this->lang->line('select'); ?></option>';
        var data = $(e.relatedTarget).data();
        var modal = $(this);
        var type = data.type;
        var amount = data.amount;
        var group = data.group;
        var fee_groups_feetype_id = data.fee_groups_feetype_id;
        var student_fees_master_id = data.student_fees_master_id;
        var student_session_id = data.student_session_id;
        var glname = data.glname;

        $('.fees_title').html("");
        $('.fees_title').html("<b>" + group + ":</b> " + type);
        $('#glnameid').val(glname);
        $('.title-input').val(type);
        $('#fee_groups_feetype_id').val(fee_groups_feetype_id);
        $('#student_fees_master_id').val(student_fees_master_id);
        $.ajax({
            type: "post",
            url: '<?php echo site_url("studentfee/geBalanceFee") ?>',
            dataType: 'JSON',
            data: {'fee_groups_feetype_id': fee_groups_feetype_id,
                'student_fees_master_id': student_fees_master_id,
                'student_session_id': student_session_id
            },
            beforeSend: function () {
                $('#discount_group').html("");
                $("span[id$='_error']").html("");
                $('#amount').val("");
                $('#amount_discount').val("0");
                $('#amount_fine').val("0");
                $('#glnameid').val(glname);
                modal.addClass('modal_loading');
            },
            success: function (data) {

                if (data.status === "success") {
                    fee_amount = data.balance;

                    $('#amount').val(0);
                    $('#amount_fine').val(data.remain_amount_fine);


                    $.each(data.discount_not_applied, function (i, obj)
                    {
                        discount_group_dropdown += "<option value=" + obj.student_fees_discount_id + " data-disamount=" + obj.amount + ">" + obj.code + "</option>";
                    });
                    $('#discount_group').append(discount_group_dropdown);

                }
            },
            error: function (xhr) { // if error occured
                alert("Error occured.please try again");

            },
            complete: function () {
                modal.removeClass('modal_loading');
            }
        });
    });

</script>

<script type="text/javascript">
    $(document).ready(function () {
        $.extend($.fn.dataTable.defaults, {
            searching: false,
            ordering: false,
            paging: false,
            bSort: false,
            info: false
        });
    });
    $(document).ready(function () {
        $('.table-fixed-header').fixedHeader();
    });

    (function ($) {

        $.fn.fixedHeader = function (options) {
            var config = {
                topOffset: 50
                        //bgColor: 'white'
            };
            if (options) {
                $.extend(config, options);
            }

            return this.each(function () {
                var o = $(this);

                var $win = $(window);
                var $head = $('thead.header', o);
                var isFixed = 0;
                var headTop = $head.length && $head.offset().top - config.topOffset;

                function processScroll() {
                    if (!o.is(':visible')) {
                        return;
                    }
                    if ($('thead.header-copy').size()) {
                        $('thead.header-copy').width($('thead.header').width());
                    }
                    var i;
                    var scrollTop = $win.scrollTop();
                    var t = $head.length && $head.offset().top - config.topOffset;
                    if (!isFixed && headTop !== t) {
                        headTop = t;
                    }
                    if (scrollTop >= headTop && !isFixed) {
                        isFixed = 1;
                    } else if (scrollTop <= headTop && isFixed) {
                        isFixed = 0;
                    }
                    isFixed ? $('thead.header-copy', o).offset({
                        left: $head.offset().left
                    }).removeClass('hide') : $('thead.header-copy', o).addClass('hide');
                }
                $win.on('scroll', processScroll);

                // hack sad times - holdover until rewrite for 2.1
                $head.on('click', function () {
                    if (!isFixed) {
                        setTimeout(function () {
                            $win.scrollTop($win.scrollTop() - 47);
                        }, 10);
                    }
                });

                $head.clone().removeClass('header').addClass('header-copy header-fixed').appendTo(o);
                var header_width = $head.width();
                o.find('thead.header-copy').width(header_width);
                o.find('thead.header > tr:first > th').each(function (i, h) {
                    var w = $(h).width();
                    o.find('thead.header-copy> tr > th:eq(' + i + ')').width(w);
                });
                $head.css({
                    margin: '0 auto',
                    width: o.width(),
                    'background-color': config.bgColor
                });
                processScroll();
            });
        };

    })(jQuery);


    $(".applydiscount").click(function () {
        $("span[id$='_error']").html("");
        $('.discount_title').html("");
        $('#student_fees_discount_id').val("");
        var student_fees_discount_id = $(this).data("student_fees_discount_id");
        var modal_title = $(this).data("modal_title");


        $('.discount_title').html("<b>" + modal_title + "</b>");

        $('#student_fees_discount_id').val(student_fees_discount_id);
        $('#myDisApplyModal').modal({
            backdrop: 'static',
            keyboard: false,
            show: true
        });
    });

    $(document).on('click', '.dis_apply_button', function (e) {
        var $this = $(this);
        $this.button('loading');

        var discount_payment_id = $('#discount_payment_id').val();
        var student_fees_discount_id = $('#student_fees_discount_id').val();
        var dis_description = $('#dis_description').val();

        $.ajax({
            url: '<?php echo site_url("admin/feediscount/applydiscount") ?>',
            type: 'post',
            data: {
                discount_payment_id: discount_payment_id,
                student_fees_discount_id: student_fees_discount_id,
                dis_description: dis_description
            },
            dataType: 'json',
            success: function (response) {
                $this.button('reset');
                if (response.status === "success") {
                    location.reload(true);
                } else if (response.status === "fail") {
                    $.each(response.error, function (index, value) {
                        var errorDiv = '#' + index + '_error';
                        $(errorDiv).empty().append(value);
                    });
                }
            }
        });
    });

</script>

<script type="text/javascript">
    $(document).ready(function () {
        $(document).on('click', '.printSelected', function () {
            var array_to_print = [];
            $.each($("input[name='fee_checkbox']:checked"), function () {
                var fee_session_group_id = $(this).data('fee_session_group_id');
                var fee_master_id = $(this).data('fee_master_id');
                var fee_groups_feetype_id = $(this).data('fee_groups_feetype_id');
                item = {};
                item ["fee_session_group_id"] = fee_session_group_id;
                item ["fee_master_id"] = fee_master_id;
                item ["fee_groups_feetype_id"] = fee_groups_feetype_id;

                array_to_print.push(item);
            });
            if (array_to_print.length === 0) {
                alert("<?php echo $this->lang->line('no_record_selected'); ?>");
            } else {
                $.ajax({
                    url: '<?php echo site_url("studentfee/printFeesByGroupArray") ?>',
                    type: 'post',
                    data: {'data': JSON.stringify(array_to_print)},
                    success: function (response) {
                        Popup(response);
                    }
                });
            }
        });

        $(document).on('click', '.printMonthlybill', function () {
            var array_to_print = [];
            $.each($("input[name='fee_checkbox']"), function () {
                var fee_session_group_id = $(this).data('fee_session_group_id');
                var fee_master_id = $(this).data('fee_master_id');
                var fee_groups_feetype_id = $(this).data('fee_groups_feetype_id');
                item = {};
                item ["fee_session_group_id"] = fee_session_group_id;
                item ["fee_master_id"] = fee_master_id;
                item ["fee_groups_feetype_id"] = fee_groups_feetype_id;

                array_to_print.push(item);
            });
            if (array_to_print.length === 0) {
                alert("No Fee Dues Left");
            } else {
                $.ajax({
                    url: '<?php echo site_url("studentfee/printMonthlybill") ?>',
                    type: 'post',
                    data: {'data': JSON.stringify(array_to_print)},
                    success: function (response) {
                        Popup(response);
                    }
                });
            }
        });


        $(document).on('click', '.printToday', function () {
            var array_to_print = [];
            $("input[name='fee_checkbox']").prop('checked', true);
            $.each($("input[name='fee_checkbox']"), function () {
                var fee_session_group_id = $(this).data('fee_session_group_id');
                var fee_master_id = $(this).data('fee_master_id');
                var fee_groups_feetype_id = $(this).data('fee_groups_feetype_id');
                item = {};
                item ["fee_session_group_id"] = fee_session_group_id;
                item ["fee_master_id"] = fee_master_id;
                item ["fee_groups_feetype_id"] = fee_groups_feetype_id;

                array_to_print.push(item);
            });
            if (array_to_print.length === 0) {
                alert("No Fee Dues Left");
            } else {
                $.ajax({
                    url: '<?php echo site_url("studentfee/printToday") ?>',
                    type: 'post',
                    data: {'data': JSON.stringify(array_to_print)},
                    success: function (response) {
                        Popup(response);
                    }
                });
            }
        });


        $(document).on('click', '.collectSelected', function () {
            var $this = $(this);
            var array_to_collect_fees = [];
            $("input[name='fee_checkbox']").prop('checked', true);
            $.each($("input[name='fee_checkbox']:checked"), function () {
                var fee_session_group_id = $(this).data('fee_session_group_id');
                var fee_master_id = $(this).data('fee_master_id');
                var fee_groups_feetype_id = $(this).data('fee_groups_feetype_id');
                item = {};
                item ["fee_session_group_id"] = fee_session_group_id;
                item ["fee_master_id"] = fee_master_id;
                item ["fee_groups_feetype_id"] = fee_groups_feetype_id;

                array_to_collect_fees.push(item);
            });

            $.ajax({
                type: 'POST',
                url: base_url + "studentfee/getcollectfee",
                data: {'data': JSON.stringify(array_to_collect_fees)},
                dataType: "JSON",
                beforeSend: function () {
                    $this.button('loading');
                },
                success: function (data) {

                    $("#listCollectionModal .modal-body").html(data.view);

                    $("#listCollectionModal").modal('show');
                    $this.button('reset');
                },
                error: function (xhr) { // if error occured
                    alert("Error occured.please try again");

                },
                complete: function () {
                    $this.button('reset');
                }
            });

        });

    });


    $(function () {
        $(document).on('change', "#discount_group", function () {
            var amount = $('option:selected', this).data('disamount');

            var balance_amount = (parseFloat(fee_amount) - parseFloat(amount)).toFixed(2);
            if (typeof amount !== typeof undefined && amount !== false) {
                $('div#myFeesModal').find('input#amount_discount').prop('readonly', true).val(amount);
                $('div#myFeesModal').find('input#amount').val(balance_amount);

            } else {
                $('div#myFeesModal').find('input#amount').val(fee_amount);
                $('div#myFeesModal').find('input#amount_discount').prop('readonly', false).val(0);
            }

        });
    });

    $("#collect_fee_group").submit(function (e) {
        var form = $(this);
        var url = form.attr('action');
        var smt_btn = $(this).find("button[type=submit]");
        $.ajax({
            type: "POST",
            url: url,
            dataType: 'JSON',
            data: form.serialize(), // serializes the form's elements.
            beforeSend: function () {
                smt_btn.button('loading');
            },
            success: function (response) {

                if (response.status === 1) {

                    location.reload(true);
                } else if (response.status === 0) {
                    $.each(response.error, function (index, value) {
                        var errorDiv = '#form_collection_' + index + '_error';
                        $(errorDiv).empty().append(value);
                    });
                }
            },
            error: function (xhr) { // if error occured

                alert("Error occured.please try again");

            },
            complete: function () {
                smt_btn.button('reset');
            }
        });

        e.preventDefault(); // avoid to execute the actual submit of the form.
    });

    $("#select_all").change(function () {  //"select all" change
        $('input:checkbox').not(this).prop('checked', this.checked);
    });

</script>

 <?php 
 $calender= $this->customlib->getcalender();
 ?>
 <?php 
 if($calender->calender=="0") : ?>
  <script type="text/javascript">

    var date_format_nepali = '<?php echo $result = strtr($this->customlib->getSchoolDateFormat(), ['d' => 'DD', 'm' => 'MM', 'Y' => 'YYYY']) ?>';
    var currentdate = '<?php echo $today= $this->customlib->currentdate() ?>';

   $(function() {
                $(".modeldate").nepaliDatePicker({
                    language:"english",
                     dateFormat: date_format_nepali,
                     container:'.modal',
                     disableAfter: currentdate,
                });
        });
</script>

<?php endif; ?>

<script>
    $(document).ready(function(){
        $(".hide-display").hide();
        $(".showall").text("Show All");
        $(".showall").click(function(){
            $(".hide-display").toggle();
            if($(this).text()=="Show All"){
                $(this).text("Hide Paid");
            }else{
                $(this).text("Show All");
            }
        })
    });
</script>
