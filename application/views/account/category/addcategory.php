

<style>
    .panel {
        margin-bottom: 20px;
        background-color: #fff;
        border: none;
        -webkit-box-shadow: none;
        box-shadow: none;
    }
</style>


<div class="content-wrapper" style="min-height: 946px;">
    <section class="content-header">
        <h1>

        </h1>
    </section>

    <section class="content">
        <div class="row">
            
            <?php if ($this->session->flashdata('msg')) { ?>
                <?php echo $this->session->flashdata('msg') ?>
            <?php } ?>
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add Category</h3>
                    </div>
                    <div class="box-body">
                        <form method="post" action="<?php echo base_url()?>account/category/createCategory">
                            <input type="hidden" name="id" value=""/>
                            <div class="form-group">
                                <label>Parent</label> <small class="req">*</small>
                                <div class="category-accordion">
                                    <div id="accordion-0"  role="tablist" aria-multiselectable="true">
                                        <?php foreach ($text["Data"] as $key => $menu) { ?>
                                        <div class="panel" data-parent="0">
                                            <label>
                                                <input type="radio" name="GLCode"  id="parent-<?php echo $menu["Id"]; ?>" value="<?php echo $menu["GLCode"]; ?>" class="toggle-accordion" aria-controls="collapse-<?php echo $menu["Id"]; ?>">
                                                <span role="tab" id="heading-<?php echo $menu["Id"]; ?>" class="tab">

                                                    <span class="check-category" role="button" data-toggle="collapse" data-parent="#accordion-0" href="#collapse-<?php echo $menu["Id"]; ?>" aria-controls="collapse-<?php echo $menu["Id"]; ?>">
                                                        <?php echo $menu["Name"]; ?> 
                                                    </span>
                                                </span>
                                            </label>
                                            <div id="collapse-<?php echo $menu["Id"]; ?>" class="panel-collapse collapse " role="tabpanel" aria-labelledby="heading-<?php echo $menu["Id"]; ?>">
                                                <div id="accordion-<?php echo $menu["Id"]; ?>" style="margin-left: 40px;" role="tablist" aria-multiselectable="true">  
                                                    <?php
                                                    $this->load->helper('account');
                                                    $subtext = getsubsidiary($menu["GLCode"]);
                                                    ?>
                                                    <?php foreach ($subtext["Data"] as $key => $menu1) { ?>
                                                    <div class="panel" data-parent="<?php echo $menu1["Id"]; ?>">
                                                        <label>
                                                        	<?php if($menu1['IsGlSubSidiary']==false) : ?>
                                                            	<input type="radio" name="GLCode"  id="parent-<?php echo $menu1["Id"]; ?>" value="<?php echo $menu1["GLCode"]; ?>" class="toggle-accordion">
                                                            <?php else : ?>
                                                            	<i class="fa fa-arrow-right"></i>
                                                        	<?php endif; ?>
                                                            <span role="tab" id="heading-<?php echo $menu1["Id"]; ?>" class="tab">
                                                                <span class="check-category" role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapse-<?php echo $menu1["Id"]; ?>" aria-controls="collapse-<?php echo $menu1["Id"]; ?>">
                                                                    <?php echo $menu1["Name"]; ?>
                                                                </span>
                                                            </span>
                                                        </label>
                                                        <div id="collapse-<?php echo $menu1["Id"]; ?>" class="panel-collapse collapse " role="tabpanel" aria-labelledby="heading-<?php echo $menu1["Id"]; ?>">
                                                            <div id="accordion-<?php echo $menu1["Id"]; ?>" style="margin-left: 40px;" role="tablist" aria-multiselectable="true">
                                                                <?php
                                                                $this->load->helper('account');
                                                                $subtext1 = getsubsidiary($menu1["GLCode"]);
                                                                ?>
                                                                <?php foreach ($subtext1["Data"] as $key => $menu2) { ?>
                                                                <div class="panel">
                                                                    <label for="parent-<?php echo $menu2['Id']; ?>">
                                                                    	<?php if($menu2['IsGlSubSidiary']==false) : ?>
	                                                                        <input type="radio" name="GLCode"  id="parent-<?php echo $menu2['Id']; ?>" value="<?php echo $menu2['GLCode']; ?>">
                                                                        <?php else : ?>
                                                                        	<i class="fa fa-arrow-right"></i>
	                                                                    <?php endif; ?>
                                                                        <span role="tab" id="heading-<?php echo $menu2["Id"]; ?>" class="tab">
			                                                                <span class="check-category" role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapse-<?php echo $menu2["Id"]; ?>" aria-controls="collapse-<?php echo $menu2["Id"]; ?>">
			                                                                    <?php echo $menu2["Name"]; ?>
			                                                                </span>
			                                                            </span>
                                                                    </label>
                                                                    <div id="collapse-<?php echo $menu2["Id"]; ?>" class="panel-collapse collapse " role="tabpanel" aria-labelledby="heading-<?php echo $menu2["Id"]; ?>">
                                                            			<div id="accordion-<?php echo $menu2["Id"]; ?>" style="margin-left: 40px;" role="tablist" aria-multiselectable="true">
                                                            				<?php
			                                                                $this->load->helper('account');
			                                                                $subtext2 = getsubsidiary($menu2["GLCode"]);
			                                                                ?>
                                                            				<?php foreach ($subtext2["Data"] as $key => $menu3) { ?>
                                                            				<div class="panel">
			                                                                    <label for="parent-<?php echo $menu3['Id']; ?>">
			                                                                    	<?php if($menu3['IsGlSubSidiary']==false) : ?>
				                                                                        <input type="radio" name="GLCode"  id="parent-<?php echo $menu3['Id']; ?>" value="<?php echo $menu3['GLCode']; ?>">
			                                                                        <?php else : ?>
			                                                                        	<i class="fa fa-arrow-right"></i>
				                                                                    <?php endif; ?>
			                                                                        <span role="tab" id="heading-<?php echo $menu3["Id"]; ?>" class="tab">
						                                                                <span class="check-category" role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapse-<?php echo $menu3["Id"]; ?>" aria-controls="collapse-<?php echo $menu3["Id"]; ?>">
						                                                                    <?php echo $menu3["Name"]; ?>
						                                                                </span>
						                                                            </span>
			                                                                    </label>
			                                                                    <div id="collapse-<?php echo $menu3["Id"]; ?>" class="panel-collapse collapse " role="tabpanel" aria-labelledby="heading-<?php echo $menu3["Id"]; ?>">
			                                                            			<div id="accordion-<?php echo $menu3["Id"]; ?>" style="margin-left: 40px;" role="tablist" aria-multiselectable="true">
			                                                            				<?php
						                                                                $this->load->helper('account');
						                                                                $subtext3 = getsubsidiary($menu3["GLCode"]);
						                                                                ?>
			                                                            				<?php foreach ($subtext3["Data"] as $key => $menu4) { ?>
			                                                            				<div class="panel">
			                                                            					<label for="parent-<?php echo $menu4['Id']; ?>">
						                                                                    	<?php if($menu4['IsGlSubSidiary']==false) : ?>
							                                                                        <input type="radio" name="GLCode"  id="parent-<?php echo $menu4['Id']; ?>" value="<?php echo $menu4['GLCode']; ?>">
						                                                                        <?php else : ?>
						                                                                        	<i class="fa fa-arrow-right"></i>
							                                                                    <?php endif; ?>
						                                                                        <span role="tab" id="heading-<?php echo $menu4["Id"]; ?>" class="tab">
									                                                                <span class="check-category" role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapse-<?php echo $menu4["Id"]; ?>" aria-controls="collapse-<?php echo $menu4["Id"]; ?>">
									                                                                    <?php echo $menu4["Name"]; ?>
									                                                                </span>
									                                                            </span>
						                                                                    </label>
			                                                            				</div>
			                                                            			<?php } ?>
			                                                            			</div>
			                                                            		</div>
			                                                                </div>
			                                                                <?php } ?>
                                                            			</div>
                                                            		</div>
                                                                </div>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                        <?php  } ?>
                                        <span style="color: red"><?php echo form_error('GLCode') ?></span>
                                    </div>  
                                </div>
                                <div class="form-group">
                                    <label>Title</label> <small class="req"> *</small>
                                    <input type="text" id="title" name="title" class="form-control"
                                    value="">
                                    <span style="color: red"><?php echo form_error('title') ?></span>
                                </div>
                                <div class="form-group">
                                	<label>Category Type</label>
                                	<select class="form-control" name="category_type">
                                		<option>--Select--</option>
                                		<option value="group">Group</option>
                                		<option value="final">Final Ledger</option>
                                	</select>
                                	<input type="hidden" name="trans_id" value="<?php echo uniqid(); ?>">
                                </div>
                                <div class="form-group">
                                    <button type="submit" name="search" value="search_full"
                                    class="btn btn-primary pull-right btn-sm checkbox-toggle"> Save</button>
                                </div>
                            </div>
                        </form>                     
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('.check-category').on('click', function () {
            let $this = $(this);
            let $input = $this.closest('label').find('input[name="GLCode"]');
            $input.prop('checked', true);
            let val = $input.val();
            collapseElement($this, val);
        });
        // $('.toggle-accordion').on('click', function () {
        //     let $this = $(this);
        //     let val = $this.val();
        //     collapseElement($this, val);
        // });
        function collapseElement($this, val){
            let $panel = $this.closest('.panel');
            let parent = $panel.data('parent');
            let element = $('#collapse-' + val);
            $('[data-parent=' + parent + ']').each(function () {
                $(this).find('.panel-collapse').not('#collapse-' + val).collapse('hide');
            });
            element.collapse('show');
        }
    });
</script>