<div class="content-wrapper" style="min-height: 946px;">
    <section class="content-header">
        <h1>

        </h1>
    </section>

    <section class="content">
        <div class="row">
        <?php if($this->session->flashdata('success')) : ?>
            <div class="alert alert-success">
            <?= $this->session->flashdata('success'); ?>
            </div>
        <?php endif; ?>
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add Category</h3>
                    </div>
                    <div class="box-body">
                        <form action="<?php echo base_url()?>admin/account/setCategory" method="post">
                            <input type="hidden" name="id" value=""/>
                            <div class="form-group">
                                <label>Parent</label> <small class="req">*</small>
                                <div class="category-accordion">
                                    <div id="accordion-0"  role="tablist" aria-multiselectable="true">
                                    <?php foreach ($childlist as $key=>$menu) { ?>
                                        <div class="panel" data-parent="0">
                                            <label>
                                                <input type="radio" name="parent_id"  id="parent-<?php echo $menu->id; ?>" value="<?php echo $menu->id; ?>" class="toggle-accordion">
                                                <span role="tab" id="heading-<?php echo $menu->id; ?>" class="tab">
                                                    <span class="check-category" role="button" data-toggle="collapse" data-parent="#accordion-0" href="#collapse-<?php echo $menu->id; ?>" aria-controls="collapse-<?php echo $menu->id; ?>">
                                                        <?php echo $menu->title; ?>
                                                    </span>
                                                </span>
                                            </label>
                                            <div id="collapse-<?php echo $menu->id; ?>" class="panel-collapse collapse " role="tabpanel" aria-labelledby="heading-<?php echo $menu->id; ?>">
                                                <div id="accordion-<?php echo $menu->id; ?>" style="margin-left: 20px;" role="tablist" aria-multiselectable="true">  

                                                <?php if(count($this->account_model->subchildlist($menu->id))>0){ ?>

                                                <?php foreach ($this->account_model->subchildlist($menu->id) as $key => $menu1) { ?>
                                                    <div class="panel" data-parent="<?php echo $menu->id; ?>">
                                                        <label>
                                                            <input type="radio" name="parent_id"  id="parent-<?php echo $menu1->id; ?>" value="<?php echo $menu1->id; ?>" class="toggle-accordion">
                                                            <span role="tab" id="heading-<?php echo $menu1->id; ?>" class="tab">
                                                                <span class="check-category" role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapse-<?php echo $menu1->id; ?>" aria-controls="collapse-<?php echo $menu1->id; ?>">
                                                                    <?php echo $menu1->title; ?>
                                                                </span>
                                                            </span>
                                                        </label>
                                                        <div id="collapse-<?php echo $menu1->id; ?>" class="panel-collapse collapse " role="tabpanel" aria-labelledby="heading-<?php echo $menu1->id; ?>">
                                                            <div id="accordion-<?php echo $menu1->id; ?>" style="margin-left: 20px;" role="tablist" aria-multiselectable="true">

                                                            <?php if(count($this->account_model->subchildlist($menu->id))>0){ ?>

                                                                <?php foreach ($this->account_model->subchildlist($menu1->id) as $key => $menu2) { ?>
                                                                <div class="panel">
                                                                    <label for="parent-<?php echo $menu2->id; ?>">
                                                                        <input type="radio" name="parent_id"  id="parent-<?php echo $menu2->id; ?>" value="<?php echo $menu2->id; ?>">
                                                                        <?php echo $menu2->title; ?>
                                                                    </label>
                                                                </div>
                                                                <?php } ?>
                                                            <?php } ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                                <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                <?php  } ?>
                                    </div>  
                                </div>
                                <div class="form-group">
                                    <label>Title</label> <small class="req"> *</small>
                                    <input type="text" id="title" name="title" class="form-control"
                                    value="">
                                    <span style="color: red"><?php echo form_error('title') ?></span>
                                </div>
                                <div class="form-group">
                                    <button type="submit" name="search" value="search_full"
                                    class="btn btn-primary pull-right btn-sm checkbox-toggle"> Save</button>
                                </div>
                            </div>
                        </form>                     
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('.check-category').on('click', function () {
            let $this = $(this);
            let $input = $this.closest('label').find('input[name="parent_id"]');
            $input.prop('checked', true);
            let val = $input.val();
            collapseElement($this, val);
        });
        $('.toggle-accordion').on('click', function () {
            let $this = $(this);
            let val = $this.val();
            collapseElement($this, val);
        });
        function collapseElement($this, val){
            let $panel = $this.closest('.panel');
            let parent = $panel.data('parent');
            let element = $('#collapse-' + val);
            $('[data-parent=' + parent + ']').each(function () {
                $(this).find('.panel-collapse').not('#collapse-' + val).collapse('hide');
            });
            element.collapse('show');
        }
    });
</script>