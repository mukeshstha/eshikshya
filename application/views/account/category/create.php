<?php  

$ch = require "init_curl.php";

curl_setopt($ch,CURLOPT_URL,"http://actm.prabhumanagement.com/api/ChartOfAccount/CreateLedgerHead");

curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($_POST));

$response =curl_exec($ch);
$status_code = curl_getinfo($ch, CURLINFO_RESPONSE_CODE);
curl_close($ch);

$data= json_decode($response, true);

if($status_code === 422){
	echo "Invalid Data : ";
	print_r($data["error"]);
	exit;
}
if($status_code !== 201){
	echo "Unexpected status code: $status_code";
	var_dump($data);
	exit;
}

?>