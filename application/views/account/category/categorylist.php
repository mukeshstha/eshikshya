

<div class="content-wrapper" style="min-height: 946px;">
    <section class="content-header">
        <h1>

        </h1>
    </section>

    <section class="content">
        <div class="row">
            <?php if($this->session->flashdata('success')) : ?>
                <div class="alert alert-success">
                    <?= $this->session->flashdata('success'); ?>
                </div>
            <?php endif; ?>
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Categories</h3>

                        


                        <small class="pull-right">
                            <a href="<?php echo base_url(); ?>account/category/addCategory" class="btn btn-primary btn-sm">
                                <i class="fa fa-plus"></i> Add Category</a>
                            </small>
                        </div>
                        <div class="box-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover example dataTable" cellspacing="0" width="100%" id="DataTables_Table_0">
                                    <thead>
                                        <tr>
                                            <th>
                                            S.no</th>
                                            <th>Category</th>
                                            <th>Parent</th>
                                            <th class="text-right">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i = 0; ?>
                                    <?php foreach ($text["Data"] as $key => $value) : ?>
                                        <tr>
                                            <td><?php echo ++$i; ?></td>
                                            <td><?php echo $value["Name"]; ?></td>
                                            <td>-----
                                            </td>     
                                            <td class="mailbox-date pull-right">
                                                
                                            </td>
                                        </tr>

                                        <?php
                                        $this->load->helper('account');
                                        $subtext = getsubsidiary($value["GLCode"]);
                                        ?>
                                        <?php if(isset($subtext)) : ?>
                                            <?php foreach ($subtext['Data'] as $key => $subvalue) : ?>

                                                <tr>
                                                    <td><?php echo ++$i; ?></td>
                                                    <td><?php echo $subvalue["Name"]; ?></td>
                                                    <td><?php echo $value["Name"]; ?>
                                                    </td>     
                                                    <td class="mailbox-date pull-right">
                                                        <a href="" class="btn btn-default btn-xs" data-toggle="modal" data-target="#editModal" title="Edit" data-id="<?php echo $subvalue['GLCode']; ?>" data-name="<?php echo $subvalue["Name"]; ?>">
                                                            <i class="fa fa-pencil"></i>
                                                        </a>
                                                    </td>
                                                </tr>

                                                <?php $subtexttwo = getsubsidiary($subvalue["GLCode"]); ?>
                                                <?php if(isset($subtexttwo)) : ?>
                                                    <?php foreach ($subtexttwo['Data'] as $key => $subvaluetwo) : ?>
                                                                                    
                                                        <tr>
                                                            <td><?php echo ++$i; ?></td>
                                                            <td><?php echo $subvaluetwo["Name"]; ?></td>
                                                            <td><?php echo $subvalue["Name"]; ?>
                                                            </td>     
                                                            <td class="mailbox-date pull-right">
                                                                <a href="" class="btn btn-default btn-xs" data-toggle="modal" data-target="#editModal" title="Edit" data-id="<?php echo $subvaluetwo['GLCode']; ?>" data-name="<?php echo $subvaluetwo["Name"]; ?>">
                                                                    <i class="fa fa-pencil"></i>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                        <?php endif; ?>

                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="modal fade" id="editModal" role="dialog" aria-hidden="false">
        <div class="modal-dialog">
            <form action="<?php echo base_url();?>account/category/editcategory" method="post">
        <div class="modal-content">
            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">Edit Ledger Name</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" name="glcode" id="glcode">
                <label>Name</label><br>
                <input type="text" name="glname" class="form-control" id="glname">
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary pull-right payment_collect">Edit</button>
            </div>
            
        </div>
        </form>
    </div>
    </div>

    <script type="text/javascript">
        $("#editModal").on('shown.bs.modal', function (e) {
            e.stopPropagation();
            var data = $(e.relatedTarget).data();
            var modal = $(this);
            var glcode = data.id;
            var glname = data.name;
            $('#glcode').val(glcode); 
            $('#glname').val(glname);       
        });
    </script>