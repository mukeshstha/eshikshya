<div class="content-wrapper" style="min-height: 946px;">
	<section class="content-header">
		<h1>
			EOM Voucher Entry
		</h1>
	</section>


	<?php
	$token = $this->input->cookie('Token');

	if(isset($token)){
	$curl = curl_init();

	curl_setopt_array($curl, array(
		CURLOPT_URL => 'http://actm.prabhumanagement.com/api/ChartOfAccount/GetSubsidiaryListByFlag?Flag=All',
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'GET',
		CURLOPT_HTTPHEADER => array(
			'Authorization: Bearer '.$token
			),
		));

	$response = curl_exec($curl);

	curl_close($curl);
	$text= json_decode($response, true);
}else{
	redirect($_SERVER['HTTP_REFERER']);
}
	?>


	<form method="post" action="<?php echo base_url(); ?>account/voucherentry/eomentrypost">
		<section class="content">
			<div class="row">
				<div class="col-md-12">
					<div class="box box-primary">
						<div class="box-header with-border">
							<h3 class="box-title">
								<i class="fa fa-plus-circle"></i> Add EOM Voucher </h3>
							</div>
							<?php if ($this->session->flashdata('msg')) {?>
						        <?php echo $this->session->flashdata('msg') ?>
						    <?php }?>
							<div class="box-body">
								<div class="row">
									<div class="filter-box">
										<input type="hidden" name="id" value=""/>
										<div class="col-md-3 form-group">
											<label>Narration&nbsp;<small style="color: red">*</small></label>
											<textarea name="narration" class="form-control" required></textarea>
										</div>

										<div class="col-md-3 form-group journal_date_form">
											<label>Entry Date&nbsp;<small style="color: red">*</small></label>
											<input type="text" id="entry_date_bs" name="entry_date_bs" class="form-control"
											readonly required
											value="<?php echo set_value('postdate', date($this->customlib->getSchoolDateFormat())); ?>">
											<input type="hidden" id="entry_date" name="entry_date" class="form-control date"
											readonly
											value="">
										</div>
										<div class="col-md-3 form-group">
											<label>Transaction Date&nbsp;<small style="color: red">*</small></label>
                                    		<input type="text" autocomplete="off" name="transdate" class="form-control date"  value="<?php  echo set_value('transdate') ?>" required>

										</div>
										<div class="col-md-3 form-group">
											<label>Reference No</label> <small
											class="text-muted">Optional</small>
											<input type="text" id="reference_no" name="reference_no" class="form-control"
											value="">
										</div>

									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>

			<section class="content">
				<div class="row">
					<div class="col-md-12">
						<div class="box box-primary">
							<div class="box-body">
								<div class="row">
									<div class="col-md-12">
										<button class="btn btn-primary pull-right" id="addProduct" type="button" style="margin-bottom: 10px"><i class="fa fa-plus"></i>Add New</button>
										<table id="journal_entry_table"
										class="table table-respomsive" cellspacing="0"
										width="100%">
										<thead>
											<tr>
												<th width="20%">Ledger</th>
												<th width="10%">Balance</th>
												<th width="20%">Debit</th>
												<th width="20%">Credit</th>
												<th width="25%">Remarks</th>
												<th width="5%" class="no-print text text-right">Action</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>

													<select class="selectpicker form-control" data-live-search="true" name="glname[]" required>
														<option value="">--Select Ledger--</option>
														<?php foreach ($text['Data'] as $key => $value) : ?>
															<option value="<?php echo $value['Id']; ?>" data-balance="<?php echo $value['Balance']; ?>"><?php echo $value['Name']; ?></option>
														<?php endforeach; ?>
													</select>
												</td>
												<td class="balance"><input type="text" class="balance form-control" name="balance[]" placeholder="0.00" readonly=""></td>
												<td class="debit"><input type="number" class="debitinput form-control" name="debit[]" placeholder="0.00"></td>
												<td class="credit"><input type="number" class="form-control" name="credit[]" placeholder="0.00"></td>
												<td><input type="text" class="form-control" name="remark[]" placeholder="remark"></td>
												<td><button class="btn btn-danger remove">Remove</button></td>
											</tr>
										</tbody>
										<tfoot style="display: table-row-group;border-top: 2px solid;border-bottom: 2px solid">
											<tr>
												<td width="30%" colspan="2">Total</td>
												<td width="20%">
													<span class="footer-debit-sum">
														<input type="text" name="debittotal" placeholder="0.00" readonly>
													</span>
												</td>
												<td width="20%">
													<span class="footer-credit-sum">
														<input type="text" name="credittotal" placeholder="0.00" readonly>
														<?php echo form_error('credittotal', '<div class="alert alert-danger print-error-msg">', '</div>'); ?>
													</span>
												</td>
												<td width="25%"></td>
												<td width="5%"></td>
											</tr>
										</tfoot>
									</table>

								</div>
							</div>
							<div class="hidden-field"></div>
							<button type="submit" name="submit" value="submit"
							class="btn btn-primary pull-right btn-sm submit-form"> Save</button>
						</div>
					</div>
				</div>
			</div>
		</section>

	</form>

</div>


<script type="text/javascript">
	$(document).ready(function(){

		var html = '<tr><td><select class="selectpicker form-control" data-show-subtext="true" data-live-search="true" name="glname[]" required><option value="">--Select Ledger--</option><?php foreach ($text['Data'] as $key => $value) : ?><option data-balance="<?php echo $value['Balance']; ?>" value="<?php echo $value['Id']; ?>"><?php echo $value['Name']; ?></option><?php endforeach; ?></select></td><td class="balance"><input type="text" name="balance" class="form-control" placeholder="0.00" readonly=""></td><td class="debit"><input type="number" class="debitinput form-control" name="debit[]" placeholder="0.00""></td><td class="credit"><input type="number" class="form-control" name="credit[]" placeholder="0.00"></td><td><input type="text" name="remark[]" placeholder="remark"></td><td><button class="btn btn-danger remove">Remove</button></td></tr>'; 

		$("#addProduct").click(function(){
			$('tbody').append(html);
			$('.selectpicker').selectpicker();
		});

		$(document).on('click','.remove',function(){
			$(this).parents('tr').remove();
			var sum = 0;
			$(".credit input").each(function(){
				if ($(this).val().trim() != "" && !isNaN($(this).val())) {
					sum += parseFloat($(this).val());
				};
			});
			$('.footer-credit-sum input').val(sum);
		});


	});


	$('#journal_entry_table').on('change', '.selectpicker', function() {
		$balance=$(this).find(':selected').data('balance');
		$text = $(this).find(':selected').text();
		$(this).parents().next(".balance").children().val($balance);

		if ($text.includes("Expenditure") || $text.includes("Assets")) {
			$(this).parents().siblings(".credit").children().prop('max',$balance);
		}
		else if($text.includes("Income") || $text.includes("Liabilities")){
			$(this).parents().siblings(".debit").children().prop('max',$balance);
		}
	});		


	$('#journal_entry_table').on('keyup', '.debitinput', function(e) {
		var sum = 0;
		$(".debitinput").each(function(){
			if ($(this).val().trim() != "" && !isNaN($(this).val())) {
				sum += parseFloat($(this).val());
			};
		});
		$('.footer-debit-sum input').val(sum);
	});

	$('#journal_entry_table').on('keyup', '.credit input', function(e) {
		var sum = 0;
		$(".credit input").each(function(){
			if ($(this).val().trim() != "" && !isNaN($(this).val())) {
				sum += parseFloat($(this).val());
			};
		});
		$('.footer-credit-sum input').val(sum);
	});
</script>

<style type="text/css">
	.bootstrap-select.btn-group .btn .filter-option{
		width: 100% !important;
	}
	.form-group{
		z-index: 9;
	}

	.bootstrap-select:not([class*=col-]):not([class*=form-control]):not(.input-group-btn) {
	    width: 120px !important;
	}
	.form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
        background-color: #e0dddd !important;
	    opacity: 1;
	    color: #000 !important;
	    cursor: not-allowed;
	    font-weight: bold;
	}
	.table button.btn-default{
		border: none;
	}
</style>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>
	<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css" rel="stylesheet" />