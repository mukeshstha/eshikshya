<div class="content-wrapper" style="min-height: 946px;">
	<section class="content-header">
		<h1>
			Payment Voucher
		</h1>
	</section>

	<?php  
	if(!isset($_COOKIE["Token"])) {
		redirect(base_url().'account/token/gettoken', 'refresh');
	}else{
		$token = $this->input->cookie('Token');
	}

	?>
	<?php

	$curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'http://actm.prabhumanagement.com/api/ChartOfAccount/GetSubsidiaryListByFlag?Flag=CB',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
            'Authorization: Bearer '.$token
            ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        $topselect=json_decode($response,true);

        ?>

        <?php 




        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'http://actm.prabhumanagement.com/api/ChartOfAccount/GetSubsidiaryListByFlag?Flag=ASEX',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Authorization: Bearer '.$token
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        $subsidairy=json_decode($response,true);


?>
<style type="text/css">
	#payment_entry_table thead th{
		padding: 15px 15px;
	}
	#addProduct{
		margin-bottom: 15px;
	}
	.remove{
		padding: 10px 15px !important;
	}
	.dropdown-menu.inner li{
	    font-weight: normal;
	    text-decoration: none;
	    border-top: 2px solid #e7e6e6;
	}
	.dropdown-menu.inner li a{
	    font-size: 15px;
    	padding: 11px;
	}
	.dropdown-menu.open{
		-webkit-appearance: listbox !important;
		position: absolute;
    top:10px;
    left:10px;
    z-index: 10;
    float: none;
	}

	.filter-box:after{
		height: 0!important;
	}
	.filter-box{
		overflow: unset;
	}
</style>
<form class="" method="post" action="<?php echo base_url(); ?>account/voucherentry/paymentVoucherPost">
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">
							<i class="fa fa-plus-circle"></i> Payment Voucher </h3>
						</div>
						<div class="box-body">
							<div class="filter-box">
							<div class="box-body row">
							<div class="payment-head">
								<input type="hidden" name="id" value=""/>

								<div class="col-md-3 form-group">
									<label>Subsidairy</label><br>
									<select class="selectpicker form-control" data-live-search="true" name="CashGlId" required>
										<option value="">--Select Ledger--</option>
										<?php foreach ($topselect['Data'] as $key => $value) : ?>
											<option value="<?php echo $value['Id']; ?>" data-balance="<?php echo $value['Balance']; ?>"><?php echo $value['Name']; ?></option>
										<?php endforeach; ?>
									</select>
									

								</div>
								<div class="col-md-3 form-group balance">
									<label>Balance</label><br>
									<input type="text" name="balance" class="form-control" placeholder="0.00" disabled>
								</div>
								<div class="col-md-3 form-group journal_date_form">
									<label>Transaction Date</label>
									<input type="text" id="entry_date_bs" name="entry_date_bs" class="form-control"
									readonly required
									value="<?php echo set_value('postdate', date($this->customlib->getSchoolDateFormat())); ?>">
									<input type="hidden" id="entry_date" name="entry_date" class="form-control date"
									readonly
									value="">
								</div>
								<div class="col-md-3 form-group">
									<label>Reference No</label> <small
									class="text-muted">Optional</small>
									<input type="text" id="reference_no" name="reference_no" class="form-control"
									value="">
								</div>
							</div>

						</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="content">
			<div class="row">
				<div class="col-md-12">
					<div class="box box-primary">
						<div class="box-body">
							<div class="row">
								<div class="col-md-12">
								<button class="btn btn-primary pull-right" id="addProduct" type="button"><i class="fa fa-plus"></i>Add New</button>
									<table id="payment_entry_table"
									class="table invoice-table" cellspacing="0"
									width="100%">
									<thead>
										<tr>
											<th width="20%">Subsidairy</th>
											<th width="20%">Balance</th>
											<th width="20%">Amount</th>
											<th width="15%">Remarks</th>
											<th width="5%" class="no-print text text-right">Action</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>
												<select class="selectpicker form-control" data-show-subtext="true" data-live-search="true" name="GLId[]">
												<option value="">--Select Option--</option>
												<?php foreach ($subsidairy['Data'] as $key => $value) : ?>
													<option value="<?php echo $value['Id']; ?>" data-balance="<?php echo $value['Balance']; ?>"><?php echo $value['Name']; ?></option>
												<?php endforeach; ?>
												</select>
											</td>
											<td class="balance"><input class="form-control" type="text" name="balance" placeholder="0.00" readonly></td>
											<td><input type="text" name="amount[]" placeholder="0.00" class="amount"></td>
											<td><input type="text" name="remark[]" placeholder="remark"></td>
											<td><button class="btn btn-danger remove">Remove</button></td>
										</tr>
									</tbody>
									<tfoot>
										<tr>
											<td colspan="2">Total Amount</td>
											<td><input type="text" name="totalamount" class="total-amount" readonly></td>
										</tr>
									</tfoot>
								</table>

							</div>
						</div>
						<div class="row">
							<div class="col-md-6 form-group">
								<label>Narration</label>
								<textarea name="narration"
								class="form-control" required></textarea>
							</div>
						</div>
						<div class="hidden-field"></div>
						<button type="submit" name="submit" value="submit"
						class="btn btn-primary pull-right btn-sm submit-form"> Save</button>
					</div>
				</div>
			</div>
		</div>
	</section>

</form>

</div>

<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css" rel="stylesheet" />

	<style type="text/css">
		.bootstrap-select:not([class*=col-]):not([class*=form-control]):not(.input-group-btn) {
		    width: 120px !important;
		}
		span.filter-option.pull-left {
		    width: 100% !important;
		}

		#payment_entry_table tfoot{
			display: table-row-group;
		}
	</style>

<script type="text/javascript">
	$(document).ready(function(){

		var html = '<tr><td><select class="selectpicker form-control" data-show-subtext="true" data-live-search="true" name="GLId[]"><option value="">--Select Option--</option><?php foreach ($subsidairy['Data'] as $key => $value) : ?><option data-balance="<?php echo $value['Balance']; ?>" value="<?php echo $value['Id']; ?>"><?php echo $value['Name']; ?></option><?php endforeach; ?></select></td><td class="balance"><input type="text" name="balance" placeholder="0.00" readonly></td><td><input type="text" name="amount[]" placeholder="0.00" class="amount"></td><td><input type="text" name="remark[]" placeholder="remark"></td><td><button class="btn btn-danger remove">Remove</button></td></tr>'; 
		$("#addProduct").click(function(){
			$('tbody').append(html);
			$('.selectpicker').selectpicker();
		});

		$(document).on('click','.remove',function(){
			$(this).parents('tr').remove();
			var sum = 0;
			$(".amount").each(function(){
				if ($(this).val().trim() != "" && !isNaN($(this).val())) {
					sum += parseFloat($(this).val());
				};
			});
			$('.total-amount').val(sum);
		});
	});

$(document).ready(function(){
	$('.payment-head').on('change', '.selectpicker', function() {

		$balance=$(this).find(':selected').data('balance');
		$(this).parents().next(".balance").children().val($balance);
	});	


$('#payment_entry_table').on('change', '.selectpicker', function() {
		$balance=$(this).find(':selected').data('balance');
		$(this).parents().next(".balance").children().val($balance);

		$(this).parent().next(".balance").children().addClass("hawa");
	});
});

	$('#payment_entry_table').on('keyup', '.amount', function(e) {
		var sum = 0;
		$(".amount").each(function(){
			if ($(this).val().trim() != "" && !isNaN($(this).val())) {
				sum += parseFloat($(this).val());
			};
		});
		$('.total-amount').val(sum);
	});

</script>
	

	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>
	