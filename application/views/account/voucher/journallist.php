<?php //require(__DIR__."/../init_curl.php"); ?>

<div class="content-wrapper" style="min-height: 946px;">
    <section class="content-header">
        <h1>

        </h1>
    </section>

    <section class="content">
        <div class="row">
            <?php if($this->session->flashdata('success')) : ?>
                <div class="alert alert-success">
                    <?= $this->session->flashdata('success'); ?>
                </div>
            <?php endif; ?>
            <div class="col-md-12">
                <div class="box box-primary">

                    <div class="box-header ptbnull with-border">
                        <h3 class="box-title titlefix"><i
                            class="fa fa-book"></i>Unapproved Journals</h3>
                            <small class="pull-right">
                                <a href="<?php echo base_url(); ?>account/voucherentry/journalentry"
                                 class="btn btn-primary btn-sm">
                                 <i class="fa fa-plus"></i> Add Journal</a>
                             </small>
                         </div><!-- /.box-header -->
                         <div class="box-body">
                            <div class="filter-box">
                            <form action="" action="post">
                                <div class="box-body row">
                                <div class="col-md-4">
                                    <label>From date</label>
                                    <input type="text" autocomplete="off" name="FromDate" class="form-control  date"  value="<?php  echo set_value('FromDate') ?>">
                                </div>
                                <div class="col-md-4">
                                    <label>To date</label>
                                    <input type="text" autocomplete="off" name="ToDate" class="form-control date"  value="<?php  echo set_value('ToDate') ?>">
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                    <label class="d-block">&nbsp;</label>
                                    <button class="btn btn-primary btn-sm checkbox-toggle" type="submit">Search</button>
                                </div>
                                </div>
                            </div>
                            </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                        <div class="box-body">
                            <div class="table-responsive mailbox-messages">
                                <div class="download_label">Unapproved Journal List</div>
                               
                                <table id="" class="table table-striped table-bordered table-hover example" cellspacing="0"
                                width="100%">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Created Date</th>
                                        <th>Journal No</th>
                                        <th>Narration</th>
                                        <th>Balance</th>
                                        <th class="no-print text text-right">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($list['Data'] as $key => $value) : ?>
                                
                                        <tr>
                                            <td class="mailbox-name"><?php echo $value['Id']; ?></td>
                                            <td class="mailbox-name">
                                                <?php 
                                                if($this->customlib->getcalender()->calender=="0"){
                                                    echo $this->customlib->nepalidate(date("Y-m-d", strtotime($value['Date']))); 
                                                }else{
                                                    echo date("Y-m-d", strtotime($value['Date'])); 
                                                }
                                                ?>
                                            </td>
                                            <td class="mailbox-name"><?php echo $value['VoucherNo']; ?></td>
                                            <td class="mailbox-name"><?php echo $value['Narration']; ?></td>
                                            <td class="mailbox-name"> <?php echo $value['Balance']; ?></td>
                                            <td class="mailbox-date no-print text text-right">
                                                <a role="button" href="<?php echo base_url(); ?>account/journal/viewjournal/<?php echo $value['RefNo']; ?>/<?php echo $value['Id']; ?>/<?php echo $value['VoucherNo']; ?>/<?php echo $value['TransId']; ?>" class="btn btn-default btn-xs " title="View"><i class="fa fa-eye"></i></a>

                                                <a onclick="return confirm('Approve Invoice?');" title="Approve" class="btn btn-default btn-xs" href="<?php echo base_url(); ?>account/journal/approve/<?php echo $value['Id']; ?>/<?php echo $value['VoucherNo']; ?>/<?php echo $value['TransId']; ?>"><i class="fa fa-check" aria-hidden="true"></i></a>

                                        		<a onclick="return confirm('Reject Voucher')" href="<?php echo base_url();?>account/journal/reject/<?php echo $value['RefNo']; ?>/<?php echo $value['Id']; ?>/<?php echo $value['VoucherNo']; ?>/<?php echo $value['TransId']; ?>" class="btn btn-default btn-xs" data-toggle="tooltip" title="Reject"><i class="fa fa-remove"></i>
                                    			</a>
                                			</td>
                            			</tr>

                            		<?php endforeach; ?>
                    			</tbody>
                			</table>
           	 			</div>
        			</div>
    			</div>
			</div>
		</div>
	</section>
</div>



