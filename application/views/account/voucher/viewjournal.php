<div class="content-wrapper" style="min-height: 946px;">
	<section class="content-header">
		<h1>
			Journal
		</h1>
	</section>
	<section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">
                        <i class="fa fa-plus-circle"></i>Journal Voucher Detail</h3>
                        <?php foreach ($list['Data'] as $value) : ?>
                            
                            <?php if($value['Parent']==true) : ?>
                                <?php if($value['ApprovedDate']=="") :?>
                                    <a onclick="return confirm('Reject Voucher')" href="<?php echo base_url();?>account/journal/reject/<?php echo $id; ?>/<?php echo $vouchernumber; ?>/<?php echo $transid; ?>" class="btn btn-danger btn-sm pull-right" data-toggle="tooltip" title="Reject">Reject Voucher</a>
                                    <a onclick="return confirm('Approve Invoice?');" title="Approve" class="btn btn-success btn-sm pull-right" href="<?php echo base_url(); ?>account/journal/approve/<?php echo $id; ?>/<?php echo $value['VoucherNo']; ?>">Approve Voucher</a>
                                <?php endif; ?>
                            <?php endif; ?>
                            
                        <?php endforeach; ?> 
                    </div>
                    <div class="box-body">
                        <div class="row" style="background: #f6f3f3;padding-top: 15px;padding-left: 15px;margin-bottom: 15px;border: 1px solid">
                        <?php foreach ($list['Data'] as $value) : ?>
                            
                            <?php if($value['Parent']==true) : ?>
                                <div class="col-md-12 form-group double-column">
                                    <ul>
                                        <?php if($value['Parent']==true) : ?>
                                            <li><label class="wd-40"><strong>Title : </strong></label>&nbsp;<p class="wd-60"><?php echo $value['Narration']; ?></p></li>
                                            <li><label class="wd-40"><strong>Voucher Number : </strong></label>&nbsp;<p class="wd-60"><?php echo $value['VoucherNo']; ?></p></li>
                                            <li><label class="wd-40"><strong>Entry Date : </strong></label>&nbsp;<p class="wd-60"><?php $newentrydate=date("Y-m-d",strtotime($value['CreatedDate']));
                                                        if($this->customlib->getcalender()->calender=="0"){
                                                            echo $this->customlib->nepalidate($newentrydate);
                                                        }else{
                                                            echo date($this->customlib->getSchoolDateFormat(), strtotime($newentrydate));
                                                        }?></p></li>
                                            <li><label class="wd-40"><strong>Reference No : </strong></label>&nbsp;<p class="wd-60"><?php echo $value['RefNumber']; ?></p></li>
                                            <li><label class="wd-40"><strong>Created By : </strong></label>&nbsp;<p class="wd-60"><?php echo $value['CreatedBy']; ?></p></li>
                                            <li><label class="wd-40"><strong>Approved By : </strong></label>&nbsp;<p class="wd-60"><?php echo $value['ApprovedBy']; ?></p></li>
                                            <li><label class="wd-40"><strong>Approved Date : </strong></label>&nbsp;<p class="wd-60"><?php $new_date = date("Y-m-d",strtotime($value['ApprovedDate']));
                                                        if($this->customlib->getcalender()->calender=="0"){
                                                            echo $this->customlib->nepalidate($new_date);
                                                        }else{
                                                            echo date($this->customlib->getSchoolDateFormat(), strtotime($new_date));
                                                        } ?></p></li>

                                        <?php endif; ?>
                                    </ul>
                                    
                                </div>
                        	<?php endif; ?>
                            
                        <?php endforeach; ?>
                        </div>
                    
                        <div class="row">
                            <div class="col-md-12" style="padding: 0">
                                <table id="journal_entry_table" class="table table-striped invoice-table" cellspacing="0" width="100%" style="box-shadow: 1px 1px 6px 1px #6e6565;">
                                    <thead>
	                                    <tr>
                                            <th width="7%" class="text-center">S no</th>
	                                        <th width="35%">Account Name</th>
	                                        <!-- <th width="25%">Category</th> -->
	                                        <th width="20%" class="text-center">Debit Amount</th>
	                                        <th width="20%" class="text-center">Credit Amount</th>
	                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                            $totalcredit = 0;
                                            $totaldebit = 0;
                                        ?>
                                    <?php foreach ($list['Data'] as $listkey=>$value) : ?>
                                        <?php 
                                        $totalcredit = $totalcredit + $value['Credit'];
                                        $totaldebit = $totaldebit + $value['Debit'];
                                        ?>
                                    <?php if($value['Parent']==false) : ?>
                                        <tr class="editable-row">
                                            <td class="text-center" width="7%"><?php echo $listkey;?></td>
                                            <td width="35%"><?php echo $value['GLSubsidiary']; ?>(<?php echo $value['Code']; ?>)</td>
                                            <!-- <td width="25%">Cleaning expense</td> -->
                                            <td width="20%" class="text-center"><?php echo $value['Debit']; ?></td>
                                            <td width="20%" class="text-center"><?php echo $value['Credit']; ?></td>
                                        </tr>
                                      <?php endif; ?>
                                  <?php endforeach; ?>
                                    </tbody>
                                    <tfoot style="display: table-footer-group;border-top: 2px solid;border-bottom: 3px solid;">
	                                    <tr>
                                            <td></td>
	                                        <td width="35%" class="text-center">Total</td>
	                                        <!-- <td width="25%"></td> -->
	                                        <td width="20%" class="text-center"><span class="footer-debit-sum">NRs.&nbsp;<?php echo $this->customlib->numberFormat($totaldebit); ?></span></td>
	                                        <td width="20%" class="text-center"><span class="footer-credit-sum">NRs.&nbsp;<?php echo $this->customlib->numberFormat($totalcredit); ?></span></td>
	                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </section>

</div>

<style>
    .double-column ul{
        list-style-type: none;
    -webkit-column-count: 2;
    -moz-column-count: 2;
    column-count: 2;
    }
    .wd-40{
        width: 30%;
    }
    .wd-60{
        width: 70%;
        display: inline;
    }
    tfoot td{
        background: #b9debb;
    }
</style>