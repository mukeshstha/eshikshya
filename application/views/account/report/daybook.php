<style type="text/css">
	a{
		color: #103feb;
	}
</style>

<div class="content-wrapper" style="min-height: 946px;">
	<section class="content-header">
		<h1>
			Daybook
		</h1>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title"><i class="fa fa-plus-circle"></i>Day Book </h3>
					</div>
					<div class="box-body">

						<div class="filter-box">
							<form action="" action="post">
								<div class="col-md-2">
									<label>From Date</label>
									<input type="text" autocomplete="off" name="FromDate" class="form-control  date"  value="">
								</div>
								<div class="col-md-2">
									<label>To Date</label>
									<input type="text" autocomplete="off" name="ToDate" class="form-control  date"  value="<?php echo set_value($todate) ?>">
								</div>
								<div class="col-md-3">
									<div class="col-md-4">
										<label>&nbsp;</label><br>
										<input type="checkbox" id="cashflag" checked>&nbsp;
										<label>Cash</label>
									</div>
									<div class="col-md-8">
										<label>Cash Ledger</label>
										<select class="form-control" id="cash-ledger" name="cashledger">
											<option value="">Select Ledger</option>
											<?php foreach ($cash['Data'] as $key => $value) : ?>
												<option value="<?php echo $value['Id']?>"><?php echo $value['Name']; ?></option>
											<?php endforeach; ?>
										</select>
									</div>

								</div>
								<div class="col-md-3">
									<div class="col-md-4">
										<label>&nbsp;</label><br>
										<input type="checkbox" id="bankflag">
										<label>Bank</label>
									</div>
									<div class="col-md-8">
										<label>Bank Ledger</label>
										<select class="form-control" id="bank-ledger" name="bankledger">
											<option value="">Select Ledger</option>
											<?php foreach ($bank['Data'] as $key => $value) : ?>
												<option value="<?php echo $value['Id']?>"><?php echo $value['Name']; ?></option>
											<?php endforeach; ?>
										</select>
									</div>
								</div>


								<div class="col-md-2">
									<label>&nbsp;</label><br>
									<button class="btn btn-primary btn-sm pull-right" type="submit" id="btnSearch">
										<i class="fa fa-search" aria-hidden="true"></i> View
									</button>
								</div>
							</form>
						</div>

					</div>
				</div>
			</div>
		</div>
	</section>




	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-body">
						<div id="SearchResult">
							<table class="table table-bordered table-condensed" style="border-collapse:collapse;">
								<thead class="header-a bg-danger">
									<tr>
										<th class="text-center">
											Date
										</th>
										<th class="text-center">
											VoucherNo
										</th>
										<th class="text-center">
											Code
										</th>
										<th class="text-center">
											Ledger Name
										</th>
										<th class="text-center">
											Title
										</th>
										<th class="text-center">
											Debit(रू)
										</th>
										<th class="text-center">
											Credit(रू)
										</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($cashdata['Data'] as $key => $value) { 
										if($value['VoucherType'] == "Opening") {?>
										<tr style="border-top: 2px solid #000;border-bottom: 2px solid #000;color: #e91616">
											<?php if($this->customlib->getcalender()->calender=="0"){ ?>
												<td><strong><?php echo $this->customlib->nepalidate(date("Y-m-d", strtotime($value['Date']))); ?></strong></td>
											<?php }else{ ?>
												<td><strong><?php echo date("Y-m-d", strtotime($value['Date'])); ?></strong></td>
											<?php } ?>
											<td></td>
											<td></td>
											<td><strong><?php echo $value['LedgerName']; ?></strong></td>
											<td><strong><?php echo $value['Narration']; ?></strong></td>
											<td class="text-right"><strong><?php echo $value['Debit']; ?></strong></td>
											<td class="text-right"><strong><?php echo $value['Credit']; ?></strong></td>
										</tr>
									<?php }elseif ($value['VoucherType'] == "Receipt") { ?>
										<tr class="receipt-detail">
											<?php if($this->customlib->getcalender()->calender=="0"){ ?>
												<td><?php echo $this->customlib->nepalidate(date("Y-m-d", strtotime($value['Date']))); ?></td>
											<?php }else{ ?>
												<td><?php echo date("Y-m-d", strtotime($value['Date'])); ?></td>
											<?php } ?>
											<td>
												<a data-id="<?php echo $value['VoucherId']; ?>" data-number="<?php echo $value['VoucherNo']; ?>" class="voucher_popup" title="<?php echo $this->lang->line('details for') .$value['VoucherNo'] ?>">
													<?php echo $value['VoucherNo']; ?>
												</a>
											</td>
											<td> <?php echo $value['Code']; ?></td>
											<td><?php echo $value['LedgerName'] . " (" . $value['VoucherType'] . ")"; ?></td>
											<td><?php echo $value['Narration']; ?></td>
											<td class="text-right"> <?php echo $value['Debit']; ?> </td>
											<td class="text-right"><?php echo $value['Credit']; ?></td>
										</tr>
									<?php }elseif ($value['VoucherType']=="Payment") { ?>
										<tr class="payment-detail">
											<?php if($this->customlib->getcalender()->calender=="0"){ ?>
												<td><?php echo $this->customlib->nepalidate(date("Y-m-d", strtotime($value['Date']))); ?></td>
											<?php }else{ ?>
												<td><?php echo date("Y-m-d", strtotime($value['Date'])); ?></td>
											<?php } ?>
											<td>
												<a class="voucher_popup" data-toggle="tooltip" data-id="<?php echo $value['VoucherId']; ?>"  data-number="<?php echo $value['VoucherNo']; ?>">
													<?php echo $value['VoucherNo']; ?>
												</a>
											</td>
											<td> <?php echo $value['Code']; ?></td>
											<td><?php echo $value['LedgerName'] . " (" . $value['VoucherType'] . ")"; ?></td>
											<td><?php echo $value['Narration']; ?></td>
											<td class="text-right"> <?php echo $value['Debit']; ?> </td>
											<td class="text-right"><?php echo $value['Credit']; ?></td>
										</tr>
									<?php }elseif ($value['VoucherType']=="Closing") { ?>
										<tr style="border-top: 2px solid #000;border-bottom: 2px solid #000;color: #e91616">
											<?php if($this->customlib->getcalender()->calender=="0"){ ?>
												<td><strong><?php echo $this->customlib->nepalidate(date("Y-m-d", strtotime($value['Date']))); ?></strong></td>
											<?php }else{ ?>
												<td><strong><?php echo date("Y-m-d", strtotime($value['Date'])); ?></strong></td>
											<?php } ?>
											<td></td>
											<td></td>
											<td><strong><?php echo $value['LedgerName']; ?></strong></td>
											<td><strong><?php echo $value['Narration']; ?></strong></td>
											<td class="text-right"><strong><?php echo $value['Debit']; ?></strong></td>
											<td class="text-right"><strong><?php echo $value['Credit']; ?></strong></td>
										</tr>
									<?php }} ?>
								</tbody>
							</table>
						</div>


					</div>

				</div>
			</div>
		</div>
	</section>
</div>


			<div class="modal fade" id="modal-FV-Details" role="dialog">
				<div class="modal-dialog" style="width: 80%">
					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" style="position: static;color: #000">&times;</button>
							<h4 class="modal-title">Details for <span class="FVNumber">FV0000286</span></h4>
						</div>
						<div class="modal-body voucher_body">
							
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						</div>
					</div>

				</div>
			</div>

<script type="text/javascript">

	var change_check = function () {
		if ($("#cashflag").is(":checked")) {
			$('#cash-ledger').prop('disabled', false);
		}
		else {
			$('#cash-ledger').prop('disabled', 'disabled');
		}
	};
	$(change_check);
	$("#cashflag").change(change_check);

	var change_bank = function () {
		if ($("#bankflag").is(":checked")) {
			$('#bank-ledger').prop('disabled', false);
		}
		else {
			$('#bank-ledger').prop('disabled', 'disabled');
		}
	};
	$(change_bank);
	$("#bankflag").change(change_bank);
</script> 


<script type="text/javascript">
	$(document).on("click",".voucher_popup",function () {
		var base_url = '<?php echo base_url() ?>';
        var voucherno = $(this).data('number');
        var voucherid = $(this).data('id');
        $('#modal-FV-Details').modal({
            backdrop: 'static',
            keyboard: false
        });
        $.ajax({
        	type: "POST",
        	url: base_url + "account/report/getvoucher",
        	data: {'voucher_id': voucherid, 'voucher_no': voucherno},
            dataType: "json",
            beforeSend: function () {
                $('.voucher_body').html();
            },
            success: function (response) {
                $('.voucher_body').html(response.sendingdetails);
            },
            complete: function () {
            }
        });
    });
</script>