<div class="content-wrapper" style="min-height: 946px;">
	<section class="content-header">
		<h1>
			General Ledger
		</h1>
	</section>
	<section class="content">

		<div class="row">
			<?php
			$token = $this->input->cookie('Token'); 
			if(isset($token)){
				$curl = curl_init();

				curl_setopt_array($curl, array(
					CURLOPT_URL => 'http://actm.prabhumanagement.com/api/ChartOfAccount/GetSubsidiaryListByFlag?Flag=All',
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => '',
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 0,
					CURLOPT_FOLLOWLOCATION => true,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => 'GET',
					CURLOPT_HTTPHEADER => array(
						'Authorization: Bearer '.$token
					),
				));

				$response = curl_exec($curl);

				curl_close($curl);
				$text= json_decode($response, true);
			}else{
				redirect($_SERVER['HTTP_REFERER']);
			}
			?>
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title"><i class="fa fa-plus-circle"></i> General Ledger </h3>
					</div>
					<div class="row">
						<div class="col-md-12">
							
							<form action="" action="post">
								<div class="filter-box">
									<div class="box-body row">

										<div class="col-md-3">
											<label>From Date</label>
											<input type="text" autocomplete="off" name="FromDate" class="form-control  date"  value="<?php  echo set_value('FromDate') ?>">
										</div>
										<div class="col-md-3">
											<label>To Date</label>
											<input type="text" autocomplete="off" name="ToDate" class="form-control  date"  value="<?php  echo set_value('ToDate') ?>">
										</div>

										<div class="col-md-3">
											<label>GL Ledger &nbsp;</label><br>
											<select class="selectpicker form-control" data-show-subtext="true" data-live-search="true" name="glname">
												<option>Select Ledger</option>
												<?php foreach ($text['Data'] as $key => $value) : ?>
													<option value="<?php echo $value['Id']; ?>"><?php echo $value['Name']; ?></option>
												<?php endforeach; ?>
											</select>
										</div>
										<div class="col-md-3 text-center">
											<label style="display: block">&nbsp;</label>
											<button class="btn btn-primary btn-sm checkbox-toggle" type="submit">View</button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="content">

		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div id="SearchResult" style="padding: 25px">
						<div class="table-responsive">
							<table class="table table-bordered table-color table-striped table-danger table-hover js-dataTable-buttons" id="showtable" width="100%" style="margin-bottom: 0px">
								
								<thead class="bg-danger">
									<tr class="text-nowrap">
										<th>S.N</th>
										<th class="text-center">Date</th>
										<th class="text-center">
											Voucher No
										</th>
										<th class="text-center">Title</th>
										<th class="bg-warning text-right">
											Debit(रू)
										</th>
										<th class="bg-success text-right">
											Credit(रू)
										</th>
										<th class="bg-info text-right">Balance(रू)</th>
									</tr>
								</thead>
								<tbody>
									<?php if(isset($list['Data'])) : ?>
									<?php $totalcredit = 0; $totaldebit = 0; ?>
									<?php foreach ($list['Data'] as $key => $data) : ?>
										<div style="display: none"><?php echo "<pre>"; print_r($data); echo "</pre>"; ?></div>
										<td width="10px">
											<?php echo $key+1; ?>
										</td>	
										<td class="text-center">										
											<?php if($this->customlib->getcalender()->calender=="0"){ ?>
												<strong><?php echo $this->customlib->nepalidate(date("Y-m-d", strtotime($data['CreatedDate']))); ?></strong>
											<?php }else{ ?>
												<strong><?php echo date("Y-m-d", strtotime($data['CreatedDate'])); ?></strong>
											<?php } ?>
										</td>
										<td class="text-center">

											<a class="btn btn-alt-danger voucher_popup" data-toggle="tooltip" data-id="<?php echo $data['Id']; ?>"  data-number="<?php echo $data['VoucherNo']; ?>">
												<?php echo $data['VoucherNo']; ?>
											</a>

										</td>
										<td class="bg-warning"><?php echo $data['Narration'];?></td>
										<td class="bg-success text-right"><?php echo $data['Debit']; ?></td>
										<td class="bg-info text-right"><?php echo $data['Credit'];?> </td>
										<td class="text-right">
											<?php if($data['Debit'] > $data['Credit']){
												echo  ($data['Debit'] - $data['Credit']) . " " . "Dr";
											}else{
												echo ($data['Credit'] - $data['Debit']). " " . "Cr";
											}
											?>
										</td>
										</tr>
										<?php $totalcredit = $totalcredit + $data['Credit']; $totaldebit = $totaldebit + $data['Debit']; ?>
									<?php endforeach; ?>

											</tbody>
											<tfoot style="display: table-footer-group;border-top: 2px solid;border-bottom: 3px solid;">
												<tr class="table-secondary text-right">
													<td colspan="2"></td>
													<td style="text-align: left"><strong>Grand Total(रू.)</strong></td>
													<td class="bg-warning text-right">
													</td>
													<td class="bg-success text-right">
														<strong>
															<span class="double-underline"><?php echo $totaldebit;?></span>
														</strong>
													</td>
													<td class="bg-info text-right">
														<strong>
															<span class="double-underline"><?php echo $totalcredit;?> </span>
														</strong>
													</td>
													<td>
														<?php if($totaldebit > $totalcredit){
															echo ($totaldebit - $totalcredit) . " " . "Dr";
														}else{
															echo ($totalcredit - $totaldebit) . " " . "Cr";
														}
														?>
													</td>
												</tr>
												
											</tfoot>
										<?php endif; ?>
										</table>
									</div>

								</div>



							</div>
						</div>
					</div>
				</section>
			</div>
			<div class="modal fade" id="modal-FV-Details" role="dialog">
				<div class="modal-dialog" style="width: 80%">
					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" style="position: static;color: #000">&times;</button>
							<h4 class="modal-title">Details for <span class="FVNumber">FV0000286</span></h4>
						</div>
						<div class="modal-body voucher_body">
							
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						</div>
					</div>

				</div>
			</div>

			<style type="text/css">
				span.filter-option.pull-left{
					width: auto !important;
				}

				.btn-group.bootstrap-select.dropup{
					width: 100% !important;
					height: 40px !important;
				}

				.btn-group.bootstrap-select.dropup button{
					line-height: 25px !important;
				}
				.btn-group.bootstrap-select{
					width: 100% !important;
				}
				.btn-alt-danger {
					color: #af1e1e;
					background-color: #fbeaea;
					border-color: #fbeaea;
				}
				.double-underline {
					border-top: 1px solid;
					border-bottom: 3px double;
					padding: 4px;
				}
				.filter-box {
					overflow: unset;
				}
				.filter-box:after {
					height: 0!important;
				}
			</style>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>
			<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css" rel="stylesheet" />

<script type="text/javascript">
	$(document).on("click",".voucher_popup",function () {
		var base_url = '<?php echo base_url() ?>';
        var voucherno = $(this).data('number');
        var voucherid = $(this).data('id');
        $('#modal-FV-Details').modal({
            backdrop: 'static',
            keyboard: false
        });
        $.ajax({
        	type: "POST",
        	url: base_url + "account/report/getvoucher",
        	data: {'voucher_id': voucherid, 'voucher_no': voucherno},
            dataType: "json",
            beforeSend: function () {
                $('.voucher_body').html();
            },
            success: function (response) {
                $('.voucher_body').html(response.sendingdetails);
            },
            complete: function () {
            }
        });
    });
</script>