<style>
	.switch {
		position: relative;
		display: inline-block;
		width: 60px;
		height: 34px;
	}

	.switch input { 
		opacity: 0;
		width: 0;
		height: 0;
	}

	.slider {
		position: absolute;
		cursor: pointer;
		top: 0;
		left: 0;
		right: 0;
		bottom: 0;
		background-color: #ccc;
		-webkit-transition: .4s;
		transition: .4s;
	}

	.slider:before {
		position: absolute;
		content: "";
		height: 26px;
		width: 26px;
		left: 4px;
		bottom: 4px;
		background-color: white;
		-webkit-transition: .4s;
		transition: .4s;
	}

	input:checked + .slider {
		background-color: #2196F3;
	}

	input:focus + .slider {
		box-shadow: 0 0 1px #2196F3;
	}

	input:checked + .slider:before {
		-webkit-transform: translateX(26px);
		-ms-transform: translateX(26px);
		transform: translateX(26px);
	}

	/* Rounded sliders */
	.slider.round {
		border-radius: 34px;
	}

	.slider.round:before {
		border-radius: 50%;
	}
	.double-underline {
		border-top: 1px solid;
		border-bottom: 3px double;
		padding: 4px;
	}
	.badge-secondary {
		color: #fff;
		background-color: rgba(0, 0, 0, 0.33);
	}
</style>


<div class="content-wrapper" style="min-height: 946px;">
	<section class="content-header">
		<h1>
			Trail Balance
		</h1>
	</section>
	<section class="content">

		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title"><i class="fa fa-plus-circle"></i> Trail Balance </h3>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="box-body">
								<form action="" action="post">
									<div class="col-md-3">
										<label>From date</label>
										<input type="text" autocomplete="off" name="FromDate" class="form-control  date"  value="<?php  echo set_value('FromDate') ?>">
									</div>
									<div class="col-md-3">
										<label>To date</label>
										<input type="text" autocomplete="off" name="ToDate" class="form-control date"  value="<?php  echo set_value('ToDate') ?>">
									</div>
									<div class="col-md-3">
										<label>Summary &nbsp;</label>
										<label class="switch">
											<input type="checkbox" class="summary_check">
											<span class="slider round"></span>
										</label>
									</div>
									<div class="col-md-3">
										<button type="submit">Search</button>
									</div>
								</form>
							</div>
						</div>
					</div>



					<div id="SearchResult" style="margin-top: 25px">

						<div class="table-responsive summary">
							<?php if(isset($list['Data'])) : ?>
								<table class="table table-bordered  table-striped table-danger table-hover js-dataTable-buttons" width="100%">
									<thead>
										<tr>
											<th colspan="2"></th>
											<th colspan="2" class="bg-warning">
												Opening Balance
											</th>
											<th colspan="2" class="bg-success">
												Transaction Balance
											</th>
											<th colspan="2" class="badge-secondary">
												Closing Balance
											</th>
										</tr>
										<tr class="text-nowrap">
											<th>
												GLHead
											</th>
											<th>Particulars</th>
											<th class="bg-warning">
												Debit(रू)
											</th>
											<th class="bg-warning">
												Credit(रू)
											</th>
											<th class="bg-success">
												Debit(रू)
											</th>
											<th class="bg-success">
												Credit(रू)
											</th>
											<th class="badge-secondary">
												Debit(रू)
											</th>
											<th class="badge-secondary">
												Credit(रू)
											</th>
										</tr>
									</thead>
									<tbody>

										<?php foreach ($list['Data'] as $key => $value) : ?>
											<tr class="font-weight-bold">
												<td colspan="2"><?php echo $value['Name']; ?></td>
												<td class="bg-warning text-right smopeningdr">
													<?php echo $value['OpeningDR']; ?>
												</td>
												<td class="bg-warning text-right smopeningcr">
													<?php echo $value['OpeningCR']; ?>
												</td>
												<td class="bg-success text-right smtrnxdr">
													<?php echo $value['TxnDR']; ?>
												</td>
												<td class="bg-success text-right smtrnxcr">
													<?php echo $value['TxnCR']; ?>
												</td>
												<td class="badge-secondary text-right smclosingdr">
													<?php echo $value['ClosingDR']; ?>
												</td>
												<td class="badge-secondary text-right smclosingcr">
													<?php echo $value['ClosingCR']; ?>
												</td>
											</tr>

											<?php if(isset($value['tBMainHeadDtos'])) : ?>
												<?php foreach ($value['tBMainHeadDtos'] as $key => $subvalue) : ?>
													<tr class="font-weight-bold">
														<td></td>
														<td colspan="1"><?php echo $subvalue['Name']; ?></td>
														<td class="bg-warning text-right">
															<?php echo $subvalue['OpeningDR']; ?>
														</td>
														<td class="bg-warning text-right">
															<?php echo $subvalue['OpeningCR']; ?>
														</td>
														<td class="bg-success text-right">
															<?php echo $subvalue['TxnDR']; ?>
														</td>
														<td class="bg-success text-right">
															<?php echo $subvalue['TxnCR']; ?>
														</td>
														<td class="badge-secondary text-right">
															<?php echo $subvalue['ClosingDR']; ?>
														</td>
														<td class="badge-secondary text-right">
															<?php echo $subvalue['ClosingCR']; ?>
														</td>
													</tr>
												<?php endforeach; ?>

											<?php endif; ?>
										<?php endforeach; ?>


									</tbody>
									<tfoot style="display: table-row-group">
										<tr class="font-weight-bold">
											<td colspan="1"></td>
											<td colspan="1"><strong>Grand Total(रू)</strong></td>
											<td class="bg-warning text-right">
												<span class="double-underline" id="smopeningdr_total">
													
												</span>
											</td>
											<td class="bg-warning text-right">
												<span class="double-underline" id="smopeningcr_total">
													
												</span>
											</td>
											<td class="bg-success text-right">
												<span class="double-underline" id="smtrnxdr_total">
												</span>
											</td>
											<td class="bg-success text-right">
												<span class="double-underline" id="smtrnxcr_total">
												</span>
											</td>
											<td class="badge-secondary text-right">
												<span class="double-underline" id="smclosingdr_total">
												</span>
											</td>
											<td class="badge-secondary text-right">
												<span class="double-underline" id="smclosingcr_total">
												</span>
											</td>
										</tr>
									</tfoot>
								</table>

							<?php endif; ?>
						</div>

						<div class="table-responsive detail">
							<?php if(isset($list['Data'])) : ?>
								<table class="table table-bordered  table-striped table-danger table-hover js-dataTable-buttons" width="100%">
									<thead>
										<tr>
											<th colspan="4"></th>
											<th colspan="2" class="bg-warning">
												Opening Balance
											</th>
											<th colspan="2" class="bg-success">
												Transaction Balance
											</th>
											<th colspan="2" class="badge-secondary">
												Closing Balance
											</th>
										</tr>
										<tr class="text-nowrap">
											<th>
												GLHead
											</th>
											<th>Particulars</th>
											<th>GL Code</th>
											<th>
												Ledger Name
											</th>
											<th class="bg-warning">
												Debit(रू)
											</th>
											<th class="bg-warning">
												Credit(रू)
											</th>
											<th class="bg-success">
												Debit(रू)
											</th>
											<th class="bg-success">
												Credit(रू)
											</th>
											<th class="badge-secondary">
												Debit(रू)
											</th>
											<th class="badge-secondary">
												Credit(रू)
											</th>
										</tr>
									</thead>
									<tbody>

										<?php foreach ($detail['Data'] as $key => $value) : ?>
											<tr class="font-weight-bold">
												<td colspan="4"><?php echo $value['Name']; ?></td>
												<td class="bg-warning text-right dtopeningdr">
													<?php echo $value['OpeningDR']; ?>
												</td>
												<td class="bg-warning text-right dtopeningcr">
													<?php echo $value['OpeningCR']; ?>
												</td>
												<td class="bg-success text-right dttrnxdr">
													<?php echo $value['TxnDR']; ?>
												</td>
												<td class="bg-success text-right dttrnxcr">
													<?php echo $value['TxnCR']; ?>
												</td>
												<td class="badge-secondary text-right dtclosingdr">
													<?php echo $value['ClosingDR']; ?>
												</td>
												<td class="badge-secondary text-right dtclosingcr">
													<?php echo $value['ClosingCR']; ?>
												</td>
											</tr>

											<?php if(isset($value['tBMainHeadDtos'])) : ?>
												<?php foreach ($value['tBMainHeadDtos'] as $key => $subvalue) : ?>
													<tr class="font-weight-bold">
														<td></td>
														<td colspan="3"><?php echo $subvalue['Name']; ?></td>
														<td class="bg-warning text-right">
															<?php echo $subvalue['OpeningDR']; ?>
														</td>
														<td class="bg-warning text-right">
															<?php echo $subvalue['OpeningCR']; ?>
														</td>
														<td class="bg-success text-right">
															<?php echo $subvalue['TxnDR']; ?>
														</td>
														<td class="bg-success text-right">
															<?php echo $subvalue['TxnCR']; ?>
														</td>
														<td class="badge-secondary text-right">
															<?php echo $subvalue['ClosingDR']; ?>
														</td>
														<td class="badge-secondary text-right">
															<?php echo $subvalue['ClosingCR']; ?>
														</td>
													</tr>

													<?php if(isset($subvalue['subsidiaryDtos'])) : ?>
														<?php foreach ($subvalue['subsidiaryDtos'] as $key => $subsidairy) : ?>
															<tr>
																<td></td>
																<td></td>
																<td>
																	<a href="" target="_blank"><?php echo $subsidairy['GLCode']; ?></a>
																</td>
																<td><?php echo $subsidairy['Name']; ?></td>
																<td class="bg-warning text-right"><?php echo $subsidairy['OpeningDR']; ?></td>
																<td class="bg-warning text-right"><?php echo $subsidairy['OpeningCR']; ?></td>
																<td class="bg-success text-right"><?php echo $subsidairy['TxnDR']; ?></td>
																<td class="bg-success text-right"><?php echo $subsidairy['TxnCR']; ?></td>
																<td class="badge-secondary text-right"><?php echo $subsidairy['ClosingDR']; ?></td>
																<td class="badge-secondary text-right"><?php echo $subsidairy['ClosingCR']; ?></td>
															</tr>

														<?php endforeach; ?>
													<?php endif; ?>
												<?php endforeach; ?>

											<?php endif; ?>
										<?php endforeach; ?>


									</tbody>
									<tfoot style="display: table-row-group">
										<tr class="font-weight-bold">
											<td colspan="3"></td>
											<td colspan="1"><strong>Grand Total(रू)</strong></td>
											<td class="bg-warning text-right">
												<span class="double-underline" id="dtopeningdr_total">
													
												</span>
											</td>
											<td class="bg-warning text-right">
												<span class="double-underline" id="dtopeningcr_total">
													
												</span>
											</td>
											<td class="bg-success text-right">
												<span class="double-underline" id="dttrnxdr_total">
												</span>
											</td>
											<td class="bg-success text-right">
												<span class="double-underline" id="dttrnxcr_total">
												</span>
											</td>
											<td class="badge-secondary text-right">
												<span class="double-underline" id="dtclosingdr_total">
												</span>
											</td>
											<td class="badge-secondary text-right">
												<span class="double-underline" id="dtclosingcr_total">
												</span>
											</td>
										</tr>
									</tfoot>
								</table>
							<?php endif; ?>
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</section>
</div>


<script type="text/javascript">
	smopeningdr_sum();
	smopeningcr_sum();
	smtrnxdr_sum();
	smtrnxcr_sum();
	smclosingcr_sum();
	smclosingdr_sum();

	dtopeningdr_sum();
	dtopeningcr_sum();
	dttrnxdr_sum();
	dttrnxcr_sum();
	dtclosingcr_sum();
	dtclosingdr_sum();

	function smopeningdr_sum(){
	  var sum = 0;
	  $(".smopeningdr").each(function(){
	    sum += parseFloat($(this).text());
	  });
	  $('#smopeningdr_total').text(sum);
	}

	function smopeningcr_sum(){
	  var sum = 0;
	  $(".smopeningcr").each(function(){
	    sum += parseFloat($(this).text());
	  });
	  $('#smopeningcr_total').text(sum);
	}

	function smtrnxdr_sum(){
	  var sum = 0;
	  $(".smtrnxdr").each(function(){
	    sum += parseFloat($(this).text());
	  });
	  $('#smtrnxdr_total').text(sum);
	}

	function smtrnxcr_sum(){
	  var sum = 0;
	  $(".smtrnxcr").each(function(){
	    sum += parseFloat($(this).text());
	  });
	  $('#smtrnxcr_total').text(sum);
	}

	function smclosingcr_sum(){
	  var sum = 0;
	  $(".smclosingcr").each(function(){
	    sum += parseFloat($(this).text());
	  });
	  $('#smclosingcr_total').text(sum);
	}
	function smclosingdr_sum(){
	  var sum = 0;
	  $(".smclosingdr").each(function(){
	    sum += parseFloat($(this).text());
	  });
	  $('#smclosingdr_total').text(sum);
	}






	function dtopeningdr_sum(){
	  var sum = 0;
	  $(".dtopeningdr").each(function(){
	    sum += parseFloat($(this).text());
	  });
	  $('#dtopeningdr_total').text(sum);
	}

	function dtopeningcr_sum(){
	  var sum = 0;
	  $(".dtopeningcr").each(function(){
	    sum += parseFloat($(this).text());
	  });
	  $('#dtopeningcr_total').text(sum);
	}

	function dttrnxdr_sum(){
	  var sum = 0;
	  $(".dttrnxdr").each(function(){
	    sum += parseFloat($(this).text());
	  });
	  $('#dttrnxdr_total').text(sum);
	}

	function dttrnxcr_sum(){
	  var sum = 0;
	  $(".dttrnxcr").each(function(){
	    sum += parseFloat($(this).text());
	  });
	  $('#dttrnxcr_total').text(sum);
	}

	function dtclosingcr_sum(){
	  var sum = 0;
	  $(".dtclosingcr").each(function(){
	    sum += parseFloat($(this).text());
	  });
	  $('#dtclosingcr_total').text(sum);
	}
	function dtclosingdr_sum(){
	  var sum = 0;
	  $(".dtclosingdr").each(function(){
	    sum += parseFloat($(this).text());
	  });
	  $('#dtclosingdr_total').text(sum);
	}


	$(".detail").hide();
	$(".summary_check").change(function() {
	    if(this.checked) {
	        $(".detail").show();
	        $(".summary").hide();
	    }
	    else{
	    	$(".summary").show();
	        $(".detail").hide();
	    }
	});


</script>