<style>
	.badge-secondary {
	    color: #fff;
	    background-color: rgba(0, 0, 0, 0.33);
	}
	.badge-warning {
	    color: #fff;
	    background-color: #e5ae67;
	}

</style>

<div class="content-wrapper" style="min-height: 946px;">
	<section class="content-header">
		<h1>
			Journal
		</h1>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title"><i class="fa fa-plus-circle"></i> Day Sheet </h3>
					</div>
					<div class="box-body">
						<div class="form-group row">
							<div class="col-md-12">
								<div class="col-md-3">
									<label>Date</label>
									<input type="text" autocomplete="off" name="FromDate" class="form-control  date"  value="<?php  echo set_value('FromDate') ?>">
								</div>
								<div class="col-md-2">
									<button class="btn btn-danger" type="button" id="btnSearch">
										<i class="fa fa-search" aria-hidden="true"></i> View
									</button>
								</div>
							</div>
						</div>



						<div id="SearchResult">
							<div class="table-responsive">
								<table class="table table-bordered  table-striped table-danger table-hover js-dataTable-buttons" width="100%">
									<thead>
										<tr class="text-nowrap">
											<th>
												S.N
											</th>
											<th>
												GLSubsidiary
											</th>
											<th>
												GLCode
											</th>
											<th class="bg-success">
												Closing Date
											</th>
											<th class="bg-success">
												Closing Balance
											</th>
											<th class="badge-secondary">
												Opening Date
											</th>
											<th class="badge-secondary">
												Opening Balance
											</th>
											<th class="badge-warning">
												Difference Balance
											</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>1</td>
											<td>Promoter Share</td>
											<td>
												001000211111
											</td>
											<td class="bg-success">
												2078-04-01
											</td>
											<td class="bg-success text-right">
												<strong>
													50,000.00
												</strong>
											</td>
											<td class="badge-secondary text-right">
												2078-04-02
											</td>
											<td class="badge-secondary text-right">
												<strong>
													50,000.00
												</strong>
											</td>
											<td class="badge-warning text-right">
												<strong>
													0.00
												</strong>
											</td>
										</tr>
										<tr>
											<td>2</td>
											<td>COOPA Cash In Hand</td>
											<td>
												001000121112
											</td>
											<td class="bg-success">
												2078-04-01
											</td>
											<td class="bg-success text-right">
												<strong>
													10,000.00
												</strong>
											</td>
											<td class="badge-secondary text-right">
												2078-04-02
											</td>
											<td class="badge-secondary text-right">
												<strong>
													10,000.00
												</strong>
											</td>
											<td class="badge-warning text-right">
												<strong>
													0.00
												</strong>
											</td>
										</tr>
										<tr>
											<td>3</td>
											<td>Cash on Vault</td>
											<td>
												001000121111
											</td>
											<td class="bg-success">
												2078-04-01
											</td>
											<td class="bg-success text-right">
												<strong>
													57,000.00
												</strong>
											</td>
											<td class="badge-secondary text-right">
												2078-04-02
											</td>
											<td class="badge-secondary text-right">
												<strong>
													57,000.00
												</strong>
											</td>
											<td class="badge-warning text-right">
												<strong>
													0.00
												</strong>
											</td>
										</tr>
										<tr>
											<td>4</td>
											<td>Petrol</td>
											<td>
												001000131211
											</td>
											<td class="bg-success">
												2078-04-01
											</td>
											<td class="bg-success text-right">
												<strong>
													3,000.00
												</strong>
											</td>
											<td class="badge-secondary text-right">
												2078-04-02
											</td>
											<td class="badge-secondary text-right">
												<strong>
													3,000.00
												</strong>
											</td>
											<td class="badge-warning text-right">
												<strong>
													0.00
												</strong>
											</td>
										</tr>

									</tbody>
								</table>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>