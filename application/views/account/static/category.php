<div class="content-wrapper" style="min-height: 946px;">
	<section class="content-header">
		<h1>

		</h1>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title"></h3>
						<small class="pull-right">
							<a href="<?php echo base_url(); ?>account/view/createcategory" class="btn btn-primary btn-sm">
								<i class="fa fa-plus"></i> Add Category </a>
							</small>
						</div>
						<div class="box-body">
							<div class="table-responsive">
								<table class="table table-striped table-bordered table-hover example dataTable" cellspacing="0" width="100%" id="DataTables_Table_0">
									<thead>
										<tr>
											<th>S.No</th>
											<th>Category</th>
											<th>Parent</th>
											<th class="pull-right"></th>
											<th class="text-right">Action</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>
												1  
											</td>
											<td>
												Assests
											</td>
											<td>- - - -</td>
											<td class="pull-right"></td>
											<td class="mailbox-date pull-right">
												<a href="<?php echo base_url(); ?>account/view/editcategory" class="btn btn-default btn-xs" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>
											</td>
										</tr>

										<tr>
											<td>
												2                                    
											</td>
											<td>
												Liabilities 
											</td>
											<td>- - - -</td>
											<td class="pull-right"></td>
											<td class="mailbox-date pull-right">
												<a href="<?php echo base_url(); ?>account/view/editcategory" class="btn btn-default btn-xs" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>
											</td>
										</tr>

										<tr>
											<td>
												3  
											</td>
											<td>
												Incomes 
											</td>
											<td>- - - -</td>
											<td class="pull-right"></td>
											<td class="mailbox-date pull-right">
												<a href="<?php echo base_url(); ?>account/view/editcategory" class="btn btn-default btn-xs" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>
											</td>
										</tr>

										<tr>
											<td>
												4                                    
											</td>
											<td>
												Expenses
											</td>
											<td>- - - -</td>
											<td class="pull-right"></td>
											<td class="mailbox-date pull-right">
												<a href="<?php echo base_url(); ?>account/view/editcategory" class="btn btn-default btn-xs" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>
											</td>
										</tr>

										<tr>
											<td>
												5     
											</td>
											<td>-  - Cash, Banks and E-wallets</td>
											<td>Current Assets</td>
											<td class="pull-right"></td>
											<td class="mailbox-date pull-right">
												<a href="<?php echo base_url(); ?>account/view/editcategory" class="btn btn-default btn-xs" data-toggle="tooltip" title="Edit">
													<i class="fa fa-pencil"></i>
												</a>
												<a href="" class="btn btn-default btn-xs" data-toggle="tooltip" title="Delete" onclick="return confirm('All Associated Categories Will Be Deleted. Delete Confirm?');"><i class="fa fa-remove"></i></a>
											</td>
										</tr>

										<tr>
											<td>
												6    
											</td>
											<td>- - - Cash</td>
											<td>Cash, Banks and E-wallets</td>
											<td class="pull-right"></td>
											<td class="mailbox-date pull-right">
												<a href="<?php echo base_url(); ?>account/view/editcategory" class="btn btn-default btn-xs" data-toggle="tooltip" title="Edit">
													<i class="fa fa-pencil"></i>
												</a>
												<a href="" class="btn btn-default btn-xs" data-toggle="tooltip" title="Delete" onclick="return confirm('All Associated Categories Will Be Deleted. Delete Confirm?');"><i class="fa fa-remove"></i></a>
											</td>
										</tr>

										<tr>
											<td>
												7   
											</td>
											<td>- - - Bank</td>
											<td>Cash, Banks and E-wallets</td>
											<td class="pull-right"></td>
											<td class="mailbox-date pull-right">
												<a href="<?php echo base_url(); ?>account/view/editcategory" class="btn btn-default btn-xs" data-toggle="tooltip" title="Edit">
													<i class="fa fa-pencil"></i>
												</a>
												<a href="" class="btn btn-default btn-xs" data-toggle="tooltip" title="Delete" onclick="return confirm('All Associated Categories Will Be Deleted. Delete Confirm?');"><i class="fa fa-remove"></i></a>
											</td>
										</tr>

										<tr>
											<td>
												8     
											</td>
											<td>- - - Cheque</td>
											<td>Cash, Banks and E-wallets</td>
											<td class="pull-right"></td>
											<td class="mailbox-date pull-right">
												<a href="<?php echo base_url(); ?>account/view/editcategory" class="btn btn-default btn-xs" data-toggle="tooltip" title="Edit">
													<i class="fa fa-pencil"></i>
												</a>
												<a href="" class="btn btn-default btn-xs" data-toggle="tooltip" title="Delete" onclick="return confirm('All Associated Categories Will Be Deleted. Delete Confirm?');"><i class="fa fa-remove"></i></a>
											</td>
										</tr>

										<tr>
											<td>
												9     
											</td>
											<td>- - TDS Receivable</td>
											<td>Current Assets</td>
											<td class="pull-right"></td>
											<td class="mailbox-date pull-right">
												<a href="<?php echo base_url(); ?>account/view/editcategory" class="btn btn-default btn-xs" data-toggle="tooltip" title="Edit">
													<i class="fa fa-pencil"></i>
												</a>
												<a href="" class="btn btn-default btn-xs" data-toggle="tooltip" title="Delete" onclick="return confirm('All Associated Categories Will Be Deleted. Delete Confirm?');"><i class="fa fa-remove"></i></a>
											</td>
										</tr>
										<tr>
											<td>
												10     
											</td>
											<td>- Fixed Assets</td>
											<td>Assets</td>
											<td class="pull-right"></td>
											<td class="mailbox-date pull-right">
												<a href="<?php echo base_url(); ?>account/view/editcategory" class="btn btn-default btn-xs" data-toggle="tooltip" title="Edit">
													<i class="fa fa-pencil"></i>
												</a>
												<a href="" class="btn btn-default btn-xs" data-toggle="tooltip" title="Delete" onclick="return confirm('All Associated Categories Will Be Deleted. Delete Confirm?');"><i class="fa fa-remove"></i></a>
											</td>
										</tr>
										<tr>
											<td>
												11     
											</td>
											<td>- - Plant and Machinery</td>
											<td>Fixed Assets</td>
											<td class="pull-right"></td>
											<td class="mailbox-date pull-right">
												<a href="<?php echo base_url(); ?>account/view/editcategory" class="btn btn-default btn-xs" data-toggle="tooltip" title="Edit">
													<i class="fa fa-pencil"></i>
												</a>
												<a href="" class="btn btn-default btn-xs" data-toggle="tooltip" title="Delete" onclick="return confirm('All Associated Categories Will Be Deleted. Delete Confirm?');"><i class="fa fa-remove"></i></a>
											</td>
										</tr>
										<tr>
											<td>
												12    
											</td>
											<td>- - Depreciation</td>
											<td>Fixed Assets</td>
											<td class="pull-right"></td>
											<td class="mailbox-date pull-right">
												<a href="<?php echo base_url(); ?>account/view/editcategory" class="btn btn-default btn-xs" data-toggle="tooltip" title="Edit">
													<i class="fa fa-pencil"></i>
												</a>
												<a href="" class="btn btn-default btn-xs" data-toggle="tooltip" title="Delete" onclick="return confirm('All Associated Categories Will Be Deleted. Delete Confirm?');"><i class="fa fa-remove"></i></a>
											</td>
										</tr>
										<tr>
											<td>
												13    
											</td>
											<td>- - - Computers and Printers</td>
											<td>Computers and Printers</td>
											<td class="pull-right"></td>
											<td class="mailbox-date pull-right">
												<a href="<?php echo base_url(); ?>account/view/editcategory" class="btn btn-default btn-xs" data-toggle="tooltip" title="Edit">
													<i class="fa fa-pencil"></i>
												</a>
												<a href="" class="btn btn-default btn-xs" data-toggle="tooltip" title="Delete" onclick="return confirm('All Associated Categories Will Be Deleted. Delete Confirm?');"><i class="fa fa-remove"></i></a>
											</td>
										</tr>
										<tr>
											<td>
												14   
											</td>
											<td>- - Musical Instruments</td>
											<td>Fixed Assets</td>
											<td class="pull-right"></td>
											<td class="mailbox-date pull-right">
												<a href="<?php echo base_url(); ?>account/view/editcategory" class="btn btn-default btn-xs" data-toggle="tooltip" title="Edit">
													<i class="fa fa-pencil"></i>
												</a>
												<a href="" class="btn btn-default btn-xs" data-toggle="tooltip" title="Delete" onclick="return confirm('All Associated Categories Will Be Deleted. Delete Confirm?');"><i class="fa fa-remove"></i></a>
											</td>
										</tr>
										<tr>
											<td>
												15    
											</td>
											<td>- - - Musical Instruments</td>
											<td>Musical Instruments</td>
											<td class="pull-right"></td>
											<td class="mailbox-date pull-right">
												<a href="<?php echo base_url(); ?>account/view/editcategory" class="btn btn-default btn-xs" data-toggle="tooltip" title="Edit">
													<i class="fa fa-pencil"></i>
												</a>
												<a href="" class="btn btn-default btn-xs" data-toggle="tooltip" title="Delete" onclick="return confirm('All Associated Categories Will Be Deleted. Delete Confirm?');"><i class="fa fa-remove"></i></a>
											</td>
										</tr>
										<tr>
											<td>
												16    
											</td>
											<td>- - Science lab equipment</td>
											<td>Fixed Assets</td>
											<td class="pull-right"></td>
											<td class="mailbox-date pull-right">
												<a href="<?php echo base_url(); ?>account/view/editcategory" class="btn btn-default btn-xs" data-toggle="tooltip" title="Edit">
													<i class="fa fa-pencil"></i>
												</a>
												<a href="" class="btn btn-default btn-xs" data-toggle="tooltip" title="Delete" onclick="return confirm('All Associated Categories Will Be Deleted. Delete Confirm?');"><i class="fa fa-remove"></i></a>
											</td>
										</tr>
										<tr>
											<td>
												17     
											</td>
											<td>- - Vehicle</td>
											<td>Fixed Assets</td>
											<td class="pull-right"></td>
											<td class="mailbox-date pull-right">
												<a href="<?php echo base_url(); ?>account/view/editcategory" class="btn btn-default btn-xs" data-toggle="tooltip" title="Edit">
													<i class="fa fa-pencil"></i>
												</a>
												<a href="" class="btn btn-default btn-xs" data-toggle="tooltip" title="Delete" onclick="return confirm('All Associated Categories Will Be Deleted. Delete Confirm?');"><i class="fa fa-remove"></i></a>
											</td>
										</tr>
										<tr>
											<td>
												18     
											</td>
											<td>- Current Liabilities</td>
											<td>	Liabilities</td>
											<td class="pull-right"></td>
											<td class="mailbox-date pull-right">
												<a href="<?php echo base_url(); ?>account/view/editcategory" class="btn btn-default btn-xs" data-toggle="tooltip" title="Edit">
													<i class="fa fa-pencil"></i>
												</a>
												<a href="" class="btn btn-default btn-xs" data-toggle="tooltip" title="Delete" onclick="return confirm('All Associated Categories Will Be Deleted. Delete Confirm?');"><i class="fa fa-remove"></i></a>
											</td>
										</tr>
										<tr>
											<td>
												19    
											</td>
											<td>- - Audit Fee Tax</td>
											<td>Current Liabilities</td>
											<td class="pull-right"></td>
											<td class="mailbox-date pull-right">
												<a href="<?php echo base_url(); ?>account/view/editcategory" class="btn btn-default btn-xs" data-toggle="tooltip" title="Edit">
													<i class="fa fa-pencil"></i>
												</a>
												<a href="" class="btn btn-default btn-xs" data-toggle="tooltip" title="Delete" onclick="return confirm('All Associated Categories Will Be Deleted. Delete Confirm?');"><i class="fa fa-remove"></i></a>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>