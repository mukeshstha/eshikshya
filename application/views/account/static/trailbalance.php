<style>
	.switch {
		position: relative;
		display: inline-block;
		width: 60px;
		height: 34px;
	}

	.switch input { 
		opacity: 0;
		width: 0;
		height: 0;
	}

	.slider {
		position: absolute;
		cursor: pointer;
		top: 0;
		left: 0;
		right: 0;
		bottom: 0;
		background-color: #ccc;
		-webkit-transition: .4s;
		transition: .4s;
	}

	.slider:before {
		position: absolute;
		content: "";
		height: 26px;
		width: 26px;
		left: 4px;
		bottom: 4px;
		background-color: white;
		-webkit-transition: .4s;
		transition: .4s;
	}

	input:checked + .slider {
		background-color: #2196F3;
	}

	input:focus + .slider {
		box-shadow: 0 0 1px #2196F3;
	}

	input:checked + .slider:before {
		-webkit-transform: translateX(26px);
		-ms-transform: translateX(26px);
		transform: translateX(26px);
	}

	/* Rounded sliders */
	.slider.round {
		border-radius: 34px;
	}

	.slider.round:before {
		border-radius: 50%;
	}
	.double-underline {
		border-top: 1px solid;
		border-bottom: 3px double;
		padding: 4px;
	}
	.badge-secondary {
	    color: #fff;
	    background-color: rgba(0, 0, 0, 0.33);
	}
</style>


<div class="content-wrapper" style="min-height: 946px;">
	<section class="content-header">
		<h1>
			Trail Balance
		</h1>
	</section>
	<section class="content">

		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title"><i class="fa fa-plus-circle"></i> Trail Balance </h3>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="alert alert-secondary">
								<div class="col-md-3">
									<label>From Date</label>
									<input type="text" autocomplete="off" name="FromDate" class="form-control  date"  value="<?php  echo set_value('FromDate') ?>">
								</div>
								<div class="col-md-3">
									<label>To Date</label>
									<input type="text" autocomplete="off" name="FromDate" class="form-control  date"  value="<?php  echo set_value('FromDate') ?>">
								</div>

								<div class="col-md-3">
									<label>Summary &nbsp;</label>
									<label class="switch">
										<input type="checkbox" checked>
										<span class="slider round"></span>
									</label>
								</div>
								<div class="col-md-3">
									<button class="btn btn-primary btn-sm pull-right checkbox-toggle">View</button>
								</div>
							</div>
						</div>
					</div>



					<div id="SearchResult" style="margin-top: 25px">
						<div class="table-responsive">
							<table class="table table-bordered  table-striped table-danger table-hover js-dataTable-buttons" width="100%">
								<thead>
									<tr>
										<th colspan="4"></th>
										<th colspan="2" class="bg-warning">
											Opening Balance
										</th>
										<th colspan="2" class="bg-success">
											Transaction Balance
										</th>
										<th colspan="2" class="badge-secondary">
											Closing Balance
										</th>
									</tr>
									<tr class="text-nowrap">
										<th>
											GLHead
										</th>
										<th>Particulars</th>
										<th>GL Code</th>
										<th>
											Ledger Name
										</th>
										<th class="bg-warning">
											Debit(रू)
										</th>
										<th class="bg-warning">
											Credit(रू)
										</th>
										<th class="bg-success">
											Debit(रू)
										</th>
										<th class="bg-success">
											Credit(रू)
										</th>
										<th class="badge-secondary">
											Debit(रू)
										</th>
										<th class="badge-secondary">
											Credit(रू)
										</th>
									</tr>
								</thead>
								<tbody>
									<tr class="font-weight-bold">
										<td colspan="4">Liabilities</td>
										<td class="bg-warning text-right">
											0.00
										</td>
										<td class="bg-warning text-right">
											70,000.00
										</td>
										<td class="bg-success text-right">
											40,000.00
										</td>
										<td class="bg-success text-right">
											40,000.00
										</td>
										<td class="badge-secondary text-right">
											0.00
										</td>
										<td class="badge-secondary text-right">
											70,000.00
										</td>
									</tr>
									<tr class="font-weight-bold">
										<td></td>
										<td colspan="3">Promoter Share</td>
										<td class="bg-warning text-right">
											0.00
										</td>
										<td class="bg-warning text-right">
											50,000.00
										</td>
										<td class="bg-success text-right">
											0.00
										</td>
										<td class="bg-success text-right">
											10,000.00
										</td>
										<td class="badge-secondary text-right">
											0.00
										</td>
										<td class="badge-secondary text-right">
											60,000.00
										</td>
									</tr>
									<tr>
										<td></td>
										<td></td>
										<td>

											<a href="" target="_blank">001000211111</a>
										</td>

										<td>Promoter Share</td>
										<td class="bg-warning text-right">0.00</td>
										<td class="bg-warning text-right">50,000.00</td>
										<td class="bg-success text-right">0.00</td>
										<td class="bg-success text-right">10,000.00</td>
										<td class="badge-secondary text-right">0.00</td>
										<td class="badge-secondary text-right">60,000.00</td>
									</tr>
									<tr class="font-weight-bold">
										<td></td>
										<td colspan="3">Reserve Funds</td>
										<td class="bg-warning text-right">
											0.00
										</td>
										<td class="bg-warning text-right">
											0.00
										</td>
										<td class="bg-success text-right">
											20,000.00
										</td>
										<td class="bg-success text-right">
											20,000.00
										</td>
										<td class="badge-secondary text-right">
											0.00
										</td>
										<td class="badge-secondary text-right">
											0.00
										</td>
									</tr>
									<tr>
										<td></td>
										<td></td>
										<td>

											<a href="" target="_blank">001000111211</a>
										</td>

										<td>Share Dividend Fund</td>
										<td class="bg-warning text-right">0.00</td>
										<td class="bg-warning text-right">0.00</td>
										<td class="bg-success text-right">20,000.00</td>
										<td class="bg-success text-right">20,000.00</td>
										<td class="badge-secondary text-right">0.00</td>
										<td class="badge-secondary text-right">0.00</td>
									</tr>
									<tr class="font-weight-bold">
										<td></td>
										<td colspan="3">Others Payable</td>
										<td class="bg-warning text-right">
											0.00
										</td>
										<td class="bg-warning text-right">
											0.00
										</td>
										<td class="bg-success text-right">
											0.00
										</td>
										<td class="bg-success text-right">
											10,000.00
										</td>
										<td class="badge-secondary text-right">
											0.00
										</td>
										<td class="badge-secondary text-right">
											10,000.00
										</td>
									</tr>
									<tr>
										<td></td>
										<td></td>
										<td>

											<a href="" target="_blank">001111611114</a>
										</td>

										<td>ShareCashDividendPayable</td>
										<td class="bg-warning text-right">0.00</td>
										<td class="bg-warning text-right">0.00</td>
										<td class="bg-success text-right">0.00</td>
										<td class="bg-success text-right">10,000.00</td>
										<td class="badge-secondary text-right">0.00</td>
										<td class="badge-secondary text-right">10,000.00</td>
									</tr>
									<tr class="font-weight-bold">
										<td></td>
										<td colspan="3">Other Liabilities</td>
										<td class="bg-warning text-right">
											0.00
										</td>
										<td class="bg-warning text-right">
											20,000.00
										</td>
										<td class="bg-success text-right">
											20,000.00
										</td>
										<td class="bg-success text-right">
											0.00
										</td>
										<td class="badge-secondary text-right">
											0.00
										</td>
										<td class="badge-secondary text-right">
											0.00
										</td>
									</tr>
									<tr>
										<td></td>
										<td></td>
										<td>

											<a href="" target="_blank">001011161215</a>
										</td>

										<td>Surplus</td>
										<td class="bg-warning text-right">0.00</td>
										<td class="bg-warning text-right">20,000.00</td>
										<td class="bg-success text-right">20,000.00</td>
										<td class="bg-success text-right">0.00</td>
										<td class="badge-secondary text-right">0.00</td>
										<td class="badge-secondary text-right">0.00</td>
									</tr>
									<tr class="font-weight-bold">
										<td colspan="4">Assets</td>
										<td class="bg-warning text-right">
											70,000.00
										</td>
										<td class="bg-warning text-right">
											0.00
										</td>
										<td class="bg-success text-right">
											200.00
										</td>
										<td class="bg-success text-right">
											3,000.00
										</td>
										<td class="badge-secondary text-right">
											67,200.00
										</td>
										<td class="badge-secondary text-right">
											0.00
										</td>
									</tr>
									<tr class="font-weight-bold">
										<td></td>
										<td colspan="3">Cash on Vault</td>
										<td class="bg-warning text-right">
											60,000.00
										</td>
										<td class="bg-warning text-right">
											0.00
										</td>
										<td class="bg-success text-right">
											0.00
										</td>
										<td class="bg-success text-right">
											3,000.00
										</td>
										<td class="badge-secondary text-right">
											57,000.00
										</td>
										<td class="badge-secondary text-right">
											0.00
										</td>
									</tr>
									<tr>
										<td></td>
										<td></td>
										<td>

											<a href="" target="_blank">001000121111</a>
										</td>

										<td>Cash on Vault</td>
										<td class="bg-warning text-right">60,000.00</td>
										<td class="bg-warning text-right">0.00</td>
										<td class="bg-success text-right">0.00</td>
										<td class="bg-success text-right">3,000.00</td>
										<td class="badge-secondary text-right">57,000.00</td>
										<td class="badge-secondary text-right">0.00</td>
									</tr>
									<tr class="font-weight-bold">
										<td></td>
										<td colspan="3">Petty Cash</td>
										<td class="bg-warning text-right">
											10,000.00
										</td>
										<td class="bg-warning text-right">
											0.00
										</td>
										<td class="bg-success text-right">
											0.00
										</td>
										<td class="bg-success text-right">
											0.00
										</td>
										<td class="badge-secondary text-right">
											10,000.00
										</td>
										<td class="badge-secondary text-right">
											0.00
										</td>
									</tr>
									<tr>
										<td></td>
										<td></td>
										<td>

											<a href="" target="_blank">001000121112</a>
										</td>

										<td>COOPA Cash In Hand</td>
										<td class="bg-warning text-right">10,000.00</td>
										<td class="bg-warning text-right">0.00</td>
										<td class="bg-success text-right">0.00</td>
										<td class="bg-success text-right">0.00</td>
										<td class="badge-secondary text-right">10,000.00</td>
										<td class="badge-secondary text-right">0.00</td>
									</tr>
									<tr class="font-weight-bold">
										<td></td>
										<td colspan="3">Current Account</td>
										<td class="bg-warning text-right">
											0.00
										</td>
										<td class="bg-warning text-right">
											0.00
										</td>
										<td class="bg-success text-right">
											200.00
										</td>
										<td class="bg-success text-right">
											0.00
										</td>
										<td class="badge-secondary text-right">
											200.00
										</td>
										<td class="badge-secondary text-right">
											0.00
										</td>
									</tr>
									<tr>
										<td></td>
										<td></td>
										<td>

											<a href="" target="_blank">001000321211</a>
										</td>

										<td>Nabil Bank</td>
										<td class="bg-warning text-right">0.00</td>
										<td class="bg-warning text-right">0.00</td>
										<td class="bg-success text-right">200.00</td>
										<td class="bg-success text-right">0.00</td>
										<td class="badge-secondary text-right">200.00</td>
										<td class="badge-secondary text-right">0.00</td>
									</tr>
									<tr class="font-weight-bold">
										<td colspan="4">Expenditure</td>
										<td class="bg-warning text-right">
											0.00
										</td>
										<td class="bg-warning text-right">
											0.00
										</td>
										<td class="bg-success text-right">
											3,000.00
										</td>
										<td class="bg-success text-right">
											0.00
										</td>
										<td class="badge-secondary text-right">
											3,000.00
										</td>
										<td class="badge-secondary text-right">
											0.00
										</td>
									</tr>
									<tr class="font-weight-bold">
										<td></td>
										<td colspan="3">Transportation</td>
										<td class="bg-warning text-right">
											0.00
										</td>
										<td class="bg-warning text-right">
											0.00
										</td>
										<td class="bg-success text-right">
											3,000.00
										</td>
										<td class="bg-success text-right">
											0.00
										</td>
										<td class="badge-secondary text-right">
											3,000.00
										</td>
										<td class="badge-secondary text-right">
											0.00
										</td>
									</tr>
									<tr>
										<td></td>
										<td></td>
										<td>

											<a href="" target="_blank">001000131211</a>
										</td>

										<td>Petrol</td>
										<td class="bg-warning text-right">0.00</td>
										<td class="bg-warning text-right">0.00</td>
										<td class="bg-success text-right">3,000.00</td>
										<td class="bg-success text-right">0.00</td>
										<td class="badge-secondary text-right">3,000.00</td>
										<td class="badge-secondary text-right">0.00</td>
									</tr>
									<tr class="font-weight-bold">
										<td colspan="4">Income</td>
										<td class="bg-warning text-right">
											0.00
										</td>
										<td class="bg-warning text-right">
											0.00
										</td>
										<td class="bg-success text-right">
											0.00
										</td>
										<td class="bg-success text-right">
											200.00
										</td>
										<td class="badge-secondary text-right">
											0.00
										</td>
										<td class="badge-secondary text-right">
											200.00
										</td>
									</tr>
									<tr class="font-weight-bold">
										<td></td>
										<td colspan="3">Entry Fee</td>
										<td class="bg-warning text-right">
											0.00
										</td>
										<td class="bg-warning text-right">
											0.00
										</td>
										<td class="bg-success text-right">
											0.00
										</td>
										<td class="bg-success text-right">
											200.00
										</td>
										<td class="badge-secondary text-right">
											0.00
										</td>
										<td class="badge-secondary text-right">
											200.00
										</td>
									</tr>
									<tr>
										<td></td>
										<td></td>
										<td>

											<a href="" target="_blank">001000141611</a>
										</td>

										<td>Form Registration</td>
										<td class="bg-warning text-right">0.00</td>
										<td class="bg-warning text-right">0.00</td>
										<td class="bg-success text-right">0.00</td>
										<td class="bg-success text-right">200.00</td>
										<td class="badge-secondary text-right">0.00</td>
										<td class="badge-secondary text-right">200.00</td>
									</tr>

								</tbody>
								<tfoot style="display: table-row-group">
									<tr class="font-weight-bold">
										<td colspan="3"></td>
										<td colspan="1"><strong>Grand Total(रू)</strong></td>
										<td class="bg-warning text-right">
											<span class="double-underline">
												70,000.00
											</span>
										</td>
										<td class="bg-warning text-right">
											<span class="double-underline">
												70,000.00
											</span>
										</td>
										<td class="bg-success text-right">
											<span class="double-underline">
												43,200.00
											</span>
										</td>
										<td class="bg-success text-right">
											<span class="double-underline">
												43,200.00
											</span>
										</td>
										<td class="badge-secondary text-right">
											<span class="double-underline">
												70,200.00
											</span>
										</td>
										<td class="badge-secondary text-right">
											<span class="double-underline">
												70,200.00
											</span>
										</td>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>