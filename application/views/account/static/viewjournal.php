<div class="content-wrapper" style="min-height: 946px;">
	<section class="content-header">
		<h1>
			Journal
		</h1>
	</section>
	<section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            <i class="fa fa-plus-circle"></i> Journal                        </h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-4 form-group">
                                <label>Journal Number</label>
                                <div>JOR12</div>
                            </div>
                            <div class="col-md-4 form-group">
                                <label>Entry Date</label>
                                <div>
                                    2079-01-25                                </div>
                            </div>
                            <div class="col-md-4 form-group">
                                <label>Reference No</label>
                                <div>-</div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <table id="journal_entry_table" class="table invoice-table" cellspacing="0" width="100%">
                                    <thead>
	                                    <tr>
	                                        <th width="35%">Account Name</th>
	                                        <th width="25%">Category</th>
	                                        <th width="20%">Debit Amount</th>
	                                        <th width="20%">Credit Amount</th>
	                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr data-id="24" class="editable-row">
                                            <td width="35%">Cleaning expense (0001)</td>
                                            <td width="25%">Cleaning expense</td>
                                            <td width="20%">NPR. 5,000.00</td>
                                            <td width="20%"></td>
                                        </tr>
                                        <tr data-id="25" class="editable-row">
                                            <td width="35%">Cash in hand (CASHINHAND)</td>
                                            <td width="25%">Cash</td>
                                            <td width="20%"></td>
                                            <td width="20%">NPR. 5,000.00</td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
	                                    <tr>
	                                        <td width="35%">Total</td>
	                                        <td width="25%"></td>
	                                        <td width="20%"><span class="footer-debit-sum">NPR. 5,000.00</span></td>
	                                        <td width="20%"><span class="footer-credit-sum">NPR. 5,000.00</span></td>
	                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label>Narration</label>
                                <div>test</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>