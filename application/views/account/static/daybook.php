<div class="content-wrapper" style="min-height: 946px;">
	<section class="content-header">
		<h1>
			Journal
		</h1>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title"><i class="fa fa-plus-circle"></i> Add Journal </h3>
					</div>
					<div class="box-body">

						<div class="alert alert-secondary">
							<div class="form-group form-row">
								<div class="col-md-2">
									<label>From Date</label>
									<input type="text" autocomplete="off" name="FromDate" class="form-control  date"  value="<?php  echo set_value('FromDate') ?>">
								</div>
								<div class="col-md-2">
									<label>To Date</label>
									<input type="text" autocomplete="off" name="FromDate" class="form-control  date"  value="<?php  echo set_value('FromDate') ?>">
								</div>
								<div class="col-md-2 text-center">
									<input type="checkbox" id="cashflag" checked="">
									<label>
										Cash
									</label>&nbsp;
									<input type="checkbox" id="bankflag">
									<label>Bank</label>
								</div>
								<div class="col-md-2">
									<label>Cash Ledger</label>
									<select class="form-control">
										<option value="">All</option>
										<option>Cash on Vault</option>
										<option>COOPA Cash In Hand</option>
										<option>BankingFinance</option>
										<option>BankingFinance</option>
										<option>BankingFinance</option>
										<option>testsubsidairy</option>
									</select>
								</div>
								<div class="col-md-2">
									<label>Bank Ledger</label>
									<select class="form-control">
										<option>All</option>
										<option>Lokendra Rijal [Bank]</option>
										<option>Civil Bank [Bank]</option>
										<option>Nabil Bank [Bank]</option>
										<option>Prabhu Bank [Bank]</option>
										<option>test bank [Bank]</option>
									</select>
								</div>
								<div class="col-md-2">
									<button class="btn btn-danger" type="button" id="btnSearch">
										<i class="fa fa-search" aria-hidden="true"></i> View
									</button>
								</div>
							</div>
							
						</div>


						<div id="SearchResult">
							<table class="table table-bordered table-condensed" style="border-collapse:collapse;">
								<thead class="header-a bg-danger">
									<tr>
										<th class="text-center">
											Date
										</th>
										<th class="text-center">
											VoucherNo
										</th>
										<th class="text-center">
											Code
										</th>
										<th class="text-center">
											Ledger Name
										</th>
										<th class="text-center">
											Narration
										</th>
										<th class="text-center">
											Debit(रू)
										</th>
										<th class="text-center">
											Credit(रू)
										</th>
										<th hidden="">
											OrderSeq
										</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>2078-02-01</td>
										<td>
										</td>
										<td></td>
										<td>Cash on Vault [001000121111]</td>
										<td>Opening Balance</td>
										<td class="text-right">0.00</td>
										<td class="text-right">0.00</td>
										<td hidden="">0</td>
									</tr>
									<tr>
										<td>2078-02-01</td>
										<td>
										</td>
										<td></td>
										<td>Lokendra Rijal [001000121212]</td>
										<td>Opening Balance</td>
										<td class="text-right">0.00</td>
										<td class="text-right">0.00</td>
										<td hidden="">1</td>
									</tr>
									<tr data-toggle="collapse" data-target="#receipt" class="accordion-toggle">
										<td colspan="7" style="color: blue" ;=""><b><a href="#">Receipt</a></b></td>
									</tr>
									<tr class="accordian-body collapse" id="receipt">
										<td class="hiddenRow">2078-03-29</td>
										<td class="hiddenRow">
											<a href="" target="_blank">
												JV0000109
											</a>
										</td>
										<td class="hiddenRow"> 001000121111</td>
										<td class="hiddenRow">Cash on Vault </td>
										<td class="hiddenRow"> Registration Fee </td>
										<td class="hiddenRow"> 10,000.00 </td>
										<td class="hiddenRow">0.00 </td>
									</tr>
									
									<tr data-toggle="collapse" data-target="#payment" class="accordion-toggle">
										<td colspan="7" style="color: blue" ;=""><b><a href="#">Payment</a></b></td>
									</tr>
									<tr class="accordian-body collapse" id="payment">
										<td class="hiddenRow">2078-03-30</td>
										<td class="hiddenRow">
											<a href="" target="_blank">
												JV0000110
											</a>
										</td>
										<td class="hiddenRow">001000121111 </td>
										<td class="hiddenRow">Cash on Vault </td>
										<td class="hiddenRow"> Expenses Voucher </td>
										<td class="hiddenRow">0.00</td>
										<td class="hiddenRow"> 2,000.00 </td>
									</tr>
									
									<tr data-toggle="collapse" data-target="#bank" class="accordion-toggle">
									</tr>
									<tr>
										<td></td>
										<td>
										</td>
										<td></td>
										<td>Cash on Vault [001000121111]</td>
										<td>Closing Balance</td>
										<td class="text-right"> 57,000.00</td>
										<td class="text-right"> 0.00</td>
										<td hidden="">5</td>
									</tr>
									<tr>
										<td></td>
										<td>
										</td>
										<td></td>
										<td>Lokendra Rijal [001000121212]</td>
										<td>Closing Balance</td>
										<td class="text-right"> 0.00</td>
										<td class="text-right"> 0.00</td>
										<td hidden="">6</td>
									</tr>

								</tbody>
							</table>





						</div>



					</div>

				</div>
			</div>
		</div>
	</section>
</div>