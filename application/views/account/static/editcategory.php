<div class="content-wrapper" style="min-height: 946px;">
    <section class="content-header">
        <h1>

        </h1>
    </section>

    <section class="content">
        <div class="row">
            <?php if($this->session->flashdata('success')) : ?>
                <div class="alert alert-success">
                    <?= $this->session->flashdata('success'); ?>
                </div>
            <?php endif; ?>
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add Category</h3>
                    </div>
                    <div class="box-body">
                        <form action="" method="post" accept-charset="utf-8">
                        <input type="hidden" name="id" value=""/>
                        <div class="form-group">
                            <label>Parent</label> <small class="req">*</small>
                                                        <!--<select autofocus="" id="parent_id" name="parent_id" class="form-control">
                                <option value="0"></option>
                                                                    <option value="" ></option>
                                                                </select>-->
                            <div class="category-accordion">
                                <div id="accordion-0"  role="tablist" aria-multiselectable="true"><div class="panel" data-parent="0">
                    <label>
                        <input type="radio" name="parent_id" checked="checked" id="parent-1" value="1" class="toggle-accordion">
                        <span role="tab" id="heading-1" class="tab">
                            <span class="check-category" role="button" data-toggle="collapse" data-parent="#accordion-0" href="#collapse-1" aria-controls="collapse-1">
                                Assets
                            </span>
                        </span>
                    </label>
                    <div id="collapse-1" class="panel-collapse collapse " role="tabpanel" aria-labelledby="heading-1"><div id="accordion-1" style="margin-left: 20px;" role="tablist" aria-multiselectable="true"><div class="panel" data-parent="1">
                    <label>
                        <input type="radio" name="parent_id"   id="parent-10" value="10" class="toggle-accordion">
                        <span role="tab" id="heading-10" class="tab">
                            <span class="check-category" role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapse-10" aria-controls="collapse-10">
                                Current Assets
                            </span>
                        </span>
                    </label>
                    <div id="collapse-10" class="panel-collapse collapse " role="tabpanel" aria-labelledby="heading-10"><div id="accordion-10" style="margin-left: 20px;" role="tablist" aria-multiselectable="true"><div class="panel"><label for="parent-15">
                    <input type="radio" name="parent_id"  id="parent-15" value="15"  3  >Cash, Banks and E-wallets
                </label></div><div class="panel"><label for="parent-41">
                    <input type="radio" name="parent_id"  id="parent-41" value="41"  13  >TDS Receivable
                </label></div></div></div></div><div class="panel" data-parent="1">
                    <label>
                        <input type="radio" name="parent_id"  id="parent-22" value="22" class="toggle-accordion">
                        <span role="tab" id="heading-22" class="tab">
                            <span class="check-category" role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapse-22" aria-controls="collapse-22">
                                Fixed Assets
                            </span>
                        </span>
                    </label>
                    <div id="collapse-22" class="panel-collapse collapse " role="tabpanel" aria-labelledby="heading-22"><div id="accordion-22" style="margin-left: 20px;" role="tablist" aria-multiselectable="true"><div class="panel"><label for="parent-23">
                    <input type="radio" name="parent_id"  id="parent-23" value="23"  19  >Plant and Machinery
                </label></div><div class="panel"><label for="parent-25">
                    <input type="radio" name="parent_id"  id="parent-25" value="25"  23  >Land and Building
                </label></div><div class="panel"><label for="parent-28">
                    <input type="radio" name="parent_id"  id="parent-28" value="28"  27  >Depreciation
                </label></div><div class="panel"><label for="parent-505">
                    <input type="radio" name="parent_id"  id="parent-505" value="505"  29  >Books and library
                </label></div><div class="panel"><label for="parent-507">
                    <input type="radio" name="parent_id"  id="parent-507" value="507"  33  >Computers and Printers
                </label></div><div class="panel"><label for="parent-509">
                    <input type="radio" name="parent_id"  id="parent-509" value="509"  37  >Furniture
                </label></div><div class="panel"><label for="parent-511">
                    <input type="radio" name="parent_id"  id="parent-511" value="511"  41  >Musical Instruments
                </label></div><div class="panel"><label for="parent-513">
                    <input type="radio" name="parent_id"  id="parent-513" value="513"  45  >Science lab equipment
                </label></div><div class="panel"><label for="parent-515">
                    <input type="radio" name="parent_id"  id="parent-515" value="515"  49  >Sports equipment
                </label></div><div class="panel"><label for="parent-517">
                    <input type="radio" name="parent_id"  id="parent-517" value="517"  53  >Vehicle
                </label></div><div class="panel"><label for="parent-519">
                    <input type="radio" name="parent_id"  id="parent-519" value="519"  57  >Generator
                </label></div></div></div></div></div></div></div><div class="panel" data-parent="0">
                    <label>
                        <input type="radio" name="parent_id"  id="parent-2" value="2" class="toggle-accordion">
                        <span role="tab" id="heading-2" class="tab">
                            <span class="check-category" role="button" data-toggle="collapse" data-parent="#accordion-0" href="#collapse-2" aria-controls="collapse-2">
                                Liabilities
                            </span>
                        </span>
                    </label>
                    <div id="collapse-2" class="panel-collapse collapse " role="tabpanel" aria-labelledby="heading-2"><div id="accordion-2" style="margin-left: 20px;" role="tablist" aria-multiselectable="true"><div class="panel" data-parent="2">
                    <label>
                        <input type="radio" name="parent_id"  id="parent-11" value="11" class="toggle-accordion">
                        <span role="tab" id="heading-11" class="tab">
                            <span class="check-category" role="button" data-toggle="collapse" data-parent="#accordion-2" href="#collapse-11" aria-controls="collapse-11">
                                Current Liabilities
                            </span>
                        </span>
                    </label>
                    <div id="collapse-11" class="panel-collapse collapse " role="tabpanel" aria-labelledby="heading-11"><div id="accordion-11" style="margin-left: 20px;" role="tablist" aria-multiselectable="true"><div class="panel"><label for="parent-503">
                    <input type="radio" name="parent_id"  id="parent-503" value="503"  65  >short term loan
                </label></div><div class="panel"><label for="parent-536">
                    <input type="radio" name="parent_id"  id="parent-536" value="536"  69  >Audit Fee Tax
                </label></div><div class="panel"><label for="parent-538">
                    <input type="radio" name="parent_id"  id="parent-538" value="538"  73  >House Rent Payable
                </label></div><div class="panel"><label for="parent-540">
                    <input type="radio" name="parent_id"  id="parent-540" value="540"  77  >Payable Salary
                </label></div><div class="panel"><label for="parent-542">
                    <input type="radio" name="parent_id"  id="parent-542" value="542"  81  >Payable Expenses
                </label></div><div class="panel"><label for="parent-544">
                    <input type="radio" name="parent_id"  id="parent-544" value="544"  87  >Refundable Deposits
                </label></div><div class="panel"><label for="parent-550">
                    <input type="radio" name="parent_id"  id="parent-550" value="550"  91  >TDS payable
                </label></div><div class="panel"><label for="parent-558">
                    <input type="radio" name="parent_id"  id="parent-558" value="558"  95  >Social Service Tax
                </label></div></div></div></div><div class="panel" data-parent="2">
                    <label>
                        <input type="radio" name="parent_id"  id="parent-32" value="32" class="toggle-accordion">
                        <span role="tab" id="heading-32" class="tab">
                            <span class="check-category" role="button" data-toggle="collapse" data-parent="#accordion-2" href="#collapse-32" aria-controls="collapse-32">
                                Loan
                            </span>
                        </span>
                    </label>
                    <div id="collapse-32" class="panel-collapse collapse " role="tabpanel" aria-labelledby="heading-32"><div id="accordion-32" style="margin-left: 20px;" role="tablist" aria-multiselectable="true"><div class="panel"><label for="parent-39">
                    <input type="radio" name="parent_id"  id="parent-39" value="39"  101  >Long Term Loan
                </label></div><div class="panel"><label for="parent-501">
                    <input type="radio" name="parent_id"  id="parent-501" value="501"  105  >Profit
                </label></div></div></div></div><div class="panel" data-parent="2">
                    <label>
                        <input type="radio" name="parent_id"  id="parent-560" value="560" class="toggle-accordion">
                        <span role="tab" id="heading-560" class="tab">
                            <span class="check-category" role="button" data-toggle="collapse" data-parent="#accordion-2" href="#collapse-560" aria-controls="collapse-560">
                                Long Term Liabilities
                            </span>
                        </span>
                    </label>
                    <div id="collapse-560" class="panel-collapse collapse " role="tabpanel" aria-labelledby="heading-560"><div id="accordion-560" style="margin-left: 20px;" role="tablist" aria-multiselectable="true"><div class="panel"><label for="parent-561">
                    <input type="radio" name="parent_id"  id="parent-561" value="561"  109  >Hire Purchase
                </label></div><div class="panel"><label for="parent-563">
                    <input type="radio" name="parent_id"  id="parent-563" value="563"  113  >OD Loan
                </label></div></div></div></div></div></div></div><div class="panel" data-parent="0">
                    <label>
                        <input type="radio" name="parent_id"  id="parent-3" value="3" class="toggle-accordion">
                        <span role="tab" id="heading-3" class="tab">
                            <span class="check-category" role="button" data-toggle="collapse" data-parent="#accordion-0" href="#collapse-3" aria-controls="collapse-3">
                                Incomes
                            </span>
                        </span>
                    </label>
                    <div id="collapse-3" class="panel-collapse collapse " role="tabpanel" aria-labelledby="heading-3"><div id="accordion-3" style="margin-left: 20px;" role="tablist" aria-multiselectable="true"><div class="panel" data-parent="3">
                    <label>
                        <input type="radio" name="parent_id"  id="parent-6" value="6" class="toggle-accordion">
                        <span role="tab" id="heading-6" class="tab">
                            <span class="check-category" role="button" data-toggle="collapse" data-parent="#accordion-3" href="#collapse-6" aria-controls="collapse-6">
                                Regular Income
                            </span>
                        </span>
                    </label>
                    <div id="collapse-6" class="panel-collapse collapse " role="tabpanel" aria-labelledby="heading-6"><div id="accordion-6" style="margin-left: 20px;" role="tablist" aria-multiselectable="true"><div class="panel"><label for="parent-7">
                    <input type="radio" name="parent_id"  id="parent-7" value="7"  121  >Fees & Charges
                </label></div></div></div></div><div class="panel" data-parent="3">
                    <label>
                        <input type="radio" name="parent_id"  id="parent-33" value="33" class="toggle-accordion">
                        <span role="tab" id="heading-33" class="tab">
                            <span class="check-category" role="button" data-toggle="collapse" data-parent="#accordion-3" href="#collapse-33" aria-controls="collapse-33">
                                Other Income
                            </span>
                        </span>
                    </label>
                    <div id="collapse-33" class="panel-collapse collapse " role="tabpanel" aria-labelledby="heading-33"><div id="accordion-33" style="margin-left: 20px;" role="tablist" aria-multiselectable="true"><div class="panel"><label for="parent-35">
                    <input type="radio" name="parent_id"  id="parent-35" value="35"  129  >Other Income
                </label></div><div class="panel"><label for="parent-502">
                    <input type="radio" name="parent_id"  id="parent-502" value="502"  133  >Profit
                </label></div><div class="panel"><label for="parent-565">
                    <input type="radio" name="parent_id"  id="parent-565" value="565"  135  >Office Income
                </label></div></div></div></div></div></div></div><div class="panel" data-parent="0">
                    <label>
                        <input type="radio" name="parent_id"  id="parent-4" value="4" class="toggle-accordion">
                        <span role="tab" id="heading-4" class="tab">
                            <span class="check-category" role="button" data-toggle="collapse" data-parent="#accordion-0" href="#collapse-4" aria-controls="collapse-4">
                                Expenses
                            </span>
                        </span>
                    </label>
                    <div id="collapse-4" class="panel-collapse collapse " role="tabpanel" aria-labelledby="heading-4"><div id="accordion-4" style="margin-left: 20px;" role="tablist" aria-multiselectable="true"><div class="panel" data-parent="4">
                    <label>
                        <input type="radio" name="parent_id"  id="parent-12" value="12" class="toggle-accordion">
                        <span role="tab" id="heading-12" class="tab">
                            <span class="check-category" role="button" data-toggle="collapse" data-parent="#accordion-4" href="#collapse-12" aria-controls="collapse-12">
                                General Expenses
                            </span>
                        </span>
                    </label>
                    <div id="collapse-12" class="panel-collapse collapse " role="tabpanel" aria-labelledby="heading-12"><div id="accordion-12" style="margin-left: 20px;" role="tablist" aria-multiselectable="true"><div class="panel"><label for="parent-13">
                    <input type="radio" name="parent_id"  id="parent-13" value="13"  141  >Discounts & Concession
                </label></div><div class="panel"><label for="parent-19">
                    <input type="radio" name="parent_id"  id="parent-19" value="19"  145  >Salary of staff
                </label></div></div></div></div><div class="panel" data-parent="4">
                    <label>
                        <input type="radio" name="parent_id"  id="parent-34" value="34" class="toggle-accordion">
                        <span role="tab" id="heading-34" class="tab">
                            <span class="check-category" role="button" data-toggle="collapse" data-parent="#accordion-4" href="#collapse-34" aria-controls="collapse-34">
                                Other Expense
                            </span>
                        </span>
                    </label>
                    <div id="collapse-34" class="panel-collapse collapse " role="tabpanel" aria-labelledby="heading-34"><div id="accordion-34" style="margin-left: 20px;" role="tablist" aria-multiselectable="true"><div class="panel"><label for="parent-37">
                    <input type="radio" name="parent_id"  id="parent-37" value="37"  151  >Other Expense
                </label></div></div></div></div><div class="panel" data-parent="4">
                    <label>
                        <input type="radio" name="parent_id"  id="parent-554" value="554" class="toggle-accordion">
                        <span role="tab" id="heading-554" class="tab">
                            <span class="check-category" role="button" data-toggle="collapse" data-parent="#accordion-4" href="#collapse-554" aria-controls="collapse-554">
                                Administrative Expense
                            </span>
                        </span>
                    </label>
                    <div id="collapse-554" class="panel-collapse collapse " role="tabpanel" aria-labelledby="heading-554"><div id="accordion-554" style="margin-left: 20px;" role="tablist" aria-multiselectable="true"><div class="panel"><label for="parent-556">
                    <input type="radio" name="parent_id"  id="parent-556" value="556"  157  >Cleaning expense
                </label></div><div class="panel"><label for="parent-566">
                    <input type="radio" name="parent_id"  id="parent-566" value="566"  161  >Advertisement Expenses
                </label></div><div class="panel"><label for="parent-568">
                    <input type="radio" name="parent_id"  id="parent-568" value="568"  165  >Bank Charge Expenses
                </label></div><div class="panel"><label for="parent-570">
                    <input type="radio" name="parent_id"  id="parent-570" value="570"  169  >Books Purchase
                </label></div><div class="panel"><label for="parent-572">
                    <input type="radio" name="parent_id"  id="parent-572" value="572"  173  >Electricity Expenses
                </label></div><div class="panel"><label for="parent-574">
                    <input type="radio" name="parent_id"  id="parent-574" value="574"  177  >Extra Curricular Activities Expenses
                </label></div><div class="panel"><label for="parent-576">
                    <input type="radio" name="parent_id"  id="parent-576" value="576"  181  >Gardening Expenses
                </label></div><div class="panel"><label for="parent-578">
                    <input type="radio" name="parent_id"  id="parent-578" value="578"  185  >Hostel Expenses
                </label></div><div class="panel"><label for="parent-580">
                    <input type="radio" name="parent_id"  id="parent-580" value="580"  189  >Internet Expenses
                </label></div><div class="panel"><label for="parent-582">
                    <input type="radio" name="parent_id"  id="parent-582" value="582"  193  >Medical Expenses
                </label></div><div class="panel"><label for="parent-584">
                    <input type="radio" name="parent_id"  id="parent-584" value="584"  197  >Printing & Stationery Expenses
                </label></div><div class="panel"><label for="parent-586">
                    <input type="radio" name="parent_id"  id="parent-586" value="586"  201  >Promotional Expenses
                </label></div><div class="panel"><label for="parent-589">
                    <input type="radio" name="parent_id"  id="parent-589" value="589"  205  >Renewal Expenses
                </label></div><div class="panel"><label for="parent-591">
                    <input type="radio" name="parent_id"  id="parent-591" value="591"  209  >Rent Expenses
                </label></div><div class="panel"><label for="parent-593">
                    <input type="radio" name="parent_id"  id="parent-593" value="593"  213  >Repair & Maintenance
                </label></div><div class="panel"><label for="parent-595">
                    <input type="radio" name="parent_id"  id="parent-595" value="595"  217  >Security Expenses
                </label></div><div class="panel"><label for="parent-597">
                    <input type="radio" name="parent_id"  id="parent-597" value="597"  221  >Tea & Refreshment
                </label></div><div class="panel"><label for="parent-599">
                    <input type="radio" name="parent_id"  id="parent-599" value="599"  225  >Vehicle Repair & Maintenance
                </label></div><div class="panel"><label for="parent-601">
                    <input type="radio" name="parent_id"  id="parent-601" value="601"  229  >Water Expenses
                </label></div></div></div></div></div></div></div><div class="panel" data-parent="0">
                    <label>
                        <input type="radio" name="parent_id"  id="parent-5" value="5" class="toggle-accordion">
                        <span role="tab" id="heading-5" class="tab">
                            <span class="check-category" role="button" data-toggle="collapse" data-parent="#accordion-0" href="#collapse-5" aria-controls="collapse-5">
                                Equity
                            </span>
                        </span>
                    </label>
                    <div id="collapse-5" class="panel-collapse collapse " role="tabpanel" aria-labelledby="heading-5"><div id="accordion-5" style="margin-left: 20px;" role="tablist" aria-multiselectable="true"><div class="panel" data-parent="5">
                    <label>
                        <input type="radio" name="parent_id"  id="parent-29" value="29" class="toggle-accordion">
                        <span role="tab" id="heading-29" class="tab">
                            <span class="check-category" role="button" data-toggle="collapse" data-parent="#accordion-5" href="#collapse-29" aria-controls="collapse-29">
                                CAPITAL
                            </span>
                        </span>
                    </label>
                    <div id="collapse-29" class="panel-collapse collapse " role="tabpanel" aria-labelledby="heading-29"><div id="accordion-29" style="margin-left: 20px;" role="tablist" aria-multiselectable="true"><div class="panel"><label for="parent-30">
                    <input type="radio" name="parent_id"  id="parent-30" value="30"  237  >CAPITAL
                </label></div></div></div></div><div class="panel"><label for="parent-500">
                    <input type="radio" name="parent_id"  id="parent-500" value="500"  242  >Profit
                </label></div></div></div></div></div>                            </div>
                            <div class="form-group">
                                <label>Title</label> <small class="req"> *</small>
                                                                <input type="text" id="title" name="title" class="form-control"
                                       value="Title">
                            </div>
                            <div class="form-group">
                                <button type="submit" name="search" value="search_full"
                                        class="btn btn-primary pull-right btn-sm checkbox-toggle"> Save</button>
                            </div>
                            </form>                            
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('.check-category').on('click', function () {
            let $this = $(this);
            let $input = $this.closest('label').find('input[name="GLCode"]');
            $input.prop('checked', true);
            let val = $input.val();
            collapseElement($this, val);
        });
        $('.toggle-accordion').on('click', function () {
            let $this = $(this);
            let val = $this.val();
            collapseElement($this, val);
        });
        function collapseElement($this, val){
            let $panel = $this.closest('.panel');
            let parent = $panel.data('parent');
            let element = $('#collapse-' + val);
            $('[data-parent=' + parent + ']').each(function () {
                $(this).find('.panel-collapse').not('#collapse-' + val).collapse('hide');
            });
            element.collapse('show');
        }
    });
</script>