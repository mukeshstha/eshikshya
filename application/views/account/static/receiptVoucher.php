<div class="content-wrapper" style="min-height: 946px;">
	<section class="content-header">
		<h1>
			Receipt Voucher
		</h1>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">
							<i class="fa fa-plus-circle"></i> Receipt Voucher </h3>
						</div>
						<div class="box-body">
							<div class="row">
							<div class="filter-box">
							<form>
								<input type="hidden" name="id" value=""/>

								<div class="col-md-4 form-group">
									<label>Subsidairy</label><br>
									<select>
										<option>Cash on Vault</option>
										<option>Banking Finance</option>
										<option>Cash in Hand</option>
										<option>test Subsidairy</option>
									</select>

								</div>
								<div class="col-md-4 form-group journal_date_form">
									<label>Transaction Date</label>
									<input type="text" id="entry_date_bs" name="entry_date_bs" class="form-control"
									readonly required
									value="<?php echo set_value('postdate', date($this->customlib->getSchoolDateFormat())); ?>">
									<input type="hidden" id="entry_date" name="entry_date" class="form-control date"
									readonly
									value="">
								</div>
								<div class="col-md-4 form-group">
									<label>Reference No</label> <small
									class="text-muted">Optional</small>
									<input type="text" id="reference_no" name="reference_no" class="form-control"
									value="">
								</div>
								</form>
							</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="content">
			<div class="row">
				<div class="col-md-12">
					<div class="box box-primary">
						<div class="box-body">
							<div class="row">
								<div class="col-md-12">
								<button class="btn btn-primary pull-right" id="addProduct"><i class="fa fa-plus"></i>Add New</button>
									<table id="journal_entry_table"
									class="table invoice-table" cellspacing="0"
									width="100%">
									<thead>
										<tr>
											<th width="20%">Subsidairy</th>
											<th width="20%">Balance</th>
											<th width="20%">Amount</th>
											<th width="15%">Remarks</th>
											<th width="5%" class="no-print text text-right">Action</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>
												<select class="selectpicker form-control" data-show-subtext="true" data-live-search="true">
													<option>Interest Expense</option>
													<option>Refundable Deposits</option>
													<option>Land and Building</option>
													<option>Furniture</option>
													<option>Office Bus</option>
													<option>Salary Payable</option>
												</select>
											</td>
											<td><input type="text" name="balance" placeholder="0.00"></td>
											<td><input type="text" name="amount" placeholder="0.00"></td>
											<td><input type="text" name="remark" placeholder="remark"></td>
											<td><button class="btn btn-danger remove">Remove</button></td>
										</tr>
									</tbody>
								</table>

							</div>
						</div>
						<div class="row">
							<div class="col-md-6 form-group">
								<label>Narration</label>
								<textarea name="narration"
								class="form-control"></textarea>
							</div>
						</div>
						<div class="hidden-field"></div>
						<button type="submit" name="submit" value="submit"
						class="btn btn-primary pull-right btn-sm submit-form"> Save</button>
					</div>
				</div>
			</div>
		</div>
	</section>

</div>

<style type="text/css">
	/*span.filter-option.pull-left{
		width: auto !important;
	}

	.btn-group.bootstrap-select.dropup{
		width: 100% !important;
		height: 40px !important;
	}

	.btn-group.bootstrap-select.dropup button{
		line-height: 25px !important;
	}*/
</style><!-- 
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css" rel="stylesheet" /> -->

<script type="text/javascript">
	$(document).ready(function(){

		var html = '<tr><td><select class="selectpicker form-control" data-show-subtext="true" data-live-search="true"><option >Interest Expense</option><option>Refundable Deposits</option><option>Land and Building</option><option>Furniture</option><option>Office Bus</option><option>Salary Payable</option></select></td><td><input type="text" name="balance" placeholder="0.00"></td><td><input type="text" name="amount" placeholder="0.00"></td><td><input type="text" name="remark" placeholder="remark"></td><td><button class="btn btn-danger remove">Remove</button></td></tr>'; 
		$("#addProduct").click(function(){
			$('tbody').append(html);
			$('.selectpicker').selectpicker();
		});

		$(document).on('click','.remove',function(){
			$(this).parents('tr').remove();
		});
	});


</script>
	