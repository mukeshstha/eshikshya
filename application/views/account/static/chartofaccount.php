
<div class="content-wrapper" style="min-height: 946px;">
    <section class="content-header">
        <h1>

        </h1>
    </section>

    <section class="content">
    	<div class="row">
            <div class="col-md-12">
                <div class="nav-tabs-custom coalist">
                    <ul class="nav nav-tabs"> 
                    <li class="pull-left header"><i class="fa fa-list-ul"></i> Chart of Accounts</li>
                        <li class="active"><a data-toggle="tab" href="#Assests">Assests</a></li>
                        <li><a data-toggle="tab" href="#Liabilities">Liabilities</a></li>
                        <li><a data-toggle="tab" href="#Incomes">Incomes</a></li>
                        <li><a data-toggle="tab" href="#Expenses">Expenses</a></li>
                        <li><a data-toggle="tab" href="#Equity">Equity</a></li>
                    </ul>

                    <div class="tab-content">
                        <div id="Assests" class="tab-pane fade in">
                        	<div style="height: 50px">
                            	<a href="<?php echo base_url(); ?>account/view/createchart" class="btn btn-primary btn-sm pull-right">
                                    <i class="fa fa-plus"></i>Add Assests</a>
                              </div>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover example dataTable" cellspacing="0" width="100%" id="DataTables_Table_0">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Category</th>
                                            <th>Sub Category</th>
                                            <th>SubCategory 2</th>
                                            <th>Code</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
		                              <tr>
		                                <td>Prabhupay</td>
		                                <td>Current Assets</td>
		                                <td>Cash, Banks and E-wallets</td>
		                                <td>E-wallets</td>
		                                <td>EWALLETPRABHUPAY</td>
		                                <td class="mailbox-date no-print text ">
		                                    <a href="<?php echo base_url(); ?>account/view/createchart" class="btn btn-default btn-xs" data-toggle="tooltip" title="" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
		                                    <a href="" class="btn btn-default btn-xs deletecoa" data-toggle="tooltip" title="" data-original-title="Delete"><i class="fa fa-remove"></i></a>
                            			</td>
		                              </tr>

		                              <tr>
		                                <td>Cash in hand</td>
		                                <td>Current Assets</td>
		                                <td>Cash, Banks and E-wallets</td>
		                                <td>Cash</td>
		                                <td>CASHINHAND</td>
		                                <td class="mailbox-date no-print text ">
		                                    <a href="<?php echo base_url(); ?>account/view/createchart" class="btn btn-default btn-xs" data-toggle="tooltip" title="" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
		                                    <a href="" class="btn btn-default btn-xs deletecoa" data-toggle="tooltip" title="" data-original-title="Delete"><i class="fa fa-remove"></i></a>
                            			</td>
		                              </tr>
		                              <tr>
		                                <td>Land and Building</td>
		                                <td>Fixed Assets</td>
		                                <td>Land and Building</td>
		                                <td>Land</td>
		                                <td>005</td>
		                                <td class="mailbox-date no-print text ">
		                                    <a href="<?php echo base_url(); ?>account/view/createchart" class="btn btn-default btn-xs" data-toggle="tooltip" title="" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
		                                    <a href="" class="btn btn-default btn-xs deletecoa" data-toggle="tooltip" title="" data-original-title="Delete"><i class="fa fa-remove"></i></a>
                            			</td>
		                              </tr>
		                              <tr>
		                                <td>BOOKS AND LIBRARY</td>
		                                <td>Fixed Assets</td>
		                                <td>Books and library</td>
		                                <td>Books and library</td>
		                                <td>2222</td>
		                                <td class="mailbox-date no-print text ">
		                                    <a href="<?php echo base_url(); ?>account/view/createchart" class="btn btn-default btn-xs" data-toggle="tooltip" title="" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
		                                    <a href="" class="btn btn-default btn-xs deletecoa" data-toggle="tooltip" title="" data-original-title="Delete"><i class="fa fa-remove"></i></a>
                            			</td>
		                              </tr>
		                              <tr>
		                                <td>Computers and Printers</td>
		                                <td>Fixed Assets</td>
		                                <td>Computers and Printers</td>
		                                <td>Computers and Printers</td>
		                                <td>3333</td>
		                                <td class="mailbox-date no-print text ">
		                                    <a href="<?php echo base_url(); ?>account/view/createchart" class="btn btn-default btn-xs" data-toggle="tooltip" title="" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
		                                    <a href="" class="btn btn-default btn-xs deletecoa" data-toggle="tooltip" title="" data-original-title="Delete"><i class="fa fa-remove"></i></a>
                            			</td>
		                              </tr>
                                    </tbody>
                                </table>
                              </div>
                        </div>

                        <div id="Liabilities" class="tab-pane fade in">
                        	<div style="height: 50px">
                            	<a href="<?php echo base_url(); ?>account/view/createchart" class="btn btn-primary btn-sm pull-right">
                                    <i class="fa fa-plus"></i>Add Liabilities</a>
                           	</div>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover example dataTable" cellspacing="0" width="100%" id="DataTables_Table_0">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Category</th>
                                            <th>Sub Category</th>
                                            <th>SubCategory 2</th>
                                            <th>Code</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
		                              <tr>
		                                <td>Long Term Loan</td>
		                                <td>Loan</td>
		                                <td>Long Term Loan</td>
		                                <td>Long Term Loan</td>
		                                <td>L 1</td>
		                                <td class="mailbox-date no-print text ">
		                                    <a href="<?php echo base_url(); ?>account/view/createchart" class="btn btn-default btn-xs" data-toggle="tooltip" title="" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
		                                    <a href="" class="btn btn-default btn-xs deletecoa" data-toggle="tooltip" title="" data-original-title="Delete"><i class="fa fa-remove"></i></a>
                            			</td>
		                              </tr>
		                              <tr>
		                                <td>Salary Payable</td>
		                                <td>Current Liabilities</td>
		                                <td>Payable Salary</td>
		                                <td>Salary Payable</td>
		                                <td>4563</td>
		                                <td class="mailbox-date no-print text ">
		                                    <a href="<?php echo base_url(); ?>account/view/createchart" class="btn btn-default btn-xs" data-toggle="tooltip" title="" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
		                                    <a href="" class="btn btn-default btn-xs deletecoa" data-toggle="tooltip" title="" data-original-title="Delete"><i class="fa fa-remove"></i></a>
                            			</td>
		                              </tr>
		                              <tr>
		                                <td>TDS Payable</td>
		                                <td>Current Liabilities</td>
		                                <td>TDS payable</td>
		                                <td>TDS Payable</td>
		                                <td>4613</td>
		                                <td class="mailbox-date no-print text ">
		                                    <a href="<?php echo base_url(); ?>account/view/createchart" class="btn btn-default btn-xs" data-toggle="tooltip" title="" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
		                                    <a href="" class="btn btn-default btn-xs deletecoa" data-toggle="tooltip" title="" data-original-title="Delete"><i class="fa fa-remove"></i></a>
                            			</td>
		                              </tr>
		                              <tr>
		                                <td>Hire Purchase</td>
		                                <td>Long Term Liabilities</td>
		                                <td>Hire Purchase</td>
		                                <td>Hire Purchase</td>
		                                <td>3241</td>
		                                <td class="mailbox-date no-print text ">
		                                    <a href="<?php echo base_url(); ?>account/view/createchart" class="btn btn-default btn-xs" data-toggle="tooltip" title="" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
		                                    <a href="" class="btn btn-default btn-xs deletecoa" data-toggle="tooltip" title="" data-original-title="Delete"><i class="fa fa-remove"></i></a>
                            			</td>
		                              </tr>
		                              <tr>
		                                <td>OD Loan</td>
		                                <td>Long Term Liabilities</td>
		                                <td>OD Loan</td>
		                                <td>OD Loan</td>
		                                <td>324165</td>
		                                <td class="mailbox-date no-print text ">
		                                    <a href="<?php echo base_url(); ?>account/view/createchart" class="btn btn-default btn-xs" data-toggle="tooltip" title="" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
		                                    <a href="" class="btn btn-default btn-xs deletecoa" data-toggle="tooltip" title="" data-original-title="Delete"><i class="fa fa-remove"></i></a>
                            			</td>
		                              </tr>
		                              <tr>
		                                <td>House rent payable</td>
		                                <td>Current Liabilities</td>
		                                <td>Payable Expenses</td>
		                                <td>PAYABLES</td>
		                                <td>O98be1614325950cf50</td>
		                                <td class="mailbox-date no-print text ">
		                                    <a href="<?php echo base_url(); ?>account/view/createchart" class="btn btn-default btn-xs" data-toggle="tooltip" title="" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
		                                    <a href="" class="btn btn-default btn-xs deletecoa" data-toggle="tooltip" title="" data-original-title="Delete"><i class="fa fa-remove"></i></a>
                            			</td>
		                              </tr>
		                              <tr>
		                                <td>Audit fee tax</td>
		                                <td>Current Liabilities</td>
		                                <td>Payable Expenses</td>
		                                <td>PAYABLES</td>
		                                <td>S10f81614325950c7af</td>
		                                <td class="mailbox-date no-print text ">
		                                    <a href="<?php echo base_url(); ?>account/view/createchart" class="btn btn-default btn-xs" data-toggle="tooltip" title="" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
		                                    <a href="" class="btn btn-default btn-xs deletecoa" data-toggle="tooltip" title="" data-original-title="Delete"><i class="fa fa-remove"></i></a>
                            			</td>
		                              </tr>
                                    </tbody>
                                </table>
                              </div>
                        </div>

                        <div id="Incomes" class="tab-pane fade in">
                        	<div style="height: 50px">
                            <a href="<?php echo base_url(); ?>account/view/createchart" class="btn btn-primary btn-sm pull-right">
                                    <i class="fa fa-plus"></i>Add Incomes</a>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover example dataTable" cellspacing="0" width="100%" id="DataTables_Table_0">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Category</th>
                                            <th>Sub Category</th>
                                            <th>SubCategory 2</th>
                                            <th>Code</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
		                              <tr>
		                                <td>Tuition Fee</td>
		                                <td>Regular Income</td>
		                                <td>Fees & Charges</td>
		                                <td>Student Fees</td>
		                                <td>STF</td>
		                                <td class="mailbox-date no-print text ">
		                                    <a href="<?php echo base_url(); ?>account/view/createchart" class="btn btn-default btn-xs" data-toggle="tooltip" title="" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
		                                    <a href="" class="btn btn-default btn-xs deletecoa" data-toggle="tooltip" title="" data-original-title="Delete"><i class="fa fa-remove"></i></a>
                            			</td>
		                              </tr>

		                              <tr>
		                                <td>Exam  Fee</td>
		                                <td>Regular Income</td>
		                                <td>Fees & Charges</td>
		                                <td>Student Fees</td>
		                                <td>SEF</td>
		                                <td class="mailbox-date no-print text ">
		                                    <a href="<?php echo base_url(); ?>account/view/createchart" class="btn btn-default btn-xs" data-toggle="tooltip" title="" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
		                                    <a href="" class="btn btn-default btn-xs deletecoa" data-toggle="tooltip" title="" data-original-title="Delete"><i class="fa fa-remove"></i></a>
                            			</td>
		                              </tr>
		                              <tr>
		                                <td>Computer  Fee</td>
		                                <td>Regular Income</td>
		                                <td>Fees & Charges</td>
		                                <td>Student Fees</td>
		                                <td>SCF</td>
		                                <td class="mailbox-date no-print text ">
		                                    <a href="<?php echo base_url(); ?>account/view/createchart" class="btn btn-default btn-xs" data-toggle="tooltip" title="" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
		                                    <a href="" class="btn btn-default btn-xs deletecoa" data-toggle="tooltip" title="" data-original-title="Delete"><i class="fa fa-remove"></i></a>
                            			</td>
		                              </tr>
		                              <tr>
		                                <td>Library  Fee</td>
		                                <td>Regular Income</td>
		                                <td>Fees & Charges</td>
		                                <td>Student Fees</td>
		                                <td>SLF</td>
		                                <td class="mailbox-date no-print text ">
		                                    <a href="<?php echo base_url(); ?>account/view/createchart" class="btn btn-default btn-xs" data-toggle="tooltip" title="" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
		                                    <a href="" class="btn btn-default btn-xs deletecoa" data-toggle="tooltip" title="" data-original-title="Delete"><i class="fa fa-remove"></i></a>
                            			</td>
		                              </tr>
		                              <tr>
		                                <td>Fine</td>
		                                <td>Regular Income</td>
		                                <td>Fees & Charges</td>
		                                <td>Student Fees</td>
		                                <td>FINE</td>
		                                <td class="mailbox-date no-print text ">
		                                    <a href="<?php echo base_url(); ?>account/view/createchart" class="btn btn-default btn-xs" data-toggle="tooltip" title="" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
		                                    <a href="" class="btn btn-default btn-xs deletecoa" data-toggle="tooltip" title="" data-original-title="Delete"><i class="fa fa-remove"></i></a>
                            			</td>
		                              </tr>
                                    </tbody>
                                </table>
                              </div>
                        </div>

                        <div id="Expenses" class="tab-pane fade in">
                        	<div style="height: 50px">
                            <a href="<?php echo base_url(); ?>account/view/createchart" class="btn btn-primary btn-sm pull-right">
                                    <i class="fa fa-plus"></i>Add Expenses</a>
                               </div>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover example dataTable" cellspacing="0" width="100%" id="DataTables_Table_0">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Category</th>
                                            <th>Sub Category</th>
                                            <th>SubCategory 2</th>
                                            <th>Code</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
		                              <tr>
		                                <td>Salary and Wages</td>
		                                <td>General Expenses</td>
		                                <td>Salary of staff</td>
		                                <td>Staff salary</td>
		                                <td>SS 001</td>
		                                <td class="mailbox-date no-print text ">
		                                    <a href="<?php echo base_url(); ?>account/view/createchart" class="btn btn-default btn-xs" data-toggle="tooltip" title="" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
		                                    <a href="" class="btn btn-default btn-xs deletecoa" data-toggle="tooltip" title="" data-original-title="Delete"><i class="fa fa-remove"></i></a>
                            			</td>
		                              </tr>
		                              <tr>
		                                <td>Interest Expense</td>
		                                <td>Other Expense</td>
		                                <td>Other Expense</td>
		                                <td>Interest Expense</td>
		                                <td>EX 1</td>
		                                <td class="mailbox-date no-print text ">
		                                    <a href="<?php echo base_url(); ?>account/view/createchart" class="btn btn-default btn-xs" data-toggle="tooltip" title="" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
		                                    <a href="" class="btn btn-default btn-xs deletecoa" data-toggle="tooltip" title="" data-original-title="Delete"><i class="fa fa-remove"></i></a>
                            			</td>
		                              </tr>
		                              <tr>
		                                <td>Cleaning expense</td>
		                                <td>Administrative Expense</td>
		                                <td>Cleaning expense</td>
		                                <td>Cleaning expense</td>
		                                <td>0001</td>
		                                <td class="mailbox-date no-print text ">
		                                    <a href="<?php echo base_url(); ?>account/view/createchart" class="btn btn-default btn-xs" data-toggle="tooltip" title="" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
		                                    <a href="" class="btn btn-default btn-xs deletecoa" data-toggle="tooltip" title="" data-original-title="Delete"><i class="fa fa-remove"></i></a>
                            			</td>
		                              </tr>
		                              <tr>
		                                <td>Advertisement Expenses</td>
		                                <td>Administrative Expense</td>
		                                <td>	Advertisement Expenses</td>
		                                <td>	Advertisement Expenses</td>
		                                <td>0002</td>
		                                <td class="mailbox-date no-print text ">
		                                    <a href="<?php echo base_url(); ?>account/view/createchart" class="btn btn-default btn-xs" data-toggle="tooltip" title="" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
		                                    <a href="" class="btn btn-default btn-xs deletecoa" data-toggle="tooltip" title="" data-original-title="Delete"><i class="fa fa-remove"></i></a>
                            			</td>
		                              </tr>
		                              <tr>
		                                <td>Bank Charge Expenses</td>
		                                <td>Administrative Expense</td>
		                                <td>Bank Charge Expenses</td>
		                                <td>Bank Charge Expenses</td>
		                                <td>0003</td>
		                                <td class="mailbox-date no-print text ">
		                                    <a href="<?php echo base_url(); ?>account/view/createchart" class="btn btn-default btn-xs" data-toggle="tooltip" title="" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
		                                    <a href="" class="btn btn-default btn-xs deletecoa" data-toggle="tooltip" title="" data-original-title="Delete"><i class="fa fa-remove"></i></a>
                            			</td>
		                              </tr>
		                              
		                              

                                    </tbody>
                                </table>
                              </div>
                        </div>
                        <div id="Equity" class="tab-pane fade in">
                        	<div style="height: 50px">
                            <a href="<?php echo base_url(); ?>account/view/createchart" class="btn btn-primary btn-sm pull-right">
                                    <i class="fa fa-plus"></i>Add Equity</a>
                                    </div>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover example dataTable" cellspacing="0" width="100%" id="DataTables_Table_0">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Category</th>
                                            <th>Sub Category</th>
                                            <th>SubCategory 2</th>
                                            <th>Code</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
		                              <tr>
		                                <td>CAPITAL</td>
		                                <td>CAPITAL</td>
		                                <td>CAPITAL</td>
		                                <td>CAPITAL</td>
		                                <td>CP001</td>
		                                <td class="mailbox-date no-print text ">
		                                    <a href="<?php echo base_url(); ?>account/view/createchart" class="btn btn-default btn-xs" data-toggle="tooltip" title="" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
		                                    <a href="" class="btn btn-default btn-xs deletecoa" data-toggle="tooltip" title="" data-original-title="Delete"><i class="fa fa-remove"></i></a>
                            			</td>
		                              </tr>
		                              <tr>
		                                <td>Profit</td>
		                                <td>Profit</td>
		                                <td>Profit</td>
		                                <td>Profit</td>
		                                <td>YRLYPROFIT</td>
		                                <td class="mailbox-date no-print text ">
		                                    <a href="<?php echo base_url(); ?>account/view/createchart" class="btn btn-default btn-xs" data-toggle="tooltip" title="" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
		                                    <a href="" class="btn btn-default btn-xs deletecoa" data-toggle="tooltip" title="" data-original-title="Delete"><i class="fa fa-remove"></i></a>
                            			</td>
		                              </tr>
                                    </tbody>
                                </table>
                              </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>




<script>
$(document).ready(function ($) {
    $("div.tab-pane:first").addClass('active'); // This is the change.
});
</script>