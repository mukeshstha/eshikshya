<div class="content-wrapper" style="min-height: 946px;">
    <section class="content-header">
        <h1>

        </h1>
    </section>

<section class="content">

        <div class="row">
            <div class="col-md-9 align-center">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-plus-circle"></i> New Assets                        </h3>

                    </div>
                    <form role="form" id="add_coa" action="" class="form-horizontal" method="post">
                        <div class="box-body">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Assets Name<small class="req">*</small></label>
                                <div class="col-md-9">
                                    <input required type="text" class="form-control" id="name" name="name" placeholder="Name" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Assets Category<small class="req">*</small></label>
                                <div class="col-md-9">
                                    <select required class="custom-select form-control" id="category" name="category" onchange="Fetchcategory(this.value);">
                                        <option value="">--</option>
                                        <option>Current Assets</option>
                                        <option>Fixed Assets</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Sub Category 1<small class="req">*</small></label>
                                <div class="col-md-9">
                                    <select required class="custom-select form-control" id="subcategory1"
                                            name="subcategory1"  onchange="Fetchsubcategory(this.value)">
                                        <option value="">--</option>
                                        <option>Cash, Banks and E-wallets</option> 
                                        <option>TDS Receivable</option>  
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Sub Category 2<small class="req">*</small></label>
                                <div class="col-md-9">
                                    <select class="custom-select form-control" id="subcategory2"
                                            name="subcategory2">
                                        <option value="">--</option>
                                        <option value="7" data-chained="6" >Cash</option>
                                        <option value="8" data-chained="7" >Bank</option>
                                        <option>E-wallets</option>
                                        <option>Cheque</option>
                                    </select>
                                    <span class="text-danger"></span>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-3 control-label">Assets Code<small class="req">*</small></label>
                                <div class="col-md-9">
                                    <input required type="text" class="form-control" id="code" name="code"
                                           placeholder="Unique code"
                                           value="">
                                    <span class="text-danger"></span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Assets Description                                </label>
                                <div class="col-md-9">
                                <textarea rows="5" class="form-control" id="description" name="description"
                                ></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <button type="submit" name="submit" value="submit"
                                            class="btn btn-primary pull-right btn-sm checkbox-toggle"> Save</button>

                                </div>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>


    </section>

    </div>

    <style type="text/css">
    .form-control{
        background: #fff;
        border: 1px solid #cdcaca;
        box-shadow: none;
    }
    .form-horizontal{
        background: #fff;
    }
    </style>