<div class="content-wrapper" style="min-height: 946px;">
	<section class="content-header">
		<h1>
			Journal
		</h1>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title"><i class="fa fa-plus-circle"></i> Day Sheet </h3>
					</div>
					<div class="box-body">
						<div class="form-group row">
						<div class="col-md-12">
							<div class="col-md-3">
								<label>From Date</label>
								<input type="text" autocomplete="off" name="FromDate" class="form-control  date"  value="<?php  echo set_value('FromDate') ?>">
							</div>
							<div class="col-md-3">
								<label>To Date</label>
								<input type="text" autocomplete="off" name="FromDate" class="form-control  date"  value="<?php  echo set_value('FromDate') ?>">
							</div>
							<div class="col-md-2">
								<button class="btn btn-danger" type="button" id="btnSearch">
									<i class="fa fa-search" aria-hidden="true"></i> View
								</button>
							</div>
							</div>
						</div>


						<div id="SearchResult" style="margin-top:25px">
							<div class="table-responsive">
								<table class="table table-bordered  table-striped table-danger table-hover js-dataTable-buttons" width="100%">
									<thead>
										<tr class="text-nowrap">
											<th>
												GLHead
											</th>
											<th>Particulars</th>
											<th>GL Code</th>
											<th>
												Ledger Name
											</th>
											<th class="bg-success">
												Debit(रू)
											</th>
											<th class="bg-success">
												Credit(रू)
											</th>
											<th class="bg-warning">
												Balance
											</th>
										</tr>
									</thead>
									<tbody>
										<tr class="font-weight-bold">
											<td colspan="4">Liabilities</td>
											<td class="bg-success text-right">
												0.00
											</td>
											<td class="bg-success text-right">
												0.00
											</td>
											<td class="bg-warning text-right">

											</td>
										</tr>
										<tr class="font-weight-bold">
											<td colspan="4">Assets</td>
											<td class="bg-success text-right">
												200.00
											</td>
											<td class="bg-success text-right">
												0.00
											</td>
											<td class="bg-warning text-right">
												200.00 DR
											</td>
										</tr>
										<tr class="font-weight-bold">
											<td></td>
											<td colspan="3">Current Account</td>
											<td class="bg-success text-right">
												200.00
											</td>
											<td class="bg-success text-right">
												0.00
											</td>
											<td class="bg-warning text-right">
												200.00 DR
											</td>
										</tr>
										<tr>
											<td></td>
											<td></td>
											<td>

												<a href="" target="_blank">001000321211</a>
											</td>

											<td>Nabil Bank</td>
											<td class="bg-success text-right">200.00</td>
											<td class="bg-success text-right">0.00</td>
											<td class="bg-warning text-right">
												200.00 DR
											</td>
										</tr>
										<tr class="font-weight-bold">
											<td colspan="4">Expenditure</td>
											<td class="bg-success text-right">
												0.00
											</td>
											<td class="bg-success text-right">
												0.00
											</td>
											<td class="bg-warning text-right">

											</td>
										</tr>
										<tr class="font-weight-bold">
											<td colspan="4">Income</td>
											<td class="bg-success text-right">
												0.00
											</td>
											<td class="bg-success text-right">
												200.00
											</td>
											<td class="bg-warning text-right">
												200.00 CR
											</td>
										</tr>
										<tr class="font-weight-bold">
											<td></td>
											<td colspan="3">Entry Fee</td>
											<td class="bg-success text-right">
												0.00
											</td>
											<td class="bg-success text-right">
												200.00
											</td>
											<td class="bg-warning text-right">
												200.00 CR
											</td>
										</tr>
										<tr>
											<td></td>
											<td></td>
											<td>

												<a href="" target="_blank">001000141611</a>
											</td>

											<td>Form Registration</td>
											<td class="bg-success text-right">0.00</td>
											<td class="bg-success text-right">200.00</td>
											<td class="bg-warning text-right">
												200.00 CR
											</td>
										</tr>

									</tbody>
									<tfoot>
										<tr class="font-weight-bold">
											<td colspan="3"></td>
											<td colspan="1"><strong>Grand Total(रू)</strong></td>
											<td class="bg-success text-right">
												<span class="double-underline">
													200.00
												</span>
											</td>
											<td class="bg-success text-right">
												<span class="double-underline">
													200.00
												</span>
											</td>
											<td class="bg-warning text-right">
												<span class="double-underline">
													0.00
												</span>
											</td>
										</tr>
									</tfoot>
								</table>
							</div>
						</div>

					</div>

				</div>
			</div>
		</div>
	</section>
</div>