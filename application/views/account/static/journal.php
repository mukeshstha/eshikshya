

<div class="content-wrapper" style="min-height: 946px;">
    <section class="content-header">
        <h1>

        </h1>
    </section>

    <section class="content">
        <div class="row">
            <?php if($this->session->flashdata('success')) : ?>
                <div class="alert alert-success">
                    <?= $this->session->flashdata('success'); ?>
                </div>
            <?php endif; ?>
            <div class="col-md-12">
                <div class="box box-primary">

                    <div class="box-header ptbnull">
                        <h3 class="box-title titlefix"><i
                            class="fa fa-book"></i> Journals</h3>
                            <small class="pull-right">
                                <a href="<?php echo base_url(); ?>account/view/createjournal"
                                 class="btn btn-primary btn-sm">
                                 <i class="fa fa-plus"></i> Add Journal</a>
                             </small>
                         </div><!-- /.box-header -->
                         <div class="box-body">
                            <form action="" action="post">
                                <div class="col-md-4">
                                    <label>From date</label>
                                    <input type="text" autocomplete="off" name="FromDate" class="form-control  date"  value="<?php  echo set_value('FromDate') ?>">
                                </div>
                                <div class="col-md-4">
                                    <label>To date</label>
                                    <input type="text" autocomplete="off" name="ToDate" class="form-control date"  value="<?php  echo set_value('ToDate') ?>">
                                </div>
                                <div class="col-md-4">
                                    <button type="submit">Search</button>
                                </div>
                            </form>
                        </div>
                        <div class="box-body">
                            <div class="table-responsive mailbox-messages">
                                <div class="download_label">Journal List</div>
                               
                                <table id="" class="table table-striped table-bordered table-hover example" cellspacing="0"
                                width="100%">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Created Date</th>
                                        <th>Journal No</th>
                                        <th>Narration</th>
                                        <th>Balance</th>
                                        <th>Prepared By</th>
                                        <th class="no-print text text-right">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                        <tr>
                                            <td class="mailbox-name">1</td>
                                            <td class="mailbox-name">2022-12-25</td>
                                            <td class="mailbox-name">JOR11</td>
                                            <td class="mailbox-name">pump sale</td>
                                            <td class="mailbox-name"> NPR. 15,000.00</td>
                                            <td>Prem</td>
                                            <td class="mailbox-date no-print text text-right">
                                                <a role="button" href="<?php echo base_url(); ?>account/view/viewjournal" class="btn btn-default btn-xs " title="View"><i class="fa fa-eye"></i></a>

                                            	<a href="<?php echo base_url(); ?>account/view/createjournal" class="btn btn-default btn-xs" data-toggle="tooltip"
                                            	title="Edit"> <i class="fa fa-pencil"></i></a>
                                        		<a href="" class="btn btn-default btn-xs" data-toggle="tooltip"title="Delete"><i class="fa fa-remove"></i>
                                    			</a>
                                			</td>
                            			</tr>
                            			<tr>
                                            <td class="mailbox-name">2</td>
                                            <td class="mailbox-name">2022-12-25</td>
                                            <td class="mailbox-name">JOR12</td>
                                            <td class="mailbox-name">Long Term Loan</td>
                                            <td class="mailbox-name"> NPR. 15,000.00</td>
                                            <td>Prem</td>
                                            <td class="mailbox-date no-print text text-right">
                                                <a role="button" href="<?php echo base_url(); ?>account/view/viewjournal" class="btn btn-default btn-xs " title="View"><i class="fa fa-eye"></i></a>

                                            	<a href="<?php echo base_url(); ?>account/view/createjournal" class="btn btn-default btn-xs" data-toggle="tooltip"
                                            	title="Edit"> <i class="fa fa-pencil"></i></a>
                                        		<a href="" class="btn btn-default btn-xs" data-toggle="tooltip"title="Delete"><i class="fa fa-remove"></i>
                                    			</a>
                                			</td>
                            			</tr><tr>
                                            <td class="mailbox-name">3</td>
                                            <td class="mailbox-name">2022-12-25</td>
                                            <td class="mailbox-name">JOR13</td>
                                            <td class="mailbox-name">TDS Payable</td>
                                            <td class="mailbox-name"> NPR. 15,000.00</td>
                                            <td>Prem</td>
                                            <td class="mailbox-date no-print text text-right">
                                                <a role="button" href="<?php echo base_url(); ?>account/view/viewjournal" class="btn btn-default btn-xs " title="View"><i class="fa fa-eye"></i></a>

                                            	<a href="<?php echo base_url(); ?>account/view/createjournal" class="btn btn-default btn-xs" data-toggle="tooltip"
                                            	title="Edit"> <i class="fa fa-pencil"></i></a>
                                        		<a href="" class="btn btn-default btn-xs" data-toggle="tooltip"title="Delete"><i class="fa fa-remove"></i>
                                    			</a>
                                			</td>
                            			</tr><tr>
                                            <td class="mailbox-name">4</td>
                                            <td class="mailbox-name">2022-12-25</td>
                                            <td class="mailbox-name">JOR11</td>
                                            <td class="mailbox-name">pump sale</td>
                                            <td class="mailbox-name"> NPR. 15,000.00</td>
                                            <td>Prem</td>
                                            <td class="mailbox-date no-print text text-right">
                                                <a role="button" href="<?php echo base_url(); ?>account/view/viewjournal" class="btn btn-default btn-xs " title="View"><i class="fa fa-eye"></i></a>

                                            	<a href="<?php echo base_url(); ?>account/view/createjournal" class="btn btn-default btn-xs" data-toggle="tooltip"
                                            	title="Edit"> <i class="fa fa-pencil"></i></a>
                                        		<a href="" class="btn btn-default btn-xs" data-toggle="tooltip"title="Delete"><i class="fa fa-remove"></i>
                                    			</a>
                                			</td>
                            			</tr>
                    			</tbody>
                			</table>
           	 			</div>
        			</div>
    			</div>
			</div>
		</div>
	</section>
</div>
