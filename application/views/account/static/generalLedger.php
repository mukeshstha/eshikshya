<div class="content-wrapper" style="min-height: 946px;">
	<section class="content-header">
		<h1>
			General Ledger
		</h1>
	</section>
	<section class="content">

		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title"><i class="fa fa-plus-circle"></i> General Ledger </h3>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="alert alert-secondary">
								<div class="col-md-3">
									<label>From Date</label>
									<input type="text" autocomplete="off" name="FromDate" class="form-control  date"  value="<?php  echo set_value('FromDate') ?>">
								</div>
								<div class="col-md-3">
									<label>To Date</label>
									<input type="text" autocomplete="off" name="FromDate" class="form-control  date"  value="<?php  echo set_value('FromDate') ?>">
								</div>

								<div class="col-md-3">
									<label>GL Ledger &nbsp;</label><br>
									<select class="selectpicker" data-show-subtext="true" data-live-search="true">
										<option>Interest Expense</option>
										<option>Refundable Deposits</option>
										<option>Land and Building</option>
										<option>Furniture</option>
										<option>Office Bus</option>
										<option>Salary Payable</option>
									</select>
								</div>
								<div class="col-md-3">
									<button class="btn btn-primary btn-sm pull-right checkbox-toggle">View</button>
								</div>
							</div>
						</div>
					</div>



					<div id="SearchResult" style="margin-top: 25px">
						<div class="table-responsive">
							<table class="table table-bordered table-color table-striped table-danger table-hover js-dataTable-buttons" id="showtable" width="100%">
								<thead class="bg-danger">
									<tr class="text-nowrap">
										<th>S.N</th>
										<th class="text-center">Date</th>
										<th class="text-center">
											Voucher No
										</th>

										<th class="bg-warning text-right">
											Debit(रू)
										</th>
										<th class="bg-success text-right">
											Credit(रू)
										</th>
										<th class="bg-info text-right">Balance(रू)</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td colspan="2">

										</td>
										<td class="text-right">
											Opening Balance
										</td>
										<td>

										</td>
										<td class="text-right">
											0.00
										</td>
										<td class="text-right">
											0.00 CR
										</td>
									</tr>
									<tr>
										<td width="10px">
											1
										</td>
										<td class="text-center">2078-03-29</td>
										<td class="text-sm-center">

											<a link="" class="btn btn-alt-danger" id="voucherno" data-toggle="modal" data-target="#modal-FV-Details" data-voucherdate="2078-03-29" value="2078-03-29">FV0000284</a>

										</td>
										<td class="bg-warning text-right">-</td>
										<td class="bg-success text-right">10000.00</td>
										<td class="bg-info text-right">
											10000.00 Cr                            </td>
										</tr>
										<tr>
											<td width="10px">
												2
											</td>
											<td class="text-center">2078-03-31</td>
											<td class="text-sm-center">

												<a link="" class="btn btn-alt-danger" id="voucherno" data-toggle="modal" data-target="#modal-FV-Details" data-voucherdate="2078-03-31" value="2078-03-31">FV0000286</a>

											</td>
											<td class="bg-warning text-right">-</td>
											<td class="bg-success text-right">15000.00</td>
											<td class="bg-info text-right">
												15000.00 Cr                            </td>
											</tr>
											<tr>
												<td width="10px">
													3
												</td>
												<td class="text-center">2078-04-05</td>
												<td class="text-sm-center">

													<a link="" class="btn btn-alt-danger" id="voucherno" data-toggle="modal" data-target="#modal-FV-Details" data-voucherdate="2078-04-05" value="2078-04-05">JV0000115</a>

												</td>
												<td class="bg-warning text-right">-</td>
												<td class="bg-success text-right">200.00</td>
												<td class="bg-info text-right">
													200.00 Cr                            </td>
												</tr>

											</tbody>
											<tfoot>
												<tr class="table-secondary text-right">
													<td colspan="2"></td>
													<td><strong>Grand Total(रू.)</strong></td>
													<td class="bg-warning text-right">
														<strong>
															<span class="double-underline">0.00</span>
														</strong>
													</td><td class="bg-success text-right">
													<strong>
														<span class="double-underline">25200.00</span>
													</strong>
												</td>
												<td class="bg-info text-right">
													<strong>
														<span class="double-underline">
															25200.00 Cr                            </span>
														</strong>
													</td>
												</tr>
											</tfoot>
										</table>
									</div>

								</div>



							</div>
						</div>
					</div>
				</section>
			</div>

			<div class="modal fade" id="modal-FV-Details" role="dialog">
				<div class="modal-dialog" style="width: 80%">

					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" style="position: static;color: #000">&times;</button>
							<h4 class="modal-title">Details for <span class="FVNumber">FV0000286</span></h4>
						</div>
						<div class="modal-body">
							<div class="table-responsive">
								<table class="table table-bordered table-color table-striped table-danger table-hover js-dataTable-buttons" id="VoucherDetailstable" width="100%">
									<thead class="bg-danger">
										<tr class="text-nowrap">
											<th>S.N</th>
											<th class="text-center">
												Voucher No
											</th>
											<th class="bg-warning text-right">
												Debit(रू)
											</th>
											<th class="bg-success text-right">
												Credit(रू)
											</th>
											<th class="bg-info text-right">Balance(रू)</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td width="10px">
												1
											</td>
											<td class="text-center">
												<a link="" class="btn btn-alt-danger" id="voucherno" data-toggle="modal" data-target="#modal-Voucher-Details">JV0000111</a>
											</td>
											<td class="bg-warning text-right">-</td>
											<td class="bg-success text-right">15000.00</td>
											<td class="bg-info text-right">
												15000.00 Cr                        </td>
											</tr>

										</tbody>
										<tfoot style="display: table-row-group;">
											<tr class="table-secondary">
												<td colspan="1"></td>
												<td class="text-right"><strong>Grand Total(रू.):</strong></td>
												<td class="bg-warning text-right">
													<strong>
														<span class="double-underline">0.00</span>
													</strong>
												</td><td class="bg-success text-right">
												<strong>
													<span class="double-underline">15000.00</span>
												</strong>
											</td>
											<td class="bg-info text-right">
												<strong>
													<span class="double-underline">
														15000.00 Cr                        </span>
													</strong>
												</td>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							</div>
						</div>

					</div>
				</div>


				<style type="text/css">
					span.filter-option.pull-left{
						width: auto !important;
					}

					.btn-group.bootstrap-select.dropup{
						width: 100% !important;
						height: 40px !important;
					}

					.btn-group.bootstrap-select.dropup button{
						line-height: 25px !important;
					}

					.btn.dropdown-toggle.btn-default{
						border: 1px solid #ccc;
						height: 28px;
						padding: 5px;
						line-height: 15px;
						font-size: 14px;
					}
					.btn-group.bootstrap-select{
						width: 100% !important;
					}
					.btn-alt-danger {
						color: #af1e1e;
						background-color: #fbeaea;
						border-color: #fbeaea;
					}
					.double-underline {
    border-top: 1px solid;
    border-bottom: 3px double;
    padding: 4px;
}
				</style>
				<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>
				<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css" rel="stylesheet" />