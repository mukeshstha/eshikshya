<?php
$this->load->helper('account');
$text = getsubsidiary($id);
$CI =& get_instance();
$CI->load->model('Account_category_model');
?>

<div class="content-wrapper" style="min-height: 946px;">
    <section class="content-header">
        <h1>

        </h1>
    </section>

<section class="content">
                            <?php if ($this->session->flashdata('msg')) {?>
                                <?php echo $this->session->flashdata('msg') ?>
                            <?php }?>
        <div class="row">
            <div class="col-md-9 align-center">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-plus-circle"></i><?php echo $this->lang->line('new') . " " . $CI->Account_category_model->getParentname($id); ?></h3>
                        <a class="btn btn-primary btn-sm" style="float: right;" href="<?php echo base_url(); ?>account/chartofaccount/view"><?php echo $this->lang->line('back'); ?></a>

                    </div>
                    <input type="hidden" class="date" name="">
                    <form role="form" id="add_coa" action="<?php echo base_url(); ?>account/chartofaccount/create_subsidiary" class="form-horizontal" method="post">
                        <div class="box-body">
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo $CI->Account_category_model->getParentname($id) . " " . $this->lang->line('name'); ?> &nbsp;<small class="req">* &nbsp;</small></label>
                                <div class="col-md-9">
                                    <input required type="text" class="form-control" id="name" name="name" placeholder="Name" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo $CI->Account_category_model->getParentname($id) . " " . $this->lang->line('category'); ?><small class="req">* &nbsp;</small></label>
                                <div class="col-md-9">
                                    <select required class="custom-select form-control" id="category" name="category">
                                        <option value="">--</option>
                                        <?php foreach ($text["Data"] as $key => $value) : ?>
                                            <option value="<?php echo $value["GLCode"]; ?>" ><?php echo $value["Name"]; ?></option>
                                           <?php endforeach; ?>                                  
                                    </select>
                                    <span class="text-danger"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Sub Category 1 &nbsp;<small class="req">*</small></label>
                                <div class="col-md-9">
                                    <select required class="custom-select form-control" id="subcategory1" name="subcategory1">
                                        <option value="">--</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Sub Category 2 &nbsp;<small class="req">*</small></label>
                                <div class="col-md-9">
                                    <select class="custom-select form-control" id="subcategory2" name="subcategory2">
                                        <option value="">--</option>
                                    </select>
                                    <span class="text-danger"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <button type="submit" name="submit" value="submit" class="btn btn-primary pull-right btn-sm checkbox-toggle"> Save</button>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
<div class="result"></div>

    </section>

    </div>


<script type="text/javascript">
    var baseURL="<?php echo base_url(); ?>";

    $(document).ready(function() {
        $('#category').on('change', function() {
            var category_id = this.value;
            $.ajax({
                url:baseURL + 'account/category/getsubcategory',
                type: "POST",
                data: {
                    category_id: category_id
                },
                cache: false,
                success: function(result) {
                    $("#subcategory1").html(result);
                }
            });
        });

        $('#subcategory1').on('change', function() {
            var category_id = this.value;
            $.ajax({
                url:baseURL + 'account/category/getsubchildcategory',
                type: "POST",
                data: {
                    category_id: category_id
                },
                cache: false,
                success: function(result) {
                    $("#subcategory2").html(result);
                }
            });
        });
    });
</script>