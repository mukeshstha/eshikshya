

 


<div class="content-wrapper" style="min-height: 946px;">
    <section class="content-header">
        <h1>

        </h1>
    </section>

    <section class="content">
    	<div class="row">
            <div class="col-md-12">
                <div class="nav-tabs-custom coalist">
                    <ul class="nav nav-tabs"> 
                    <?php foreach ($text["Data"] as $key => $value) : ?>
                      <?php if ($key==0) : ?>
                        <li class="active"><a data-toggle="tab" href="#<?php echo $value["Name"]; ?>"><?php echo $value["Name"]; ?></a></li>
                      <?php else : ?>
                        
                        <li><a data-toggle="tab" href="#<?php echo $value["Name"]; ?>"><?php echo $value["Name"]; ?></a></li>
                      <?php endif; ?>
                      <?php endforeach; ?>
                    </ul>

                    <div class="tab-content">
                    <?php foreach ($text["Data"] as $key => $value) : ?>
                        <div id="<?php echo $value["Name"]; ?>" class="tab-pane fade in">
                            <a href="<?php echo base_url(); ?>account/chartofaccount/create/<?php echo $value["GLCode"]; ?>" class="btn btn-primary btn-sm">
                                    <i class="fa fa-plus"></i>Add <?php echo $value["Name"]; ?></a>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover example dataTable" cellspacing="0" width="100%" id="DataTables_Table_0">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Category</th>
                                            <th>Sub Category</th>
                                            <th>SubCategory 2</th>
                                            <th>Code</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>

<?php
    $this->load->helper('account');
    $subtext = getsubsidiary($value["GLCode"]);
    ?>


    <?php foreach ($subtext["Data"] as $key => $value1) : ?>

        <?php
            $this->load->helper('account');
            $subtext1 = getsubsidiary($value1["GLCode"]);
            ?>
    


          <?php foreach ($subtext1["Data"] as $key => $value2) : ?>
            <?php
            $this->load->helper('account');
            $subtext2 = getsubsidiary($value2["GLCode"]);
            ?>
                  <?php foreach ($subtext2["Data"] as $key => $value3) : ?>
                    <?php
            $this->load->helper('account');
            $subtext3 = getsubsidiary($value3["GLCode"]);
            ?>
                            <?php foreach ($subtext3["Data"] as $key => $value4) : ?>
                              <tr>
                                <td><?php echo $value4["Name"]; ?></td>
                                <td><?php echo $value1["Name"]; ?></td>
                                <td><?php echo $value2["Name"]; ?></td>
                                <td><?php echo $value3["Name"]; ?></td>
                                <td><?php echo $value4["GLCode"]; ?></td>
                                <td></td>
                              </tr>
                            <?php endforeach; ?>


                    <?php endforeach; ?>
          <?php endforeach; ?>
    <?php endforeach; ?>
                                        <tr></tr>
                                    </tbody>
                                </table>
                              </div>
                        </div>
                      <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>




<script>
$(document).ready(function ($) {
    $("div.tab-pane:first").addClass('active'); // This is the change.
});
</script>