<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <i class="fa fa-gears"></i> <?php echo $this->lang->line('system_settings'); ?><small><?php echo $this->lang->line('setting1'); ?></small>

            </h1>
    </section> 
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="nav-tabs-custom theme-shadow">
                    <div class="box-header with-border">
                       <h3 class="box-title titlefix"></i> <?php echo $this->lang->line('sms_setting'); ?></h3>
                       <small class="pull-right">
                            <a type="button" onclick="sms_test()" class="btn btn-primary btn-sm">SMS Test</a>
                        </small>
                    </div>
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_11" data-toggle="tab"><?php echo $this->lang->line('creation_sms'); ?></a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_11">
                            <form role="form" id="creationsms" action="<?php echo site_url('smsconfig/smscreation') ?>" class="form-horizontal" method="post">
                                <div class="box-body">
                                    <div class="row">
                                        <div class="minheight170">
                                            <div class="col-md-7">
                                                <?php
                                                $creationsms_result = check_in_array('creationsms', $smslist);
                                                ?>

                                                <div class="form-group">
                                                    <label class="col-sm-5 control-label"><?php echo $this->lang->line('creation_sms_token'); ?><small class="req"> *</small></label>
                                                    <div class="col-sm-7">
                                                        <input autofocus="" type="text" class="form-control" name="smscreation_token" value="<?php echo $creationsms_result->api_id; ?>">
                                                        <span class=" text text-danger creationsms_api_id_error"></span>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                   <label class="col-sm-5 control-label">Status<small class="req"> *</small></label>
                                                    <div class="col-sm-7">
                                                        <select class="form-control" name="creationsms_status" autocomplete="off">
                                                            <option value="">Select</option>
                                                            <option value="enabled" <?php if($creationsms_result->is_active=="enabled"){ echo "selected"; } ?> >Enabled</option>
                                                            <option value="disabled" <?php if($creationsms_result->is_active=="disabled"){ echo "selected"; } ?> >Disabled</option>
                                                        </select>
                                                                 
                                                        <span class=" text text-danger smscountry_status_error"></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-5 text text-center disblock">
                                                <a href="https://www.creationsoftnepal.com/" target="_blank"><img src="<?php echo base_url() ?>backend/images/creationsoftlogo.png"><p></p></a>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                <!-- /.box-body -->

                                <div class="box-footer">
                                        <div class="col-md-offset-3">
                                            <?php if ($this->rbac->hasPrivilege('sms_setting', 'can_edit')) {
                                                ?>
                                                <button type="submit" class="btn btn-primary btnleftinfo"><?php echo $this->lang->line('save'); ?></button>&nbsp;&nbsp;<span class="creationsms_loader"></span>
                                            <?php } ?>
                                        </div>       
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- /.tab-content -->
                </div>
            </div>
        </div>  
    </section>
</div>
<div id="myModal" class="modal fade in" role="dialog" aria-hidden="true" >
    <div class="modal-dialog modal-dialog2">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">Test SMS</h4>
            </div>
            <div class="modal-body pt0 pb0">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 paddlr">
                        <div class="">
                            <form id="sendform" style="background: none" action="" name="employeeform" class="form-horizontal form-label-left" method="post" accept-charset="utf-8"> 
                                <div class="">

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="pwd">Mobile Number</label><small class="req"> *</small>  
                                            <input type="text" id="title" autocomplete="off" class="form-control" value="" name="mobile">
                                            <span id="name_add_error" class="text-danger"></span>
                                        </div>

                                    </div>
                                </div>

                        </div><!--./row--> 
                        <div class="box-footer">
                            <div class="pull-right paddA10">

                                <button type="submit" class="btn btn-primary pull-right">Send</button>
                            </div>
                        </div>
                        </form>  
                    </div>                     
                </div><!--./col-md-12-->       

            </div><!--./row--> 

        </div>
    </div>
</div>
</div>
</div>
<?php

function check_in_array($find, $array) {

    foreach ($array as $element) {
        if ($find == $element->type) {
            return $element;
        }
    }
    $object = new stdClass();
    $object->id = "";
    $object->type = "";
    $object->api_id = "";
    $object->username = "";
    $object->url = "";
    $object->name = "";
    $object->contact = "";
    $object->password = "";
    $object->authkey = "";
    $object->senderid = "";
    $object->is_active = "";
    return $object;
}
?>


<script type="text/javascript">
    function sms_test() {
        $('#myModal').modal('show');
    }

    $(document).ready(function (e) {
        $("#sendform").on('submit', (function (e) {
            $.ajax({
                url: '<?php echo base_url() ?>emailconfig/test_sms',
                type: "POST",
                data: new FormData(this),
                dataType: 'json',
                contentType: false,
                cache: false,
                processData: false,
                success: function (data) {
                    if (data.status == "fail") {
                        var message = "";
                        $.each(data.error, function (index, value) {
                            message += value;
                        });
                        errorMsg(message);
                    } else {
                        successMsg(data.message);
                        window.location.reload(true);
                    }
                },
                error: function () {}
            });
        }));
    });

    var img_path = "<?php echo base_url() . '/backend/images/loading.gif' ?>";
    
    $("#creationsms").submit(function (e) {
        $("[class$='_error']").html("");
        $(".creationsms_loader").html('<img src="' + img_path + '">');
        var url = $(this).attr('action'); // the script where you handle the form input.
        $.ajax({
            type: "POST",
            dataType: 'JSON',
            url: url,
            data: $("#creationsms").serialize(), // serializes the form's elements.
            success: function (data, textStatus, jqXHR)
            {
                if (data.st === 1) {
                    $.each(data.msg, function (key, value) {
                        $('.' + key + "_error").html(value);
                    });
                } else {
                    successMsg(data.msg);
                }

                $(".creationsms_loader").html("");

            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                $(".creationsms_loader").html("");
                //if fails      
            }
        });

        e.preventDefault(); // avoid to execute the actual submit of the form.
    });
</script>


