<style type="text/css">
    @media print
    {
        .no-print, .no-print *
        {
            display: none !important;
        }
    }
</style>
<div class="content-wrapper" style="min-height: 946px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-credit-card"></i> <?php echo $this->lang->line('expenses'); ?> <small><?php echo $this->lang->line('student_fee'); ?></small>        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <?php
            if ($this->rbac->hasPrivilege('expense_head', 'can_add')) {
                ?>
                <div class="col-md-4">
                    <!-- Horizontal Form -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title"><?php echo $this->lang->line('expense_head'); ?></h3>
                        </div><!-- /.box-header -->
                        <!-- form start -->
                        <form id="form1" action="<?php echo site_url('admin/expensehead/create') ?>"  id="employeeform" name="employeeform" method="post" accept-charset="utf-8">
                            <div class="box-body">                            
                                <?php if ($this->session->flashdata('msg')) { ?>
                                    <?php echo $this->session->flashdata('msg') ?>
                                <?php } ?>
                                <?php echo $this->customlib->getCSRF(); ?>
                                <div class="form-group">
                                    <label for="exampleInputEmail1"><?php echo "Main Category" ?></label><small class="req"> *</small>
                                    
                                    <div class="category-accordion">
                                    <div id="accordion-0"  role="tablist" aria-multiselectable="true">
                                        <?php foreach ($expenditurelist["Data"] as $key => $menu) { ?>
                                        <div class="panel" data-parent="0">
                                            <label>
                                                <input type="radio" name="GLCode"  id="parent-<?php echo $menu["Id"]; ?>" value="<?php echo $menu["GLCode"]; ?>" class="toggle-accordion" aria-controls="collapse-<?php echo $menu["Id"]; ?>">
                                                <span role="tab" id="heading-<?php echo $menu["Id"]; ?>" class="tab">

                                                    <span class="check-category" role="button" data-toggle="collapse" data-parent="#accordion-0" href="#collapse-<?php echo $menu["Id"]; ?>" aria-controls="collapse-<?php echo $menu["Id"]; ?>">
                                                        <?php echo $menu["Name"]; ?> 
                                                    </span>
                                                </span>
                                            </label>
                                            <div id="collapse-<?php echo $menu["Id"]; ?>" class="panel-collapse collapse " role="tabpanel" aria-labelledby="heading-<?php echo $menu["Id"]; ?>">
                                                <div id="accordion-<?php echo $menu["Id"]; ?>" style="margin-left: 40px;" role="tablist" aria-multiselectable="true">  
                                                    <?php
                                                    $this->load->helper('account');
                                                    $subtext = getsubsidiary($menu["GLCode"]);
                                                    ?>
                                                    <?php foreach ($subtext["Data"] as $key => $menu1) { ?>
                                                    <div class="panel" data-parent="<?php echo $menu1["Id"]; ?>">
                                                        <label>
                                                            <?php if($menu1['IsGlSubSidiary']==false) : ?>
                                                                <input type="radio" name="GLCode"  id="parent-<?php echo $menu1["Id"]; ?>" value="<?php echo $menu1["GLCode"]; ?>" class="toggle-accordion">
                                                            <?php else : ?>
                                                                <i class="fa fa-arrow-right"></i>
                                                            <?php endif; ?>
                                                            <span role="tab" id="heading-<?php echo $menu1["Id"]; ?>" class="tab">
                                                                <span class="check-category" role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapse-<?php echo $menu1["Id"]; ?>" aria-controls="collapse-<?php echo $menu1["Id"]; ?>">
                                                                    <?php echo $menu1["Name"]; ?>
                                                                </span>
                                                            </span>
                                                        </label>
                                                        <div id="collapse-<?php echo $menu1["Id"]; ?>" class="panel-collapse collapse " role="tabpanel" aria-labelledby="heading-<?php echo $menu1["Id"]; ?>">
                                                            <div id="accordion-<?php echo $menu1["Id"]; ?>" style="margin-left: 40px;" role="tablist" aria-multiselectable="true">
                                                                <?php
                                                                $this->load->helper('account');
                                                                $subtext1 = getsubsidiary($menu1["GLCode"]);
                                                                ?>
                                                                <?php foreach ($subtext1["Data"] as $key => $menu2) { ?>
                                                                <div class="panel">
                                                                    <label for="parent-<?php echo $menu2['Id']; ?>">
                                                                        <?php if($menu2['IsGlSubSidiary']==false) : ?>
                                                                            <input type="radio" name="GLCode"  id="parent-<?php echo $menu2['Id']; ?>" value="<?php echo $menu2['GLCode']; ?>">
                                                                        <?php else : ?>
                                                                            <i class="fa fa-arrow-right"></i>
                                                                        <?php endif; ?>
                                                                        <span role="tab" id="heading-<?php echo $menu2["Id"]; ?>" class="tab">
                                                                            <span class="check-category" role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapse-<?php echo $menu2["Id"]; ?>" aria-controls="collapse-<?php echo $menu2["Id"]; ?>">
                                                                                <?php echo $menu2["Name"]; ?>
                                                                            </span>
                                                                        </span>
                                                                    </label>
                                                                    <div id="collapse-<?php echo $menu2["Id"]; ?>" class="panel-collapse collapse " role="tabpanel" aria-labelledby="heading-<?php echo $menu2["Id"]; ?>">
                                                                        <div id="accordion-<?php echo $menu2["Id"]; ?>" style="margin-left: 40px;" role="tablist" aria-multiselectable="true">
                                                                            <?php
                                                                            $this->load->helper('account');
                                                                            $subtext2 = getsubsidiary($menu2["GLCode"]);
                                                                            ?>
                                                                            <?php foreach ($subtext2["Data"] as $key => $menu3) { ?>
                                                                            <div class="panel">
                                                                                <label for="parent-<?php echo $menu3['Id']; ?>">
                                                                                    <?php if($menu3['IsGlSubSidiary']==false) : ?>
                                                                                        <input type="radio" name="GLCode"  id="parent-<?php echo $menu3['Id']; ?>" value="<?php echo $menu3['GLCode']; ?>">
                                                                                    <?php else : ?>
                                                                                        <i class="fa fa-arrow-right"></i>
                                                                                    <?php endif; ?>
                                                                                    <span role="tab" id="heading-<?php echo $menu3["Id"]; ?>" class="tab">
                                                                                        <span class="check-category" role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapse-<?php echo $menu3["Id"]; ?>" aria-controls="collapse-<?php echo $menu3["Id"]; ?>">
                                                                                            <?php echo $menu3["Name"]; ?>
                                                                                        </span>
                                                                                    </span>
                                                                                </label>
                                                                                <div id="collapse-<?php echo $menu3["Id"]; ?>" class="panel-collapse collapse " role="tabpanel" aria-labelledby="heading-<?php echo $menu3["Id"]; ?>">
                                                                                    <div id="accordion-<?php echo $menu3["Id"]; ?>" style="margin-left: 40px;" role="tablist" aria-multiselectable="true">
                                                                                        <?php
                                                                                        $this->load->helper('account');
                                                                                        $subtext3 = getsubsidiary($menu3["GLCode"]);
                                                                                        ?>
                                                                                        <?php foreach ($subtext3["Data"] as $key => $menu4) { ?>
                                                                                        <div class="panel">
                                                                                            <label for="parent-<?php echo $menu4['Id']; ?>">
                                                                                                <?php if($menu4['IsGlSubSidiary']==false) : ?>
                                                                                                    <input type="radio" name="GLCode"  id="parent-<?php echo $menu4['Id']; ?>" value="<?php echo $menu4['GLCode']; ?>">
                                                                                                <?php else : ?>
                                                                                                    <i class="fa fa-arrow-right"></i>
                                                                                                <?php endif; ?>
                                                                                                <span role="tab" id="heading-<?php echo $menu4["Id"]; ?>" class="tab">
                                                                                                    <span class="check-category" role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapse-<?php echo $menu4["Id"]; ?>" aria-controls="collapse-<?php echo $menu4["Id"]; ?>">
                                                                                                        <?php echo $menu4["Name"]; ?>
                                                                                                    </span>
                                                                                                </span>
                                                                                            </label>
                                                                                        </div>
                                                                                    <?php } ?>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <?php } ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                        <?php  } ?>
                                        <span style="color: red"><?php echo form_error('GLCode') ?></span>
                                    </div>  
                                </div>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1"><?php echo $this->lang->line('expense_head'); ?></label><small class="req"> *</small>
                                    <input autofocus="" id="expensehead" name="expensehead" placeholder="" type="text" class="form-control"  value="<?php echo set_value('expensehead'); ?>" />
                                    <span class="text-danger"><?php echo form_error('expensehead'); ?></span>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1"><?php echo $this->lang->line('description'); ?></label>
                                    <textarea class="form-control" id="description" name="description" placeholder="" rows="3" placeholder="Enter ..."><?php echo set_value('description'); ?></textarea>
                                    <span class="text-danger"><?php echo form_error('description'); ?></span>
                                </div>
                            </div><!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary pull-right"><?php echo $this->lang->line('save'); ?></button>
                            </div>
                        </form>
                    </div>            
                </div><!--/.col (right) -->
                <!-- left column -->
            <?php } ?>
            <div class="col-md-<?php
            if ($this->rbac->hasPrivilege('expense_head', 'can_add')) {
                echo "8";
            } else {
                echo "12";
            }
            ?>">
                <!-- general form elements -->
                <div class="box box-primary" id="exphead">
                    <div class="box-header ptbnull">
                        <h3 class="box-title titlefix"><?php echo $this->lang->line('expense_head_list'); ?></h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive mailbox-messages">
                            <div class="download_label"><?php echo $this->lang->line('expense_head_list'); ?></div>
                           <table class="table table-striped table-bordered table-hover expense-head-list" data-export-title="<?php echo $this->lang->line('expense_head_list'); ?>">
                                <thead>
                                    <tr>
                                        <th><?php echo $this->lang->line('expense_head'); ?></th>
                                        <th class="text-right noExport"><?php echo $this->lang->line('action'); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                             

                                </tbody>
                            </table><!-- /.table -->
                        </div><!-- /.mail-box-messages -->
                    </div><!-- /.box-body -->
                </div>
            </div>

            <!-- right column -->

        </div>   <!-- /.row -->
    </section><!-- /.content -->
</div>

<script type="text/javascript">
    $(document).ready(function() {
      initDatatable('expense-head-list','admin/expensehead/ajaxSearch',[],[],100);
  });
</script>