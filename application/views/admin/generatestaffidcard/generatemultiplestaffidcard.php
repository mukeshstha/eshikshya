<style type="text/css">
    @media print {
      .page-break { display: block; page-break-before: always; }


      .col-sm-8 {
            width: 66.66666667%;
             float: left;
        }
        .col-sm-4 {
            width: 33.33333333%;
             float: left;
        }
        .col-sm-6 {
            width: 47%;
             float: left;
        }
        .col-sm-3 {
            width: 25%;
            float: left;
        }
        .col-sm-9 {
            width: 75%;
            float: left;
        }

        .last-div{
            position: absolute !important;
            bottom: -5px;
            right: 5px;
        }
    
    *{ margin:0; padding: 0;}
    table{ font-family: 'arial'; margin:0; padding: 0;font-size: 12px; color: #000;}
    .tc-container{width: 100%;position: relative; text-align: center;margin-bottom:60px;padding-bottom: 10px;}
    .tcmybg {
        background: top center;
        background-size: contain;
        position: absolute;
        left: 0;
        bottom: 10px;
        width: 160px;
        height: 160px;
        margin-left: auto;
        margin-right: auto;
        right: 0;
        opacity: 0.10;
    }
    /*begin students id card*/
    .studentmain{background: #efefef;width: 100%; margin-bottom: 30px;}
    .studenttop img{width:30px;vertical-align: middle;}
    .studenttop{background: #453278;padding:2px;color: #fff;overflow: hidden;position: relative;z-index: 1;}
    .sttext1{font-size: 16px;font-weight: bold;line-height: normal;}
    .stgray{background: #efefef;padding-top: 5px; padding-bottom: 10px;}
    .staddress{margin-bottom: 0; padding-top: 2px;}
    .stdivider{border-bottom: 2px solid #000;margin-top: 5px; margin-bottom: 5px;}
    .stlist{padding: 0; margin:0; list-style: none;}
    .stlist li{text-align: left;display: inline-block;width: 100%;padding: 0px 5px;}
    .stlist li span{width:65%;float: right;}
    .stimg{width: 80px;height: auto;}
    .stimg img{width: 100%;height: auto;border-radius: 2px;display: block;}
    .img-circle {border-radius:16px;}
    .center-block {display: block;margin-right: auto;margin-left: auto;}
    .staround{padding:3px 10px 3px 0;position: relative;overflow: hidden;}
    .staround2{position: relative; z-index: 9;}
    .stbottom{background: #453278;height: 20px;width: 100%;clear: both;margin-bottom: 5px;}
    .principal{margin-top: -40px;margin-right:10px; float:right;}
    .stred{color: #000;}
    .spanlr{padding-left: 5px; padding-right: 5px;}
    .cardleft{width: 20%;float: left;}
    .cardright{width: 77%;float: right; }
    .width32{width: 32.55%; padding: 3px; float: left;}
    .signature{border:1px solid #ddd; display:block; text-align: center; padding: 5px 20px; margin-top: 5px;}
    .vertlist{padding: 0; margin:0; list-style: none;}
    .vertlist li{text-align: left;display: inline-block;width: 100%;color: #000;padding-bottom: 2px;}
    .vertlist li span{width:55%;float: right;}
}
</style>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>backend/bootstrap/css/bootstrap.min.css">
<?php $i=0; ?>

<?php 
if($id_card[0]->enable_vertical_card)
{
    ?>



        <style type="text/css">
            @media print {
                <?php if(isset($id_card[0]->background)): ?>
                .background-div:before{
                    opacity: 0.6;
                    background-image: url(<?php echo base_url('uploads/'.$_SERVER['SERVER_NAME'].'/student_id_card/background/'.$id_card[0]->background) ?>);
                    background-repeat: no-repeat;
                    background-size: cover;
                    position: absolute;
                    height: 100%;
                    width: 100%;
                }
                <?php endif; ?>
            .img-cover{
                width: 50px;
                height: 50px;
                overflow: hidden;
                border: 1px solid #000;
                margin: auto;
                border:3px solid <?php echo $id_card[0]->header_color; ?>;
                border-radius: 10px;
            }
            .background-div{
                position: relative;
                height: 324px !important;
                width: 204px !important;
            }
            :before{
                display: table;
                content: " ";
            }
            .back-img{
                position: absolute;
                z-index: 1;
                height: auto;
                width: 100%;
                opacity: 0.5
            }
            .text-center{
                text-align: center;
            }
        }
        </style>
    
    <div class="row">
<?php
$i= 0;
        foreach ($staffs as $staff_key => $staff_value) {
            $i++;
            ?>
    <div class="col-sm-6 background-div" style="margin: 10px;border: 1px solid;border-radius: 15px;overflow: hidden;padding-top: 15px">
        <?php if(isset($id_card[0]->background)): ?>
            <img class="back-img" src="<?php echo base_url('uploads/'.$_SERVER['SERVER_NAME'].'/staff_id_card/background/') ?><?php echo $id_card[0]->background ?>">
        <?php endif; ?>
        <div class="col-sm-12" style="background: <?php echo $id_card[0]->header_color; ?>;z-index: 99">
            <div class="col-sm-12" style="display: table-cell;vertical-align: middle;text-align: center">
                <img src="<?php echo base_url('uploads/'.$_SERVER['SERVER_NAME'].'/staff_id_card/logo/'.$id_card[0]->logo); ?>" width="30%">
            </div>
            <div class="col-sm-12 text-center">
                <h4 style="font-weight: bold;font-size: 10px"><?php echo $id_card[0]->school_name; ?></h4>
           </div>
        </div>
        <div class="col-sm-12 text-center" style="z-index: 99">
            <div class="img-cover">
            <img src="<?php
                        if (!empty($staff_value->image)) {
                            echo base_url() . "uploads/".$_SERVER['SERVER_NAME']."/staff_images/". $staff_value->image;
                        } else {

                            if ($staff_value->gender == 'Female') {
                                echo base_url() . "uploads/".$_SERVER['SERVER_NAME']."/staff_images/default_female.jpg";
                            } elseif ($staff_value->gender == 'Male') {
                                echo base_url() . "uploads/".$_SERVER['SERVER_NAME']."/staff_images/default_male.jpg";
                            }

                        }
                        ?>" class="img-responsive img-circles block-center" style="max-width: 100%">
                    </div>
            <h4 style="margin:0; text-transform: uppercase;font-weight: bold; margin-top: 10px;font-size: 8px"><?php echo $staff_value->name." ".$staff_value->surname; ?></h4>
            <?php if ($id_card[0]->enable_designation == 1) { ?>
                <p style="font-size: 8px;color: #9b1818;line-height: 8px;margin:0px"><?php echo $staff_value->designation; ?></p>
            <?php } ?>  
        </div>
        <div class="col-sm-12" style="z-index: 99">
            <ul class="vertlist" style="margin : 5px 5px;font-size: 9px; font-weight: bold">
                <?php if ($id_card[0]->enable_name == 1) { ?>
                <!-- <li><?php //echo $this->lang->line('staff'); ?> <?php //echo $this->lang->line('name'); ?><span> <?php //echo $staff_value->name; ?> <?php //echo $staff_value->surname; ?></span></li>--><?php } ?> 
                <?php if ($id_card[0]->enable_staff_id == 1) { ?><li><?php echo $this->lang->line('staff_id'); ?><span> <?php echo $staff_value->employee_id; ?></span></li><?php } ?>
              
                <?php if ($id_card[0]->enable_staff_department == 1) { ?><li><?php echo $this->lang->line('department'); ?><span> <?php echo $staff_value->department; ?></span></li><?php } ?>
                 <?php if ($id_card[0]->enable_fathers_name == 1) { ?><li><?php echo $this->lang->line('father_name'); ?><span><?php echo $staff_value->father_name; ?></span></li><?php } ?>
                <?php if ($id_card[0]->enable_mothers_name == 1) { ?><li><?php echo $this->lang->line('mother_name'); ?><span><?php echo $staff_value->mother_name; ?></span></li><?php } ?>
                <?php if ($id_card[0]->enable_date_of_joining == 1) { ?><li><?php echo $this->lang->line('date_of_joining'); ?><span><?php if(!empty($staff_value->date_of_joining) && $staff_value->date_of_joining !='0000-00-00'){echo date($this->customlib->getSchoolDateFormat(), $this->customlib->dateYYYYMMDDtoStrtotime($staff_value->date_of_joining));} ?></span></li><?php } ?>
                <?php if ($id_card[0]->enable_permanent_address == 1) { ?><li class="stred"><?php echo $this->lang->line('address'); ?><span><?php echo $staff_value->local_address; ?></span></li><?php } ?>
                <?php if ($id_card[0]->enable_staff_phone == 1) { ?><li><?php echo $this->lang->line('phone'); ?><span><?php echo $staff_value->contact_no; ?></span></li><?php } ?>
                <?php
                if ($id_card[0]->enable_staff_dob == 1) {
                    ?>
                    <li><?php echo $this->lang->line('date_of_birth'); ?>
                        <span>
                            <?php
                            echo $dob = "";
                            if ($staff_value->dob != "0000-00-00") {
                                $dob = date($this->customlib->getSchoolDateFormat(), $this->customlib->dateYYYYMMDDtoStrtotime($staff_value->dob));
                            }
                            echo $dob;
                            ?>
                        </span></li>
                    <?php
                }
                ?>
            </ul>
        </div>
        <div class="col-sm-12 last-div" style="margin-top: 0px;z-index: 99">
            <div class="col-sm-8">
            </div>
            <div class="col-sm-4 text-center">
                <img src="<?php echo base_url('uploads/'.$_SERVER['SERVER_NAME'].'/staff_id_card/signature/') ?><?php echo $id_card[0]->sign_image; ?>" style="width:80%" style="margin-top: -30px"/><br>
                <hr style="margin-top: 5px;margin-bottom: 3px;border: 0;border-top: 2px solid #000;">
                <h5 style="font-size: 10px">Principal</h5>

            </div>
        </div>
    </div>

<?php if($i % 9 == 0){ ?>
    <hr >
    <div class="page-break"></div>
<?php $i = 0;} } ?>
</div>


<?php
}else{
?>



        <style type="text/css">
            @media print {
            .img-cover{
                overflow: hidden;border: 1px solid #000;
                margin: auto;
                border:3px solid <?php echo $id_card[0]->header_color; ?>;
                border-radius: 10px;
                min-height: 70px
            }
            .background-div{
                position: relative;
                height: 204px !important;
                width: 324px !important;
            }
            :before{
                display: table;
                content: " ";
            }
            .back-img{
                position: absolute;
                z-index: 1;
                height: auto;
                width: 100%;
                opacity: 0.5
            }
        }
        </style>
    
<?php if(isset($id_card[0]->background)): ?>
        <style type="text/css">
            .background-div:before{
                opacity: 0.6;
                background-image: url(<?php echo base_url('uploads/'.$_SERVER['SERVER_NAME'].'/student_id_card/background/') ?><?php echo $id_card[0]->background ?>);
                background-repeat: no-repeat;
                background-size: cover;
                position: absolute;
                height: 100%;
                width: 100%;
            }
        </style>
    <?php endif; ?>

<?php
        foreach ($staffs as $staff_key => $staff_value) {
            $i++;
            ?>
            <div class="col-sm-6">
    <div class="row background-div" style="border: 1px solid;border-radius: 15px;overflow: hidden;position: relative;margin-bottom: 5px">
        <?php if(isset($id_card[0]->background)): ?>
            <img class="back-img" src="<?php echo base_url('uploads/'.$_SERVER['SERVER_NAME'].'/staff_id_card/background/') ?><?php echo $id_card[0]->background ?>">
        <?php endif; ?>
        <div class="col-sm-12" style="margin-top: 10px;z-index: 99">
            <div class="col-sm-3 text-center">
                <img src="<?php echo base_url('uploads/'.$_SERVER['SERVER_NAME'].'/staff_id_card/logo/'.$id_card[0]->logo); ?>" />
            </div>
            <div class="col-sm-9 text-center">
                <h4 style="font-weight: bold;font-size: 10px;line-height: 12px;margin: 0"><?php echo $id_card[0]->school_name; ?></h4>
                <p style="font-size: 8px;line-height: 8px"><?php echo $id_card[0]->school_address; ?></p>
            </div>
        </div>
        <div class="col-sm-12" style="margin-top: 10px;z-index: 99">
            <div class="col-sm-3">
                <figure class="img-cover">
                    <img src="<?php
                        if (!empty($staff_value->image)) {
                            echo base_url() . "uploads/".$_SERVER['SERVER_NAME']."/staff_images/". $staff_value->image;
                        } else {

                            if ($staff_value->gender == 'Female') {
                                echo base_url() . "uploads/".$_SERVER['SERVER_NAME']."/staff_images/default_female.jpg";
                            } elseif ($staff_value->gender == 'Male') {
                                echo base_url() . "uploads/".$_SERVER['SERVER_NAME']."/staff_images/default_male.jpg";
                            }

                        }
                        ?>" class="img-responsive" />
                </figure>
            </div>
            <div class="col-sm-9">
                <ul class="vertlist" style="margin : 0px 10px;font-size: 10px; font-weight: bold">
                    <?php if ($id_card[0]->enable_name == 1) { ?>
                    <!-- <li><?php //echo $this->lang->line('staff'); ?> <?php //echo $this->lang->line('name'); ?><span> <?php //echo $staff_value->name; ?> <?php //echo $staff_value->surname; ?></span></li>--><?php } ?> 
                    <?php if ($id_card[0]->enable_staff_id == 1) { ?><li><?php echo $this->lang->line('staff_id'); ?><span> <?php echo $staff_value->employee_id; ?></span></li><?php } ?>
                  
                    <?php if ($id_card[0]->enable_staff_department == 1) { ?><li><?php echo $this->lang->line('department'); ?><span> <?php echo $staff_value->department; ?></span></li><?php } ?>
                     <?php if ($id_card[0]->enable_fathers_name == 1) { ?><li><?php echo $this->lang->line('father_name'); ?><span><?php echo $staff_value->father_name; ?></span></li><?php } ?>
                    <?php if ($id_card[0]->enable_mothers_name == 1) { ?><li><?php echo $this->lang->line('mother_name'); ?><span><?php echo $staff_value->mother_name; ?></span></li><?php } ?>
                    <?php if ($id_card[0]->enable_date_of_joining == 1) { ?><li><?php echo $this->lang->line('date_of_joining'); ?><span><?php if(!empty($staff_value->date_of_joining) && $staff_value->date_of_joining !='0000-00-00'){echo date($this->customlib->getSchoolDateFormat(), $this->customlib->dateYYYYMMDDtoStrtotime($staff_value->date_of_joining));} ?></span></li><?php } ?>
                    <?php if ($id_card[0]->enable_permanent_address == 1) { ?><li class="stred"><?php echo $this->lang->line('address'); ?><span><?php echo $staff_value->local_address; ?></span></li><?php } ?>
                    <?php if ($id_card[0]->enable_staff_phone == 1) { ?><li><?php echo $this->lang->line('phone'); ?><span><?php echo $staff_value->contact_no; ?></span></li><?php } ?>
                    <?php
                    if ($id_card[0]->enable_staff_dob == 1) {
                        ?>
                        <li><?php echo $this->lang->line('date_of_birth'); ?>
                            <span>
                                <?php
                                echo $dob = "";
                                if ($staff_value->dob != "0000-00-00") {
                                    $dob = date($this->customlib->getSchoolDateFormat(), $this->customlib->dateYYYYMMDDtoStrtotime($staff_value->dob));
                                }
                                echo $dob;
                                ?>
                            </span></li>
                        <?php
                    }
                    ?>
                </ul>
            </div>
        </div>
        <div class="col-sm-12 last-div" style="z-index: 99">
            <div class="col-sm-9">
            </div>
            <div class="col-sm-3 text-center">
                <img src="<?php echo base_url('uploads/'.$_SERVER['SERVER_NAME'].'/staff_id_card/signature/') ?><?php echo $id_card[0]->sign_image; ?>" style="width:80%;margin-top: -30px" /><br>
                <hr style="margin-top: 0px;margin-bottom: 0px;border: 0;border-top: 2px solid #000;">
                <h5 style="margin-top: 2px">Principal</h5>

            </div>
        </div>
    </div>
</div>
<?php if($i % 10 == 0){ ?>
    <hr >
    <div class="page-break"></div>
<?php $i = 0;} } ?>
<?php
}

 ?>
