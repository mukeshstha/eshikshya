<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <link rel="icon" type="image/png" href="assets/img/s-favican.png">
        <meta http-equiv="X-UA-Compatible" content="" />

        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="theme-color" content="" />
        <style type="text/css">
            *{padding: 0; margin:0;}
            .body{padding: 0; margin:0; font-family: arial; color: #000; font-size: 14px; line-height: normal;}
            .tableone{}
            .tableone td{border:1px solid #000; padding: 5px 0}
            .denifittable th{border-top: 1px solid #999;}
            .denifittable th,
            .denifittable td {border-bottom: 1px solid #999;
                              border-collapse: collapse;border-left: 1px solid #999;}
            .denifittable tr th {padding: 10px 10px; font-weight: bold;}
            .denifittable tr td {padding: 10px 10px; font-weight: bold;}
            .tcmybg {
                background:top center;
                background-size: 100% 100%;
                position: absolute;
                top: 0;
                left: 0;
                bottom: 0;
                z-index: 0;
            }
            .table.table-bordered td{
                border: 1px solid #000;
            }
        </style>
    </head>
    <body class="body">
        <?php
        if ($marksheet->background_img != "") {
            ?> 
            <img src="<?php echo base_url('uploads/marksheet/' . $marksheet->background_img); ?>" class="tcmybg" width="100%" height="100%" />
            <?php
        }
        ?>

        <div style="width: 100%; margin: 0 auto; border:1px solid #000; padding: 10px 15px 10px;position: relative;">
            <div class="row">
                <div class="col-sm-3" style="text-align:right">
                    <?php
                        if ($marksheet->left_logo != "") {
                            ?>
                            <img src="<?php echo base_url('uploads/marksheet/' . $marksheet->left_logo); ?>" width="250px"> 


                            <?php
                        }
                    ?>
                </div>
                
                <div class="col-sm-7 text-center">
                <?php
                if ($marksheet->school_name != "" || $marksheet->exam_center != "") {
                    ?>
                     <?php
                        if ($marksheet->school_name != "") {
                            ?>
                        <p style="font-size: 42px; font-weight: bold; text-align: center;"><?php echo $marksheet->school_name; ?></p>
                    <?php } ?>
                    <?php
                        if ($marksheet->exam_center != "") {
                            ?>
                        <p style="font-size: 20px; font-weight: 900; text-align: center; text-transform: uppercase;"><?php echo $marksheet->exam_center; ?></p>
                    <?php } ?>
                <?php } ?>
                 <?php
                    if ($marksheet->exam_name != "") {
                        ?>
                            <p style="font-size: 20px; font-weight: bold; text-align: center; text-transform: uppercase;">
                                <?php echo $marksheet->exam_name; ?></p>
                        <?php
                    }
                    ?>
                </div>
                <div class="col-sm-2">
                </div>
           
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="col-md-3">
                    </div>
                    <div class="col-md-6">
                        <p style="text-transform:uppercase; font-weight:bold;background: #e5e5e5; padding: 10px 10px;text-align: center;"><?php echo $this->lang->line('progressive_report_card'); ?></p>
                    </div>
                    <div class="col-md-3">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-8">
                    <?php if ($marksheet->is_name) { ?>
                        <p style="text-transform: uppercase;"><?php echo $this->lang->line('name'); ?> :<span style="padding-left: 30px; font-weight: bold;">Student Name</span></p>
                    <?php } ?>

                    <?php if ($marksheet->is_class || $marksheet->is_section) { ?>
                        <p style="text-transform: uppercase;"><?php echo $this->lang->line('class'); ?> :<span style="padding-left: 30px; font-weight: bold;">
                            <?php
                                if ($marksheet->is_class && $marksheet->is_section) {
                                    ?>
                                    1 (A)
                                    <?php
                                } elseif ($marksheet->is_class) {
                                    ?>
                                    1
                                    <?php
                                } elseif ($marksheet->is_section) {
                                    ?>
                                    (A)
                                    <?php
                                }
                                ?>
                        </span></p>
                    <?php } ?>

                    <?php if ($marksheet->is_father_name) { ?>
                        <p style="text-transform: uppercase;"><?php echo $this->lang->line('father_name'); ?> :<span style="padding-left: 30px; font-weight: bold;">Father Name</span></p>
                    <?php } ?>

                    <?php if ($marksheet->is_mother_name) { ?>
                        <p style="text-transform: uppercase;"><?php echo $this->lang->line('mother_name'); ?> :<span style="padding-left: 30px; font-weight: bold;">Mother Name</span></p>
                    <?php } ?>
                </div>

                <div class="col-sm-4">
                    <?php if ($marksheet->is_roll_no) { ?>
                        <p style="text-transform: uppercase;"><?php echo $this->lang->line('roll_no') ?> :<span style="padding-left: 30px; font-weight: bold;">XXXXX</span></p>
                    <?php } ?>

                    <?php if ($marksheet->is_dob) { ?>
                        <p style="text-transform: uppercase;"><?php echo $this->lang->line('date') . " " . $this->lang->line('of') . " " . $this->lang->line('birth'); ?> :<span style="padding-left: 30px; font-weight: bold;">8/10/2049</span></p>
                    <?php } ?>

                    <?php if ($marksheet->is_admission_no) { ?>
                        <p style="text-transform: uppercase;"><?php echo $this->lang->line('admission_no') ?> :<span style="padding-left: 30px; font-weight: bold;">XXXXX</span></p>
                    <?php } ?>


                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <table cellpadding="0" cellspacing="0" width="100%" class="denifittable" style="text-align: center; text-transform: uppercase;">
                        <thead>
                        <tr>
                            <th rowspan="2" valign="middle" style="text-align: center;"><?php echo $this->lang->line('s_no') ?></th>
                            <th rowspan="2" valign="middle" width="35%"><?php echo $this->lang->line('subjects') ?></th>
                            <th rowspan="2" valign="middle" style="text-align: center;"><?php echo $this->lang->line('full') . " " . $this->lang->line('marks') ?></th>
                            <!-- <th valign="middle" style="text-align: center;"><?php echo $this->lang->line('min') . " " . $this->lang->line('marks') ?></th> -->
                            <th colspan="2" valign="top" style="text-align: center;"><?php echo $this->lang->line('marks') . " " . $this->lang->line('obtained') ?></th>
                            <th rowspan="2" valign="middle" style="border-right:1px solid #999; text-align: center;"><?php echo $this->lang->line('total') . " " . $this->lang->line('marks') ?></th>
                            <th rowspan="2" valign="middle" style="border-right:1px solid #999; text-align: center;"><?php echo $this->lang->line('grade') ?></th>
                            <th rowspan="2" valign="middle" style="border-right:1px solid #999; text-align: center;"><?php echo $this->lang->line('grade') . "" . $this->lang->line('point') ?></th>
                        </tr>
                        <tr>
                            <th><?php echo $this->lang->line('practical'); ?></th>
                            <th><?php echo $this->lang->line('theory'); ?></th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td valign="top" style="text-align: center;">1.</td>
                                <td valign="top" style="text-align: left;">Nepali</td>
                                <td valign="top" style="text-align: center;">100</td>
                                <td valign="top" style="text-align: center;">75</td>
                                <td valign="top" style="text-align: center;">25</td>
                                <td valign="top" style="text-align: center;">85</td>
                                <td valign="top" style="text-align: center;">A+</td>
                                <td valign="top" style="text-align: center;border-right:1px solid #999;">4.0</td>
                            </tr>
                            <tr>
                                <td valign="top" style="text-align: center;">2.</td>
                                <td valign="top" style="text-align: left;">English</td>
                                <td valign="top" style="text-align: center;">100</td>
                                <td valign="top" style="text-align: center;">75</td>
                                <td valign="top" style="text-align: center;">25</td>
                                <td valign="top" style="text-align: center;">85</td>
                                <td valign="top" style="text-align: center;">A+</td>
                                <td valign="top" style="text-align: center;border-right:1px solid #999;">4.0</td>
                            </tr>
                            <tr>
                                <td valign="top" style="text-align: center;">3.</td>
                                <td valign="top" style="text-align: left;">Science</td>
                                <td valign="top" style="text-align: center;">100</td>
                                <td valign="top" style="text-align: center;">75</td>
                                <td valign="top" style="text-align: center;">25</td>
                                <td valign="top" style="text-align: center;">85</td>
                                <td valign="top" style="text-align: center;">A+</td>
                                <td valign="top" style="text-align: center;border-right:1px solid #999;">4.0</td>
                            </tr>
                            <tr>
                                <td valign="top" style="text-align: center;">4.</td>
                                <td valign="top" style="text-align: left;">Health</td>
                                <td valign="top" style="text-align: center;">100</td>
                                <td valign="top" style="text-align: center;">75</td>
                                <td valign="top" style="text-align: center;">25</td>
                                <td valign="top" style="text-align: center;">85</td>
                                <td valign="top" style="text-align: center;">A+</td>
                                <td valign="top" style="text-align: center;border-right:1px solid #999;">4.0</td>
                            </tr>
                            <tr>
                                <td valign="top" style="text-align: center;">5.</td>
                                <td valign="top" style="text-align: left;">Math</td>
                                <td valign="top" style="text-align: center;">100</td>
                                <td valign="top" style="text-align: center;">75</td>
                                <td valign="top" style="text-align: center;">25</td>
                                <td valign="top" style="text-align: center;">85</td>
                                <td valign="top" style="text-align: center;">A+</td>
                                <td valign="top" style="text-align: center;border-right:1px solid #999;">4.0</td>
                            </tr>
                            <tr>
                                <td valign="top" style="text-align: center;">7.</td>
                                <td valign="top" style="text-align: left;">Social</td>
                                <td valign="top" style="text-align: center;">100</td>
                                <td valign="top" style="text-align: center;">75</td>
                                <td valign="top" style="text-align: center;">25</td>
                                <td valign="top" style="text-align: center;">85</td>
                                <td valign="top" style="text-align: center;">A+</td>
                                <td valign="top" style="text-align: center;border-right:1px solid #999;">4.0</td>
                            </tr>
                            <tr style="border-right: 1px solid #000;">
                                <td>&nbsp;</td><td></td><td></td><td></td><td></td><td></td><td></td><td>&nbsp;</td>
                            </tr>
                            <tr style="border-right: 1px solid #000;padding-right: 20px;">
                                <td></td>
                                <td><?php echo $this->lang->line('total') ?> : </td>
                                <td>600</td>
                                <td>450</td>
                                <td>150</td>
                                <td>510</td>
                                <td></td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="8" style="text-align:right;">
                                    <span style="font-weight: bold;"><?php echo $this->lang->line('grade') . "" . $this->lang->line('point') . "" . $this->lang->line('total') ."(" . $this->lang->line('gpa') .")" ?> : 4.0</span><br>
                                    <span style="font-weight: bold;"><?php echo $this->lang->line('average') . "" . $this->lang->line('grade') ?>: A+</span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-3">
                    <p style="font-weight:bold;text-align: center;"><?php echo $this->lang->line('attendance'); ?></p>
                    <table class="table table-bordered table-responsive" style="margin-bottom: 30px;">
                        <tbody>
                            <tr>
                                <td><?php echo $this->lang->line('working_days'); ?></td>
                                <td>-</td>
                            </tr>
                            <tr>
                                <td><?php echo $this->lang->line('present_days'); ?></td>
                                <td>-</td>
                            </tr>
                            <tr>
                                <td><?php echo $this->lang->line('absent_days'); ?></td>
                                <td>-</td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="table table-bordered table-responsive">
                        <tr>
                            <td>
                                <?php echo $this->lang->line('remarks') ?>: <br>
                                EXcellence:Go ahead
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="col-sm-8 col-sm-offset-1">
                    <p style="font-weight:bold;text-align: center;"><?php echo $this->lang->line('grading_scale'); ?> </p>
                    <table class="table table-responsive table-bordered">
                        <thead>
                            <th><?php echo $this->lang->line('marks_range'); ?></th>
                            <th><?php echo $this->lang->line('grade'); ?></th>
                            <th><?php echo $this->lang->line('performance'); ?></th>
                            <th><?php echo $this->lang->line('grade') . "" . $this->lang->line('point'); ?></th>
                        </thead>
                        <tbody>
                            <tr>
                                <td>90-100</td>
                                <td>A+</td>
                                <td><?php echo $this->lang->line('outstanding'); ?></td>
                                <td>4.0</td>
                            </tr>
                            <tr>
                                <td>80-90</td>
                                <td>A</td>
                                <td><?php echo $this->lang->line('excellence'); ?></td>
                                <td>3.6</td>
                            </tr>
                            <tr>
                                <td>70-80</td>
                                <td>B+</td>
                                <td><?php echo $this->lang->line('very_good'); ?></td>
                                <td>2.8</td>
                            </tr>
                            <tr>
                                <td>60-70</td>
                                <td>B</td>
                                <td><?php echo $this->lang->line('good'); ?></td>
                                <td>2.8</td>
                            </tr>
                            <tr>
                                <td>50-60</td>
                                <td>C+</td>
                                <td><?php echo $this->lang->line('satisfactory'); ?></td>
                                <td>2.4</td>
                            </tr>
                            <tr>
                                <td>40-50</td>
                                <td>C</td>
                                <td><?php echo $this->lang->line('acceptable'); ?></td>
                                <td>2.0</td>
                            </tr>
                            <tr>
                                <td>30-40</td>
                                <td>D+</td>
                                <td><?php echo $this->lang->line('partially_acceptable'); ?></td>
                                <td>1.6</td>
                            </tr>
                            <tr>
                                <td>20-30</td>
                                <td>D</td>
                                <td><?php echo $this->lang->line('insufficient'); ?></td>
                                <td>1.2</td>
                            </tr><tr>
                                <td>Below 20</td>
                                <td>E</td>
                                <td><?php echo $this->lang->line('very_insufficient'); ?></td>
                                <td>0.8</td>
                            </tr>
                        </tbody>
                    </table>

                </div>
            </div>

            <div class="row" style="margin-top: 40px;">
                <div class="col-sm-4" style="height: 130px;position: relative;">
                    <div style="width: 80%;overflow: hidden;float:left;text-align: center;position: absolute;bottom: 0;">
                        <img src="<?php echo base_url('uploads/marksheet/' . $marksheet->left_sign); ?>"  width="100">
                        <hr style="width:100%;border-color: #000;">
                        <p style="text-align:center"><?php echo $this->lang->line('class') . "" . $this->lang->line('teacher');  ?></p>
                    </div>
                </div>
                <div class="col-sm-4" style="height: 130px;position: relative;">
                    <div style="width: 80%;overflow: hidden;margin: auto;position: absolute;bottom: 0;">
                        <p style="text-align:center;"><?php echo $marksheet->date; ?></p>
                        <hr style="width:100%;border-color: #000;">
                        <p style="text-align:center"><?php echo $this->lang->line('date'); ?></p>
                    </div>
                </div>
                <div class="col-sm-4" style="height: 130px;position: relative;">
                    <div style="width: 80%;overflow: hidden;float:right;text-align: center;position: absolute;bottom: 0;">
                        <img src="<?php echo base_url('uploads/marksheet/' . $marksheet->right_sign); ?>" width="100">
                        <hr style="width:100%;border-color: #000;">
                        <p style="text-align:center"><?php echo $this->lang->line('principal'); ?></p>
                    </div>
                </div>
            </div>

        <?php if ($marksheet->content_footer != "") { ?>
            <div class="row">
                <div class="col-sm-12">
                    <p style="padding-bottom: 15px; line-height: normal;"><?php echo $marksheet->content_footer; ?></p>
                </div>
            </div>
        <?php } ?>
        </div>
    </body>
</html>