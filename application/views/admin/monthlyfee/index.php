
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <i class="fa fa-map-o"></i> <?php echo $this->lang->line('examinations'); ?> <small><?php echo $this->lang->line('student_fee1'); ?></small>  </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-search"></i> <?php echo $this->lang->line('select_criteria'); ?></h3>
                    </div>
                    <div class="box-body">
                        <div class="filter-box">
                            <form role="form" action="<?php echo site_url('studentfee/printmonthlyfee') ?>" method="post" class="row">

                                <?php echo $this->customlib->getCSRF(); ?>
                                <div class="col-sm-5 col-lg-4 col-md-4">
                                    <div class="form-group">   
                                        <label><?php echo $this->lang->line('class'); ?></label><small class="req"> *</small>
                                        <select id="class_id" name="class_id" class="form-control" >
                                            <option value=""><?php echo $this->lang->line('select'); ?></option>
                                            <?php
                                            foreach ($classlist as $class) {
                                                ?>
                                                <option value="<?php echo $class['id'] ?>" <?php
                                                if (set_value('class_id') == $class['id']) {
                                                    echo "selected=selected";
                                                }
                                                ?>><?php echo $class['class'] ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                        </select>
                                        <span class="text-danger"><?php echo form_error('class_id'); ?></span>
                                    </div>  
                                </div>
                                <div class="col-sm-5 col-lg-4 col-md-4">
                                    <div class="form-group"> 
                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('section'); ?></label><small class="req"> *</small>
                                        <select  id="section_id" name="section_id" class="form-control" >
                                            <option value=""><?php echo $this->lang->line('select'); ?></option>
                                        </select>
                                        <span class="text-danger"><?php echo form_error('section_id'); ?></span>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label>&nbsp;</label><br>
                                        <button type="submit" name="search" value="search_filter" class="btn btn-primary pull-right btn-sm checkbox-toggle"><i class="fa fa-search"></i> <?php echo $this->lang->line('search'); ?></button>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>

                    <?php
                    if (isset($studentList)) {
                        ?>
                        <form method="post" action="<?php echo base_url('studentfee/printmonthlyfeeinvoice') ?>" id="printMarksheet">

                            <div class="box-header ptbnull"></div>  
                            <div class="box-header ptbnull">
                                <h3 class="box-title titlefix"><i class="fa fa-users"></i> <?php echo $this->lang->line('student'); ?> <?php echo $this->lang->line('list'); ?></h3>
                                <button  class="btn btn-primary btn-sm printSelected pull-right" type="submit" name="generate" title="generate multiple certificate"><?php echo $this->lang->line('generate'); ?></button>
                            </div>
                            <div class="box-body">
                                <div class="tab-pane active table-responsive no-padding" id="tab_1">
                                    <table class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th><input type="checkbox" id="select_all" /></th>
                                                <th><?php echo $this->lang->line('admission_no'); ?></th>
                                                <th><?php echo $this->lang->line('student_name'); ?></th>
                                                <th><?php echo $this->lang->line('father_name'); ?></th>
                                                <th><?php echo $this->lang->line('date_of_birth'); ?></th>
                                                <th><?php echo $this->lang->line('gender'); ?></th>
                                                <th class=""><?php echo $this->lang->line('mobile_no'); ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if (empty($studentList)) {
                                                ?>

                                                <?php
                                            } else {
                                                $count = 1;
                                                foreach ($studentList as $student_key => $student_value) {
                                                  
                                                    ?>

                                                    <!-- <?php print_r($student_value); ?> -->
                                                    
                                                    <tr>
                                                        <td class="text-center"><input type="checkbox" name="student_class_id[]" value="<?php echo $student_value['student_session_id']; ?>"></td>
                                                        <td><?php echo $student_value['admission_no']; ?></td>
                                                        <td><?php echo $student_value['firstname'] . "" . $student_value['middlename']. "" . $student_value['lastname']; ?></td>
                                                        <td><?php echo $student_value['father_name']; ?></td>
                                                        <td><?php echo $student_value['dob']; ?></td>
                                                        <td><?php echo $student_value['gender']; ?></td>
                                                        <td><?php echo $student_value['mobileno']; ?></td>
                                                    </tr>
                                                    <tr style="display: none;">
                                                        <td>
                                                    <table>
                                                        <tbody>
                                                            <?php

    $total_amount           = 0;
    $total_deposite_amount  = 0;
    $total_fine_amount      = 0;
    $total_fees_fine_amount = 0;

    $total_discount_amount = 0;
    $total_balance_amount  = 0;
    $alot_fee_discount     = 0;
$student_due_fee      = $this->studentfeemaster_model->getStudentFees($student_value['student_session_id']);
    foreach ($student_due_fee as $key => $fee) {
        $CI =& get_instance();
        $CI->load->model('Feediscount_model');
        $student_sess = $fee->id;
        $total_result = count($CI->feediscount_model->discount_active($student_sess));
        $data = $CI->feediscount_model->discount_active($student_sess);

        foreach ($fee->fees as $fee_key => $fee_value) {
            $fee_paid         = 0;
            $fee_discount     = 0;
            $fee_fine         = 0;
            $fees_fine_amount = 0;
            $discount_applied = 0;
            $total_discount_paid = 0;
            
            if (!empty($fee_value->amount_detail)) {
                $fee_deposits = json_decode(($fee_value->amount_detail));

                foreach ($fee_deposits as $fee_deposits_key => $fee_deposits_value) {
                    $fee_paid     = $fee_paid + $fee_deposits_value->amount;
                    $fee_discount = $fee_discount + $fee_deposits_value->amount_discount;
                    $fee_fine     = $fee_fine + $fee_deposits_value->amount_fine;
                }
            }

            if (($fee_value->due_date != "0000-00-00" && $fee_value->due_date != null) && (strtotime($fee_value->due_date) < strtotime(date('Y-m-d')))) {
                $fees_fine_amount       = $fee_value->fine_amount;
                $total_fees_fine_amount = $total_fees_fine_amount + $fee_value->fine_amount;
            }
            if($total_result>0){
                foreach($data as $value){
                    $discount_applied=$value['amount_detail'];
                }
            }

            $total_amount          = $total_amount + $fee_value->amount;
            $total_discount_amount = $total_discount_amount + $fee_discount + $discount_applied;
            $total_deposite_amount = $total_deposite_amount + $fee_paid;
            $total_fine_amount     = $total_fine_amount + $fee_fine;
            $feetype_balance       = $fee_value->amount - ($fee_paid + $fee_discount)-$discount_applied;
            $total_balance_amount  = $total_balance_amount + $feetype_balance;
        
        ?>
        <?php
            if ($feetype_balance > 0 && strtotime($fee_value->due_date) < strtotime(date('Y-m-d'))) {
        ?>
                <tr class="danger font12">
            <?php } else { ?>

                <tr class="dark-gray">
            <?php
                }
            ?>
                    <td class="no-print"><input class="checkbox" type="checkbox" name="fee_checkbox" data-fee_master_id="<?php echo $fee_value->id ?>" data-fee_session_group_id="<?php echo $fee_value->fee_session_group_id ?>" data-fee_groups_feetype_id="<?php echo $fee_value->fee_groups_feetype_id ?>"></td>
                    <td align="left"><?php echo $fee_value->name; ?></td>
                    <td align="left" class="text text-left no-print">
                        <?php
                            if ($fee_value->due_date == "0000-00-00") {
                            } else {
                                echo date($this->customlib->getSchoolDateFormat(), $this->customlib->dateyyyymmddTodateformat($fee_value->due_date));
                            }
                            ?>
                    </td>
                    <td align="left" class="text text-left width85">
                        <?php
                            if ($feetype_balance == 0) {
                                        ?><span class="label label-success"><?php echo $this->lang->line('paid'); ?></span><?php
                            } else if (!empty($fee_value->amount_detail)) {
                                        ?><span class="label label-warning"><?php echo $this->lang->line('partial'); ?></span><?php
                            } else {
                                        ?><span class="label label-danger"><?php echo $this->lang->line('unpaid'); ?></span><?php
                            }
                        ?>
                    </td>
                    <td class="text text-right">
                        <?php echo $fee_value->amount;
                            if (($fee_value->due_date != "0000-00-00" && $fee_value->due_date != null) && (strtotime($fee_value->due_date) < strtotime(date('Y-m-d')))) {
                        ?>
                        <span class="text text-danger"><?php echo " + " . ($fee_value->fine_amount); ?></span>
                        <?php
                            }
                        ?>
                    </td>

                    <td class="text text-left"></td>
                    <td class="text text-right"><?php echo (number_format($fee_discount+ $discount_applied, 2, '.', '')); ?></td>
                    <td class="text text-right"><?php echo (number_format($fee_fine, 2, '.', '')); ?></td>
                    <td class="text text-right"><?php echo (number_format($fee_paid, 2, '.', '')); ?></td>
                    <td class="text text-right">
                        <?php 
                        $display_none = "ss-none";
                        if ($feetype_balance > 0) {
                            $display_none = "";

                            echo (number_format($feetype_balance, 2, '.', ''));
                        }
                        ?>
                    </td>

                    <td class="no-print" width="100">
                        <div class="btn-group">
                            <div class="pull-right">
                                <button  style="display: none" type="button" data-student_session_id="<?php echo $fee->student_session_id; ?>" data-glname="<?php echo $fee_value->glname; ?>" data-student_fees_master_id="<?php echo $fee->id; ?>" data-fee_groups_feetype_id="<?php echo $fee_value->fee_groups_feetype_id; ?>" data-group="<?php echo $fee_value->name; ?>" data-type="<?php echo $fee_value->type; ?>" data-remaining="<?php echo (number_format($feetype_balance, 2, '.', '')); ?>" class="btn btn-xs btn-default myCollectFeeBtn <?php echo $display_none; ?>" title="<?php echo $this->lang->line('add_fees'); ?>" data-toggle="modal" data-target="#myFeesModal"><i class="fa fa-plus"></i></button>

                                <button  class="btn btn-xs btn-default printInv" data-fee_master_id="<?php echo $fee_value->id ?>" data-fee_session_group_id="<?php echo $fee_value->fee_session_group_id ?>" data-fee_groups_feetype_id="<?php echo $fee_value->fee_groups_feetype_id ?>" title="<?php echo $this->lang->line('print'); ?>"><i class="fa fa-print"></i> </button>

                                <button class="view-btn btn btn-xs btn-default"><i class="fa fa-eye"></i></button>
                            </div>
                        </div>
                    </td>
                </tr>

                <?php
                    if (!empty($fee_value->amount_detail)) {
                        $fee_deposits = json_decode(($fee_value->amount_detail));

                        foreach ($fee_deposits as $fee_deposits_key => $fee_deposits_value) {
                        ?>
                            <tr class="white-td deposit-description">
                                <td class="no-print" align="left"></td>
                                <td class="text-right"><img src="<?php echo base_url(); ?>backend/images/table-arrow.png" alt="" /></td>
                                <td class="text text-left">
                                    <a href="#" data-toggle="popover" class="detail_popover" > <?php echo $fee_value->student_fees_deposite_id . "/" . $fee_deposits_value->inv_no; ?></a>
                                    <div class="fee_detail_popover">
                                        <?php
                                            if ($fee_deposits_value->description == "") {
                                        ?>
                                            <p class="text text-danger"><?php echo $this->lang->line('no_description'); ?></p>
                                            <?php
                                            } else {
                                            ?>
                                            <p class="text text-info"><?php echo $fee_deposits_value->description; ?></p>
                                            <?php
                                            }
                                            ?>
                                    </div>
                                </td>
                                <td class="text"><?php echo $this->lang->line(strtolower($fee_deposits_value->payment_mode)); ?></td>
                                <td></td>
                                <td class="text text-left">
                                    <?php echo date($this->customlib->getSchoolDateFormat(), $this->customlib->dateyyyymmddTodateformat($fee_deposits_value->date)); ?>
                                </td>
                                <td class="text text-right"><?php echo (number_format($fee_deposits_value->amount_discount, 2, '.', '')); ?></td>
                                <td class="text text-right"><?php echo (number_format($fee_deposits_value->amount_fine, 2, '.', '')); ?></td>
                                <td class="text text-right"><?php echo (number_format($fee_deposits_value->amount, 2, '.', '')); ?></td>
                                <td></td>
                                <td class="text text-right no-print">
                                    <div class="btn-group ">
                                       <div class="pull-right">
                                        <?php if ($this->rbac->hasPrivilege('collect_fees', 'can_delete')) {?>
                                            <!-- title="<?php echo $this->lang->line('revert'); ?>" -->
                                            <!-- <button class="btn btn-default btn-xs" data-invoiceno="<?php echo $fee_value->student_fees_deposite_id . "/" . $fee_deposits_value->inv_no; ?>" data-main_invoice="<?php echo $fee_value->student_fees_deposite_id; ?>" data-sub_invoice="<?php echo $fee_deposits_value->inv_no; ?>" data-toggle="modal" data-target="#confirm-delete" >
                                                <i class="fa fa-undo"> </i>
                                            </button> -->
                                        <?php }?>
                                        <button  class="btn btn-xs btn-default printDoc" data-main_invoice="<?php echo $fee_value->student_fees_deposite_id; ?>" data-sub_invoice="<?php echo $fee_deposits_value->inv_no; ?>"  title="<?php echo $this->lang->line('print'); ?>"><i class="fa fa-print"></i> </button>
                                    </div>
                                  </div>
                                </td>
                            </tr>
                        <?php
                        }
                    }
                ?>
<?php if($total_result>0) : ?>
        <?php foreach($v as $value) : ?>
            <tr class="discount_row deposit-description">
                <td></td>
                <td><?php echo $value['name']; ?></td>
                <td>Discount Applied</td>
                <td></td>
                <td></td>
                <td></td>
                <td class="text text-right"><?php echo $value['amount_detail']; ?></td>
                <td></td>
                <td></td>
                <td></td>
                <td><button class="btn btn-default btn-xs" data-id="<?php echo $value['id']; ?>" data-toggle="modal" data-target="#discount_revert" >
                    <i class="fa fa-undo"> </i>
                    </button></td>
            </tr>
        <?php endforeach; ?>
    


    <?php else : ?>

                <?php

                    if (!empty($student_discount_fee)) {
        
                        foreach ($student_discount_fee as $discount_key => $discount_value) { ?>
                            <?php if($discount_value['feetype_id']==$fee_value->feetype_id) : ?>
                                <tr class="discount_row deposit-description">
                                    <td></td>
                                    <td align="left"> <?php echo $discount_value['name']; ?> (<?php echo $discount_value['code']; ?>) </td>
                                    <td>Discount Not Applied</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td class="text-right">
                                        <?php if($discount_value['amount'] >0 && $discount_value['amount']!== "") {
                                            echo $discount_value['amount'];
                                        }else{
                                            echo $discount_value['discount_percent']. "%";
                                        }
                                        ?>
                                    </td>
                                    <td></td>
                                    <td></td>
                                    <td></td>

                                    <td>
                                        <?php if ($feetype_balance> 0) : ?>
                                        <button type="button" data-student_session_id="<?php echo $fee->student_session_id; ?>" data-student_fees_master_id="<?php echo $fee->id; ?>" data-fee_groups_feetype_id="<?php echo $fee_value->fee_groups_feetype_id; ?>" data-group="<?php echo $fee_value->name; ?>" data-amount="<?php echo $fee_value->amount; ?>" data-discount="<?php echo $discount_value['name']; ?>" data-type="<?php echo $fee_value->type; ?>" data-percent="<?php echo $discount_value['discount_percent']; ?>" data-amt_discount="<?php echo$discount_value['amount'] ?>" class="btn btn-xs btn-default discount_collect" title="<?php echo $this->lang->line('add_discount'); ?>" data-toggle="modal" data-target="#feediscountmodal"><i class="fa fa-plus"></i></button>
                                    <?php endif; ?>
                                    </td>
                                </tr>
                                        <?php endif; ?>
                                            <?php
                                            }
                                            }
                                        

                                    ?>
        <?php endif; ?>


                                            <?php
                                    }
                                }
 
                                    ?>


                                <tr class="box box-solid total-bg">
                                    <td align="left" ></td>
                                    <td  align="left" class="text text-left" ><?php echo $this->lang->line('grand_total'); ?></td>
                                    <td></td>
                                    <td></td>
                                    <td class="text text-right">
                                        <?php echo number_format($total_amount, 2, '.', '') . "<span class='text text-danger'>+" . number_format($total_fees_fine_amount, 2, '.', '') . "</span>";?>
                                    </td>
                                    <td class="text text-left"></td>

                                    <td class="text text-right"><?php echo (number_format($total_discount_amount, 2, '.', '')); ?></td>
                                    <td class="text text-right"><?php echo (number_format($total_fine_amount, 2, '.', '')); ?></td>
                                    <td class="text text-right"><?php echo (number_format($total_deposite_amount, 2, '.', '')); ?></td>
                                    <td class="text text-right"><?php echo (number_format($total_balance_amount - $alot_fee_discount, 2, '.', '')); ?></td>  
                                    <td></td>
                                </tr>
                                                        </tbody>
                                                    </table>
</td>
                                                    </tr>
                                                    <?php
                                                    $count++;
                                                }
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>                                                                           
                            </div>                                           

                        </form>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
    </section>
</div>

<script type="text/javascript">
 $(document).ready(function () {
        $('.select2').select2();

    });
    var date_format = '<?php echo $result = strtr($this->customlib->getSchoolDateFormat(), ['d' => 'dd', 'm' => 'mm', 'Y' => 'yyyy']) ?>';
    var class_id = '<?php echo set_value('class_id') ?>';
    var section_id = '<?php echo set_value('section_id') ?>';
    getSectionByClass(class_id, section_id);

    $(document).on('change', '#class_id', function (e) {
        $('#section_id').html("");
        var class_id = $(this).val();
        getSectionByClass(class_id, 0);
    });

    function getSectionByClass(class_id, section_id) {
        if (class_id != "") {
            $('#section_id').html("");
            var base_url = '<?php echo base_url() ?>';
            var div_data = '<option value=""><?php echo $this->lang->line('select'); ?></option>';

            $.ajax({
                type: "GET",
                url: base_url + "sections/getByClass",
                data: {'class_id': class_id},
                dataType: "json",
                beforeSend: function () {
                    $('#section_id').addClass('dropdownloading');
                },
                success: function (data) {
                    $.each(data, function (i, obj)
                    {
                        var sel = "";
                        if (section_id == obj.section_id) {
                            sel = "selected";
                        }
                        div_data += "<option value=" + obj.section_id + " " + sel + ">" + obj.section + "</option>";
                    });
                    $('#section_id').append(div_data);
                },
                complete: function () {
                    $('#section_id').removeClass('dropdownloading');
                }
            });
        }
    }

</script>
<script>

    $(document).on('submit', 'form#printMarksheet', function (e) {

        e.preventDefault();
        var form = $(this);
        var subsubmit_button = $(this).find(':submit');
        var formdata = form.serializeArray();

        var list_selected =  $('form#printMarksheet input[name="student_class_id[]"]:checked').length;
      if(list_selected > 0){
        $.ajax({
            type: "POST",
            url: form.attr('action'),
            data: formdata, // serializes the form's elements.
            dataType: "JSON", // serializes the form's elements.
            beforeSend: function () {
                subsubmit_button.button('loading');
            },
            success: function (response)
            {
                Popup(response.page);
            },
            error: function (xhr) { // if error occured

                alert("Error occured.please try again");
                subsubmit_button.button('reset');
            },
            complete: function () {
                subsubmit_button.button('reset');
            }
        });
      }else{
         confirm("<?php echo $this->lang->line('please_select_student'); ?>");
      }
    });


    $(document).on('click', '#select_all', function () {
        $(this).closest('table').find('td input[name="student_class_id[]"]:checkbox').prop('checked', this.checked);
    });

</script>
<script type="text/javascript">

    var base_url = '<?php echo base_url() ?>';
    function Popup(data)
    {

        var frame1 = $('<iframe />');
        frame1[0].name = "frame1";
        $("body").append(frame1);
        var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
        frameDoc.document.open();
        //Create a new HTML document.
        frameDoc.document.write('<html>');
        frameDoc.document.write('<head>');
        frameDoc.document.write('<title></title>');
        frameDoc.document.write('</head>');
        frameDoc.document.write('<body>');
        frameDoc.document.write(data);
        frameDoc.document.write('</body>');
        frameDoc.document.write('</html>');
        frameDoc.document.close();
        setTimeout(function () {
            window.frames["frame1"].focus();
            window.frames["frame1"].print();
            frame1.remove();
        }, 500);
        return true;
    }
</script>