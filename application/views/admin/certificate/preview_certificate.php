
<style type="text/css">
    *{padding: 0; margin:0;}
    body{ font-family: 'arial';}
    .tc-container{width: 100%;position: relative; text-align: center;padding: 2%;}
    .tc-container tr td{vertical-align: bottom;}

    .tcmybg {
        background:top center;
        position: absolute;
        top: 0;
        left: 0;
        bottom: 0;
        z-index: 1;
    }

    .tc-container tr td h1, h2 ,h3{margin-top: 0;font-weight: normal;} 
</style>               

<div class="col-sm-12" style="position: relative; text-align: center; font-family: 'arial'; width: 100%; height: 100vh;background: #fff">   
    <?php if (!empty($certificate->background_image)) { ?>
        <img src="<?php echo base_url('uploads/'.$_SERVER['SERVER_NAME'].'/certificate/' . $certificate->background_image); ?>" style="position: absolute;left: 0;right: 0;width: 100%;height: 100vh"/>
    <?php } ?>
    <div class="main-div" style="width: 80%;margin: auto;">
    <div class="col-sm-12" style="position: absolute;right: 75px;top: -40px;">
        <div class="col-sm-offset-8 col-sm-4">
            <?php if ($certificate->enable_student_image == 1) { ?>
                <img style="position: relative;top: 35vh;float: right" src="<?php echo base_url('uploads/'.$_SERVER['SERVER_NAME'].'/student_images/no_image.png') ?>" width="100" height="100">
            <?php } ?>
        </div>
    </div>

    <div class="col-sm-12" style="position: relative;top:45vh">
        <div class="col-sm-4 text-left">
            <?php echo $certificate->left_header ?>
        </div>
        <div class="col-sm-4 text-center">
            <?php echo $certificate->center_header ?>
        </div>
        <div class="col-sm-4 text-right">
            <?php echo $certificate->right_header ?>
        </div>
    </div>
    <div class="col-sm-12" style="position: relative;top:50vh">
        <p class="text-center" style="font-size: 14px; text-align:center;line-height: normal;"><?php echo $certificate->certificate_text ?></p>
    </div>
    <div class="col-sm-12" style="text-align:left;position: relative; top:60vh">
        <div class="col-sm-4 text-center">
            <?php echo $certificate->left_footer ?>
        </div>
        <div class="col-sm-4 text-center">
            <?php echo $certificate->center_footer ?>
        </div>
        <div class="col-sm-4 text-center">
            <?php echo $certificate->right_footer ?>
        </div>
    </div>
</div>
</div>
    <table width="100%" cellspacing="0" cellpadding="0" style="display:none;position: absolute;top: 0; margin-left: auto; margin-right: auto;left: 0;right: 0;<?php echo "width:" . $certificate->content_width . "px" ?>">
        <tr>
            <td style="position: absolute;right:0;">
                
            </td></tr>
        <tr>
            <td valign="top" style="text-align:left; position: relative; <?php echo "top:" . $certificate->header_height . "px" ?>"></td>
            <td valign="top" style="text-align:center; position: relative; <?php echo "top:" . $certificate->header_height . "px" ?>"></td>
            <td valign="top" style="text-align:right; position: relative; <?php echo "top:" . $certificate->header_height . "px" ?>"></td>
        </tr>
        <tr>
            <td colspan="3" valign="top" style="position: relative;<?php echo "top:" . $certificate->content_height . "px" ?>">
                </td>
        </tr>
        <tr style="">
            <td valign="top" style="text-align:left;position: relative; <?php echo "top:" . $certificate->footer_height . "px" ?>"><?php echo $certificate->left_footer ?></td>
            <td valign="top" style="text-align:center;position: relative; <?php echo "top:" . $certificate->footer_height . "px" ?>"><?php echo $certificate->center_footer ?></td>
            <td valign="top" style="text-align:right;position: relative; <?php echo "top:" . $certificate->footer_height . "px" ?>"><?php echo $certificate->right_footer ?></td>
        </tr>
    </table>


