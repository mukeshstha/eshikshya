 <style type="text/css">
     @media print {
      .page-break { display: block; page-break-before: always; }

    }        
       *{ margin:0; padding: 0;}
            /*body{ font-family: 'arial'; margin:0; padding: 0;font-size: 12px; color: #000;}*/
            .tc-container{width: 100%;position: relative; text-align: center;}
            .tcmybg {
                background: top center;
                background-size: contain;
                position: absolute;
                left: 0;
                bottom: 10px;
                width: 200px;
                height: 200px;
                margin-left: auto;
                margin-right: auto;
                right: 0;
            }
            /*begin students id card*/
            .studentmain{background: #efefef;width: 100%; margin-bottom: 30px;}
            .studenttop img{width:30px;vertical-align: top;}
            .studenttop{background: <?php echo $idcard->header_color; ?>;padding:2px;color: #fff;overflow: hidden;
                        position: relative;z-index: 1;}
            .sttext1{font-size: 24px;font-weight: bold;line-height: 30px;}
            .stgray{background: #efefef;padding-top: 5px; padding-bottom: 10px;}
            .staddress{margin-bottom: 0; padding-top: 2px;}
            .stdivider{border-bottom: 2px solid #000;margin-top: 5px; margin-bottom: 5px;}
            .stlist{padding: 0; margin:0; list-style: none;}
            .stlist li{text-align: left;display: inline-block;width: 100%;padding: 0px 5px;}
            .stlist li span{width:65%;float: right;}
            .stimg{width: 80px;height: auto;}
            .stimg img{width: 100%;height: auto;border-radius: 2px;display: block;}
            .img-circles {border-radius: 8px !important;}
            .center-block {display: block;margin-right: auto;margin-left: auto;}
            .staround{padding:3px 10px 3px 0;position: relative;overflow: hidden;}
            .staround2{position: relative; z-index: 9;}
            .stbottom{background: #453278;height: 20px;width: 100%;clear: both;margin-bottom: 5px;}
            .principal{margin-top: -40px;margin-right:10px; float:right;}
            .stred{color: #000;}
            .spanlr{padding-left: 5px; padding-right: 5px;}
            .cardleft{width: 20%;float: left;}
            .cardright{width: 77%;float: right; }
            .signature{border:1px solid #ddd; display:block; text-align: center; padding: 5px 20px; margin-top: 20px;}
            .vertlist{padding: 0; margin:0; list-style: none;}
            .vertlist li{text-align: left;display: inline-block;width: 100%; padding-bottom: 5px;color: #000;}
            .vertlist li span{width:65%;float: right;}
            
    </style>
        <?php 
if($idcard->enable_vertical_card){
    ?>

    <?php if(isset($idcard->background)): ?>
        <style type="text/css">
            .background-div:before{
                opacity: 0.6;
                background-image: url(<?php echo base_url('uploads/'.$_SERVER['SERVER_NAME'].'/student_id_card/background/') ?><?php echo $idcard->background ?>);
                background-repeat: no-repeat;
                background-size: cover;
                position: absolute;
                height: 100%;
                width: 100%;
            }
        </style>
    <?php endif; ?>

    <div class="row background-div" style="width: 350px;margin: auto;border: 1px solid;border-radius: 15px;overflow: hidden;position: relative;">
        <div class="col-sm-12" style="background: <?php echo $idcard->header_color; ?>;">
            <div class="col-sm-12" style="display: table-cell;vertical-align: middle;text-align: center">
                <img src="<?php echo base_url('uploads/'.$_SERVER['SERVER_NAME'].'/student_id_card/logo/'.$idcard->logo); ?>" width="50%">
            </div>
            <div class="col-sm-12 text-center">
                <h4 style="font-weight: bold"><?php echo $idcard->school_name; ?></h4>
                <!--                 <p><?php echo $idcard->school_address; ?></p>-->            
            </div>
        </div>
        <div class="col-sm-12 text-center">
            <img src="<?php echo base_url() . "uploads/".$_SERVER['SERVER_NAME']."/student_images/no_image.png"; ?>" class="img-responsive img-circles block-center" style="border:3px solid <?php echo $idcard->header_color; ?>;max-width: 25%;margin:10px auto">
            <h4 style="margin:0; text-transform: uppercase;font-weight: bold; margin-top: 10px;">S.Tudent Name</h4>
            <p style="font-size: 15px;color: #9b1818;">Student</p>
        </div>
        <div class="col-sm-12">
            <ul class="vertlist" style="margin : 0 20px">
               <?php
                                if ($idcard->enable_student_name == 1) {
                                    
                                }
                                ?>
                                <?php
                                if ($idcard->enable_admission_no == 1) {
                                    echo "<li>Admission No.<span>: 123456789</span></li>";
                                }
                                ?>
                                <?php
                                if ($idcard->enable_class == 1) {
                                    echo "<li>Class<span>: Class 6 - A (2018-19)</span></li>";
                                }
                                ?>
                                <?php
                                if ($idcard->enable_fathers_name == 1) {
                                    echo "<li>Father's Name<span>: S.Tudent Name</span></li>";
                                }
                                ?>
                                <?php
                                if ($idcard->enable_mothers_name == 1) {
                                    echo "<li>Mothers's Name<span>: S.Tudent Name</span></li>";
                                }
                                ?>
                                <?php
                                if ($idcard->enable_address == 1) {
                                    echo "<li>Address<span>: D.No.1 Street Name Address Line 2 Address Line 3</span></li>";
                                }
                                ?>
                                <?php
                                if ($idcard->enable_phone == 1) {
                                    echo "<li>Phone<span>: 1234567890</span></li>";
                                }
                                ?>
                                <?php
                                if ($idcard->enable_dob == 1) {
                                    echo "<li>D.O.B<span>: 2049/12/25</span></li>";
                                }
                                ?>
                                <?php
                                if ($idcard->enable_blood_group == 1) {
                                    echo "<li class='stred'>Blood Group<span>: A+</span></li>";
                                }
                                ?>
            </ul>
        </div>
        <div class="col-sm-12">
            <div class="col-sm-8">
            </div>
            <div class="col-sm-4 text-center">
                <img src="<?php echo base_url('uploads/'.$_SERVER['SERVER_NAME'].'/student_id_card/signature/') ?><?php echo $idcard->sign_image; ?>" style="width:80%" /><br>
                <hr style="margin-top: 10px;margin-bottom: 10px;border: 0;border-top: 3px solid #000;">
                <h5>principal</h5>

            </div>
        </div>
    </div>



 <table style="display: none" cellpadding="0" cellspacing="0" width="100%" style="background: <?php echo $idcard->header_color; ?>;">
    <tr>
        <td valign="top" style="text-align: center;color: #fff;padding: 10px;min-height: 110px;display: block;">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                   <td valign="top">
                        <div style="color: #fff;position: relative; z-index: 1;">
                            <div class="sttext1"><img src="<?php echo base_url('uploads/'.$_SERVER['SERVER_NAME'].'/student_id_card/logo/'.$idcard->logo); ?>" width="30" height="30"> <?php echo $idcard->school_name; ?> 

                        </div>
                    </td>
                </tr>
                <tr>
                    <td valign="top" style="color: #fff"><?php echo $idcard->school_address; ?></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td valign="top" style="background: #fff">
           <table cellpadding="0" cellspacing="0" width="100%" style="margin-top: -40px; position: relative;z-index: 1;">
               <tr>
                    <td valign="top" style="text-align: center;">
                        <div class="stimg center-block">
                            <img src="<?php echo base_url() . "uploads/".$_SERVER['SERVER_NAME']."/student_images/no_image.png"; ?>" class="img-responsive img-circles block-center" style="border:3px solid <?php echo $idcard->header_color; ?>">
                        </div>

                    </td>
                </tr>
                <tr>
                    <td valign="top" style="text-align: center;">
                        <h4 style="margin:0; text-transform: uppercase;font-weight: bold; margin-top: 10px;">S.Tudent Name</h4>
                        <p style="font-size: 15px;color: #9b1818;">Student</p>
                    </td>
                </tr>  
           </table> 
        </td>
    </tr>
    <tr>
        <td valign="top">
            <table cellpadding="0" cellpadding="0" width="90%" align="center" style="background: #fff;padding: 20px 20px;display: block;width: 90%; margin:0 auto">
                <tr>
                    <td valign="top">
                        <ul class="vertlist">
                           <?php
                                            if ($idcard->enable_student_name == 1) {
                                                
                                            }
                                            ?>
                                            <?php
                                            if ($idcard->enable_admission_no == 1) {
                                                echo "<li>Admission Number<span> 123456789</span></li>";
                                            }
                                            ?>
                                            <?php
                                            if ($idcard->enable_class == 1) {
                                                echo "<li>Class<span>Class 6 - A (2018-19)</span></li>";
                                            }
                                            ?>
                                            <?php
                                            if ($idcard->enable_fathers_name == 1) {
                                                echo "<li>Father's Name<span>S.Tudent Name</span></li>";
                                            }
                                            ?>
                                            <?php
                                            if ($idcard->enable_mothers_name == 1) {
                                                echo "<li>Mothers's Name<span>S.Tudent Name</span></li>";
                                            }
                                            ?>
                                            <?php
                                            if ($idcard->enable_address == 1) {
                                                echo "<li>Address<span>D.No.1 Street Name Address Line 2 Address Line 3</span></li>";
                                            }
                                            ?>
                                            <?php
                                            if ($idcard->enable_phone == 1) {
                                                echo "<li>Phone<span>1234567890</span></li>";
                                            }
                                            ?>
                                            <?php
                                            if ($idcard->enable_dob == 1) {
                                                echo "<li>D.O.B<span>25.06.2006</span></li>";
                                            }
                                            ?>
                                            <?php
                                            if ($idcard->enable_blood_group == 1) {
                                                echo "<li class='stred'>Blood Group<span>A+</span></li>";
                                            }
                                            ?>
                        </ul>
                        <div class="signature"><img src="<?php echo base_url('uploads/'.$_SERVER['SERVER_NAME'].'/student_id_card/signature/'.$idcard->sign_image); ?>" width="200" height="24" /></div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
    <?php

}else{
    ?>
<?php if(isset($idcard->background)): ?>
        <style type="text/css">
            .background-div:before{
                opacity: 0.6;
                background-image: url(<?php echo base_url('uploads/'.$_SERVER['SERVER_NAME'].'/student_id_card/background/') ?><?php echo $idcard->background ?>);
                background-repeat: no-repeat;
                background-size: cover;
                position: absolute;
                height: 100%;
                width: 100%;
            }
        </style>
    <?php endif; ?>


    <div class="row background-div" style="border: 1px solid;border-radius: 15px;overflow: hidden;position: relative;">
        <div class="col-sm-12" style="margin-top: 15px">
            <div class="col-sm-3">
                <img src="<?php echo base_url('uploads/'.$_SERVER['SERVER_NAME'].'/student_id_card/logo/') ?><?php echo $idcard->logo; ?>" style="width: 100%" />
            </div>
            <div class="col-sm-9 text-center">
                <h4 style="font-weight: bold"><?php echo $idcard->school_name; ?></h4>
                <p style="font-size: 14px"><?php echo $idcard->school_address; ?></p>
            </div>
        </div>
        <div class="col-sm-12" style="margin-top: 15px">
            <div class="col-sm-3">
                <figure class="photo-cover">
                    <img src="<?php echo base_url() . "uploads/".$_SERVER['SERVER_NAME']."/student_images/no_image.png"; ?>" class="img-responsive" />
                </figure>
            </div>
            <div class="col-sm-9">
                <ul class="stlist">
                    <?php
                    if ($idcard->enable_student_name == 1) {
                        echo "<li>Student Name<span>: Student Name</span></li>";
                    }
                    ?>
                    <?php
                    if ($idcard->enable_admission_no == 1) {
                        echo "<li>Admission No.<span>: AD1234</span></li>";
                    }
                    ?>
                    <?php
                    if ($idcard->enable_class == 1) {
                        echo "<li>Class<span>: Class 6 - A (2018-19)</span></li>";
                    }
                    ?>
                    <?php
                    if ($idcard->enable_fathers_name == 1) {
                        echo "<li>Father's Name<span>: Father Name</span></li>";
                    }
                    ?>
                    <?php
                    if ($idcard->enable_mothers_name == 1) {
                        echo "<li>Mothers's Name<span>: Mother Name</span></li>";
                    }
                    ?>
                    <?php
                    if ($idcard->enable_address == 1) {
                        echo "<li>Address<span>: Tinkune Kathmandu, Nepal</span></li>";
                    }
                    ?>
                    <?php
                    if ($idcard->enable_phone == 1) {
                        echo "<li>Phone<span>: 9818118181</span></li>";
                    }
                    ?>
                    <?php
                    if ($idcard->enable_dob == 1) {
                        echo "<li>D.O.B<span>: 2049/12/25</span></li>";
                    }
                    ?>
                    <?php
                    if ($idcard->enable_blood_group == 1) {
                        echo "<li class='stred'>Blood Group<span>: A+</span></li>";
                    }
                    ?>

                </ul>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="col-sm-9">
            </div>
            <div class="col-sm-3 text-center">
                <img src="<?php echo base_url('uploads/'.$_SERVER['SERVER_NAME'].'/student_id_card/signature/') ?><?php echo $idcard->sign_image; ?>" style="width:80%" /><br>
                <hr style="margin-top: 10px;margin-bottom: 10px;border: 0;border-top: 3px solid #000;">
                <h5>principal</h5>

            </div>
        </div>
    </div>
 <table cellpadding="0" cellspacing="0" width="100%" style="display: none">
            <tr> 
                <td valign="top" width="32%" style="padding: 3px;">
                    <table cellpadding="0" cellspacing="0" width="100%" class="tc-container" style="background: #efefef;">
                        <tr>
                            <td valign="top">
                                <img src="<?php echo base_url('uploads/'.$_SERVER['SERVER_NAME'].'/student_id_card/background/') ?><?php echo $idcard->background; ?>" class="tcmybg" style="opacity: .1"/></td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <div class="studenttop">
                                    <div class="sttext1"><img src="<?php echo base_url('uploads/'.$_SERVER['SERVER_NAME'].'/student_id_card/logo/') ?><?php echo $idcard->logo; ?>" width="30" height="30" />
                                        <?php echo $idcard->school_name; ?></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" align="center" style="padding: 1px 0;">
                                <p><?php echo $idcard->school_address; ?></p>
                            </td>
                        </tr>

                        <tr>
                            <td valign="top" style="color: #fff;font-size: 16px; padding: 2px 0 0; position: relative; z-index: 1;background: <?php echo $idcard->header_color; ?>;text-transform: uppercase;"><?php echo $idcard->title; ?></td>
                        </tr>

                        <tr>
                            <td valign="top">
                                <div class="staround">
                                    <div class="cardleft">
                                        <div class="stimg">
                                            <img src="<?php echo base_url() . "uploads/".$_SERVER['SERVER_NAME']."/student_images/no_image.png"; ?>" class="img-responsive" />
                                        </div>
                                    </div><!--./cardleft-->
                                    <div class="cardright">
                                        <ul class="stlist">
                                            <?php
                                            if ($idcard->enable_student_name == 1) {
                                                echo "<li>Student Name<span> S.Tudent Name</span></li>";
                                            }
                                            ?>
                                            <?php
                                            if ($idcard->enable_admission_no == 1) {
                                                echo "<li>Admission Number<span> 123456789</span></li>";
                                            }
                                            ?>
                                            <?php
                                            if ($idcard->enable_class == 1) {
                                                echo "<li>Class<span>Class 6 - A (2018-19)</span></li>";
                                            }
                                            ?>
                                            <?php
                                            if ($idcard->enable_fathers_name == 1) {
                                                echo "<li>Father's Name<span>S.Tudent Name</span></li>";
                                            }
                                            ?>
                                            <?php
                                            if ($idcard->enable_mothers_name == 1) {
                                                echo "<li>Mothers's Name<span>S.Tudent Name</span></li>";
                                            }
                                            ?>
                                            <?php
                                            if ($idcard->enable_address == 1) {
                                                echo "<li>Address<span>D.No.1 Street Name Address Line 2 Address Line 3</span></li>";
                                            }
                                            ?>
                                            <?php
                                            if ($idcard->enable_phone == 1) {
                                                echo "<li>Phone<span>1234567890</span></li>";
                                            }
                                            ?>
                                            <?php
                                            if ($idcard->enable_dob == 1) {
                                                echo "<li>D.O.B<span>25.06.2006</span></li>";
                                            }
                                            ?>
                                            <?php
                                            if ($idcard->enable_blood_group == 1) {
                                                echo "<li class='stred'>Blood Group<span>A+</span></li>";
                                            }
                                            ?>

                                        </ul>
                                    </div><!--./cardright-->
                                </div><!--./staround-->
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" align="right" class="principal"><img src="<?php echo base_url('uploads/'.$_SERVER['SERVER_NAME'].'/student_id_card/signature/') ?><?php echo $idcard->sign_image; ?>" width="66" height="40" /></td>
                        </tr>
                    </table>
                </td>
            </tr>  
        </table>
        
    <?php
}
         ?>
       