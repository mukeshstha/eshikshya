<?php $currency_symbol = $this->customlib->getSchoolCurrencyFormat(); ?>
<div class="content-wrapper">

    <section class="content-header">
        <h1>
            <i class="fa fa-credit-card"></i> <?php echo $this->lang->line('expenses'); ?> <small><?php echo $this->lang->line('student_fee'); ?></small></h1>
    </section>
<style type="text/css">
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
      -webkit-appearance: none;
      margin: 0;
    }

    input[type=number] {
      -moz-appearance: textfield;
    }
</style>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <?php
            if ($this->rbac->hasPrivilege('expense', 'can_add')) {
                ?>
                <div class="col-md-4">
                    <!-- Horizontal Form -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title"><?php echo $this->lang->line('add_expense'); ?></h3>
                        </div><!-- /.box-header -->
                        <form id="form1" action="<?php echo base_url() ?>admin/expense"  id="employeeform" name="employeeform" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                            <div class="box-body">
                                <?php if ($this->session->flashdata('msg')) { ?>
                                    <?php echo $this->session->flashdata('msg') ?>
                                <?php } ?>
                                <?php
                                if (isset($error_message)) {
                                    echo "<div class='alert alert-danger'>" . $error_message . "</div>";
                                }
                                ?>
                                <?php echo $this->customlib->getCSRF(); ?>

                                <input type="hidden" name="trans_id" value="<?php echo $this->customlib->GUID(); ?>">

                                <div class="form-group">
                                    <label for="exampleInputEmail1"><?php echo $this->lang->line('name'); ?></label> <small class="req">*</small>
                                    <input id="name" name="name" placeholder="" type="text" class="form-control"  value="<?php echo set_value('name'); ?>" />
                                    <span class="text-danger"><?php echo form_error('name'); ?></span>
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1"><?php echo $this->lang->line('expense_head'); ?></label> <small class="req">*</small>

                                    <select autofocus="" id="exp_head_id" name="exp_head_id" class="form-control" >
                                        <option value=""><?php echo $this->lang->line('select'); ?></option>
                                        <?php foreach ($expheadlist['Data'] as $exphead) { ?>
                                            <option value="<?php echo $exphead['Id'] ?>"<?php if (set_value('exp_head_id') == $exphead['Id']) { echo "selected =selected"; } ?>><?php echo $exphead['Name'] ?></option>
                                        <?php
                                            $count++;
                                        }
                                        ?>
                                    </select>
                                    <span class="text-danger"><?php echo form_error('exp_head_id'); ?></span>
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Paid By</label> <small class="req">*</small>

                                    <select autofocus="" id="paid_by" name="paid_by" class="form-control" >
                                        <option value=""><?php echo $this->lang->line('select'); ?></option>

                                        <?php
                                        foreach ($paymentmethod['Data'] as $payment) {
                                            ?>
                                            <option value="<?php echo $payment['Id'] ?>"<?php if (set_value('paid_by') == $payment['Id']) { echo "selected =selected"; } ?> data-balance="<?php echo $payment['Balance'] ?>"><?php echo $payment['Name'] ?></option>

                                            <?php
                                            $count++;
                                        }
                                        ?>
                                    </select>
                                    <span class="text-danger"><?php echo form_error('paid_by'); ?></span>
                                </div>
                                
                                <div class="form-group" style="display: none">
                                    <label for="exampleInputEmail1"><?php echo $this->lang->line('invoice_no'); ?></label>
                                    <input id="invoice_no" name="invoice_no" placeholder="" type="text" class="form-control"  value="<?php echo set_value('invoice_no'); ?>" />
                                    <span class="text-danger"><?php echo form_error('invoice_no'); ?></span>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1"><?php echo $this->lang->line('date'); ?></label> <small class="req">*</small>
                                    <?php if($this->customlib->getcalender()->calender=="0"){ ?>
                                        <input id="date" name="date" placeholder="" type="text" class="form-control date"  value="<?php echo set_value('date', date($this->customlib->currentdate())); ?>" readonly="readonly" />
                                    <?php }else{ ?>
                                        <input id="date" name="date" placeholder="" type="text" class="form-control date"  value="<?php echo set_value('date', date($this->customlib->getSchoolDateFormat())); ?>" readonly="readonly" />
                                    <?php } ?>
                                    
                                    <span class="text-danger"><?php echo form_error('date'); ?></span>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1"><?php echo $this->lang->line('amount'); ?></label> <small class="req">*</small>
                                    <input id="amount" name="amount" placeholder="" type="number" class="form-control"  value="<?php echo set_value('amount'); ?>" />
                                    <span class="text-danger"><?php echo form_error('amount'); ?></span>
                                </div>
                                <div class="form-group" style="display:none">
                                    <label for="exampleInputEmail1"><?php echo $this->lang->line('attach_document'); ?></label>
                                    <input id="documents" name="documents" placeholder="" type="file" class="filestyle form-control"  value="<?php echo set_value('documents'); ?>" />
                                    <span class="text-danger"><?php echo form_error('documents'); ?></span>
                                </div>
                                <div class="form-group" style="display: none">
                                    <label for="exampleInputEmail1"><?php echo $this->lang->line('description'); ?></label>
                                    <textarea class="form-control" id="description" name="description" placeholder="" rows="3" placeholder="Enter ..."><?php echo set_value('description'); ?></textarea>
                                    <span class="text-danger"></span>
                                </div>
                            </div><!-- /.box-body -->

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary pull-right"><?php echo $this->lang->line('save'); ?></button>
                            </div>
                        </form>
                    </div>

                </div><!--/.col (right) -->
                <!-- left column -->
            <?php } ?>
            <div class="col-md-<?php
            if ($this->rbac->hasPrivilege('expense', 'can_add')) {
                echo "8";
            } else {
                echo "12";
            }
            ?>">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header ptbnull">
                        <h3 class="box-title titlefix"><?php echo $this->lang->line('expense_list'); ?></h3>
                        <div class="box-tools pull-right">
                        </div><!-- /.box-tools -->
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="mailbox-messages table-responsive">
                            <div class="download_label"><?php echo $this->lang->line('expense_list'); ?></div>
                            <div class="table-responsive"> 
                                <table class="table table-striped table-bordered table-hover expense-list" data-export-title="<?php echo $this->lang->line('expense_list'); ?>">
                                    <thead>
                                        <tr>
                                            <th><?php echo $this->lang->line('name'); ?>
                                            </th>
                                            <th><?php echo $this->lang->line('invoice_no'); ?>
                                            </th>
                                            <th><?php echo $this->lang->line('date'); ?>
                                            </th>
                                            <th><?php echo $this->lang->line('amount'); ?>
                                            </th>
                                            <th class="text-right noExport"><?php echo $this->lang->line('action'); ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($voucher['Data'] as $key => $value) { ?>
                                            <?php if($value['RefNo']=="expense"){ ?>
                                            <tr>
                                                <td><?php echo $value['Narration']; ?></td>
                                                <td><?php echo $value['VoucherNo']; ?></td>
                                                <td>
                                                    <?php 
                                                        if($this->customlib->getcalender()->calender=="0"){
                                                            echo $this->customlib->nepalidate(date("Y-m-d", strtotime($value['Date']))); 
                                                        }else{
                                                            echo date("Y-m-d", strtotime($value['Date'])); 
                                                        }
                                                    ?>
                                                </td>
                                                <td><?php echo $value['Balance']; ?></td>
                                                <td class="mailbox-date no-print text text-right">

                                                    <a data-id="<?php echo $value['Id']; ?>" data-number="<?php echo $value['VoucherNo']; ?>" class="voucher_popup btn btn-default btn-xs" title="<?php echo $this->lang->line('details for') .$value['VoucherNo'] ?>"><i class="fa fa-eye"></i></a>

                                                    <a onclick="return confirm('Approve Invoice?');" title="Approve" class="btn btn-default btn-xs" href="<?php echo base_url(); ?>account/journal/approve/<?php echo $value['Id']; ?>/<?php echo $value['VoucherNo']; ?>/<?php echo $value['TransId']; ?>"><i class="fa fa-check" aria-hidden="true"></i></a>

                                                    <a onclick="return confirm('Reject Voucher')" href="<?php echo base_url();?>account/journal/reject/<?php echo $value['RefNo']; ?>/<?php echo $value['Id']; ?>/<?php echo $value['VoucherNo']; ?>/<?php echo $value['TransId']; ?>" class="btn btn-default btn-xs" data-toggle="tooltip" title="Reject"><i class="fa fa-remove"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        <?php }} ?>
                                        <?php foreach ($approvedvoucher['Data'] as $key => $value) { ?>
                                            <?php if($value['RefNo']=="expense"){ ?>
                                            <tr>
                                                <td><?php echo $value['Narration']; ?></td>
                                                <td><?php echo $value['VoucherNo']; ?></td>
                                                <td>
                                                    <?php 
                                                        if($this->customlib->getcalender()->calender=="0"){
                                                            echo $this->customlib->nepalidate(date("Y-m-d", strtotime($value['Date']))); 
                                                        }else{
                                                            echo date("Y-m-d", strtotime($value['Date'])); 
                                                        }
                                                    ?>
                                                </td>
                                                <td><?php echo $value['Balance']; ?></td>
                                                <td class="mailbox-date no-print text text-right">

                                                    <a data-id="<?php echo $value['Id']; ?>" data-number="<?php echo $value['VoucherNo']; ?>" class="voucher_popup btn btn-default btn-xs" title="<?php echo $this->lang->line('details for') .$value['VoucherNo'] ?>"><i class="fa fa-eye"></i></a>
                                                    </a>
                                                </td>
                                            </tr>
                                        <?php }} ?>
                                        </tr>
                                    </tbody>
                                </table><!-- /.table -->
                            </div>  
                        </div><!-- /.mail-box-messages -->
                    </div><!-- /.box-body -->
                </div>
            </div><!--/.col (left) -->

        </div>
        <div class="row">
            <!-- left column -->

            <!-- right column -->
            <div class="col-md-12">

            </div><!--/.col (right) -->
        </div>   <!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<div class="modal fade" id="modal-FV-Details" role="dialog">
    <div class="modal-dialog" style="width: 80%">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" style="position: static;color: #000">&times;</button>
                <h4 class="modal-title">Details for <span class="FVNumber"></span></h4>
            </div>
            <div class="modal-body voucher_body">
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

<script>
//     ( function ( $ ) {
//     'use strict';
//     $(document).ready(function () {
//         initDatatable('expense-list','admin/expense/getexpenselist',[],[],100);
//     });
// } ( jQuery ) )
</script>

<script>
    $(document).ready(function(){
        $("#paid_by").on('change', function() {
            $balance=$(this).find(':selected').data('balance');
            $("#amount").prop('max',$balance);
        });
    });
</script>

<script type="text/javascript">
    $(document).on("click",".voucher_popup",function () {
        var base_url = '<?php echo base_url() ?>';
        var voucherno = $(this).data('number');
        var voucherid = $(this).data('id');
        $(".FVNumber").text(voucherno);
        $('#modal-FV-Details').modal({
            backdrop: 'static',
            keyboard: false
        });
        $.ajax({
            type: "POST",
            url: base_url + "account/report/getvoucher",
            data: {'voucher_id': voucherid, 'voucher_no': voucherno},
            dataType: "json",
            beforeSend: function () {
                $('.voucher_body').html();
            },
            success: function (response) {
                $('.voucher_body').html(response.sendingdetails);
            },
            complete: function () {
            }
        });
    });
</script>