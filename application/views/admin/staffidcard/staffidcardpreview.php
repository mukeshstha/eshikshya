  <style type="text/css">
    { margin:0; padding: 0;}
    /*body{ font-family: 'arial'; margin:0; padding: 0;font-size: 12px; color: #000;}*/
    .tc-container{width: 100%;position: relative; text-align: center;}
            .tcmybg {
                background: top center;
                background-size: contain;
                position: absolute;
                left: 0;
                bottom: 10px;
                width: 200px;
                height: 200px;
                margin-left: auto;
                margin-right: auto;
                right: 0;
            }
            /*begin students id card*/
            .studenttop img{width:30px;vertical-align: top;}
            .studenttop{background: <?php echo $idcard->header_color; ?>;padding:2px;color: #fff;overflow: hidden;
                        position: relative;z-index: 1;}
            .sttext1{font-size: 24px;font-weight: bold;line-height: 30px;}
            .stgray{background: #efefef;padding-top: 5px; padding-bottom: 10px;}
            .staddress{margin-bottom: 0; padding-top: 2px;}
            .stdivider{border-bottom: 2px solid #000;margin-top: 5px; margin-bottom: 5px;}
            .stlist{padding: 0; margin:0; list-style: none;}
            .stlist li{text-align: left;display: inline-block;width: 100%;padding: 0px 5px;}
            .stlist li span{width:65%;float: right;}
            .stimg{ width: 80px; height: auto;}
            .stimg img{width: 100%;height: auto;border-radius: 2px;display: block;}
            .img-circle {border-radius: 50% !important;}
            .staround{padding:3px 10px 3px 0;position: relative;overflow: hidden;}
            .staround2{position: relative; z-index: 9;}
            .stbottom{background: #453278;height: 20px;width: 100%;clear: both;margin-bottom: 5px;}
            .principal{margin-top: -40px;margin-right:10px; float:right;}
            .stred{color: #000;}
            .spanlr{padding-left: 5px; padding-right: 5px;}
            .cardleft{width: 20%;float: left;}
            .cardright{width: 77%;float: right; }
            .signature{border:1px solid #ddd; display:block; text-align: center; padding: 5px 20px; margin-top: 20px;}
            .vertlist{padding: 0; margin:0; list-style: none;}
            .vertlist li{text-align: left;display: inline-block;width: 100%; padding-bottom: 5px;color: #000;}
            .vertlist li span{width:65%;float: right;}
        </style>
        <?php $dummy_date = "2020-01-01";?>
        <?php
if ($idcard->enable_vertical_card) {
?>
<?php if(isset($idcard->background)): ?>
        <style type="text/css">
            .background-div:before{
                opacity: 0.6;
                background-image: url(<?php echo base_url('uploads/'.$_SERVER['SERVER_NAME'].'/staff_id_card/background/') ?><?php echo $idcard->background ?>);
                background-repeat: no-repeat;
                background-size: cover;
                position: absolute;
                height: 100%;
                width: 100%;
            }
        </style>
    <?php endif; ?>
<div class="row background-div" style="width: 350px;margin: auto;border: 1px solid;border-radius: 15px;overflow: hidden;position: relative;">
        <div class="col-sm-12" style="background: <?php echo $idcard->header_color; ?>;">
            <div class="col-sm-12" style="display: table-cell;vertical-align: middle;text-align: center">
                <img src="<?php echo base_url('uploads/'.$_SERVER['SERVER_NAME'].'/staff_id_card/logo/'.$idcard->logo); ?>" style="height: 40px;width: auto;margin-top: 10px">
            </div>
            <div class="col-sm-12 text-center">
                <h4 style="font-weight: bold;line-height: 10px"><?php echo $idcard->school_name; ?></h4>
                <p style=""><?php echo $idcard->school_address; ?></p>     
            </div>
        </div>
        <div class="col-sm-12 text-center">
            <img src="<?php echo base_url() . "uploads/".$_SERVER['SERVER_NAME']."/student_images/no_image.png"; ?>" class="img-responsive img-circles block-center" style="border:3px solid <?php echo $idcard->header_color; ?>;max-width: 25%;margin:10px auto;border-radius: 15px">
            <h4 style="margin:0; text-transform: uppercase;font-weight: bold; margin-top: 10px;">Teacher Name</h4>
            <p style="font-size: 15px;color: #9b1818;">Staff</p>
        </div>
        <div class="col-sm-12">
            <ul class="vertlist">
                <?php
                    if ($idcard->enable_name == 1) {
                        // echo "<li>"; echo $this->lang->line('staff').' '; echo $this->lang->line('name'); echo "<span>Mohan Patil</span></li>";
                     } 
                ?>
                <?php
                if ($idcard->enable_staff_id == 1) {
                    echo "<li>"; echo $this->lang->line('staff_id');echo "<span>: 9000</span></li>";
                }
                ?>
                <?php
                if ($idcard->enable_designation == 1) {
                    // echo "<li>"; echo $this->lang->line('designation');echo "<span>Administator</span></li>";
                }
                ?>
                <?php
                if ($idcard->enable_staff_department == 1) {
                     echo "<li>"; echo $this->lang->line('department');echo "<span>: Admin</span></li>";
                }
                ?>
                <?php
                if ($idcard->enable_fathers_name == 1) {
                    echo "<li>"; echo $this->lang->line('father_name'); echo "<span>: Father Name</span></li>";
                }
                ?>
                <?php
                if ($idcard->enable_mothers_name == 1) {
                    echo "<li>"; echo $this->lang->line('mother_name'); echo "<span>: Mother Name</span></li>";
                }
                ?>
                <?php
                if ($idcard->enable_date_of_joining == 1) {
                    echo "<li>"; echo $this->lang->line('date_of_joining'); echo "<span>: "; echo date($this->customlib->getSchoolDateFormat(), $this->customlib->dateyyyymmddTodateformat($dummy_date)); echo "</span></li>";
                }
                ?>
                <?php
                if ($idcard->enable_permanent_address == 1) {
                    echo "<li>"; echo $this->lang->line('address'); echo "<span>: Kathmandu Nepal</span></li>";
                }
                ?>
                 <?php
                if ($idcard->enable_staff_phone == 1) {
                    echo "<li>"; echo $this->lang->line('phone'); echo "<span>: 9845624781</span></li>";
                }
                ?>
                <?php
                if ($idcard->enable_staff_dob == 1) {
                    echo "<li>"; echo $this->lang->line('date_of_birth'); echo "<span>: "; echo date($this->customlib->getSchoolDateFormat(), $this->customlib->dateyyyymmddTodateformat($dummy_date)); echo "</span></li>";
                }
                ?>
            </ul>
        </div>
        <div class="col-sm-12">
            <div class="col-sm-8">
            </div>
            <div class="col-sm-4 text-center">
                <img src="<?php echo base_url('uploads/'.$_SERVER['SERVER_NAME'].'/staff_id_card/signature/') ?><?php echo $idcard->sign_image; ?>" style="width:80%" /><br>
                <hr style="margin-top: 10px;margin-bottom: 10px;border: 0;border-top: 3px solid #000;">
                <h5>principal</h5>
            </div>
        </div>
    </div>

<?php
} else {
    ?>

    <?php if(isset($idcard->background)): ?>
        <style type="text/css">
            .background-div:before{
                opacity: 0.6;
                background-image: url(<?php echo base_url('uploads/'.$_SERVER['SERVER_NAME'].'/staff_id_card/background/') ?><?php echo $idcard->background ?>);
                background-repeat: no-repeat;
                background-size: cover;
                position: absolute;
                height: 100%;
                width: 100%;
            }
        </style>
    <?php endif; ?>


    <div class="row background-div" style="border: 1px solid;border-radius: 15px;overflow: hidden;position: relative;">
        <div class="col-sm-12" style="margin-top: 15px">
            <div class="col-sm-3">
                <img src="<?php echo base_url('uploads/'.$_SERVER['SERVER_NAME'].'/staff_id_card/logo/') ?><?php echo $idcard->logo; ?>" style="width: 100%" />
            </div>
            <div class="col-sm-9 text-center">
                <h4 style="font-weight: bold"><?php echo $idcard->school_name; ?></h4>
                <p style="font-size: 14px"><?php echo $idcard->school_address; ?></p>
            </div>
        </div>
        <div class="col-sm-12" style="margin-top: 15px">
            <div class="col-sm-3">
                <figure class="photo-cover" style="border:3px solid <?php echo $idcard->header_color;?>;border-radius: 15px;overflow: hidden;">
                    <img src="<?php echo base_url() . "uploads/".$_SERVER['SERVER_NAME']."/student_images/no_image.png"; ?>" class="img-responsive" />
                </figure>
            </div>
            <div class="col-sm-9">
                <ul class="vertlist">
                <?php
                if ($idcard->enable_staff_id == 1) {
                    echo "<li>"; echo $this->lang->line('staff_id');echo "<span>: 9000</span></li>";
                }
                ?>
                <?php
                if ($idcard->enable_staff_department == 1) {
                     echo "<li>"; echo $this->lang->line('department');echo "<span>: Admin</span></li>";
                }
                ?>
                <?php
                if ($idcard->enable_fathers_name == 1) {
                    echo "<li>"; echo $this->lang->line('father_name'); echo "<span>: Father Name</span></li>";
                }
                ?>
                <?php
                if ($idcard->enable_mothers_name == 1) {
                    echo "<li>"; echo $this->lang->line('mother_name'); echo "<span>: Mother Name</span></li>";
                }
                ?>
                <?php
                if ($idcard->enable_date_of_joining == 1) {
                    echo "<li>"; echo $this->lang->line('date_of_joining'); echo "<span>: "; echo date($this->customlib->getSchoolDateFormat(), $this->customlib->dateyyyymmddTodateformat($dummy_date)); echo "</span></li>";
                }
                ?>
                <?php
                if ($idcard->enable_permanent_address == 1) {
                    echo "<li>"; echo $this->lang->line('address'); echo "<span>: Kathmandu Nepal</span></li>";
                }
                ?>
                 <?php
                if ($idcard->enable_staff_phone == 1) {
                    echo "<li>"; echo $this->lang->line('phone'); echo "<span>: 9845624781</span></li>";
                }
                ?>
                <?php
                if ($idcard->enable_staff_dob == 1) {
                    echo "<li>"; echo $this->lang->line('date_of_birth'); echo "<span>: "; echo date($this->customlib->getSchoolDateFormat(), $this->customlib->dateyyyymmddTodateformat($dummy_date)); echo "</span></li>";
                }
                ?>
            </ul>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="col-sm-9">
            </div>
            <div class="col-sm-3 text-center">
                <img src="<?php echo base_url('uploads/'.$_SERVER['SERVER_NAME'].'/staff_id_card/signature/') ?><?php echo $idcard->sign_image; ?>" style="width:80%" /><br>
                <hr style="margin-top: 10px;margin-bottom: 10px;border: 0;border-top: 3px solid #000;">
                <h5>principal</h5>

            </div>
        </div>
    </div>
    <?php
}
?>
