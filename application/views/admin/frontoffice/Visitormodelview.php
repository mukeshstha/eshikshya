<div class="recipe-card">

    <article>

        <h2><?php echo $this->lang->line('name'); ?>: <?php print_r($data['name']); ?></h2>
        <h3><?php echo $this->lang->line('purpose'); ?>: <?php print_r($data['purpose']); ?></h3>
        <p></p>

        <ul>
            <li><span class="fa fa-phone"></span>
                <strong><?php echo $this->lang->line('phone'); ?></strong>
                <span><?php print_r($data['contact']); ?></span>
            </li>
            <li><span class="fa fa-users"></span>
                <strong><?php echo $this->lang->line('number_of_person'); ?></strong>
                <span><?php print_r($data['no_of_pepple']); ?></span>
            </li>
            <li><span class="fa fa-calendar"></span>
                <strong><?php echo $this->lang->line('date'); ?></strong>
                <span><?php print_r(date($this->customlib->getSchoolDateFormat(), $this->customlib->dateyyyymmddTodateformat($data['date']))); ?></span>
            </li>
            <li><span class="fa fa-clock-o"></span><strong><?php echo $this->lang->line('in_time'); ?></strong><span><?php echo $this->lang->line('in_time'); ?></span></li>
            <li><span class="fa fa-clock-o"></span><strong><?php echo $this->lang->line('out_time'); ?></strong><span><?php print_r($data['out_time']); ?></span></li>
            <li><span class="fa fa-file"></span><strong><?php echo $this->lang->line('id')." ".$this->lang->line('card'); ?></strong><span><?php echo $data['id_proof']; ?></span></li>
        </ul>
        <p class="ingredients"><span><?php echo $this->lang->line('note'); ?>:&nbsp;</span><?php print_r($data['note']); ?></p>
    </article>
</div>


<table class="table table-striped" style="display: none;">    
    <tr>
        <th class="border0"><?php echo $this->lang->line('purpose'); ?></th>
        <td class="border0"><?php print_r($data['purpose']); ?></td>
        <th class="border0"><?php echo $this->lang->line('name'); ?></th>
        <td class="border0"><?php print_r($data['name']); ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang->line('phone'); ?></th>
        <td><?php print_r($data['contact']); ?></td>
        <th><?php echo $this->lang->line('number_of_person'); ?></th>
        <td><?php print_r($data['no_of_pepple']); ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang->line('date'); ?></th>
        <td><?php print_r(date($this->customlib->getSchoolDateFormat(), $this->customlib->dateyyyymmddTodateformat($data['date']))); ?></td>
        <th><?php echo $this->lang->line('in_time'); ?></th>
        <td>c</td>
    </tr>
    <tr>
        <th><?php echo $this->lang->line('out_time'); ?></th>
        <td><?php print_r($data['out_time']); ?></td>
        <th><?php echo $this->lang->line('note'); ?></th>
        <td><?php print_r($data['note']); ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang->line('id')." ".$this->lang->line('card'); ?></th>
        <td><?php echo $data['id_proof']; ?></td>
        <th></th>
        <td></td>
    </tr>
</table>