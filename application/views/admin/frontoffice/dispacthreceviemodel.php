<div class="recipe-card">

    <article>

        <h2><?php echo $this->lang->line('to_title'); ?>: <?php print_r($data['to_title']); ?></h2>
        <h3><?php echo $this->lang->line('from_title'); ?>: <?php echo $this->lang->line('from_title'); ?></h3>
        <p></p>

        <ul>
            
            <li><span class="fa fa-calendar"></span>
                <strong><?php echo $this->lang->line('reference_no'); ?></strong>
                <span><?php print_r($data['reference_no']); ?></span>
            </li>
            <li><span class="fa fa-calendar"></span>
                <strong><?php echo $this->lang->line('address'); ?></strong>
                <span>
                    <?php print_r($data['address']); ?>
                </span>
            </li>
        
            <li>
                <span class="fa fa-file"></span>
                <strong><?php echo $this->lang->line('note'); ?></strong>
                <span><?php print_r($data['note']); ?></span>
            </li>
            <li>
                <span class="fa fa-file"></span>
                <strong><?php echo $this->lang->line('date'); ?></strong>
                <span><?php print_r(date($this->customlib->getSchoolDateFormat(), $this->customlib->dateyyyymmddTodateformat($data['date']))); ?></span>
            </li>
            
            
        </ul>
        
    </article>
</div>


<table class="table table-striped" style="display: none;">      
    <tr>
        <th class="border0"><?php echo $this->lang->line('to_title'); ?></th>
        <td class="border0"><?php print_r($data['to_title']); ?></td>
        <th class="border0"><?php echo $this->lang->line('reference_no'); ?></th>
        <td class="border0"><?php print_r($data['reference_no']); ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang->line('address'); ?></th>
        <td><?php print_r($data['address']); ?></td>
        <th><?php echo $this->lang->line('note'); ?></th>
        <td><?php print_r($data['note']); ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang->line('from_title'); ?></th>
        <td><?php print_r($data['from_title']); ?></td>
        <th><?php echo $this->lang->line('date'); ?></th>
        <td><?php print_r(date($this->customlib->getSchoolDateFormat(), $this->customlib->dateyyyymmddTodateformat($data['date']))); ?></td>
    </tr> 
</table>