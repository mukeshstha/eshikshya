<div class="recipe-card">

    <article>

        <h2><?php echo $this->lang->line('name'); ?>: <?php print_r($Call_data['name']); ?></h2>
        <h3><?php echo $this->lang->line('phone'); ?>: <?php print_r($Call_data['contact']); ?></h3>
        <p></p>

        <ul>
            <!-- <li><span class="fa fa-users"></span>
                <strong><?php echo $this->lang->line('number_of_person'); ?></strong>
                <span><?php print_r($Call_data['no_of_pepple']); ?></span>
            </li> -->
            <li><span class="fa fa-calendar"></span>
                <strong><?php echo $this->lang->line('date'); ?></strong>
                <span><?php print_r(date($this->customlib->getSchoolDateFormat(), $this->customlib->dateyyyymmddTodateformat($Call_data['date']))); ?></span>
            </li>
            <li><span class="fa fa-calendar"></span>
                <strong><?php echo $this->lang->line('next_follow_up_date'); ?></strong>
                <span>
                    <?php if($Call_data['follow_up_date']!='' && $Call_data['follow_up_date']!='0000-00-00'){ echo date($this->customlib->getSchoolDateFormat(), $this->customlib->dateyyyymmddTodateformat($Call_data['follow_up_date'])); } ?>
                </span>
            </li>
        
            <li>
                <span class="fa fa-file"></span>
                <strong><?php echo $this->lang->line('call_duration'); ?></strong>
                <span><?php print_r($Call_data['call_dureation']); ?></span>
            </li>
            <li>
                <span class="fa fa-file"></span>
                <strong><?php echo $this->lang->line('call_type'); ?></strong>
                <span><?php print_r($Call_data['call_type']); ?></span>
            </li>
            <li>
                <span class="fa fa-file"></span>
                <strong><?php echo $this->lang->line('description'); ?></strong>
                <span><?php print_r($Call_data['description']); ?></span>
            </li>
           
        </ul>
        <p class="ingredients"><span><?php echo $this->lang->line('note'); ?>:&nbsp;</span><?php print_r($Call_data['note']); ?></p>
    </article>
</div>


<table class="table table-striped" style="display: none;">     
    <tr>
        <th class="border0"><?php echo $this->lang->line('name'); ?></th>
        <td class="border0"><?php print_r($Call_data['name']); ?></td>
        <th class="border0"><?php echo $this->lang->line('phone'); ?></th>
        <td class="border0"> <?php print_r($Call_data['contact']); ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang->line('date'); ?></th>
        <td><?php echo date($this->customlib->getSchoolDateFormat(), $this->customlib->dateyyyymmddTodateformat($Call_data['date'])); ?></td>
        <th><?php echo $this->lang->line('next_follow_up_date'); ?></th>
        <td><?php if($Call_data['follow_up_date']!='' && $Call_data['follow_up_date']!='0000-00-00'){ echo date($this->customlib->getSchoolDateFormat(), $this->customlib->dateyyyymmddTodateformat($Call_data['follow_up_date'])); } ?></td>
        <!-- <td><?php echo date($this->customlib->getSchoolDateFormat(), $this->customlib->dateyyyymmddTodateformat($Call_data['follow_up_date'])); ?></td> -->
    </tr>
    <tr>
        <!-- <th><?php echo $this->lang->line('call_duration'); ?></th>
        <td><?php print_r($Call_data['call_dureation']); ?></td> -->
        <th><?php echo $this->lang->line('call_type'); ?></th>
        <td><?php print_r($Call_data['call_type']); ?></td>
    </tr>       
    <tr>
        <th><?php echo $this->lang->line('description'); ?></th>
        <td><?php print_r($Call_data['description']); ?></td>
        <th><?php echo $this->lang->line('note'); ?></th>
        <td><?php print_r($Call_data['note']); ?></td>
    </tr>   
</table>