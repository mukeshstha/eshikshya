<div class="recipe-card">

    <article>

        <h2><?php echo $this->lang->line('complain'); ?> #<?php print_r($complaint_data['id']); ?></h2>
        <h3><?php echo $this->lang->line('complain_type'); ?>: <?php print_r($complaint_data['complaint_type']); ?></h3>
        <p></p>

        <ul>
            
            <li><span class="fa fa-book"></span>
                <strong><?php echo $this->lang->line('source'); ?></strong>
                <span><?php print_r($complaint_data['source']); ?></span>
            </li>
            <li><span class="fa fa-user"></span>
                <strong><?php echo $this->lang->line('name'); ?></strong>
                <span>
                    <?php print_r($complaint_data['name']); ?>
                </span>
            </li>
        
            <li>
                <span class="fa fa-phone"></span>
                <strong><?php echo $this->lang->line('phone'); ?></strong>
                <span><?php print_r($complaint_data['contact']); ?></span>
            </li>
            <li>
                <span class="fa fa-calendar"></span>
                <strong><?php echo $this->lang->line('date'); ?></strong>
                <span><?php print_r(date($this->customlib->getSchoolDateFormat(), $this->customlib->dateyyyymmddTodateformat($complaint_data['date']))); ?></span>
            </li>
            <li>
                <span class="fa fa-tag"></span>
                <strong><?php echo $this->lang->line('description'); ?></strong>
                <span><?php print_r($complaint_data['description']); ?></span>
            </li>
            <li>
                <span class="fa fa-ticket"></span>
                <strong><?php echo $this->lang->line('action_taken'); ?></strong>
                <span><?php print_r($complaint_data['action_taken']); ?></span>
            </li>
            <li>
                <span class="fa fa-user-circle"></span>
                <strong><?php echo $this->lang->line('assigned'); ?></strong>
                <span><?php print_r($complaint_data['assigned']); ?></span>
            </li>
            <li>
                <span class="fa fa-sticky-note"></span>
                <strong><?php echo $this->lang->line('note'); ?></strong>
                <span><?php print_r($complaint_data['note']); ?></span>
            </li>
            
            
        </ul>
        
    </article>
</div>



<table class="table table-striped" style="display: none;">
    <?php // print_r($complaint_data); ?>
    <tr>
        <th class="border0"><?php echo $this->lang->line('complain'); ?> #</th>
        <td class="border0"><?php print_r($complaint_data['id']); ?></td>
        <th class="border0"><?php echo $this->lang->line('complain_type'); ?></th>
        <td class="border0"><?php print_r($complaint_data['complaint_type']); ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang->line('source'); ?></th>
        <td><?php print_r($complaint_data['source']); ?></td>
        <th><?php echo $this->lang->line('name'); ?></th>
        <td><?php print_r($complaint_data['name']); ?></td>
    </tr>

    <tr>
        <th><?php echo $this->lang->line('phone'); ?></th>
        <td><?php print_r($complaint_data['contact']); ?></td>
        <th><?php echo $this->lang->line('date'); ?></th>
        <td><?php print_r(date($this->customlib->getSchoolDateFormat(), $this->customlib->dateyyyymmddTodateformat($complaint_data['date']))); ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang->line('description'); ?></th>
        <td><?php print_r($complaint_data['description']); ?></td>
        <th><?php echo $this->lang->line('action_taken'); ?></th>
        <td><?php print_r($complaint_data['action_taken']); ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang->line('assigned'); ?></th>
        <td><?php print_r($complaint_data['assigned']); ?></td>
        <th><?php echo $this->lang->line('note'); ?></th>
        <td><?php print_r($complaint_data['note']); ?></td>
    </tr>
</tbody>
</table>