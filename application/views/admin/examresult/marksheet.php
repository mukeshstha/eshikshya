
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <i class="fa fa-map-o"></i> <?php echo $this->lang->line('examinations'); ?> <small><?php echo $this->lang->line('student_fee1'); ?></small>  </h1>
    </section>
    <!-- Main content -->
    <section class="content"> 
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-search"></i> <?php echo $this->lang->line('select_criteria'); ?></h3>
                    </div>
                    <div class="box-body">
                        <div class="filter-box">
                            <form role="form" action="<?php echo site_url('admin/examresult/marksheet') ?>" method="post" class="row">

                                <?php echo $this->customlib->getCSRF(); ?>
                                <div class="col-sm-6 col-lg-4 col-md-4">
                                    <div class="form-group">
                                        <label><?php echo $this->lang->line('exam') . " " . $this->lang->line('group'); ?></label><small class="req"> *</small>
                                        <select autofocus="" id="exam_group_id" name="exam_group_id" class="form-control select2" >
                                            <option value=""><?php echo $this->lang->line('select'); ?></option>
                                            <?php
                                            foreach ($examgrouplist as $ex_group_key => $ex_group_value) {
                                                ?>
                                                <option value="<?php echo $ex_group_value->id ?>" <?php
                                                if (set_value('exam_group_id') == $ex_group_value->id) {
                                                    echo "selected=selected";
                                                }
                                                ?>><?php echo $ex_group_value->name; ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                        </select>
                                        <span class="text-danger"><?php echo form_error('exam_group_id'); ?></span>
                                    </div>  
                                </div><!--./col-md-3-->
                                <div class="col-sm-6 col-lg-4 col-md-4">
                                    <div class="form-group">   
                                        <label><?php echo $this->lang->line('exam'); ?></label><small class="req"> *</small>
                                        <select  id="exam_id" name="exam_id" class="form-control select2" >
                                            <option value=""><?php echo $this->lang->line('select'); ?></option>
                                        </select>
                                        <span class="text-danger"><?php echo form_error('exam_id'); ?></span>
                                    </div>  
                                </div><!--./col-md-3-->
                                <div class="col-sm-6 col-lg-4 col-md-4">
                                    <div class="form-group">  
                                        <label><?php echo $this->lang->line('session'); ?></label><small class="req"> *</small>
                                        <select  id="session_id" name="session_id" class="form-control" >
                                            <option value=""><?php echo $this->lang->line('select'); ?></option>
                                            <?php
                                            foreach ($sessionlist as $session) {
                                                ?>
                                                <option value="<?php echo $session['id'] ?>" <?php
                                                if (set_value('session_id') == $session['id']) {
                                                    echo "selected=selected";
                                                }
                                                ?>><?php echo $session['session'] ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                        </select>
                                        <span class="text-danger"><?php echo form_error('session_id'); ?></span>
                                    </div>  
                                </div>
                                <div class="col-sm-6 col-lg-4 col-md-4">
                                    <div class="form-group">   
                                        <label><?php echo $this->lang->line('class'); ?></label><small class="req"> *</small>
                                        <select id="class_id" name="class_id" class="form-control" >
                                            <option value=""><?php echo $this->lang->line('select'); ?></option>
                                            <?php
                                            foreach ($classlist as $class) {
                                                ?>
                                                <option value="<?php echo $class['id'] ?>" <?php
                                                if (set_value('class_id') == $class['id']) {
                                                    echo "selected=selected";
                                                }
                                                ?>><?php echo $class['class'] ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                        </select>
                                        <span class="text-danger"><?php echo form_error('class_id'); ?></span>
                                    </div>  
                                </div>

                                <div class="col-sm-6 col-lg-4 col-md-4">
                                    <div class="form-group"> 
                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('section'); ?></label><small class="req"> *</small>
                                        <select  id="section_id" name="section_id" class="form-control" >
                                            <option value=""><?php echo $this->lang->line('select'); ?></option>
                                        </select>
                                        <span class="text-danger"><?php echo form_error('section_id'); ?></span>
                                    </div>
                                </div>

                                <div class="col-sm-6 col-lg-4 col-md-4">

                                    <div class="form-group">
                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('marksheet') . " " . $this->lang->line('template') ?></label><small class="req"> *</small>
                                        <select  id="marksheet" name="marksheet" class="form-control" >
                                            <option value=""><?php echo $this->lang->line('select'); ?></option>
                                            <?php
                                            foreach ($marksheetlist as $marksheet) {
                                                ?>
                                                <option value="<?php echo $marksheet->id ?>" <?php
                                                if (set_value('marksheet') == $marksheet->id) {
                                                    echo "selected=selected";
                                                }
                                                ?>><?php echo $marksheet->template; ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                        </select>
                                        <span class="text-danger"><?php echo form_error('marksheet'); ?></span>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <button type="submit" name="search" value="search_filter" class="btn btn-primary pull-right btn-sm checkbox-toggle"><i class="fa fa-search"></i> <?php echo $this->lang->line('search'); ?></button>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>

                    <?php
                    if (isset($studentList)) {
                        ?>
                        <?php
                                if (empty($studentList)) {
                                    ?>

                                    <?php
                                } else {
                                    $count = 1;
                                    $student_list_array = array();
                                    foreach ($studentList as $student_key => $student_value) {

                                        $result_status = 1;
                                        $no_subject_result = 0;
                                        $student_array = array();
                                        $student_array['admission_no'] = $student_value->admission_no;
                                        $student_array['profile_roll_no'] = ($student_value->roll_no != 0) ? $student_value->roll_no : "-";
                                        $student_array['exam_group_class_batch_exam_student_id']=$student_value->exam_group_class_batch_exam_student_id;
                                         $student_array['exam_roll_no'] = ($student_value->exam_roll_no != 0) ? $student_value->exam_roll_no : "-";
                                        $student_array['student_id'] = $student_value->student_id;
                                        $student_array['name'] = $this->customlib->getFullName($student_value->firstname,$student_value->middlename,$student_value->lastname,$sch_setting->middlename,$sch_setting->lastname);
                                        $total_subject = count($subjectList);
                                        $result_total_subject = 0;

                                        if (!empty($subjectList)) {
                                            $student_array['subject_added'] = true;
                                            $total_marks = 0;
                                            $get_marks = 0;
                                            $get_marks_practical = 0;
                                            $get_percentage = 0;
                                            $total_credit_hour = 0;
                                            $total_quality_point = 0;
                                            $subject_result_list = array();
                                            $subject_status = true;
                                            $practical_max = 0;

                                            foreach ($subjectList as $subject_key => $subject_value) {
                                                $total_marks = $total_marks + $subject_value->max_marks;
                                                $practical_max =$practical_max + $subject_value->practical_max;
                                                $result = getSubjectMarks($student_value->subject_results, $subject_value->subject_id);
                                                $subject_result = array();
                                                $subject_result['result_status'] = false;

                                                if ($result) {
                                                
                                                    $result_total_subject++;
                                                    $subject_status = false;
                                                    $subject_result['result_status'] = true;
                                                    $no_subject_result = 1;
                                                    $subject_credit_hour = $subject_value->credit_hours;
                                                    $total_credit_hour = $total_credit_hour + $subject_value->credit_hours;
                                                    $percentage_grade = ($result->get_marks * 100) / $result->max_marks;
                                                    $point = findGradePoints($exam_grades, $percentage_grade);
                                                    $subject_result['point'] = $point;
                                                    $subject_result['subject_credit_hour'] = $subject_credit_hour;

                                                    $total_quality_point = $total_quality_point + ($point * $subject_credit_hour);
                                                    $get_marks = $get_marks + $result->get_marks;
                                                    $subject_result['get_marks'] = $result->get_marks;
                                                    $get_marks_practical = $get_marks_practical + $result->get_marks_practical;
                                                    $subject_result['get_marks_practical'] =  $result->get_marks_practical;
                                                    $percentage_grade = ($result->get_marks * 100) / $subject_value->max_marks;
                                                    $subject_result['get_exam_grade'] = get_ExamGrade($exam_grades, $percentage_grade);
                                                    $subject_result['attendence'] = $result->attendence;
                                                    $subject_result['note'] = $result->note;
                                                    

                                                    if (($result->get_marks < $subject_value->min_marks) || $result->attendence == "absent") {
                                                        $result_status = 0;
                                                    }
                                                }
                                                $subject_result_list[] = $subject_result;
                                            }


                                            $student_array['total_subject'] = $total_subject;
                                            $student_array['result_total_subject'] = $result_total_subject;
                                            $student_array['subjet_results'] = $subject_result_list;
                                            $student_array['get_marks'] = $get_marks;
                                            $student_array['get_marks_practical'] = $get_marks_practical;
                                            $get_total_marks= $get_marks + $get_marks_practical;
                                            $fullmarks=$practical_max + $total_marks;

                                            $student_array['total_marks'] = $total_marks;
                                            $student_array['grand_total'] = number_format($get_total_marks, 2, '.', '') . "/" . number_format($fullmarks, 2, '.', '');  
                                            $total_percentage = ($get_total_marks * 100) / $fullmarks;
                                            $student_array['percentage'] = number_format($total_percentage, 2, '.', '');

                                            if ($total_quality_point > 0 && $total_credit_hour > 0) {
                                                $exam_qulity_point = number_format($total_quality_point / $total_credit_hour, 2, '.', '');
                                            } else {
                                                $exam_qulity_point = number_format(0, 2, '.', '');
                                            }

                                            $student_array['quality_points'] = $total_quality_point . "/" . $total_credit_hour . "=" . $exam_qulity_point;
                                            $student_array['no_subject_result'] = $no_subject_result;
                                            $student_array['exam_qulity_point'] = $exam_qulity_point;

                                            $student_array['result_status'] = $result_status;
                                        } else {
                                            $student_array['subject_added'] = false;
                                        }

                                        $student_list_array[] = $student_array;
                                    }

                                    if ($student_array['subject_added']) {
                                        if ($exam_details->exam_group_type != "gpa") {
                                            aasort($student_list_array);
                                        } else {
                                            aasort_gpa($student_list_array);
                                        }
                                    }
                                }
                                ?>
                        <form method="post" action="<?php echo base_url('admin/examresult/printmarksheet') ?>" id="printMarksheet">
                            <input type="hidden" name="marksheet_template" value="<?php echo $marksheet_template; ?>">


                            <div class="box-header ptbnull"></div>  
                            <div class="box-header ptbnull">
                                <h3 class="box-title titlefix"><i class="fa fa-users"></i> <?php echo $this->lang->line('student'); ?> <?php echo $this->lang->line('list'); ?></h3>
                                <button  class="btn btn-primary btn-sm printSelected pull-right" type="submit" name="generate" title="generate multiple certificate"><?php echo $this->lang->line('generate'); ?></button>
                            </div>
                            <div class="box-body">
                                <input type="hidden" name="post_exam_id" value="<?php echo $exam_id; ?>">
                                <input type="hidden" name="post_exam_group_id" value="<?php echo $exam_group_id; ?>">
                                <div class="tab-pane active table-responsive no-padding" id="tab_1">
                                    <table class="table table-striped table-bordered table-hover" cellspacing="0" width="100%" id="marks">
                                        <thead>
                                            <tr>
                                                <th><input type="checkbox" id="select_all" /></th>
                                                <th>Rank</th>
                                                <th><?php echo $this->lang->line('admission_no'); ?></th>
                                                <th>Roll No.</th>
                                                <th><?php echo $this->lang->line('student_name'); ?></th>
                                                <th><?php echo $this->lang->line('grand_total'); ?></th>
                                                <th><?php echo $this->lang->line('percentage'); ?></th>
                                                <th><?php echo $this->lang->line('status'); ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        if (!empty($student_list_array)) {
                                            $rank_count = 1;
                                            foreach ($student_list_array as $student_list_value) {
                                                ?>
                                                <tr>
                                                    <td class="text-center"><input type="checkbox" class="checkbox selected-students center-block"  name="exam_group_class_batch_exam_student_id[]" data-student_id="<?php echo $student_list_value['exam_group_class_batch_exam_student_id']; ?>" value="<?php echo $student_list_value['exam_group_class_batch_exam_student_id']; ?>">
                                                        <?php
//                                                        print_r($student_list_value);
                                                            if ($student_list_value['total_subject'] > 0 && $student_list_value['result_total_subject'] >= 1){
                                                                    if ($student_list_value['result_status']) {
                                                                        ?>
                                                    <input type="hidden" name="rank[<?php echo $student_list_value['exam_group_class_batch_exam_student_id']; ?>]" value="<?php echo $rank_count; ?>">
                                                    <?php }
                                                    else{ ?>
                                                        <input type="hidden" name="rank[<?php echo $student_list_value['exam_group_class_batch_exam_student_id']; ?>]" value="-">
                                                    <?php }} ?>
                                                        </td>
                                                    
                                                        
                                                    <td><?php echo $rank_count; ?></td>
                                                    <td><?php echo $student_list_value['admission_no']; ?></td>
                                                    <td>
                                                        <?php 
                                                      echo ($exam_details->use_exam_roll_no)?$student_list_value['exam_roll_no']:$student_list_value['profile_roll_no']; ?> </td>
                                                    <td>
                                                        <a href="<?php echo base_url(); ?>student/view/<?php echo $student_list_value['student_id']; ?>"><?php echo $student_list_value['name'];
 
                                                         ?>
                                                        </a>
                                                    </td>
                                                    <?php
                                                    if ($student_list_value['subject_added']) {

                                                        if (!empty($student_list_value['subjet_results'])) {
                                                            foreach ($student_list_value['subjet_results'] as $result_key => $result_value) {
                                                                ?>
                                                                <div style="display:none">
                                                                    <?php
    if ($result_value['result_status']) {
        if ($exam_details->exam_group_type == "gpa") {
            echo $result_value['point'] . " X " . $result_value['subject_credit_hour'] . " = " . number_format($result_value['point'] * $result_value['subject_credit_hour'], 2, '.', '');
        } else{
                 echo $result_value['get_marks'] . "+" . $result_value['get_marks_practical'] .($result_value['get_exam_grade'] == "-" ? "": " (" . $result_value['get_exam_grade'] . ")" );
    }


        if ($result_value['attendence'] == "absent") {
            ?>  
            <p class="text">
            <?php echo $this->lang->line($result_value['attendence']); ?>
            </p>        
            <?php
        }
        ?>
        <p class="text"><?php echo $result_value['note']; ?></p>
        <?php
    }
    ?>
                                                                </div>
                                                                <?php
                                                            }
                                                        }

                                                        if ($exam_details->exam_group_type != "gpa") {
                                                            ?>
                                                            <td>
                    <?php echo $student_list_value['grand_total']; ?>
                                                            </td>
                                                            <td>
                                                            <?php echo $student_list_value['percentage']; ?>
                                                            </td>
                                                            <?php
                                                        }
                                                        ?>
                                                        <td>
                                                            <?php
//                                                        print_r($student_list_value);
                                                            if ($student_list_value['total_subject'] > 0 && $student_list_value['result_total_subject'] >= 1)
                                                                if ($exam_details->exam_group_type == "gpa") {
                                                                    echo $student_list_value['quality_points'];
                                                                } else {
                                                                    if ($student_list_value['result_status']) {
                                                                        ?>
                                                                        <label class="label label-success"><?php echo $this->lang->line('pass'); ?><label>
                                                                                <?php
                                                                            } else {
                                                                                ?>
                                                                                <label class="label label-danger"><?php echo $this->lang->line('fail'); ?><label>
                                                                                        <?php
                                                                                    }
                                                                                }
                                                                            ?>
                                                                            </td>
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                        </tr>
                                                                        <?php
                                                                        $rank_count++;
                                                                    }
                                                                }
                                                                ?>
                                                                </tbody>
                                        
                                    </table>
                                </div>                                                                           
                            </div>                                           

                        </form>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
    </section>
</div>

<script type="text/javascript">
 $(document).ready(function () {
        $('.select2').select2();

    });
    var date_format = '<?php echo $result = strtr($this->customlib->getSchoolDateFormat(), ['d' => 'dd', 'm' => 'mm', 'Y' => 'yyyy']) ?>';
    var class_id = '<?php echo set_value('class_id') ?>';
    var section_id = '<?php echo set_value('section_id') ?>';
    var session_id = '<?php echo set_value('session_id') ?>';
    var exam_group_id = '<?php echo set_value('exam_group_id') ?>';
    var exam_id = '<?php echo set_value('exam_id') ?>';
    getSectionByClass(class_id, section_id);
    getExamByExamgroup(exam_group_id, exam_id);
    $(document).on('change', '#exam_group_id', function (e) {
        $('#exam_id').html("");
        var exam_group_id = $(this).val();
        getExamByExamgroup(exam_group_id, 0);
    });

    $(document).on('change', '#class_id', function (e) {
        $('#section_id').html("");
        var class_id = $(this).val();
        getSectionByClass(class_id, 0);
    });

    function getSectionByClass(class_id, section_id) {
        if (class_id != "") {
            $('#section_id').html("");
            var base_url = '<?php echo base_url() ?>';
            var div_data = '<option value=""><?php echo $this->lang->line('select'); ?></option>';

            $.ajax({
                type: "GET",
                url: base_url + "sections/getByClass",
                data: {'class_id': class_id},
                dataType: "json",
                beforeSend: function () {
                    $('#section_id').addClass('dropdownloading');
                },
                success: function (data) {
                    $.each(data, function (i, obj)
                    {
                        var sel = "";
                        if (section_id == obj.section_id) {
                            sel = "selected";
                        }
                        div_data += "<option value=" + obj.section_id + " " + sel + ">" + obj.section + "</option>";
                    });
                    $('#section_id').append(div_data);
                },
                complete: function () {
                    $('#section_id').removeClass('dropdownloading');
                }
            });
        }
    }

    function getExamByExamgroup(exam_group_id, exam_id) {

        if (exam_group_id !== "") {
            $('#exam_id').html("");
            var base_url = '<?php echo base_url() ?>';
            var div_data = '<option value=""><?php echo $this->lang->line('select'); ?></option>';


            $.ajax({
                type: "POST",
                url: base_url + "admin/examgroup/getExamByExamgroup",
                data: {'exam_group_id': exam_group_id},
                dataType: "json",
                beforeSend: function () {
                    $('#exam_id').addClass('dropdownloading');
                },
                success: function (data) {
                    $.each(data, function (i, obj)
                    {
                        var sel = "";
                        if (exam_id === obj.id) {
                            sel = "selected";
                        }
                        div_data += "<option value=" + obj.id + " " + sel + ">" + obj.exam + "</option>";
                    });

                    $('#exam_id').append(div_data);
                    $('#exam_id').trigger('change');
                },
                complete: function () {
                    $('#exam_id').removeClass('dropdownloading');
                }
            });
        }
    }
</script>
<script>

    $(document).on('submit', 'form#printMarksheet', function (e) {

        e.preventDefault();
        var form = $(this);
        var subsubmit_button = $(this).find(':submit');
        var formdata = form.serializeArray();
        $.each(formdata, function(i, data){
          console.log(data.name + ":" + data.value + " ");

        });


        console.log(formdata);
        var list_selected =  $('form#printMarksheet .selected-students:checked').length;
      if(list_selected > 0){
        $.ajax({
            type: "POST",
            url: form.attr('action'),
            data: formdata, // serializes the form's elements.
            dataType: "JSON", // serializes the form's elements.
            beforeSend: function () {
                subsubmit_button.button('loading');
            },
            success: function (response)
            {
                Popup(response.page);
            },
            error: function (xhr) { // if error occured

                alert("Error occured.please try again");
                subsubmit_button.button('reset');
            },
            complete: function () {
                subsubmit_button.button('reset');
            }
        });
      }else{
         confirm("<?php echo $this->lang->line('please_select_student'); ?>");
      }
    });


    $(document).on('click', '#select_all', function () {
        $(this).closest('table').find('td input:checkbox').prop('checked', this.checked);
    });

</script>
<script type="text/javascript">

    var base_url = '<?php echo base_url() ?>';
    function Popup(data)
    {

        var frame1 = $('<iframe />');
        frame1[0].name = "frame1";
        $("body").append(frame1);
        var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
        frameDoc.document.open();
//Create a new HTML document.
        frameDoc.document.write('<html>');
        frameDoc.document.write('<head>');
        frameDoc.document.write('<title></title>');
        frameDoc.document.write('</head>');
        frameDoc.document.write('<body>');
        frameDoc.document.write(data);
        frameDoc.document.write('</body>');
        frameDoc.document.write('</html>');
        frameDoc.document.close();
        setTimeout(function () {
            window.frames["frame1"].focus();
            window.frames["frame1"].print();
            frame1.remove();
        }, 500);
        return true;
    }
</script>

<?php

                                                            function getSubjectMarks($subject_results, $subject_id) {
                                                                if (!empty($subject_results)) {
                                                                    foreach ($subject_results as $subject_result_key => $subject_result_value) {
                                                                        if ($subject_id == $subject_result_value->subject_id) {
                                                                            return $subject_result_value;
                                                                        }
                                                                    }
                                                                }
                                                                return false;
                                                            }

                                                            function get_ExamGrade($exam_grades, $percentage) {
                                                                if (!empty($exam_grades)) {
                                                                    foreach ($exam_grades as $exam_grade_key => $exam_grade_value) {

                                                                        if ($exam_grade_value->mark_from >= $percentage && $exam_grade_value->mark_upto <= $percentage) {
                                                                            return $exam_grade_value->name;
                                                                        }
                                                                    }
                                                                }

                                                                return "-";
                                                            }

                                                            function findGradePoints($exam_grades, $percentage) {

                                                                if (!empty($exam_grades)) {
                                                                    foreach ($exam_grades as $exam_grade_key => $exam_grade_value) {

                                                                        if ($exam_grade_value->mark_from >= $percentage && $exam_grade_value->mark_upto <= $percentage) {
                                                                            return $exam_grade_value->point;
                                                                        }
                                                                    }
                                                                }

                                                                return 0;
                                                            }

                                                            function aasort(&$arr) {
                                                                array_multisort(
                                                                        array_column($arr, 'result_status'), SORT_DESC, array_column($arr, 'percentage'), SORT_DESC, $arr);
                                                         
                                                            }

                                                            function aasort_gpa(&$arr) {
                                                                array_multisort(
                                                                        array_column($arr, 'exam_qulity_point'), SORT_DESC, $arr);
                                                        
                                                            }
                                                            ?>


<script type="text/javascript">
    $(function () {
        $('#marks').DataTable();
    });
</script>



<!-- <tbody style="display: none;">
                                            <?php
                                            if (empty($studentList)) {
                                                ?>

                                                <?php
                                            } else {
                                                $count = 1;
                                                foreach ($studentList as $student_key => $student_value) {
                                                  
                                                    ?>
                                                    <tr>
                                                        <td class="text-center"><input type="checkbox" class="checkbox center-block"  name="exam_group_class_batch_exam_student_id[]" data-student_id="<?php echo $student_value->exam_group_class_batch_exam_student_id; ?>" value="<?php echo $student_value->exam_group_class_batch_exam_student_id; ?>">

                                                        </td>
                                                        <td><?php echo $student_value->admission_no; ?></td>
                                                        <td>
            <a href="<?php echo base_url(); ?>student/view/<?php echo $student_value->student_id; ?>"><?php echo $this->customlib->getFullName($student_value->firstname,$student_value->middlename,$student_value->lastname,$sch_setting->middlename,$sch_setting->lastname); ?>
                                                            </a>
                                                        </td>

                                                        <td><?php echo $student_value->father_name;
                                        ;
                                                    ?></td>
                                                        <td><?php 
                                                            if (!empty($student_value->dob) && $student_value->dob != '0000-00-00') {
                                                            echo date($this->customlib->getSchoolDateFormat(), $this->customlib->dateyyyymmddTodateformat($student_value->dob)); }?></td>
                                                        <td><?php echo $student_value->gender; ?></td>
                                                        <td><?php echo $student_value->category; ?></td>
                                                        <td><?php echo $student_value->mobileno; ?></td>
                                                    </tr>
                                                    <?php
                                                    $count++;
                                                }
                                            }
                                            ?>
                                        </tbody> -->