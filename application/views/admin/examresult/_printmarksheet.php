<style type="text/css">

    @media print {

        .pagebreak { page-break-before: always; } /* page-break-after works, as well */

    }

    @media print {

        .body{padding: 0; margin:0; font-family: arial; color: #000; font-size: 11px !important; line-height: 9px;}

            .tableone{}

            .tableone td{border:1px solid #000; padding: 5px 0}

            .denifittable th{border-top: 1px solid #000;}

            .denifittable th,

            .denifittable td {border-bottom: 1px solid #000;

                              border-collapse: collapse;

                              border-left: 1px solid #000;

                              font-size: 11px;

                              line-height: 9px;

                          }

            .denifittable tr th {
            	padding: 7px 0px; font-weight: bold;
            }

            .denifittable tr td {
            	padding: 7px 0px; font-weight: bold;
            }

            .belowtable tr th {
            	padding: 0px 0px;
            }

            .belowtable tr td {
            	padding: 0px 0px;
            }


            .belowtable th,

            .belowtable td {border-bottom: 1px solid #999;

                              border-collapse: collapse;

                              border-left: 1px solid #999;

                              font-size: 10px;

                              line-height: 8px;

                          }

            .tcmybg {

                background:top center;

                background-size: 100% 100%;

                position: absolute;

                top: 0;

                left: 0;

                bottom: 0;

                z-index: 0;

            }

            .table.table-bordered td{

                border: 1px solid #000;

            }

        .page-break { display: block; page-break-before: always; }

        .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12 {

            float: left;

        }

        .col-sm-12 {

            width: 100%;

        }

        .col-sm-11 {

            width: 91.66666667%;

        }

        .col-sm-10 {

            width: 83.33333333%;

        }

        .col-sm-9 {

            width: 75%;

        }

        .col-sm-8 {

            width: 66.66666667%;

        }

        .col-sm-7 {

            width: 58.33333333%;

        }

        .col-sm-6 {

            width: 50%;

        }

        .col-sm-5 {

            width: 41.66666667%;

        }

        .col-sm-4 {

            width: 33.33333333%;

        }

        .col-sm-3 {

            width: 25%;

        }

        .col-sm-2 {

            width: 16.66666667%;

        }

        .col-sm-1 {

            width: 8.33333333%;

        }

        .col-sm-pull-12 {

            right: 100%;

        }

        .col-sm-pull-11 {

            right: 91.66666667%;

        }

        .col-sm-pull-10 {

            right: 83.33333333%;

        }

        .col-sm-pull-9 {

            right: 75%;

        }

        .col-sm-pull-8 {

            right: 66.66666667%;

        }

        .col-sm-pull-7 {

            right: 58.33333333%;

        }

        .col-sm-pull-6 {

            right: 50%;

        }

        .col-sm-pull-5 {

            right: 41.66666667%;

        }

        .col-sm-pull-4 {

            right: 33.33333333%;

        }

        .col-sm-pull-3 {

            right: 25%;

        }

        .col-sm-pull-2 {

            right: 16.66666667%;

        }

        .col-sm-pull-1 {

            right: 8.33333333%;

        }

        .col-sm-pull-0 {

            right: auto;

        }

        .col-sm-push-12 {

            left: 100%;

        }

        .col-sm-push-11 {

            left: 91.66666667%;

        }

        .col-sm-push-10 {

            left: 83.33333333%;

        }

        .col-sm-push-9 {

            left: 75%;

        }

        .col-sm-push-8 {

            left: 66.66666667%;

        }

        .col-sm-push-7 {

            left: 58.33333333%;

        }

        .col-sm-push-6 {

            left: 50%;

        }

        .col-sm-push-5 {

            left: 41.66666667%;

        }

        .col-sm-push-4 {

            left: 33.33333333%;

        }

        .col-sm-push-3 {

            left: 25%;

        }

        .col-sm-push-2 {

            left: 16.66666667%;

        }

        .col-sm-push-1 {

            left: 8.33333333%;

        }

        .col-sm-push-0 {

            left: auto;

        }

        .col-sm-offset-12 {

            margin-left: 100%;

        }

        .col-sm-offset-11 {

            margin-left: 91.66666667%;

        }

        .col-sm-offset-10 {

            margin-left: 83.33333333%;

        }

        .col-sm-offset-9 {

            margin-left: 75%;

        }

        .col-sm-offset-8 {

            margin-left: 66.66666667%;

        }

        .col-sm-offset-7 {

            margin-left: 58.33333333%;

        }

        .col-sm-offset-6 {

            margin-left: 50%;

        }

        .col-sm-offset-5 {

            margin-left: 41.66666667%;

        }

        .col-sm-offset-4 {

            margin-left: 33.33333333%;

        }

        .col-sm-offset-3 {

            margin-left: 25%;

        }

        .col-sm-offset-2 {

            margin-left: 16.66666667%;

        }

        .col-sm-offset-1 {

            margin-left: 8.33333333%;

        }

        .col-sm-offset-0 {

            margin-left: 0%;

        }

        .visible-xs {

            display: none !important;

        }

        .hidden-xs {

            display: block !important;

        }

        table.hidden-xs {

            display: table;

        }

        tr.hidden-xs {

            display: table-row !important;

        }

        th.hidden-xs,

        td.hidden-xs {

            display: table-cell !important;

        }

        .hidden-xs.hidden-print {

            display: none !important;

        }

        .hidden-sm {

            display: none !important;

        }

        .visible-sm {

            display: block !important;

        }

        table.visible-sm {

            display: table;

        }

        tr.visible-sm {

            display: table-row !important;

        }

        th.visible-sm,

        td.visible-sm {

            display: table-cell !important;

        }

        

    }

</style>

<style type="text/css">

            @media print {

                .print-table th,.print-table tr.back-color td{

                    background-color: #a6a6a6 !important;

                    -webkit-print-color-adjust: exact; 

                }

                .back-color{

                    background-color: #a6a6a6 !important;

                    margin-top: 10px;

                    padding: 5px;

                    -webkit-print-color-adjust: exact; 

                }

                .grey-back{

                    background-color: #f5f5f5 !important;

                    -webkit-print-color-adjust: exact; 

                }

                .back-color td, .back-color th{

                    background-color: #a6a6a6 !important;

                    -webkit-print-color-adjust: exact; 

                }

                .print-table td,.print-table th{

                    border: 1px solid #000;

                }
                .column ul li{
                    padding-bottom: 10px;
                }

                .column ul{
                    list-style-type: none;
                    columns: 2;
                    -webkit-columns: 2;
                    -moz-columns: 2;
                    display: inline-block;
                    width: 100%;
                    padding-left: 0px;
                }

            }

        </style>

<link rel="stylesheet" href="<?php echo base_url(); ?>backend/bootstrap/css/bootstrap.min.css"> 

<?php 
$domain=$_SERVER['SERVER_NAME'];
if (empty($marksheet)) {

    ?>

    <div class="alert alter-info">

        <?php echo $this->lang->line('no_record_found'); ?>

    </div>

    <?php

} else {



    if ($marksheet['exam_connection'] == 0) {

        if (!empty($marksheet['students'])) {

            foreach ($marksheet['students'] as $student_key => $student_value) {



				

                $result_status = 1;

                $absent_status = false;

                $percentage_total = 0;

                ?>







                <?php

        if ($template->background_img != "") {

            ?> 

            <img src="<?php echo base_url('uploads/marksheet/' . $template->background_img); ?>" class="tcmybg" width="100%" height="100%" />

            <?php

        }

        ?>



        <div style="width: 100%; margin: 0 auto; border:1px solid #000; padding: 5px 10px;position: relative;">

            <div class="row">

                <div class="col-sm-2" style="margin: auto;text-align: center;">


                    <?php

                        if ($template->left_logo != "") {
                            
                            ?>

                            <img src="<?php echo base_url('uploads/'.$domain.'/marksheet/' . $template->left_logo); ?>" alt="<?php echo $this->customlib->getAppName() ?>" style="width: 80%;height: auto;margin-bottom: 0px"/>  





                            <?php

                        }

                    ?>

                </div>

                

                <div class="col-sm-9 text-center">

                    <p style="text-align:center;line-height: 8px"><i><?php echo $template->heading; ?></i></p>

                    <p style="font-size: 18px; font-weight: bold; text-align: center; text-transform: uppercase;line-height: 18px;"><?php echo $this->setting_model->getCurrentSchoolName(); ?></p>

                    <p style="font-weight: bold; text-align: center; text-transform: uppercase;line-height: 8px;"><?php echo $this->setting_model->get_address() ?>,&nbsp;<?php echo $this->lang->line('phone') ?> : <?php echo $this->setting_model->get_phone() ?></p>

                    <p style="text-align:center;line-height: 8px;"><?php echo $this->lang->line('email') ?> : <?php echo $this->setting_model->get_email() ?></p>
                    <p style="text-align:center;line-height: 8px;"><?php echo "Website" ?> : <?php echo $template->exam_name ?></p>

                </div>

                <div class="col-sm-1">

                </div>

           

            </div>
            <div class="col-sm-12">
            	<h4 style="text-decoration: underline;text-align: center;font-weight: bold;margin-top: 0px;padding-left: 35px">Student's Progress Report</h4>
            </div>

            <div class="row">

                <div class="col-sm-12">

                    <div class="col-md-3">

                    </div>

                    <div class="col-md-6">

                        <p class="grey-back" style="text-transform:uppercase !important; font-weight:bold !important;background: #c5c5c5 !important; padding: 5px 5px;text-align: center;"><?php echo $exam->exam; ?><?php echo " " .substr($exam->session, 0, strpos($exam->session, "-")); ?></p>

                    </div>

                    <div class="col-md-3">

                    </div>

                </div>

            </div>

            <!-- <p style="text-align:center;font-weight: bold;margin-bottom: 10px;">MARK & GRADE-SHEET</p> -->

            <div class="row">

                <div class="col-sm-12 column">
                    <ul>
                    <?php if ($template->is_name) { ?>

                        <li style="text-transform: uppercase;font-size: 12px;line-height: 10px"><?php echo $this->lang->line('name'); ?> :<span style="padding-left: 30px; font-weight: bold;"><?php echo $this->customlib->getFullName($student_value['firstname'],$student_value['middlename'],$student_value['lastname'],$sch_setting->middlename,$sch_setting->lastname);  ?></span></li>

                    <?php } ?>



                    



                    <?php if ($template->is_father_name) { ?>

                        <li style="text-transform: uppercase !important;font-size: 12px;line-height: 10px">

                            <?php echo $this->lang->line('father_name') ?>:<span style="padding-left: 30px; font-weight: bold;"><?php echo $student_value['father_name']; ?></span>

                        </li>

                    <?php } ?>



                    <?php if ($template->is_mother_name) { ?>

                        <li style="text-transform: uppercase !important;font-size: 12px;line-height: 10px">

                            <?php echo $this->lang->line('exam_mother_name'); ?>:<span style="padding-left: 30px; font-weight: bold;"><?php echo $student_value['mother_name']; ?></span>

                        </li>

                    <?php } ?>

                    <?php if ($template->is_class || $template->is_section) { ?>

                        <li style="text-transform: uppercase;font-size: 12px;line-height: 10px"><?php echo $this->lang->line('grade'); ?> :<span style="padding-left: 30px; font-weight: bold;">

                            <?php

                                if ($template->is_class && $template->is_section) {

                                    ?>

                                    <!-- <?php echo $this->lang->line('class'); ?><span style="padding-left: 30px; font-weight: bold;"> -->

                                        <?php echo $student_value['class'] . " (" . $student_value['section'] . ")"; ?> 

                                    <?php

                                } elseif ($template->is_class) {

                                    ?>

                                    <?php echo $student_value['class_id']; ?> 

                                    <?php

                                } elseif ($template->is_section) {

                                    ?>

                                    <?php echo $student_value['section']; ?>

                                    <?php

                                }

                                ?>

                        </span></li>

                    <?php } ?>

                    <?php if ($template->is_roll_no) { ?>

                        <?php if ($template->is_roll_no) {
                            //$roll_no=($exam->use_exam_roll_no) ? $student_value['exam_roll_no']:$student_value['student_roll_no']; 
                            $roll_no=$student_value['student_roll_no']; 
                            ?>

                        <li style="text-transform: uppercase;font-size: 12px;line-height: 10px"><?php echo $this->lang->line('roll_no') ?> :<span style="padding-left: 30px; font-weight: bold;"><?php echo $roll_no; ?></span></li>





                    <?php } ?>

                    <?php } ?>



                    <?php if ($template->is_dob) { ?>

                        <li style="text-transform: uppercase;font-size: 12px;line-height: 10px"><?php echo $this->lang->line('date') . " " . $this->lang->line('of') . " " . $this->lang->line('birth'); ?> :<span style="padding-left: 30px; font-weight: bold;"><?php echo $this->customlib->dateformat($student_value['dob']); ?></span></li>

                    <?php } ?>



                    <?php if ($template->is_admission_no) { ?>

                        <li style="text-transform: uppercase;font-size: 12px;line-height: 10px;"><?php echo $this->lang->line('admission_no') ?> :<span style="padding-left: 30px; font-weight: bold;"><?php echo $student_value['admission_no']; ?></span></li>

                    <?php } ?>




                    </ul>
                </div>

            </div>













            <div class="row">

                <div class="col-sm-12">

                    <?php

                                if (!empty($student_value['exam_result'])) {

                                    ?>

                                    <table cellpadding="0" cellspacing="0" width="100%" class="denifittable" style="text-align: center;">

                                        <thead>

                                            

<?php if ($exam->exam_group_type != "gpa") : ?>

    <tr class="back-color">

    <th rowspan="2" valign="middle" style="text-align: center;"><?php echo $this->lang->line('s_no') ?></th>

    <th rowspan="2" valign="middle" width="35%" class="text-center"><?php echo $this->lang->line('subjects') ?></th>

    <th rowspan="2" valign="middle" style="text-align: center"><?php echo $this->lang->line('credit') . " " . $this->lang->line('hours') ?></th>

    <th rowspan="2" valign="middle" style="text-align: center;display: none"><?php echo $this->lang->line('full') . " " . $this->lang->line('marks') ?></th>

    <th rowspan="2" valign="middle" class="text-center" style="display: none"><?php echo $this->lang->line('min') . " " . $this->lang->line('marks') ?></th>

    <th colspan="2" valign="top" style="text-align: center;"><?php echo $this->lang->line('grade') . " " . $this->lang->line('obtained') ?></th>

    <th rowspan="2" valign="middle" style="border-right:1px solid #000; text-align: center;display: none"><?php echo $this->lang->line('total') . " " . $this->lang->line('marks') ?></th>

    

        <?php if ($exam->exam_group_type == "school_grade_system" || $exam->exam_group_type == "coll_grade_system") : ?>

            <th rowspan="2" valign="middle" style="border-right:1px solid #000; text-align: center;"><?php echo $this->lang->line('grade'); ?></th>

        <?php endif; ?>

        <th rowspan="2" valign="middle" style="border-right:1px solid #000; text-align: center;"><?php echo $this->lang->line('grade') . "" . $this->lang->line('point') ?></th>

        <th rowspan="2" valign="middle" style="border-right:1px solid #000; text-align: center;"><?php echo "Subject Teacher Signature" ?></th>

    

</tr>

<?php if($_SERVER['SERVER_NAME']=="centralhss.eshikshya.com.np"){ ?>
    <?php if ($template->id=="5") { ?>
    <tr class="back-color">
        <th class="text-center">External</th>
        <th class="text-center">Internal</th>

    </tr>
<?php }else{ ?>
    <tr class="back-color">

    <th class="text-center"><?php echo $this->lang->line('theory'); ?></th>

    <th class="text-center"><?php echo $this->lang->line('practical'); ?></th>

</tr>
<?php } ?>
<?php }else{ ?>
<tr class="back-color">

    <th class="text-center"><?php echo $this->lang->line('theory'); ?></th>

    <th class="text-center"><?php echo $this->lang->line('practical'); ?></th>

</tr>
<?php } ?>


<?php elseif ($exam->exam_group_type == "gpa") : ?>

<tr>

    <th rowspan="2" valign="middle" style="text-align: center;"><?php echo $this->lang->line('s_no') ?></th>

    <th rowspan="2" style="text-align: center;"><?php echo $this->lang->line('subjects') ?></th>

    <th valign="middle" rowspan="2" style="text-align: center;"><?php echo $this->lang->line('credit') . " " . $this->lang->line('hours') ?></th>

    <th valign="middle" rowspan="2" style="text-align: center;"><?php echo $this->lang->line('grade') . " " . $this->lang->line('point'); ?></th>

    <th valign="middle" rowspan="2" style="border-right:1px solid #999;text-align: center;"><?php echo $this->lang->line('quality') . " " . $this->lang->line('point') ?></th>

</tr>

    <?php endif; ?>

                                        </thead>

                                        <tbody>

                                            <?php

                                            $total_max_marks = 0;

                                            $total_max_practical = 0;

                                            $total_obtain_marks = 0;

                                            $total_points = 0;

                                            $total_hours = 0;

                                            $total_quality_point = 0;

                                            $total_obtain_practical = 0;

                                            $total_min_theory = 0;

                                            $total_min_practical=0;

                                            $i = 0; 

$price = array_column($student_value['exam_result'], 'code');
            array_multisort($price, SORT_ASC, $student_value['exam_result']);
foreach ($student_value['exam_result'] as $exam_result_key => $exam_result_value) :

    $total_max_marks = $total_max_marks + $exam_result_value->max_marks;

    $total_max_practical = $total_max_practical + $exam_result_value->practical_max;



    $total_obtain_marks = $total_obtain_marks + $exam_result_value->get_marks;

    $total_obtain_practical = $total_obtain_practical + $exam_result_value->get_marks_practical;

    $total_min_theory = $total_min_theory + $exam_result_value->min_marks;

    $total_min_practical = $total_min_practical + $exam_result_value->practical_min;

?>

    

        <?php

        if ($exam->exam_group_type != "gpa") :
            
            ?>

            <tr>

                <td class="text-center"><?php echo ++$i; ?></td>

                <td style="text-align:left;padding-left:10px">

                    <?php echo $exam_result_value->name; ?>

                </td>

                <td class="text-center" style="display: none">

                    <?php

                    if($exam_result_value->practical_max!=="" || $exam_result_value->practical_max > 0){

                    $total_full=$exam_result_value->max_marks + $exam_result_value->practical_max; 

                     echo $total_full;

                     }

                     else{

                        echo $exam_result_value->max_marks;

                     } ?>

                </td>

                <td>
                	<?php

                    $total_hours = $total_hours + $exam_result_value->credit_hours;

                    echo ($exam_result_value->credit_hours);

                    ?>
                </td>

                <td class="text-center" style="display: none">

                    <?php

                    if($exam_result_value->practical_min!=="" || $exam_result_value->practical_min > 0){

                    $total_pass = $exam_result_value->min_marks + $exam_result_value->practical_min;  echo $total_pass;

                    }

                    else{

                        echo $exam_result_value->min_marks;

                    } ?>

                </td>

                <td class="text-center" style="display: none">

                    <?php

                    echo $exam_result_value->get_marks;

                    if ($exam_result_value->attendence == "absent") {

                        echo "&nbsp;" . $this->lang->line('exam_absent');

                        $absent_status = true;

                    }

                    if ($exam_result_value->get_marks < $exam_result_value->min_marks) {

                        $result_status = 0;

                    }

                    ?>

                </td>
                <td>
                	<?php
                	$total_theory=$exam_result_value->get_marks;
                	$total_full_theory = $exam_result_value->max_marks;

                        $gradetheory = ($total_theory/$total_full_theory)*100;

                        if ($exam_result_value->attendence == "absent") {
                        	echo "Abs";
                        }else{
                        	echo findGrade($exam_grades,$gradetheory);
                        }

                        ?>
                </td>

                <td class="text-center" style="display: none">

                    <?php

                    echo $exam_result_value->get_marks_practical;

                    if ($exam_result_value->attendence == "absent") {

                        echo "&nbsp;" . $this->lang->line('exam_absent');

                        $absent_status = true;

                    }

                    if ($exam_result_value->get_marks_practical < $exam_result_value->practical_min) {

                        $result_status = 0;

                    }

                    ?>

                </td>

                <td style="border-right: 1px solid #000">
                	<?php
                	$total_practical=$exam_result_value->get_marks_practical;
                	$total_full_practical = $exam_result_value->practical_max;

                        $gradepractical = ($total_practical/$total_full_practical)*100;

                        if ($exam_result_value->attendence == "absent") {
                        	echo "Abs";

                        }else{
                        	echo findGrade($exam_grades,$gradepractical);
                        }

                        ?>
                </td>

                <td class="text-center" style="display: none">

                    <?php

                    $total_marks_obtained=$exam_result_value->get_marks + $exam_result_value->get_marks_practical;

                    echo $total_marks_obtained;

                    if ($exam_result_value->attendence == "absent") {

                        echo "&nbsp;" . $this->lang->line('exam_absent');

                        $absent_status = true;

                    }

                    if ($exam_result_value->get_marks_practical < $exam_result_value->practical_min) {

                        $result_status = 0;

                    }

                    ?>

                </td>

                <?php if ($exam->exam_group_type == "school_grade_system" || $exam->exam_group_type == "coll_grade_system") : ?>

                	<td valign="top" style="text-align: center;border-right:1px solid #999">

	                    <?php

	                    if ($exam->exam_group_type == "basic_system") {

	                        if ($exam_result_value->get_marks < $exam_result_value->min_marks) {

	                            $result_status = 0;

	                            echo "(F) ";

	                        }

	                    }

	                    $percentage_grade = ($total_marks_obtained * 100) / $total_full;

	                    echo findGrade($exam_grades, $percentage_grade);

	                    // echo $percentage_grade;

	                    ?>

	                </td>

                    <td class="text-center" style="border-right:0">

                        <?php

                        // $percentage_grade = ($exam_result_value->get_marks * 100) / $exam_result_value->max_marks;

                        // echo findGrade($exam_grades, $percentage_grade);

                        $total_full=$exam_result_value->max_marks + $exam_result_value->practical_max;

                        $grade = ($total_marks_obtained/$total_full)*100;

                        echo findGradePoints($exam_grades,$grade);

                        ?>

                    </td>

                <?php endif; ?>



                
                <td style="border-right: 1px solid">
                	
                </td>


            </tr>





        <?php elseif ($exam->exam_group_type == "gpa") : ?>

            <tr>

                <td class="text-center"><?php echo ++$i; ?></td>

                <td class="text-center">

                    <?php echo $exam_result_value->name . "(" . $exam_result_value->code . ")"; ?>

                </td>

                <td class="text-center">

                    <?php

                    $total_hours = $total_hours + $exam_result_value->credit_hours;

                    echo ($exam_result_value->credit_hours);

                    ?>

                </td>



                <td class="text-center">

                    <?php

                    $percentage_grade = ($exam_result_value->get_marks * 100) / $exam_result_value->max_marks;

                    $point = findGradePoints($exam_grades, $percentage_grade);



                    $total_points = $total_points + $point;

                    echo $point;

                    ?>

                </td>

                <td class="text-center" style="border-right:1px solid #999; text-align: center;">

                    <?php

                    echo ($exam_result_value->credit_hours * $point);

                    $total_quality_point = $total_quality_point + ($exam_result_value->credit_hours * $point);

                    ?>

                </td>

            </tr>

        <?php endif; ?>

<?php endforeach; ?>

        



        <?php if($exam->exam_group_type != "gpa"){

            ?>

            <!-- <tr style="border-right: 1px solid #000;">

                <td>&nbsp;</td><td></td><td></td><td></td><td></td><td></td><td></td><td>&nbsp;</td><td></td>

            </tr> -->

            <tr style="border-right: 1px solid #000;padding-right: 20px;">

                <td></td>

                <td><?php echo $this->lang->line('total') ?> : </td>
                <td><?php

	            echo number_format($total_hours, 2, '.', '');

	            ?></td>

                <td style="display: none;"><?php echo ($total_max_marks + $total_max_practical); ?></td>

                <td style="display: none"><?php echo ($total_min_theory + $total_min_practical); ?></td>

                <td style="display: none"><?php echo $total_obtain_marks; ?></td>

                <td colspan="2"></td>

                <td style="display: none"><?php if($total_obtain_practical!==""){

                    echo $total_obtain_practical;

                        }else{

                        echo "-";

                        }

                         ?>

                    </td>

                <td style="display: none"><?php echo ($total_obtain_marks + $total_obtain_practical); ?></td>

                <?php $total_scored_marks=$total_obtain_marks + $total_obtain_practical;

                    $total_full_marks= $total_max_marks + $total_max_practical;

                    $total_percentage = ($total_scored_marks/$total_full_marks)*100; ?>

                <td><?php echo findGrade($exam_grades,$total_percentage); ?></td>

                <td><?php 

                    echo findGradePoints($exam_grades,$total_percentage);



                    ?>

                </td>

                <td></td>

                

            </tr>



            <?php 

        } else if ($exam->exam_group_type == "gpa") {

?>

    <tr style="border-right: 1px solid #000;">

        <td>&nbsp;</td><td></td><td></td><td></td><td>&nbsp;</td>

    </tr>

    <tr>

        <td></td>

        <td><?php echo $this->lang->line('total'); ?></td>

        <td class="text-center">

            <?php

            echo number_format($total_hours, 2, '.', '');

            ?>

        </td>

        <td></td>

        <td class="text-center" style="text-align: left;border-right:1px solid #999">

            <?php

            if ($total_hours <= 0) {

                echo "--";

            } else {

                $total_grade_percentage = ($total_obtain_marks * 100) / $total_max_marks;

                $exam_qulity_point = number_format($total_quality_point / $total_hours, 2, '.', '');

                echo $total_quality_point . "/" . $total_hours . "=" . $exam_qulity_point . " [" . findGrade($exam_grades, $total_grade_percentage) . "]";

            }

            ?>

        </td>

    </tr>

    <?php

}

?>

                                        </tbody>

                                    </table>

                                    <?php

                                }

                                ?>

                </div>

            </div>



            <div class="row" style="margin-top:10px;">

                <div class="col-sm-12">

                    <table class="table table-bordered table-responsive" style="font-size: 10px;margin-bottom: 5px;">

                        <tbody>

                            <tr>

                                <td style="background-color: #a6a6a6 !important;-webkit-print-color-adjust: exact;"><?php echo $this->lang->line('working_days'); ?></td>

                                <td><?php echo $exam->workingdays; ?></td>   

                            <?php
                                $student_batch_exam = $exam->id;
                                $output = $this->examresult_model->getattendance($student_batch_exam,$student_value['student_id'],$student_value['student_session_id']);
                            ?>

                                <td style="background-color: #a6a6a6 !important;-webkit-print-color-adjust: exact;"><?php echo $this->lang->line('present_days'); ?></td>

                                <td>
                                    <?php if(isset($output)){
                                        echo $output->present_days;
                                    }else{
                                        echo "-";
                                    }
                                    ?>
                                </td>

                                <td style="background-color: #a6a6a6 !important;-webkit-print-color-adjust: exact;"><?php echo $this->lang->line('absent_days'); ?></td>

                                <td>
                                    <?php 
                                    if(isset($output)){
                                        $absent_days=$exam->workingdays - $output->present_days;
                                        echo $absent_days;
                                    }else{
                                        echo "-";
                                    }
                                    ?>
                                </td>

                            </tr>

                        </tbody>

                    </table>
                </div>

                <div class="col-sm-5">

                    <table class="table table-bordered table-responsive" style="display: none;">
                        <tr>
                            <td><?php echo $this->lang->line('percentage') ?>:</td>
                            <td><strong><?php echo number_format($total_percentage, 2, '.', ''); ?></strong></td>
                        </tr>

                        <tr>
                            <td>Rank:</td>
                            <td><strong><?php echo $rank[$student_value['id']]; ?></strong></td>
                        </tr>

                        <tr>

                            <td>
                                <?php echo $this->lang->line('remarks') ?>:
                            </td>


                                   
                            <td>
                                <strong>
                                <?php

                                    $total_scored_marks=$total_obtain_marks + $total_obtain_practical;

                                    $total_full_marks= $total_max_marks + $total_max_practical;

                                    $total_percentage = ($total_scored_marks/$total_full_marks)*100;

                                    echo findRemarks($exam_grades,$total_percentage); ?>

                                </strong>

                            </td>

                        </tr>

                    </table>

                    <table class="table table-responsive table-bordered belowtable" style="font-size:11px !important">
                    	<thead>
                    		<tr class="back-color">
                    			<th colspan="4" style="text-align: center;background-color: #a6a6a6 !important;-webkit-print-color-adjust: exact; ">ECA Details</th>
                    		</tr>
                    	</thead>
                    	<tbody>
                    		<tr>
                    			<td>Description</td>
                    			<td>Excellent</td>
                    			<td>Good</td>
                    			<td>Satisfactory</td>
                    		</tr>
                    		<tr>
                    			<td style="vertical-align: middle;">Dance</td>
                    			<td style="text-align: center"><input type="checkbox" style="width: 13px;height: 13px"></td>
                    			<td style="text-align: center"><input type="checkbox" style="width: 13px;height: 13px"></td>
                    			<td style="text-align: center"><input type="checkbox" style="width: 13px;height: 13px"></td>
                    		</tr>
                    		<tr>
                    			<td style="vertical-align: middle;">Music</td>
                    			<td style="text-align: center"><input type="checkbox" style="width: 13px;height: 13px"></td>
                    			<td style="text-align: center"><input type="checkbox" style="width: 13px;height: 13px"></td>
                    			<td style="text-align: center"><input type="checkbox" style="width: 13px;height: 13px"></td>
                    		</tr>
                    		<tr>
                    			<td style="vertical-align: middle;">Art</td>
                    			<td style="text-align: center"><input type="checkbox" style="width: 13px;height: 13px"></td>
                    			<td style="text-align: center"><input type="checkbox" style="width: 13px;height: 13px"></td>
                    			<td style="text-align: center"><input type="checkbox" style="width: 13px;height: 13px"></td>
                    		</tr>
                    		<tr>
                    			<td style="vertical-align: middle;">Robotics</td>
                    			<td style="text-align: center"><input type="checkbox" style="width: 13px;height: 13px"></td>
                    			<td style="text-align: center"><input type="checkbox" style="width: 13px;height: 13px"></td>
                    			<td style="text-align: center"><input type="checkbox" style="width: 13px;height: 13px"></td>
                    		</tr>
                    		<tr>
                    			<td style="vertical-align: middle;">Sanskrit</td>
                    			<td style="text-align: center"><input type="checkbox" style="width: 13px;height: 13px"></td>
                    			<td style="text-align: center"><input type="checkbox" style="width: 13px;height: 13px"></td>
                    			<td style="text-align: center"><input type="checkbox" style="width: 13px;height: 13px"></td>
                    		</tr>
                    		<tr>
                    			<td style="vertical-align: middle;">Martial Art</td>
                    			<td style="text-align: center"><input type="checkbox" style="width: 13px;height: 13px"></td>
                    			<td style="text-align: center"><input type="checkbox" style="width: 13px;height: 13px"></td>
                    			<td style="text-align: center"><input type="checkbox" style="width: 13px;height: 13px"></td>
                    		</tr>
                    		<tr>
                    			<td style="vertical-align: middle;">Nepali Handwriting</td>
                    			<td style="text-align: center"><input type="checkbox" style="width: 13px;height: 13px"></td>
                    			<td style="text-align: center"><input type="checkbox" style="width: 13px;height: 13px"></td>
                    			<td style="text-align: center"><input type="checkbox" style="width: 13px;height: 13px"></td>
                    		</tr>
                    		<tr>
                    			<td style="vertical-align: middle;">English Handwriting</td>
                    			<td style="text-align: center"><input type="checkbox" style="width: 13px;height: 13px"></td>
                    			<td style="text-align: center"><input type="checkbox" style="width: 13px;height: 13px"></td>
                    			<td style="text-align: center"><input type="checkbox" style="width: 13px;height: 13px"></td>
                    		</tr>
                    		<tr>
                    			<td style="vertical-align: middle;">Discipline</td>
                    			<td style="text-align: center"><input type="checkbox" style="width: 13px;height: 13px"></td>
                    			<td style="text-align: center"><input type="checkbox" style="width: 13px;height: 13px"></td>
                    			<td style="text-align: center"><input type="checkbox" style="width: 13px;height: 13px"></td>
                    		</tr>
                    		<tr>
                    			<td style="vertical-align: middle;">Scout</td>
                    			<td style="text-align: center"><input type="checkbox" style="width: 13px;height: 13px"></td>
                    			<td style="text-align: center"><input type="checkbox" style="width: 13px;height: 13px"></td>
                    			<td style="text-align: center"><input type="checkbox" style="width: 13px;height: 13px"></td>
                    		</tr>
                    		<tr>
                    			<td style="vertical-align: middle;">Yoga</td>
                    			<td style="text-align: center"><input type="checkbox" style="width: 13px;height: 13px"></td>
                    			<td style="text-align: center"><input type="checkbox" style="width: 13px;height: 13px"></td>
                    			<td style="text-align: center"><input type="checkbox" style="width: 13px;height: 13px"></td>
                    		</tr>
                    	</tbody>
                    </table>

                </div>

                <div class="col-sm-7">
                    <table class="table table-responsive table-bordered belowtable" style="font-size:11px !important">

                        <thead>

                            <tr class="back-color">
                            	<th colspan="4" style="text-align: center;background-color: #a6a6a6 !important;-webkit-print-color-adjust: exact; "><?php echo $this->lang->line('grading_scale'); ?></th>
                            </tr>

                        </thead>

                        <tbody>
                        	<tr>
                        		<th><?php echo $this->lang->line('marks_range'); ?></th>

	                            <th><?php echo $this->lang->line('grade'); ?></th>

	                            <th><?php echo $this->lang->line('performance'); ?></th>

	                            <th><?php echo $this->lang->line('grade') . "" . $this->lang->line('point'); ?></th>
                        	</tr>

                            <?php foreach($exam_grades as $grades) : ?>

                                <tr>

                                    <td><?php echo $grades->mark_upto . "-" . $grades->mark_from; ?></td>

                                    <td><?php echo $grades->name; ?></td>

                                    <td><?php echo $grades->description; ?></td>

                                    <td><?php echo $grades->point; ?></td>

                                </tr>

                            <?php endforeach; ?>

                            

                        </tbody>

                    </table>



                </div>

            </div>

            <p style="border: 1px solid #f5f5f5;padding:5px 5px;">Remarks: <br></p>

            <div class="row" style="margin-top: 30px;">

                <div class="col-sm-4" style="height: 80px;position: relative;">

                    <div style="width: 80%;overflow: hidden;float:left;text-align: center;position: absolute;bottom: 0px;">

                        <?php if($template->left_sign!=="") : ?>

                            <img src="<?php echo base_url('uploads/'.$domain.'/marksheet/' . $template->left_sign); ?>"  width="100">

                        <?php endif; ?>

                        <hr style="width:100%;border-color: #000;margin-top: 5px;">

                        <p style="text-align:center"><?php echo $this->lang->line('class') . "" . $this->lang->line('teacher');  ?></p>

                    </div>

                </div>

                <div class="col-sm-4" style="height: 80px;position: relative;">

                    <div style="width: 80%;overflow: hidden;margin: auto;position: absolute;bottom: 0px;">

                        

                        <hr style="width:100%;border-color: #000;margin-top: 5px">

                        <p style="text-align:center"><?php echo "Issue" ." " .$this->lang->line('date'); ?></p>

                    </div>

                </div>

                <div class="col-sm-4" style="height: 80px;position: relative;">

                    <div style="width: 80%;overflow: hidden;float:right;text-align: center;position: absolute;bottom: 0;">

                        <?php if($template->right_sign!=="") : ?>

                        <img src="<?php echo base_url('uploads/'.$domain.'/marksheet/' . $template->right_sign); ?>" width="100">

                        <?php endif; ?>

                        <hr style="width:100%;border-color: #000;margin-top: 5px">

                        <p style="text-align:center"><?php echo $this->lang->line('principal'); ?></p>

                    </div>

                </div>

            </div>



        <?php if ($template->content_footer != "") { ?>

            <div class="row">

                <div class="col-sm-12">

                    <p style="padding-bottom: 10px; line-height: normal;"><?php echo $template->content_footer; ?></p>

                </div>

            </div>

        <?php } ?>

        </div>



                <div class="pagebreak"> </div>

                <?php

            }

        }

    }



    if ($marksheet['exam_connection'] == 1) {



        foreach ($marksheet['students'] as $student_key => $student_value) {



            $percentage_total = 0;

            ?>

            <style type="text/css">

                @page{padding: 0; margin:0;}

                body{padding: 0; margin:0; font-family: arial; color: #000; font-size: 12px; line-height: 13px;}

                .tableone{}

                .tableone td{border:1px solid #000; padding: 5px 0}

                .denifittable th{border-top: 1px solid #999;}

                .denifittable th,

                .denifittable td {border-bottom: 1px solid #999;

                                  border-collapse: collapse;border-left: 1px solid #999;}

                .denifittable tr th {padding: 5px 5px; font-weight: normal;}

                .denifittable tr td {padding: 5px 5px; font-weight: bold;}

                .tcmybg {

                    background:top center;

                    background-size: 100% 100%;

                    position: absolute;

                    top: 0;

                    left: 0;

                    bottom: 0;

                    z-index: 1;

                    width: 100%;height: 100%;

                }

                .tablemain{position: relative;z-index: 2}

            </style>

            <div style="width: 100%; margin: 0 auto; border:1px solid #000; padding: 10px 5px 5px;position: relative; z-index: 2;">

                <?php

                if ($template->background_img != "") {

                    ?> 

                    <img src="<?php echo base_url('uploads/marksheet/' . $template->background_img); ?>" class="tcmybg" width="100%" height="100%" />

                    <?php

                }

                ?>

                <table cellpadding="0" cellspacing="0" width="100%"  class="tablemain">

                    <tr>

                        <td valign="top">

                            <table cellpadding="0" cellspacing="0" width="100%">

                                <tr>

                                    <td valign="top" style="font-size: 20px; font-weight: bold; text-align: center;"><?php echo $template->heading; ?></td>

                                </tr>

                                <tr>

                                    <td valign="top" style="font-size: 12px; font-weight: 900; text-align: center; text-transform: uppercase;"><?php echo $template->title; ?></td>

                                </tr>

                                <tr>

                                    <td valign="top" height="5"></td>

                                </tr>

                            </table>

                        </td>

                    </tr>

                    <tr>

                        <td valign="top">

                            <table cellpadding="0" cellspacing="0" width="100%">

                                <tr>

                                    <td valign="top" align="center"><?php

                                        if ($template->left_logo != "") {

                                            ?><img src="<?php echo base_url('uploads/marksheet/' . $template->left_logo); ?>" width="100" height="100"> <?php } ?></td>

                                    <td valign="top">

                                        <table cellpadding="0" cellspacing="0" width="100%">

                                            <tr>

                                                <td valign="top" style="font-size: 12px; font-weight: bold; text-align: center; text-transform: uppercase;">

                                                    <?php echo $template->exam_name; ?>

                                                </td>

                                            </tr>

                                            <tr>

                                                <td valign="top" height="5"></td>

                                            </tr>

                                            <?php

                                            if ($template->exam_session) {

                                                ?>

                                                <tr>

                                                    <td valign="top" style="font-weight: bold; text-align: left; text-transform: uppercase; display: inline-block; margin-top: -10px;">

                                                        <?php echo $exam->session; ?>

                                                    </td>

                                                </tr>

                                                <?php

                                            }

                                            ?>

                                        </table>

                                    </td>

                                    <td valign="top" align="center"><?php

                                        if ($template->left_logo != "") {

                                            ?><img src="<?php echo base_url('uploads/marksheet/' . $template->left_logo); ?>" width="100" height="100"><?php } ?></td>

                                </tr>

                            </table>

                        </td>

                    </tr>

                    <?php

                    if ($template->is_admission_no || $template->is_roll_no || $template->is_photo) {

                        ?>

                        <tr>

                            <td valign="top">

                                <table cellpadding="0" cellspacing="0" width="100%" class="">

                                    <tr>

                                        <td valign="top">

                                            <table cellpadding="0" cellspacing="0" width="98%" class="denifittable">

                                                <tr>

                                                    <?php

                                                    if ($template->is_admission_no) {

                                                        ?>

                                                        <th valign="top" style="text-align: center; text-transform: uppercase;">

                                                            <?php echo $this->lang->line('admission_no') ?>

                                                        </th>

                                                        <?php

                                                    }

                                                    if ($template->is_roll_no) {

                                                        ?>

                                                        <th valign="top" style="text-align: center; text-transform: uppercase; border-right:1px solid #999"><?php echo $this->lang->line('roll_no') ?></th>

                                                        <?php

                                                    }

                                                    ?>

                                                </tr>

                                                <tr>

                                                    <?php

                                                    if ($template->is_admission_no) {

                                                        ?>

                                                        <td valign="" style="text-transform: uppercase;text-align: center;"><?php echo $student_value['admission_no']; ?></td>

                                                        <?php

                                                    }

                                                    if ($template->is_roll_no) {

                                                          $roll_no=($exam->use_exam_roll_no)?$student_value['exam_result']['exam_roll_no_' . $exam->id]:$student_value['student_roll_no'];

                                                        ?>

                                                        <td valign="" style="text-transform: uppercase;text-align: center;border-right:1px solid #999">   <?php echo $roll_no; ?>

                                                        </td>

                                                        <?php

                                                    }

                                                    ?>

                                                </tr>

                                                <tr>

                                                    <td valign="top" colspan="5" style="text-align: center; text-transform: uppercase; border:0">

                                                        <?php echo $this->lang->line('certificated_that') ?>

                                                    </td>

                                                </tr>

                                            </table>

                                        </td>

                                        <?php

                                        if ($template->is_photo) {

                                            ?>

                                            <td valign="top" align="center"><?php if ($student_value['image'] != '') { ?><img src="<?php echo base_url() . $student_value['image']; ?>" width="120" height="150" style="border: 2px solid #fff;

                                                                                                                                  outline: 1px solid #000000;"><?php } ?>

                                            </td>

                                            <?php

                                        }

                                        ?>

                                    </tr>

                                </table>

                            </td>

                        </tr>

                        <?php

                    }

                    ?>

                    <tr>

                        <td valign="top">

                            <table cellpadding="0" cellspacing="0" width="100%" class="" style="font-size:13px;line-height: 13px;">

                                <?php

                                if ($template->is_name) {

                                    ?>

                                    <tr>

                                        <td valign="top" style="text-transform: uppercase; padding-bottom: 5px;"><?php echo $this->lang->line('name_prefix'); ?><span style="padding-left: 30px; font-weight: bold;"><?php echo $student_value['firstname'] . " " . $student_value['lastname']; ?></span></td>

                                    </tr>

                                    <?php

                                }



                                if ($template->is_father_name) {

                                    ?>

                                    <tr>

                                        <td valign="top" style="text-transform: uppercase; padding-bottom: 5px;"><?php echo $this->lang->line('marksheet_father_name') ?><span style="padding-left: 30px; font-weight: bold;"><?php echo $student_value['father_name']; ?></span></td>

                                    </tr>

                                    <?php

                                }



                                if ($template->is_mother_name) {

                                    ?>

                                    <tr>

                                        <td valign="top" style="text-transform: uppercase; padding-bottom: 5px;"><?php echo $this->lang->line('exam_mother_name'); ?><span style="padding-left: 30px; font-weight: bold;"><?php echo $student_value['mother_name']; ?></span></td>

                                    </tr>

                                    <?php

                                }

								if ($template->is_class && $template->is_section) {

                                        ?>

                                        <tr>

                                            <td valign="top" style="text-transform: uppercase; padding-bottom: 5px;"><?php echo $this->lang->line('class'); ?><span style="padding-left: 30px; font-weight: bold;"><?php echo $student_value['class'] . " (" . $student_value['section'] . ")"; ?> </span></td>

                                        </tr>

                                        <?php

                                    } elseif ($template->is_class) {

                                        ?>

                                        <tr>

                                            <td valign="top" style="text-transform: uppercase; padding-bottom: 5px;"><?php echo $this->lang->line('class'); ?><span style="padding-left: 30px; font-weight: bold;"><?php echo $student_value['class']; ?> </span></td>

                                        </tr>

                                        <?php

                                    } elseif ($template->is_section) {

                                        ?>

                                        <tr>

                                            <td valign="top" style="text-transform: uppercase; padding-bottom: 5px;"><?php echo $this->lang->line('section'); ?><span style="padding-left: 30px; font-weight: bold;"><?php echo $student_value['section']; ?> </span></td>

                                        </tr>

                                        <?php

                                    }

                                    ?>

                                

                                <?php

                                if ($template->school_name != "") {

                                    ?>

                                    <tr>

                                        <td valign="top" style="text-transform: uppercase; padding-bottom: 5px;"> <?php echo $this->lang->line('school_name'); ?><span style="padding-left: 30px; font-weight: bold;"><?php echo $template->school_name; ?></span></td>

                                    </tr>

                                    <?php

                                }

                                ?>

                                <tr>

                                    <td valign="top" style="text-transform: uppercase; padding-bottom: 20px;" ><?php echo $this->lang->line('exam') . " " . $this->lang->line('center') ?><span style="text-transform: uppercase; padding-top: 5px; font-weight: bold; padding-bottom: 20px; padding-left: 30px;"><?php echo $template->exam_center; ?></span></td>

                                </tr>

                                <?php

                                if ($template->content != "") {

                                    ?>

                                    <tr>

                                        <td valign="top" style="text-transform: uppercase; padding-bottom: 5px;"><?php echo $template->content ?></td>

                                    </tr>

                                    <?php

                                }

                                ?>

                            </table>

                        </td>

                    </tr>

                    <?php

                    if (!empty($student_value['exam_result']['exam_result_' . $exam->id])) {

                        ?>

                        <tr>

                            <td valign="top">

                                <?php

                                if (!empty($student_value['exam_result']['exam_result_' . $exam->id])) {

                                    ?>

                                    <table cellpadding="0" cellspacing="0" width="100%" class="denifittable" style="text-align: center;">

                                        <tbody>

                                            <?php

                                            $total_max_marks = 0;

                                            $total_obtain_marks = 0;

                                            $total_points = 0;

                                            $total_hours = 0;

                                            $total_quality_point = 0;

                                            $result_status = 1;

                                            $absent_status = false;



                                            foreach ($student_value['exam_result']['exam_result_' . $exam->id] as $exam_result_key => $exam_result_value) {



                                                $total_max_marks = $total_max_marks + $exam_result_value->max_marks;

                                                $total_obtain_marks = $total_obtain_marks + $exam_result_value->get_marks;

                                                if ($exam_result_value->attendence == "absent") {

                                                    $absent_status = true;

                                                }

                                                ?>

                                                <tr>

                                                    <td>

                                                        <?php echo $exam_result_value->name . "(" . $exam_result_value->code . ")"; ?>

                                                    </td>

                                                    <?php

                                                    if ($exam->exam_group_type != "gpa") {

                                                        ?>

                                                        <td>

                                                            <?php echo $exam_result_value->max_marks; ?>

                                                        </td>

                                                        <td>

                                                            <?php echo $exam_result_value->min_marks; ?>

                                                        </td>

                                                        <td>

                                                            <?php

                                                            echo $exam_result_value->get_marks;



                                                            if ($exam_result_value->attendence == "absent") {

                                                                echo "&nbsp;" . $this->lang->line('exam_absent');

                                                            }

                                                            if ($exam_result_value->get_marks < $exam_result_value->min_marks) {



                                                                $result_status = 0;

                                                            }

                                                            ?>

                                                        </td>

                                                        <?php

                                                    } else if ($exam->exam_group_type == "gpa") {

                                                        ?>

                                                        <td class="text-center">

                                                            <?php

                                                            $percentage_grade = ($exam_result_value->get_marks * 100) / $exam_result_value->max_marks;

                                                            $point = findGradePoints($exam_grades, $percentage_grade);

                                                            $total_points = $total_points + $point;

                                                            echo $point;

                                                            ?>

                                                        </td>

                                                        <td class="text-center">

                                                            <?php

                                                            $total_hours = $total_hours + $exam_result_value->credit_hours;

                                                            echo ($exam_result_value->credit_hours);

                                                            ?>

                                                        </td>

                                                        <td class="text-center">

                                                            <?php

                                                            echo ($exam_result_value->credit_hours * $point);

                                                            $total_quality_point = $total_quality_point + ($exam_result_value->credit_hours * $point);

                                                            ?>

                                                        </td>

                                                        <?php

                                                    }



                                                    if ($exam->exam_group_type == "school_grade_system" || $exam->exam_group_type == "coll_grade_system") {

                                                        ?>

                                                        <td>

                                                            <?php

                                                            $percentage_grade = ($exam_result_value->get_marks * 100) / $exam_result_value->max_marks;

                                                            echo findGrade($exam_grades, $percentage_grade);

                                                            ?>

                                                        </td>

                                                        <?php

                                                    }

                                                    ?>

                                                    <td valign="top" style="text-align: left;border-right:1px solid #999">

                                                        <?php

                                                        if ($exam->exam_group_type == "basic_system") {

                                                            if ($exam_result_value->get_marks < $exam_result_value->min_marks) {

                                                                $result_status = 0;

                                                                echo "(F) ";

                                                            }

                                                        }

                                                        echo $exam_result_value->note;

                                                        ?>

                                                    </td>

                                                </tr>

                                                <?php

                                            }

                                            ?>

                                            <?php

                                            if ($exam->exam_group_type != "gpa") {

                                                ?>

                                                <tr>

                                                    <td></td>

                                                    <td>

                                                        <?php echo number_format($total_max_marks, 2, '.', ''); ?>

                                                    </td>

                                                    <td><?php echo $this->lang->line('grand_total') ?></td>

                                                    <td>

                                                        <?php echo number_format($total_obtain_marks, 2, '.', ''); ?>

                                                    </td>

                                                    <td valign="top" style="text-align: left;border-right:1px solid #999"></td>

                                                    <?php

                                                    if ($exam->exam_group_type == "school_grade_system" || $exam->exam_group_type == "coll_grade_system") {

                                                        ?>

                                                        <td valign="top" style="text-align: left;border-right:1px solid #999"></td>

                                                        <?php

                                                    }

                                                    ?>

                                                </tr>

                                                <tr>

                                                    <td><?php echo $this->lang->line('percentage') ?></td>

                                                    <td>

                                                        <?php

                                                        echo number_format((($total_obtain_marks * 100) / $total_max_marks), 2, '.', '');

                                                        $percentage_total = (($total_obtain_marks * 100) / $total_max_marks);

                                                        ?>

                                                    </td>

                                                    <td></td>

                                                    <td>

                                                    </td>

                                                    <td valign="top" style="text-align: left;border-right:1px solid #999"></td>

                                                    <?php

                                                    if ($exam->exam_group_type == "school_grade_system" || $exam->exam_group_type == "coll_grade_system") {

                                                        ?>

                                                        <td valign="top" style="text-align: left;border-right:1px solid #999"></td>

                                                        <?php

                                                    }

                                                    ?>

                                                </tr>

                                                <?php

                                            } else if ($exam->exam_group_type == "gpa") {

                                                ?>

                                                <tr>

                                                    <td></td>

                                                    <td></td>

                                                    <td class="text-center">

                                                        <?php

                                                        echo number_format($total_hours, 2, '.', '');

                                                        ?>

                                                    </td>

                                                    <td class="text-center">

                                                        <?php

                                                        if ($total_hours <= 0) {

                                                            echo "--";

                                                        } else {

                                                            $total_grade_percentage = ($total_obtain_marks * 100) / $total_max_marks;

                                                            $exam_qulity_point = number_format($total_quality_point / $total_hours, 2, '.', '');



                                                            echo $total_quality_point . "/" . $total_hours . "=" . $exam_qulity_point . " [" . findGrade($exam_grades, $total_grade_percentage) . "]";

                                                        }

                                                        ?>

                                                    </td>

                                                    <td></td>

                                                    <?php

                                                }

                                                ?>

                                        </tbody>

                                    </table>

                                    <?php

                                }

                                ?>

                            </td>

                        </tr>

                        <tr>

                            <td valign="top" height="30"></td>

                        </tr>

                        <?php

                        if ($exam->exam_group_type != "gpa") {

                            ?>

                            <tr>

                                <td valign="top" colspan="5" width="20%" style="font-weight: normal; text-align: left; border-top:0">

                                    Result

                                    <span style="border-left:0;text-align: left;font-weight: bold; padding-left: 30px;">

                                        <?php

                                        if ($result_status == 0 || $absent_status) {

                                            echo $this->lang->line('fail');

                                        } else {



                                            if ($percentage_total > 40) {

                                                echo $this->lang->line('pass');

                                            } else {

                                                echo $this->lang->line('fail');

                                            }

                                        }

                                        ?>

                                    </span>

                                </td>

                            </tr>

                            <?php

                            if ($template->is_division) {

                                ?>

                                <tr>

                                    <td valign="top" colspan="5" width="20%" style="font-weight: normal; text-align: left; border-top:0">

                                        Division

                                        <span style="border-left:0;text-align: left;font-weight: bold; padding-left: 30px;">

                                            <?php

                                            if ($percentage_total >= 60) {

                                                echo $this->lang->line('first');

                                            } elseif ($percentage_total >= 50 && $percentage_total < 60) {

                                                echo $this->lang->line('second');

                                            } elseif ($percentage_total >= 0 && $percentage_total < 50) {

                                                echo $this->lang->line('third');

                                            } else {

                                                

                                            }

                                            ?>

                                        </span>

                                    </td>

                                </tr>

                                <?php

                            }

                           if ($template->is_teacher_remark) {

                                ?>

                                 <tr>

                            <td valign="top" colspan="5" width="20%" style="font-weight: normal; text-align: left; border-top:0"> <?php echo $this->lang->line('teacher_remark'); ?>

                                        <span style="border-left:0;text-align: left;font-weight: bold; padding-left: 30px;">

                                       <?php echo $student_value['teacher_remark']; ?>

                                        </span>

                                    </td>

                            </tr>

                                <?php

                            }





                            ?>

                            <tr>

                                <td valign="top" style="font-weight: bold; padding-left: 30px; padding-top: 10px;"><?php echo $template->date; ?></td>

                            </tr>

                            <?php

                        }

                        ?>

                        <tr>

                            <td valign="top" height="30"></td>

                        </tr>

                        <tr>

                            <td valign="top" style="padding-top: 10px;">

                                <?php

                                if (!empty($student_value['exam_result'])) {

                                    $consolidate_weightage_marks_total = 0;

                                    $consolidate_marks_exam_total = 0;

                                    $is_consoledate = 1;

                                    $consolidate_exam_status = true;



                                    foreach ($marksheet['exams'] as $each_exam_key => $each_exam_value) {



                                        if (empty($marksheet['students'][$student_key]['exam_result']['exam_result_' . $each_exam_value->id])) {

                                            $is_consoledate = 0;

                                        }

                                    }

                                    ?>

                                    <table cellpadding="0" cellspacing="0" width="100%" class="denifittable" style="text-align: center; text-transform: uppercase;">

                                        <thead>

                                            <tr>

                                                <th> <?php echo $this->lang->line('exam'); ?></th>

                                                <?php

                                                // print_r($marksheet['exams']);



                                                foreach ($marksheet['exams'] as $each_exam_key => $each_exam_value) {

                                                    ?>

                                                    <th> <?php echo $each_exam_value->exam; ?></th>

                                                    <?php

                                                }

                                                ?>

                                                <th valign="top" style="text-align: left;border-right:1px solid #999"><?php echo $this->lang->line('consolidate') ?></th>

                                            </tr>

                                        </thead>

                                        <tbody>

                                            <tr>

                                                <td>Marks Obtained</td>

                                                <?php

                                                foreach ($marksheet['exams'] as $each_exam_key => $each_exam_value) {

                                                    if ($is_consoledate) {

                                                        ?>

                                                        <td>

                                                            <?php

                                                            $s = examTotalResult($marksheet['students'][$student_key]['exam_result']['exam_result_' . $each_exam_value->id]);

                                                            $sss = json_decode($s);



                                                            if (!$sss->exam_result) {

                                                                $consolidate_exam_status = false;

                                                            }



                                                            $percentage_grade = (($sss->get_marks * 100) / $sss->max_marks);

                                                            // echo $sss->get_marks . "/" . $sss->max_marks . "[" . findGrade($exam_grades, $percentage_grade) . "]";



                                                            $consolidate_marks_exam_total = $consolidate_marks_exam_total + $sss->max_marks;

                                                            $weightage_marks = getWeightageExam($marksheet['exam_connection_list'], $each_exam_value->id, $sss->get_marks);

                                                            $consolidate_weightage_marks_total = $weightage_marks + $consolidate_weightage_marks_total;

                                                            echo number_format($weightage_marks, 2, '.', '');

                                                            if (!$sss->exam_result) {

                                                                echo "&nbsp;(F) ";

                                                            }

                                                            ?>

                                                        </td>

                                                        <?php

                                                    } else {

                                                        ?>

                                                        <td>

                                                            --

                                                        </td>

                                                        <?php

                                                    }

                                                }



                                                if ($is_consoledate) {

                                                    ?>

                                                    <td valign="top" style="text-align: left;border-right:1px solid #999">

                                                        <?php

                                                        $percentage_grade_weightage = (($consolidate_weightage_marks_total * 100) / $consolidate_marks_exam_total);

                                                        echo $consolidate_weightage_marks_total . "/" . $consolidate_marks_exam_total . " [" . findGrade($exam_grades, $percentage_grade_weightage) . "]";

                                                        ?>

                                                    </td>

                                                    <?php

                                                } else {

                                                    ?>

                                                    <td>

                                                        --

                                                    </td>

                                                    <?php

                                                }

                                                ?>

                                            </tr>

                                        </tbody>

                                    </table>

                                    <?php

                                    if (!$consolidate_exam_status) {



                                        echo $this->lang->line('result') . " " . $this->lang->line('fail');

                                    } else {



                                        echo $this->lang->line('result') . " " . $this->lang->line('pass');

                                    }

                                    ?>

                                    Division

                                    <span style="border-left:0;text-align: left;font-weight: bold; padding-left: 30px;">

                                        <?php

                                        $consolidate_percentage_total = ($consolidate_weightage_marks_total * 100) / $consolidate_marks_exam_total;

                                        if ($consolidate_percentage_total >= 60) {

                                            echo $this->lang->line('first');

                                        } elseif ($consolidate_percentage_total >= 50 && $consolidate_percentage_total < 60) {

                                            echo $this->lang->line('second');

                                        } elseif ($consolidate_percentage_total >= 0 && $consolidate_percentage_total < 50) {

                                            echo $this->lang->line('third');

                                        } else {

                                            

                                        }

                                        ?>

                                    </span>

                                    <?php

                                }

                                ?>

                            </td>

                        </tr>

                        <?php

                    }

                    ?>

                    <tr>

                        <td valign="top" height="30"></td>

                    </tr>

                    <?php

                    if ($template->content_footer != "") {

                        ?>

                        <tr>

                            <td valign="top" style="text-transform: uppercase; padding-bottom: 15px; line-height: normal;"><?php echo $template->content_footer ?></td>

                        </tr>

                        <?php

                    }

                    ?>

                    <tr>

                        <td valign="top">

                            <table cellpadding="0" cellspacing="0" width="100%" class="">

                                <tr>

                                    <td valign="bottom" style="font-size: 12px;">

                                    </td>

                                    <td valign="bottom" align="center" style="text-transform: uppercase;">

                                        <?php

                                        if ($template->left_sign != "") {

                                            ?>

                                            <img src="<?php echo base_url('uploads/marksheet/' . $template->left_sign); ?>"  width="100" height="50"><?php } ?>

                                    </td>

                                    <td valign="bottom" align="center" style="text-transform: uppercase;">

                                        <?php

                                        if ($template->middle_sign != "") {

                                            ?>

                                            <img src="<?php echo base_url('uploads/marksheet/' . $template->middle_sign); ?>" width="100" height="50">

                                        <?php } ?>

                                    </td>

                                    <td valign="bottom" align="center" style="text-transform: uppercase;">

                                        <?php

                                        if ($template->right_sign != "") {

                                            ?>

                                            <img src="<?php echo base_url('uploads/marksheet/' . $template->right_sign); ?>" width="100" height="50">

                                        <?php } ?>

                                    </td>

                                </tr>

                            </table>

                        </td>

                    </tr>

                    <tr>

                        <td valign="top" height="20"></td>

                    </tr>

                </table>

            </div>

            <div class="pagebreak"> </div>

            <?php

        }

    } else {

        

    }

}

?>





















































<?php



function findGrade($exam_grades, $percentage) {



    if (!empty($exam_grades)) {

        foreach ($exam_grades as $exam_grade_key => $exam_grade_value) {



            if ($exam_grade_value->mark_from >= $percentage && $exam_grade_value->mark_upto <= $percentage) {

                return $exam_grade_value->name;

            }

        }

    }



    return "-";

}



function findGradePoints($exam_grades, $percentage) {



    if (!empty($exam_grades)) {

        foreach ($exam_grades as $exam_grade_key => $exam_grade_value) {



            if ($exam_grade_value->mark_from >= $percentage && $exam_grade_value->mark_upto <= $percentage) {

                return $exam_grade_value->point;

            }

        }

    }



    return 0;

}



function findRemarks($exam_grades, $percentage){

    if (!empty($exam_grades)) {

        foreach($exam_grades as $exam_grade_key => $exam_grade_value){

            if ($exam_grade_value->mark_from >= $percentage && $exam_grade_value->mark_upto <= $percentage) {

                    return $exam_grade_value->description;

            }

        }

    }

    return "-";

}



function examTotalResult($array) {

    $return_array = array('max_marks' => 0, 'min_marks' => 0, 'credit_hours' => 0, 'get_marks' => 0, 'exam_result' => true);

    if (!empty($array)) {

        $max_marks = 0;

        $min_marks = 0;

        $credit_hours = 0;

        $get_marks = 0;

        $exam_result = true;

        foreach ($array as $array_key => $array_value) {

            if ($array_value->attendence == "absent") {

                $exam_result = false;

            }

            $max_marks = $max_marks + $array_value->max_marks;

            $min_marks = $min_marks + $array_value->min_marks;

            $credit_hours = $credit_hours + $array_value->credit_hours;

            $get_marks = $get_marks + $array_value->get_marks;

        }

        $return_array = array('max_marks' => $max_marks, 'min_marks' => $min_marks, 'credit_hours' => $credit_hours, 'get_marks' => $get_marks, 'exam_result' => $exam_result);

    }

    return json_encode($return_array);

}



function getWeightageExam($exam_connection_list, $examid, $get_marks) {



    foreach ($exam_connection_list as $exam_connection_key => $exam_connection_value) {

        if ($exam_connection_value->exam_group_class_batch_exams_id == $examid) {

            return ($get_marks * $exam_connection_value->exam_weightage) / 100;

        }

    }

    return "";

}

?>