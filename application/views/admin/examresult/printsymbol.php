<?php
$currency_symbol = $this->customlib->getSchoolCurrencyFormat();
?>
<div class="content-wrapper">
	<section class="content-header">
		<h1>
			<i class="fa fa-user-plus"></i> <?php echo $this->lang->line('student_information'); ?> <small><?php echo $this->lang->line('student1'); ?></small></h1>
		</section>
		<!-- Main content -->
		<section class="content">
			<div class="row">
				<div class="col-md-12">
					<div class="box box-primary">
						<div class="box-header with-border">
							<h3 class="box-title"><i class="fa fa-search"></i> <?php echo $this->lang->line('select_criteria'); ?></h3>
						</div>
						<div class="filter-box">
							<div class="box-body">
								<form role="form" action="<?php echo site_url('student/searchvalidation') ?>" method="post" class="class_search_form">
									<?php if ($this->session->flashdata('msg')) {?> <div class="alert alert-success">  <?php echo $this->session->flashdata('msg') ?> </div> <?php }?>
									<div class="row">
										<div class="col-md-6">
											<div class="row">
												<?php echo $this->customlib->getCSRF(); ?>
												<div class="col-sm-5">
													<div class="form-group">
														<label><?php echo $this->lang->line('class'); ?></label> <small class="req"> *</small>
														<select autofocus="" id="class_id" name="class_id" class="form-control" >
															<option value=""><?php echo $this->lang->line('select'); ?></option>
															<?php
															$count = 0;
															foreach ($classlist as $class) {
																?>
																<option value="<?php echo $class['id'] ?>" <?php if (set_value('class_id') == $class['id']) {
																	echo "selected=selected";
																}
																?>><?php echo $class['class'] ?></option>
																<?php
																$count++;
															}
															?>
														</select>
														<span class="text-danger" id="error_class_id"></span>
													</div>
												</div>
												<div class="col-sm-5">
													<div class="form-group">
														<label><?php echo $this->lang->line('section'); ?></label>
														<select  id="section_id" name="section_id" class="form-control" >
															<option value=""><?php echo $this->lang->line('select'); ?></option>
														</select>
														<span class="text-danger"><?php echo form_error('section_id'); ?></span>
													</div>
												</div>
												<div class="col-sm-2">
													<div class="form-group">
														<label class="d-block">&nbsp;</label>
														<button type="submit" name="search" value="search_filter" class="btn btn-primary btn-sm pull-right checkbox-toggle"><i class="fa fa-search"></i> <?php echo $this->lang->line('search'); ?></button>
													</div>
												</div>
											</div>
										</div><!--./col-md-6-->
									</div><!--./row-->
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="content">
			<div class="row">
				<div class="col-md-12">
					<div class="box box-primary">
						<?php
//if (isset($resultlist)) {
						?>
						<div class="nav-tabs-custom border0 navnoshadow">
							<div class="tab-content">
								<div class="tab-pane active table-responsive no-padding" id="tab_1">
									<table class="table table-striped table-bordered table-hover student-list" data-export-title="<?php echo $this->lang->line('student') . " " . $this->lang->line('list'); ?>">
										<thead>
											<tr>
												<th></th>
												<th>Symbol No</th>
												<th><?php echo $this->lang->line('student_name'); ?></th>
												<th><?php echo $this->lang->line('class'); ?></th>
											</tr>
										</thead>
										<tbody>

										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div><!--./box box-primary -->
					<?php
//  }
					?>
				</div>
			</div>
		</section>
	</div>

	<script type="text/javascript">


		function getSectionByClass(class_id, section_id) {
			if (class_id != "" && section_id != "") {
				$('#section_id').html("");
				var base_url = '<?php echo base_url() ?>';
				var div_data = '<option value=""><?php echo $this->lang->line('select'); ?></option>';
				$.ajax({
					type: "GET",
					url: base_url + "sections/getByClass",
					data: {'class_id': class_id},
					dataType: "json",
					success: function (data) {
						$.each(data, function (i, obj)
						{
							var sel = "";
							if (section_id == obj.section_id) {
								sel = "selected";
							}
							div_data += "<option value=" + obj.section_id + " " + sel + ">" + obj.section + "</option>";
						});
						$('#section_id').append(div_data);
					}
				});
			}
		}
		$(document).ready(function () {
			var class_id = $('#class_id').val();
			var section_id = '<?php echo set_value('section_id') ?>';
			getSectionByClass(class_id, section_id);
			$(document).on('change', '#class_id', function (e) {
				$('#section_id').html("");
				var class_id = $(this).val();
				var base_url = '<?php echo base_url() ?>';
				var div_data = '<option value=""><?php echo $this->lang->line('select'); ?></option>';
				$.ajax({
					type: "GET",
					url: base_url + "sections/getByClass",
					data: {'class_id': class_id},
					dataType: "json",
					success: function (data) {
						$.each(data, function (i, obj)
						{
							div_data += "<option value=" + obj.section_id + ">" + obj.section + "</option>";
						});
						$('#section_id').append(div_data);
					}
				});
			});
		});
	</script>
	<script>
		$(document).ready(function() {
			emptyDatatable('student-list','data');
		});
	</script>
	<style type="text/css">
		tr td:last-child{
			width: 200px;
		}
		tr td:last-child a:last-child, tr td:last-child a:last-child:hover{
			padding: 10px;
		}
		tr td:last-child a:last-child span{
			font-size: 12px;
		}

	</style>
	<script type="text/javascript">
		$(document).ready(function(){

			$("form.class_search_form button[type=submit]").click(function() {
				$("button[type=submit]", $(this).parents("form")).removeAttr("clicked");
				$(this).attr("clicked", "true");
			});

			$(document).on('submit','.class_search_form',function(e){
   e.preventDefault(); // avoid to execute the actual submit of the form.
   var $this = $("button[type=submit][clicked=true]");
   var form = $(this);
   var url = form.attr('action');
   var form_data = form.serializeArray();
   form_data.push({name: 'search_type', value: $this.attr('value')});
   $.ajax({
   	url: url,
   	type: "POST",
   	dataType:'JSON',
           data: form_data, // serializes the form's elements.
           beforeSend: function () {
           	$('[id^=error]').html("");
           	$this.button('loading');
           	resetFields($this.attr('value'));
           },
              success: function(response) { // your success handler

              	if(!response.status){
              		$.each(response.error, function(key, value) {
              			$('#error_' + key).html(value);
              		});
              	}else{



        if ($.fn.DataTable.isDataTable('.student-list')) { // if exist datatable it will destrory first
        	$('.student-list').DataTable().destroy();
        }
        table= $('.student-list').DataTable({
        // "scrollX": true,
        responsive: true,
        dom: 'Bfrtip',
        buttons: [
        {
        	extend:    'print',
        	text:      '<button class="btn btn-success">Print Student Symbol No.</button>',
        	titleAttr: 'Print',
        	className: "btn-print",
        	title: $('.student-list').data("exportTitle"),
        	customize: function ( win ) {

        		$(win.document.body).find('th').addClass('display').css('text-align', 'center');
        		$(win.document.body).find('table').addClass('display').css('font-size', '14px');     
        		$(win.document.body).find('h1').css('text-align', 'center');
        	},
        	exportOptions: {
        		columns: ["thead th:not(.noExport)"]

        	}

        },
        {
            text: '<button class="btn btn-primary symbolno">Print Student Symbol No. Only</button>',
        }
        ],


        "language": {
        	processing: '<i class="fa fa-spinner fa-spin fa-1x fa-fw"></i><span class="sr-only">Loading...</span> '},
        	"pageLength": 100,
        	"processing": true,
        	"serverSide": true,
        	"ajax":{
        		"url": baseurl+"student/studentSymbol",
        		"dataSrc": 'data',
        		"type": "POST",
        		'data': response.params,

        	},"drawCallback": function(settings) {

        		$('.detail_view_tab').html("").html(settings.json.student_detail_view);
        	}

        });
        }
    },
             error: function() { // your error handler
             	$this.button('reset');
             },
             complete: function() {
             	$this.button('reset');
             }
         });

});

		});
		function resetFields(search_type){

			if(search_type == "search_full"){
				$('#class_id').prop('selectedIndex',0);
				$('#section_id').find('option').not(':first').remove();
			}else if (search_type == "search_filter") {

				$('#search_text').val("");
			}
		}
	</script>

	<script type="text/javascript">

		var base_url = '<?php echo base_url() ?>';

    function Popup(data, winload = false)
    {
        var frame1 = $('<iframe />').attr("id", "printDiv");
        frame1[0].name = "frame1";
        frame1.css({"position": "absolute", "top": "-1000000px"});
        $("body").append(frame1);
        var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
        frameDoc.document.open();
        //Create a new HTML document.
        frameDoc.document.write('<html>');
        frameDoc.document.write('<head>');
        frameDoc.document.write('<title></title>');
        frameDoc.document.write('<link rel="stylesheet" href="' + base_url + 'backend/bootstrap/css/bootstrap.min.css">');
        frameDoc.document.write('<link rel="stylesheet" href="' + base_url + 'backend/dist/css/font-awesome.min.css">');
        frameDoc.document.write('<link rel="stylesheet" href="' + base_url + 'backend/dist/css/ionicons.min.css">');
        frameDoc.document.write('<link rel="stylesheet" href="' + base_url + 'backend/dist/css/AdminLTE.min.css">');
        frameDoc.document.write('<link rel="stylesheet" href="' + base_url + 'backend/dist/css/skins/_all-skins.min.css">');
        frameDoc.document.write('<link rel="stylesheet" href="' + base_url + 'backend/plugins/iCheck/flat/blue.css">');
        frameDoc.document.write('<link rel="stylesheet" href="' + base_url + 'backend/plugins/morris/morris.css">');
        frameDoc.document.write('<link rel="stylesheet" href="' + base_url + 'backend/plugins/jvectormap/jquery-jvectormap-1.2.2.css">');
        frameDoc.document.write('<link rel="stylesheet" href="' + base_url + 'backend/plugins/datepicker/datepicker3.css">');
        frameDoc.document.write('<link rel="stylesheet" href="' + base_url + 'backend/plugins/daterangepicker/daterangepicker-bs3.css">');
        frameDoc.document.write('</head>');
        frameDoc.document.write('<body>');
        frameDoc.document.write(data);
        frameDoc.document.write('</body>');
        frameDoc.document.write('</html>');
        frameDoc.document.close();
        setTimeout(function () {
        document.getElementById('printDiv').contentWindow.focus();
        document.getElementById('printDiv').contentWindow.print();
        $("#printDiv", top.document).remove();
            // frame1.remove();
            if (winload) {
                window.location.reload(true);
            }
        }, 500);

        return true;
    }

		$(document).on('click', '.symbolno', function () {
            var array_to_print = [];
            $("input[name='fee_checkbox']").prop('checked', true);
            $.each($("input[name='fee_checkbox']"), function () {
                var symbolno = $(this).data('symbolno');
                item = {};
                item ["symbolno"] = symbolno;

                array_to_print.push(item);
            });
            if (array_to_print.length === 0) {
                alert("No Fee Dues Left");
            } else {
                $.ajax({
                    url: '<?php echo site_url("admin/examresult/symbolno") ?>',
                    type: 'post',
                    data: {'data': JSON.stringify(array_to_print)},
                    success: function (response) {
                        Popup(response);
                    }
                });
            }
        });
	</script>