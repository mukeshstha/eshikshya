<div class="content-wrapper" style="min-height: 946px;">
  <section class="content-header">
    <h1>
    </h1>
  </section>

  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-body">
            <div class="row">
              <div class="col-md-12">
                <table id="journal_entry_table" class="table table-respomsive" cellspacing="0" width="100%">
                  <thead>
                    <tr>
                      <td width="25">No</td>
                      <td>UID</td>
                      <td>ID</td>
                      <td>Name</td>
                      <td>Role</td>
                      <td>Password</td>
                    </tr>
                  </thead>

                  <tbody>
                    <?php
                    $no = 0;
                    foreach($users as $key=>$user)
                    {
                      $no++;
                      ?>

                      <tr>
                        <td align="right"><?php echo $no;?></td>
                        <td><?php echo $key;?></td>
                        <td><?php echo $user[0];?></td>
                        <td><?php echo $user[1];?></td>
                        <td><?php echo $user[2];?></td>
                        <td><?php echo $user[3];?></td>
                      </tr>

                      <?php
                    }
                    ?>

                  </tbody>
                </table>

                <table class="table table-respomsive" cellspacing="0" width="100%">
                  <thead>
                    <tr>
                      <td width="25">No</td>
                      <td>UID</td>
                      <td>ID</td>
                      <td>State</td>
                      <td>Date/Time</td>
                    </tr>
                  </thead>

                  <tbody>
                    <?php
                    $no = 0;
                    foreach($attendace as $key=>$at)
                    {
                      $no++;
                      ?>

                      <tr>
                        <td align="right"><?php echo $no;?></td>
                        <td><?php echo $at[0];?></td>
                        <td><?php echo $at[1];?></td>
                        <td><?php echo $at[2];?></td>
                        <td><?php echo $at[3];?></td>
                      </tr>

                      <?php
                    }
                    ?>

                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

</div>
