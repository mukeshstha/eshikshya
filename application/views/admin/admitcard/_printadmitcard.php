<style type="text/css">
    @media print {
        .pagebreak {
            page-break-before: always;
        }
    }
    * {
        padding: 0;
        margin: 0;
    }

    .tableone td {
        padding: 5px 10px
    }

    table.denifittable {
        border: 1px solid #999;
        border-collapse: collapse;
    }

    .denifittable th {
        padding: 5px 0px;
        font-weight: normal;
        border-collapse: collapse;
        border-right: 1px solid #999;
        border-bottom: 1px solid #999;
    }

    .denifittable td {
        padding: 5px 0px;
        font-weight: bold;
        border-collapse: collapse;
        border-left: 1px solid #999;
    }

    .mark-container {
        width: 100%;
        position: relative;
        z-index: 2;
        margin: 0 auto;
        padding: 20px 30px;
    }

    .tcmybg {
        background: top center;
        background-size: 100% 100%;
        position: absolute;
        top: 0;
        left: 0;
        bottom: 0;
        z-index: 1;
    }

    .tablemain {
        position: relative;
        z-index: 2
    }

    @media print {
        .mark-container {
            margin: 0 auto !important;
            padding: 0 !important;
        }

        .page-break {
            display: block;
            page-break-before: always;
        }

        .col-sm-1,
        .col-sm-2,
        .col-sm-3,
        .col-sm-4,
        .col-sm-5,
        .col-sm-6,
        .col-sm-7,
        .col-sm-8,
        .col-sm-9,
        .col-sm-10,
        .col-sm-11,
        .col-sm-12 {
            float: left;
        }

        .col-sm-12 {
            width: 100%;
        }

        .col-sm-11 {
            width: 91.66666667%;
        }

        .col-sm-10 {
            width: 83.33333333%;
        }

        .col-sm-9 {
            width: 75%;
        }

        .col-sm-8 {
            width: 66.66666667%;
        }

        .col-sm-7 {
            width: 58.33333333%;
        }

        .col-sm-6 {
            width: 50%;
        }

        .col-sm-5 {
            width: 41.66666667%;
        }

        .col-sm-4 {
            width: 33.33333333%;
        }

        .col-sm-3 {
            width: 25%;
        }

        .col-sm-2 {
            width: 16.66666667%;
        }

        .col-sm-1 {
            width: 8.33333333%;
        }

        .col-sm-pull-12 {
            right: 100%;
        }

        .col-sm-pull-11 {
            right: 91.66666667%;
        }

        .col-sm-pull-10 {
            right: 83.33333333%;
        }

        .col-sm-pull-9 {
            right: 75%;
        }

        .col-sm-pull-8 {
            right: 66.66666667%;
        }

        .col-sm-pull-7 {
            right: 58.33333333%;
        }

        .col-sm-pull-6 {
            right: 50%;
        }

        .col-sm-pull-5 {
            right: 41.66666667%;
        }

        .col-sm-pull-4 {
            right: 33.33333333%;
        }

        .col-sm-pull-3 {
            right: 25%;
        }

        .col-sm-pull-2 {
            right: 16.66666667%;
        }

        .col-sm-pull-1 {
            right: 8.33333333%;
        }

        .col-sm-pull-0 {
            right: auto;
        }

        .col-sm-push-12 {
            left: 100%;
        }

        .col-sm-push-11 {
            left: 91.66666667%;
        }

        .col-sm-push-10 {
            left: 83.33333333%;
        }

        .col-sm-push-9 {
            left: 75%;
        }

        .col-sm-push-8 {
            left: 66.66666667%;
        }

        .col-sm-push-7 {
            left: 58.33333333%;
        }

        .col-sm-push-6 {
            left: 50%;
        }

        .col-sm-push-5 {
            left: 41.66666667%;
        }

        .col-sm-push-4 {
            left: 33.33333333%;
        }

        .col-sm-push-3 {
            left: 25%;
        }

        .col-sm-push-2 {
            left: 16.66666667%;
        }

        .col-sm-push-1 {
            left: 8.33333333%;
        }

        .col-sm-push-0 {
            left: auto;
        }

        .col-sm-offset-12 {
            margin-left: 100%;
        }

        .col-sm-offset-11 {
            margin-left: 91.66666667%;
        }

        .col-sm-offset-10 {
            margin-left: 83.33333333%;
        }

        .col-sm-offset-9 {
            margin-left: 75%;
        }

        .col-sm-offset-8 {
            margin-left: 66.66666667%;
        }

        .col-sm-offset-7 {
            margin-left: 58.33333333%;
        }

        .col-sm-offset-6 {
            margin-left: 50%;
        }

        .col-sm-offset-5 {
            margin-left: 41.66666667%;
        }

        .col-sm-offset-4 {
            margin-left: 33.33333333%;
        }

        .col-sm-offset-3 {
            margin-left: 25%;
        }

        .col-sm-offset-2 {
            margin-left: 16.66666667%;
        }

        .col-sm-offset-1 {
            margin-left: 8.33333333%;
        }

        .col-sm-offset-0 {
            margin-left: 0%;
        }

        .visible-xs {
            display: none !important;
        }

        .hidden-xs {
            display: block !important;
        }

        table.hidden-xs {
            display: table;
        }

        tr.hidden-xs {
            display: table-row !important;
        }

        th.hidden-xs,
        td.hidden-xs {
            display: table-cell !important;
        }

        .hidden-xs.hidden-print {
            display: none !important;
        }

        .hidden-sm {
            display: none !important;
        }

        .visible-sm {
            display: block !important;
        }

        table.visible-sm {
            display: table;
        }

        tr.visible-sm {
            display: table-row !important;
        }

        th.visible-sm,
        td.visible-sm {
            display: table-cell !important;
        }

        .intro-table td {
            line-height: 10px !important;
        }
    }


    @media print {

        .back-color {
            background-color: #a7a7a7 !important;
            color: #fff;
            -webkit-print-color-adjust: exact;
        }

    }
</style>
<link rel="stylesheet" href="<?php echo base_url(); ?>backend/bootstrap/css/bootstrap.min.css">
<?php if (!empty($student_details)) {
    foreach ($student_details as $student_key => $student_value) {
        ?>
        <?php if ($this->customlib->display_admitcard() == "1") {
            $student_due_fee = $this->studentfeemaster_model->getStudentFees($student_value->student_session_id);
            $total_amount = 0;
            $total_deposite_amount = 0;
            $total_fine_amount = 0;
            $total_discount_amount = 0;
            $total_balance_amount = 0;
            $alot_fee_discount = 0;
            $totalamount = 0;
            $count = 0;

            foreach ($student_due_fee as $key => $fee) {
                foreach ($fee->fees as $fee_key => $fee_value) {
                    $paid_amount = 0;
                    $discount_given = 0;
                    $fee_session_group = $fee_value->fee_groups_feetype_id;
                    $student_fee_master = $fee_value->id;
                    $fee_deposite = $this->studentfee_model->getpayment($student_fee_master, $fee_session_group);
                    $discount_applied = $this->studentfee_model->getdiscountapplied($student_fee_master, $fee_session_group);

                    if (isset($fee_deposite)) {
                        $fee_deposits = json_decode($fee_deposite['amount_detail']);

                        foreach ($fee_deposits as $key => $value) {
                            $paid_amount = $paid_amount + $value->amount;
                        }
                    }
                    if (isset($discount_applied)) {
                        $discount_given = $discount_applied['amount_detail'];
                    }
                    $remainaing_single = $fee_value->amount - $paid_amount - $discount_given;
                }
                $count++;
                $totalamount = $totalamount + $fee_value->amount - $paid_amount - $discount_given;
            }
            if ($totalamount == "0") { 
                if($admitcard->simple_template=="0"){ ?>

                <div class="mark-container" style="margin:0px !important">
                    <?php if ($admitcard->background_img != "") { ?>
                        <img src="<?php echo base_url('uploads/admit_card/' . $admitcard->background_img); ?>" class="tcmybg" width="100%" height="100%" />
                    <?php } ?>
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-2" style="text-align: right">
                                <img src="<?php echo base_url(); ?>uploads/<?php echo $_SERVER['SERVER_NAME']; ?>/school_content/admin_logo/<?php $this->setting_model->getAdminlogo(); ?>"
                                alt="<?php echo $this->customlib->getAppName() ?>"
                                style="width: 100px;height: auto;margin-bottom: 0px" />
                            </div>
                            <div class="col-sm-9 text-center">
                                <span style="font-weight: bold;font-size: 18px">
                                    <?php echo $this->setting_model->getCurrentSchoolName(); ?>
                                </span><br>
                                <p style="margin:0px;">
                                    <?php echo $this->setting_model->get_address() ?>,&nbsp;
                                    <?php echo $this->lang->line('phone') ?> :
                                    <?php echo $this->setting_model->get_phone() ?>
                                </p>
                                <p style="margin:0px;">
                                    <?php echo $this->lang->line('email') ?> :
                                    <?php echo $this->setting_model->get_email() ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <table cellpadding="0" cellspacing="0" width="100%" class="tablemain">
                    <?php if ($admitcard->exam_name) { ?>
                        <tr height="30px">
                            <td valign="top" class="back-color"
                            style="text-align: center;font-size: 17px; text-transform: capitalize; font-weight: bold; padding-top: 5px;margin-bottom: 15px;">
                            <?php echo $admitcard->exam_name; ?></td>
                        </tr>
                    <?php } ?>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <table class="intro-table" cellpadding="0" cellspacing="0" width="100%"
                            style="text-transform: uppercase;">
                            <tr>
                                <td valign="top">
                                    <table cellpadding="0" cellspacing="0" width="100%">
                                        <tr style="font-size: 14px !important;">
                                            <?php if ($admitcard->is_roll_no) { ?>
                                                <td height="10px" valign="top" width="15%" style="padding-bottom: 10px;">
                                                    <?php echo $this->lang->line('roll') . ":" ?>
                                                </td>
                                                <td height="10px" valign="top" width="40%"
                                                style="font-weight: bold;padding-bottom: 5px;">
                                                <?php echo ($exam->use_exam_roll_no) ? $student_value->roll_no : $student_value->profile_roll_no; ?>
                                            </td>
                                        <?php } ?>
                                        <?php if ($admitcard->is_admission_no) { ?>
                                            <td height="10px" valign="top" width="20%" style="padding-bottom: 10px;">
                                                <?php echo $this->lang->line('admission_no') . ":" ?>
                                            </td>
                                            <td height="10px" valign="top" width="25%"
                                            style="font-weight: bold;padding-bottom: 5px;">
                                            <?php echo $student_value->admission_no; ?>
                                        </td>
                                    <?php } ?>
                                </tr>
                                <?php if ($admitcard->is_name) { ?>
                                    <tr>
                                        <td height="10px" valign="top" style="padding-bottom: 10px;">
                                            <?php echo $this->lang->line('name') . ":" ?>
                                        </td>
                                        <td height="10px" valign="top" style="text-transform: uppercase; font-weight: bold;padding-bottom: 10px;">
                                            <?php echo $this->customlib->getFullName($student_value->firstname, $student_value->middlename, $student_value->lastname, $sch_setting->middlename, $sch_setting->lastname); ?>
                                        </td>
                                        <?php if ($admitcard->is_class || $admitcard->is_section) { ?>
                                            <td height="10px" valign="top" style="padding-bottom: 10px;">
                                                <?php echo $this->lang->line('class') . ":"; ?>
                                            </td>
                                            <td height="10px" valign="top" style="text-transform: uppercase; font-weight: bold;padding-bottom: 10px;">
                                                <?php if ($admitcard->is_class && $admitcard->is_section) {
                                                    echo $student_value->class . " (" . $student_value->section . ")";
                                                } elseif ($admitcard->is_class) {
                                                    echo $student_value->class;
                                                } elseif ($admitcard->is_section) {
                                                    echo $student_value->section;
                                                } ?>
                                            </td>
                                        <?php } ?>
                                    </tr>
                                <?php } ?>

                                <tr>
                                    <?php if ($admitcard->is_dob) { ?>
                                        <td valign="top" style="padding-bottom: 10px;">
                                            <?php echo $this->lang->line('d_o_b') . ":"; ?>
                                        </td>
                                        <td valign="top"
                                        style="text-transform: uppercase; font-weight: bold;padding-bottom: 10px;">
                                        <?php echo date($this->customlib->getSchoolDateFormat(), strtotime($student_value->dob)); ?>
                                    </td>
                                <?php } ?>
                                <?php if ($admitcard->is_gender) { ?>
                                    <td valign="top" style="padding-bottom: 10px;">
                                        <?php echo $this->lang->line('gender') . ":"; ?>
                                    </td>
                                    <td valign="top"
                                    style="text-transform: uppercase; font-weight: bold;padding-bottom: 10px;">
                                    <?php echo $student_value->gender; ?>
                                </td>
                            <?php } ?>
                        </tr>
                        <tr>
                            <?php if ($admitcard->is_father_name) { ?>
                                <td valign="top" style="padding-bottom: 10px;">
                                    <?php echo $this->lang->line('fathers') . " " . $this->lang->line('name') . ":" ?>
                                </td>
                                <td valign="top"
                                style="text-transform: uppercase; font-weight: bold;padding-bottom: 10px;">
                                <?php echo $student_value->father_name; ?>
                            </td>
                        <?php } ?>
                        <?php if ($admitcard->is_mother_name) { ?>
                            <td valign="top" style="padding-bottom: 10px;">
                                <?php echo $this->lang->line('mothers') . " " . $this->lang->line('name') . ":"; ?>
                            </td>
                            <td valign="top"
                            style="text-transform: uppercase; font-weight: bold;padding-bottom: 10px;">
                            <?php echo $student_value->mother_name; ?>
                        </td>
                    <?php } ?>
                </tr>
                <?php if ($admitcard->is_address) { ?>
                    <tr>
                        <td valign="top" style="padding-bottom: 10px;">
                            <?php echo $this->lang->line('address') . ":"; ?>
                        </td>
                        <td colspan="3" valign="top"
                        style="text-transform: uppercase; font-weight: bold;padding-bottom: 10px;">
                        <?php echo $student_value->current_address; ?>
                    </td>
                </tr>
            <?php } ?>
            <?php if ($admitcard->school_name != "") { ?>
                                <!-- <tr style="font-size: 11px !important;">
                                                <td valign="top" style="padding-bottom: 10px;"><?php echo $this->lang->line('school_name') ?></td>
                                                <td valign="top" colspan="3" style="text-transform: uppercase; font-weight: bold;padding-bottom: 10px;"><?php echo $admitcard->school_name; ?></td>
                                            </tr> -->
                                        <?php } ?>
                                        <?php if ($admitcard->exam_center != "") { ?>
                                <!-- <tr style="font-size: 11px !important;">
                                                <td valign="top" style="padding-bottom: 10px;"><?php echo $this->lang->line('exam') . " " . $this->lang->line('center'); ?></td>
                                                <td valign="top" colspan="3" style="text-transform: uppercase; font-weight: bold;padding-bottom: 10px;"><?php echo $admitcard->exam_center; ?></td>
                                            </tr> -->
                                        <?php } ?>
                                    </table>
                                </td>
                                <?php if ($admitcard->is_photo) { ?>
                                    <td valign="top" align="center">
                                        <?php if ($student_value->image != '') { ?>
                                            <img src="<?php echo base_url() . $student_value->image; ?>" height="100" style="border: 2px solid #fff;outline: 1px solid #000000;margin-bottom: 20px">
                                        <?php } ?>
                                    </td>
                                <?php } ?>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <table cellpadding="0" cellspacing="0" width="100%" class="denifittable">
                            <tr class="back-color" style="font-weight: bold !important; ">
                                <th height="20px" valign="top" style="text-align: center; text-transform: uppercase;">
                                    <?php echo $this->lang->line('theory_exam_date_time'); ?>
                                </th>
                                <th height="20px" valign="top" style="text-align: center; text-transform: uppercase;">
                                    <?php echo $this->lang->line('paper_code') ?>
                                </th>
                                <th height="20px" valign="top" style="text-align: center; text-transform: uppercase;">
                                    <?php echo $this->lang->line('subject'); ?>
                                </th>
                                <th height="20px" valign="top" style="text-align: center; text-transform: uppercase;">
                                    <?php echo $this->lang->line('obted_by_student') ?>
                                </th>
                            </tr>
                            <?php foreach ($exam_subjects as $subject_key => $subject_value) { ?>
                                <tr style="font-size:16px;">
                                    <td height="15px" valign="top" style="text-align: center;">
                                        <?php echo date($this->customlib->getSchoolDateFormat(), strtotime($subject_value->date_from)) . " " . $subject_value->time_from; ?>
                                    </td>
                                    <td height="15px" style="text-align: center;text-transform: uppercase;">
                                        <?php echo $subject_value->subject_code; ?>
                                    </td>
                                    <td height="15px" style="text-align: center;text-transform: uppercase;">
                                        <?php echo $subject_value->subject_name; ?>
                                    </td>
                                    <td height="15px" style="text-align: center;text-transform: uppercase;">
                                        <?php echo $subject_value->subject_type; ?>
                                    </td>
                                </tr>
                            <?php } ?>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top" height="5"></td>
                </tr>
                <?php if ($admitcard->content_footer != "") { ?>
                    <tr>
                        <td valign="top" style="padding-bottom: 15px; line-height: normal;">
                            <?php echo htmlspecialchars_decode($admitcard->content_footer); ?>
                        </td>
                    </tr>
                <?php } ?>
                <tr height="160px">
                    <td align="right" valign="center">
                        <table cellpadding="0" cellspacing="0" width="100%" style="text-align: center;">
                            <tr style="text-align:right">
                                <?php if ($admitcard->sign != "") { ?>
                                    <td valign="top">
                                        <img src="<?php echo base_url('uploads/' . $_SERVER['SERVER_NAME'] . '/admit_card/' . $admitcard->sign); ?>"
                                        height="25" style="padding-right: 25px;" />
                                    </td>
                                <?php } ?>
                            </tr>
                            <tr>
                                <td style="height: 50px;text-align: right;">
                                    <span
                                    style="border-top:2px solid #000;padding: 15px 25px 0;font-size: 14px;margin-top: 20px">
                                    <?= $this->lang->line('exam_head') ?>
                                </span>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

        </table>
    </div>
    <div class="pagebreak"> </div>
<?php }else{ ?>
    <div class="next-container" style="width: 49%;height:250px;margin:0px !important;display: inline-block;border: 1px solid #000;padding: 10px;">
            <?php if ($admitcard->background_img != "") { ?>
                <img src="<?php echo base_url('uploads/admit_card/' . $admitcard->background_img); ?>" class="tcmybg" width="100%" height="100%" />
            <?php } ?>
            <div class="row">
                <div class="col-sm-2">
                    <img src="<?php echo base_url(); ?>uploads/<?php echo $_SERVER['SERVER_NAME']; ?>/school_content/admin_logo/<?php $this->setting_model->getAdminlogo();?>" alt="<?php echo $this->customlib->getAppName() ?>" style="width: 60px;height: auto;margin-bottom: 0px"/> 
                </div>
                <div class="col-sm-9 text-center">
                    <span style="font-weight: bold;"><?php echo $this->setting_model->getCurrentSchoolName(); ?></span><br>
                    <p style="margin:0px;font-size: 11px;"><?php echo $this->setting_model->get_address() ?></p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <?php if ($admitcard->exam_name) { ?>
                    <p valign="top" style="text-align: center; text-transform: capitalize; text-decoration: underline; font-weight: bold; padding-top: 5px;"><?php echo $admitcard->exam_name; ?></p>
                    <?php } ?>

                    <p style="text-align: center;"><span class="admit-section back-color" style="padding:5px 30px">Admit Card</span></p>

                    
                        <?php if ($admitcard->is_name) { ?>
                        <b>Name:</b> 
                        <span class="info name" style="border-bottom: 1px dotted #000;"><?php echo $this->customlib->getFullName($student_value->firstname, $student_value->middlename, $student_value->lastname, $sch_setting->middlename, $sch_setting->lastname); ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        <?php } ?>
                        <?php if ($admitcard->is_class || $admitcard->is_section) { ?>
                            <b>Class:</b>
                            <span class="info class" style="min-width: 100px;border-bottom: 1px dotted #000;">
                                <?php if ($admitcard->is_class && $admitcard->is_section) {
                                    echo $student_value->class . " (" . $student_value->section . ")";
                                } elseif ($admitcard->is_class) {
                                    echo $student_value->class;
                                } elseif ($admitcard->is_section) {
                                    echo $student_value->section;
                                }
                                ?>
                            </span>&nbsp;&nbsp;&nbsp;
                        <?php } ?>
                    
                        <?php if ($admitcard->is_roll_no) { ?>
                            <b>Symbol No.:</b>
                            <span class="info symbol"  style="min-width: 200px;border-bottom: 1px dotted #000;">
                                <?php echo $student_value->admission_no; ?>
                            </span>
                        <?php } ?>
                </div>
                <div class="col-sm-12">
                    <div class="col-sm-6">
                        <hr style="border-top: 1px solid #000; margin-top: 35px;margin-bottom: 0px">
                        <h5 style="text-align: center;margin-top: 5px">Principal</h5>
                    </div>
                    <div class="col-sm-6">
                        <hr style="border-top: 1px solid #000; margin-top: 35px;margin-bottom: 0px">
                        <h5 style="text-align: center;margin-top: 5px">Account Dept.</h5>
                    </div>
                </div>
            </div>
        </div>


<?php }} } else { 
    if($admitcard->simple_template=="0"){
    ?>
    <div class="mark-container" style="margin:0px !important">
        <?php if ($admitcard->background_img != "") { ?>
            <img src="<?php echo base_url('uploads/admit_card/' . $admitcard->background_img); ?>" class="tcmybg" width="100%" height="100%" />
        <?php } ?>
        <div class="container">
            <div class="row">
                <div class="col-sm-2" style="text-align: right">
                    <img src="<?php echo base_url(); ?>uploads/<?php echo $_SERVER['SERVER_NAME']; ?>/school_content/admin_logo/<?php $this->setting_model->getAdminlogo(); ?>"
                    alt="<?php echo $this->customlib->getAppName() ?>"
                    style="width: 100px;height: auto;margin-bottom: 0px" />
                </div>
                <div class="col-sm-9 text-center">
                    <span style="font-weight: bold;font-size: 18px">
                        <?php echo $this->setting_model->getCurrentSchoolName(); ?>
                    </span><br>
                    <p style="margin:0px;">
                        <?php echo $this->setting_model->get_address() ?>,&nbsp;
                        <?php echo $this->lang->line('phone') ?> :
                        <?php echo $this->setting_model->get_phone() ?>
                    </p>
                    <p style="margin:0px;">
                        <?php echo $this->lang->line('email') ?> :
                        <?php echo $this->setting_model->get_email() ?>
                    </p>
                </div>
            </div>
        </div>
        <table cellpadding="0" cellspacing="0" width="100%" class="tablemain">
            <?php if ($admitcard->exam_name) { ?>
                <tr height="30px">
                    <td valign="top" class="back-color" style="text-align: center;font-size: 17px; text-transform: capitalize; font-weight: bold; padding-top: 5px;margin-bottom: 15px;">
                        <?php echo $admitcard->exam_name; ?>
                    </td>
                </tr>
            <?php } ?>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td valign="top">
                    <table class="intro-table" cellpadding="0" cellspacing="0" width="100%" style="text-transform: uppercase;">
                        <tr>
                            <td valign="top">
                                <table cellpadding="0" cellspacing="0" width="100%">
                                    <tr style="font-size: 14px !important;">
                                        <?php if ($admitcard->is_roll_no) { ?>
                                            <td height="10px" valign="top" width="15%" style="padding-bottom: 10px;">
                                                <?php echo $this->lang->line('roll') . ":" ?>
                                            </td>
                                            <td height="10px" valign="top" width="40%"
                                            style="font-weight: bold;padding-bottom: 5px;">
                                            <?php
                                            echo ($exam->use_exam_roll_no) ? $student_value->roll_no : $student_value->profile_roll_no; ?>
                                        </td>
                                    <?php } ?>
                                    <?php if ($admitcard->is_admission_no) { ?>
                                        <td height="10px" valign="top" width="20%" style="padding-bottom: 10px;">
                                            <?php echo $this->lang->line('admission_no') . ":" ?>
                                        </td>
                                        <td height="10px" valign="top" width="25%" style="font-weight: bold;padding-bottom: 5px;">
                                            <?php echo $student_value->admission_no; ?>
                                        </td>
                                    <?php } ?>
                                </tr>
                                <?php if ($admitcard->is_name) { ?>
                                    <tr>
                                        <td height="10px" valign="top" style="padding-bottom: 10px;">
                                            <?php echo $this->lang->line('name') . ":" ?>
                                        </td>
                                        <td height="10px" valign="top" style="text-transform: uppercase; font-weight: bold;padding-bottom: 10px;">
                                            <?php echo $this->customlib->getFullName($student_value->firstname, $student_value->middlename, $student_value->lastname, $sch_setting->middlename, $sch_setting->lastname); ?>
                                        </td>
                                        <?php if ($admitcard->is_class || $admitcard->is_section) { ?>
                                            <td height="10px" valign="top" style="padding-bottom: 10px;">
                                                <?php echo $this->lang->line('class') . ":"; ?>
                                            </td>
                                            <td height="10px" valign="top" style="text-transform: uppercase; font-weight: bold;padding-bottom: 10px;">
                                                <?php if ($admitcard->is_class && $admitcard->is_section) {
                                                    echo $student_value->class . " (" . $student_value->section . ")";
                                                } elseif ($admitcard->is_class) {
                                                    echo $student_value->class;
                                                } elseif ($admitcard->is_section) {
                                                    echo $student_value->section;
                                                }
                                                ?>
                                            </td>
                                        <?php } ?>
                                    </tr>
                                <?php } ?>

                                <tr>
                                    <?php if ($admitcard->is_dob) { ?>
                                        <td valign="top" style="padding-bottom: 10px;">
                                            <?php echo $this->lang->line('d_o_b') . ":"; ?>
                                        </td>
                                        <td valign="top" style="text-transform: uppercase; font-weight: bold;padding-bottom: 10px;">
                                            <?php echo date($this->customlib->getSchoolDateFormat(), strtotime($student_value->dob)); ?>
                                        </td>
                                    <?php } ?>

                                    <?php if ($admitcard->is_gender) { ?>
                                        <td valign="top" style="padding-bottom: 10px;">
                                            <?php echo $this->lang->line('gender') . ":"; ?>
                                        </td>
                                        <td valign="top" style="text-transform: uppercase; font-weight: bold;padding-bottom: 10px;">
                                            <?php echo $student_value->gender; ?>
                                        </td>
                                    <?php } ?>
                                </tr>
                                <tr>
                                    <?php if ($admitcard->is_father_name) { ?>
                                        <td valign="top" style="padding-bottom: 10px;">
                                            <?php echo $this->lang->line('fathers') . " " . $this->lang->line('name') . ":" ?>
                                        </td>
                                        <td valign="top" style="text-transform: uppercase; font-weight: bold;padding-bottom: 10px;">
                                            <?php echo $student_value->father_name; ?>
                                        </td>
                                    <?php } ?>
                                    <?php if ($admitcard->is_mother_name) { ?>
                                        <td valign="top" style="padding-bottom: 10px;">
                                            <?php echo $this->lang->line('mothers') . " " . $this->lang->line('name') . ":"; ?>
                                        </td>
                                        <td valign="top" style="text-transform: uppercase; font-weight: bold;padding-bottom: 10px;">
                                            <?php echo $student_value->mother_name; ?>
                                        </td>
                                    <?php } ?>
                                </tr>
                                <?php if ($admitcard->is_address) { ?>
                                    <tr>
                                        <td valign="top" style="padding-bottom: 10px;">
                                            <?php echo $this->lang->line('address') . ":"; ?>
                                        </td>
                                        <td colspan="3" valign="top" style="text-transform: uppercase; font-weight: bold;padding-bottom: 10px;">
                                            <?php echo $student_value->current_address; ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                                <?php if ($admitcard->school_name != "") { ?>
                                    <!--<tr style="font-size: 11px !important;">
                                            <td valign="top" style="padding-bottom: 10px;"><?php echo $this->lang->line('school_name') ?></td>
                                            <td valign="top" colspan="3" style="text-transform: uppercase; font-weight: bold;padding-bottom: 10px;"><?php echo $admitcard->school_name; ?></td>
                                        </tr> -->
                                    <?php } ?>
                                    <?php if ($admitcard->exam_center != "") { ?>
                                    <!-- <tr style="font-size: 11px !important;">
                                            <td valign="top" style="padding-bottom: 10px;"><?php echo $this->lang->line('exam') . " " . $this->lang->line('center'); ?></td>
                                            <td valign="top" colspan="3" style="text-transform: uppercase; font-weight: bold;padding-bottom: 10px;"><?php echo $admitcard->exam_center; ?></td>
                                        </tr> -->
                                    <?php } ?>
                                </table>
                            </td>
                            <?php if ($admitcard->is_photo) { ?>
                                <td valign="top" align="center">
                                    <?php if ($student_value->image != '') { ?>
                                        <img src="<?php echo base_url() . $student_value->image; ?>" height="100" style="border: 2px solid #fff; outline: 1px solid #000000;margin-bottom: 20px">
                                    <?php } ?>

                                </td>
                            <?php } ?>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td valign="top">
                    <table cellpadding="0" cellspacing="0" width="100%" class="denifittable">
                        <tr class="back-color" style="font-weight: bold !important; ">
                            <th height="20px" valign="top" style="text-align: center; text-transform: uppercase;">
                                <?php echo $this->lang->line('theory_exam_date_time'); ?>
                            </th>
                            <th height="20px" valign="top" style="text-align: center; text-transform: uppercase;">
                                <?php echo $this->lang->line('paper_code') ?>
                            </th>
                            <th height="20px" valign="top" style="text-align: center; text-transform: uppercase;">
                                <?php echo $this->lang->line('subject'); ?>
                            </th>
                            <th height="20px" valign="top" style="text-align: center; text-transform: uppercase;">
                                <?php echo $this->lang->line('obted_by_student') ?>
                            </th>
                        </tr>
                        <?php foreach ($exam_subjects as $subject_key => $subject_value) { ?>
                            <tr style="font-size:16px;">
                                <td height="15px" valign="top" style="text-align: center;">
                                    <?php echo date($this->customlib->getSchoolDateFormat(), strtotime($subject_value->date_from)) . " " . $subject_value->time_from; ?>
                                </td>
                                <td height="15px" style="text-align: center;text-transform: uppercase;">
                                    <?php echo $subject_value->subject_code; ?>
                                </td>
                                <td height="15px" style="text-align: center;text-transform: uppercase;">
                                    <?php echo $subject_value->subject_name; ?>
                                </td>
                                <td height="15px" style="text-align: center;text-transform: uppercase;">
                                    <?php echo $subject_value->subject_type; ?>
                                </td>
                            </tr>
                        <?php } ?>
                    </table>
                </td>
            </tr>
            <tr>
                <td valign="top" height="5"></td>
            </tr>
            <?php if ($admitcard->content_footer != "") { ?>
                <tr>
                    <td valign="top" style="padding-bottom: 15px; line-height: normal;">
                        <?php echo htmlspecialchars_decode($admitcard->content_footer); ?>
                    </td>
                </tr>
            <?php } ?>

            <tr height="160px">
                <td align="right" valign="center">
                    <table cellpadding="0" cellspacing="0" width="100%" style="text-align: center;">
                        <tr style="text-align:right">
                            <?php if ($admitcard->sign != "") { ?>
                                <td valign="top">
                                    <img src="<?php echo base_url('uploads/' . $_SERVER['SERVER_NAME'] . '/admit_card/' . $admitcard->sign); ?>" height="25" style="padding-right: 25px;" />
                                </td>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td style="height: 50px;text-align: right;">
                                <span style="border-top:2px solid #000;padding: 15px 25px 0;font-size: 14px;margin-top: 20px"> <?= $this->lang->line('exam_head') ?> </span>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

        </table>
    </div>
    <div class="pagebreak"> </div>
<?php }else{ ?>
    <div class="next-container" style="width: 49%;height:250px;margin:0px !important;display: inline-block;border: 1px solid #000;padding: 10px;">
            <?php if ($admitcard->background_img != "") { ?>
                <img src="<?php echo base_url('uploads/admit_card/' . $admitcard->background_img); ?>" class="tcmybg" width="100%" height="100%" />
            <?php } ?>
            <div class="row">
                <div class="col-sm-2">
                    <img src="<?php echo base_url(); ?>uploads/<?php echo $_SERVER['SERVER_NAME']; ?>/school_content/admin_logo/<?php $this->setting_model->getAdminlogo();?>" alt="<?php echo $this->customlib->getAppName() ?>" style="width: 60px;height: auto;margin-bottom: 0px"/> 
                </div>
                <div class="col-sm-9 text-center">
                    <span style="font-weight: bold;"><?php echo $this->setting_model->getCurrentSchoolName(); ?></span><br>
                    <p style="margin:0px;font-size: 11px;"><?php echo $this->setting_model->get_address() ?></p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <?php if ($admitcard->exam_name) { ?>
                    <p valign="top" style="text-align: center; text-transform: capitalize; text-decoration: underline; font-weight: bold; padding-top: 5px;"><?php echo $admitcard->exam_name; ?></p>
                    <?php } ?>

                    <p style="text-align: center;"><span class="admit-section back-color" style="padding:5px 30px">Admit Card</span></p>

                    
                        <?php if ($admitcard->is_name) { ?>
                        <b>Name:</b> 
                        <span class="info name" style="border-bottom: 1px dotted #000;"><?php echo $this->customlib->getFullName($student_value->firstname, $student_value->middlename, $student_value->lastname, $sch_setting->middlename, $sch_setting->lastname); ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        <?php } ?>
                        <?php if ($admitcard->is_class || $admitcard->is_section) { ?>
                            <b>Class:</b>
                            <span class="info class" style="min-width: 100px;border-bottom: 1px dotted #000;">
                                <?php if ($admitcard->is_class && $admitcard->is_section) {
                                    echo $student_value->class . " (" . $student_value->section . ")";
                                } elseif ($admitcard->is_class) {
                                    echo $student_value->class;
                                } elseif ($admitcard->is_section) {
                                    echo $student_value->section;
                                }
                                ?>
                            </span>&nbsp;&nbsp;&nbsp;
                        <?php } ?>
                    
                        <?php if ($admitcard->is_roll_no) { ?>
                            <b>Symbol No.:</b>
                            <span class="info symbol"  style="min-width: 200px;border-bottom: 1px dotted #000;">
                                <?php echo $student_value->admission_no; ?>
                            </span>
                        <?php } ?>
                </div>
                <div class="col-sm-12">
                    <div class="col-sm-6">
                        <hr style="border-top: 1px solid #000; margin-top: 35px;margin-bottom: 0px">
                        <h5 style="text-align: center;margin-top: 5px">Principal</h5>
                    </div>
                    <div class="col-sm-6">
                        <hr style="border-top: 1px solid #000; margin-top: 35px;margin-bottom: 0px">
                        <h5 style="text-align: center;margin-top: 5px">Account Dept.</h5>
                    </div>
                </div>
            </div>
        </div>
<?php } }}} ?>