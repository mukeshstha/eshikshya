<style type="text/css">
    @media print {
        .pagebreak { page-break-before: always; } /* page-break-after works, as well */

        .back-color {
            background-color: #a7a7a7 !important;
            color: #fff;
            -webkit-print-color-adjust: exact;
        }
    }

    *{padding: 0; margin:0;}
    /*body{padding: 0; margin:0; font-family: arial; color: #000; font-size: 14px; line-height: normal;}*/
    .tableone{}
    .tableone td{padding:5px 10px}
    table.denifittable  {border: 1px solid #999;border-collapse: collapse;}
    .denifittable th {padding: 5px 0px; font-weight: normal;  border-collapse: collapse;border-right: 1px solid #999; border-bottom: 1px solid #999;}
    .denifittable td {padding: 5px 0px; font-weight: bold;border-collapse: collapse;border-left: 1px solid #999;}

    .mark-container{position: relative;z-index: 2; margin: 0 auto; padding: 20px 30px;}

    .tcmybg {
        background:top center;
        background-size: 100% 100%;
        position: absolute;
        top: 0;
        left: 0;
        bottom: 0;
        z-index: 1;
    }
    .tablemain{position: relative;z-index: 2}
    @media print {
        .mark-container{
            margin: 0 auto !important;
            padding: 0 !important;
        }
        .page-break { display: block; page-break-before: always; }
        .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12 {
            float: left;
        }
        .col-sm-12 {
            width: 100%;
        }
        .col-sm-11 {
            width: 91.66666667%;
        }
        .col-sm-10 {
            width: 83.33333333%;
        }
        .col-sm-9 {
            width: 75%;
        }
        .col-sm-8 {
            width: 66.66666667%;
        }
        .col-sm-7 {
            width: 58.33333333%;
        }
        .col-sm-6 {
            width: 50%;
        }
        .col-sm-5 {
            width: 41.66666667%;
        }
        .col-sm-4 {
            width: 33.33333333%;
        }
        .col-sm-3 {
            width: 25%;
        }
        .col-sm-2 {
            width: 16.66666667%;
        }
        .col-sm-1 {
            width: 8.33333333%;
        }
        .col-sm-pull-12 {
            right: 100%;
        }
        .col-sm-pull-11 {
            right: 91.66666667%;
        }
        .col-sm-pull-10 {
            right: 83.33333333%;
        }
        .col-sm-pull-9 {
            right: 75%;
        }
        .col-sm-pull-8 {
            right: 66.66666667%;
        }
        .col-sm-pull-7 {
            right: 58.33333333%;
        }
        .col-sm-pull-6 {
            right: 50%;
        }
        .col-sm-pull-5 {
            right: 41.66666667%;
        }
        .col-sm-pull-4 {
            right: 33.33333333%;
        }
        .col-sm-pull-3 {
            right: 25%;
        }
        .col-sm-pull-2 {
            right: 16.66666667%;
        }
        .col-sm-pull-1 {
            right: 8.33333333%;
        }
        .col-sm-pull-0 {
            right: auto;
        }
        .col-sm-push-12 {
            left: 100%;
        }
        .col-sm-push-11 {
            left: 91.66666667%;
        }
        .col-sm-push-10 {
            left: 83.33333333%;
        }
        .col-sm-push-9 {
            left: 75%;
        }
        .col-sm-push-8 {
            left: 66.66666667%;
        }
        .col-sm-push-7 {
            left: 58.33333333%;
        }
        .col-sm-push-6 {
            left: 50%;
        }
        .col-sm-push-5 {
            left: 41.66666667%;
        }
        .col-sm-push-4 {
            left: 33.33333333%;
        }
        .col-sm-push-3 {
            left: 25%;
        }
        .col-sm-push-2 {
            left: 16.66666667%;
        }
        .col-sm-push-1 {
            left: 8.33333333%;
        }
        .col-sm-push-0 {
            left: auto;
        }
        .col-sm-offset-12 {
            margin-left: 100%;
        }
        .col-sm-offset-11 {
            margin-left: 91.66666667%;
        }
        .col-sm-offset-10 {
            margin-left: 83.33333333%;
        }
        .col-sm-offset-9 {
            margin-left: 75%;
        }
        .col-sm-offset-8 {
            margin-left: 66.66666667%;
        }
        .col-sm-offset-7 {
            margin-left: 58.33333333%;
        }
        .col-sm-offset-6 {
            margin-left: 50%;
        }
        .col-sm-offset-5 {
            margin-left: 41.66666667%;
        }
        .col-sm-offset-4 {
            margin-left: 33.33333333%;
        }
        .col-sm-offset-3 {
            margin-left: 25%;
        }
        .col-sm-offset-2 {
            margin-left: 16.66666667%;
        }
        .col-sm-offset-1 {
            margin-left: 8.33333333%;
        }
        .col-sm-offset-0 {
            margin-left: 0%;
        }
        .visible-xs {
            display: none !important;
        }
        .hidden-xs {
            display: block !important;
        }
        table.hidden-xs {
            display: table;
        }
        tr.hidden-xs {
            display: table-row !important;
        }
        th.hidden-xs,
        td.hidden-xs {
            display: table-cell !important;
        }
        .hidden-xs.hidden-print {
            display: none !important;
        }
        .hidden-sm {
            display: none !important;
        }
        .visible-sm {
            display: block !important;
        }
        table.visible-sm {
            display: table;
        }
        tr.visible-sm {
            display: table-row !important;
        }
        th.visible-sm,
        td.visible-sm {
            display: table-cell !important;
        }
        .intro-table td{
            line-height: 10px !important;
        }
    }
</style>
<link rel="stylesheet" href="<?php echo base_url(); ?>backend/bootstrap/css/bootstrap.min.css"> 
<?php if (!empty($student_details)) {
        foreach ($student_details as $student_key => $student_value) { ?>
            <?php $studentdata = $this->student_model->getRecentRecord($student_value); ?>



            <div class="next-container" style="width: 49%;height:250px;margin:0px !important;display: inline-block;border: 1px solid #000;padding: 10px;">
            <?php if ($admitcard->background_img != "") { ?>
                <img src="<?php echo base_url('uploads/admit_card/' . $admitcard->background_img); ?>" class="tcmybg" width="100%" height="100%" />
            <?php } ?>
            <div class="row">
                <div class="col-sm-2">
                    <img src="<?php echo base_url(); ?>uploads/<?php echo $_SERVER['SERVER_NAME']; ?>/school_content/admin_logo/<?php $this->setting_model->getAdminlogo();?>" alt="<?php echo $this->customlib->getAppName() ?>" style="width: 60px;height: auto;margin-bottom: 0px"/> 
                </div>
                <div class="col-sm-9 text-center">
                    <span style="font-weight: bold;"><?php echo $this->setting_model->getCurrentSchoolName(); ?></span><br>
                    <p style="margin:0px;font-size: 11px;"><?php echo $this->setting_model->get_address() ?></p>
                </div>
            </div>
            <div class="row" style="margin-top: 0px">
                <div class="col-sm-12" style="line-height: 25px">
                    <?php if ($admitcard->exam_name) { ?>
                    <p valign="top" style="text-align: center; text-transform: capitalize; text-decoration: underline; font-weight: bold; padding-top: 5px;"><?php echo $admitcard->exam_name; ?></p>
                    <?php } ?>

                    <p style="text-align: center;"><span class="admit-section back-color" style="padding:5px 30px">Admit Card</span></p>

                    
                        <?php if ($admitcard->is_name) { ?>
                        <b>Name:</b> 
                        <span class="info name" style="border-bottom: 1px dotted #000;"><?php echo $studentdata['firstname'] . " " . $studentdata['middlename'] . " " . $studentdata['lastname']; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        <?php } ?>
                        <?php if ($admitcard->is_class || $admitcard->is_section) { ?>
                            <b>Class:</b>
                            <span class="info class" style="min-width: 100px;border-bottom: 1px dotted #000;">
                                <?php if ($admitcard->is_class && $admitcard->is_section) {
                                    echo $studentdata['class'] . " (" . $studentdata['section'] . ")";
                                } elseif ($admitcard->is_class) {
                                    echo $studentdata['class'];
                                } elseif ($admitcard->is_section) {
                                    echo $studentdata['section'];
                                }
                                ?>
                            </span>&nbsp;&nbsp;&nbsp;
                        <?php } ?>
                    
                        <?php if ($admitcard->is_roll_no) { ?>
                            <b>Symbol No.:</b>
                            <span class="info symbol"  style="min-width: 200px;border-bottom: 1px dotted #000;">
                                <?php echo $studentdata['admission_no']; ?>
                            </span>
                        <?php } ?>
                </div>
                <div class="col-sm-12">
                    <div class="col-sm-6">
                        <hr style="border-top: 1px solid #000; margin-top: 35px;margin-bottom: 0px">
                        <h5 style="text-align: center;margin-top: 5px">Principal</h5>
                    </div>
                    <div class="col-sm-6">
                        <hr style="border-top: 1px solid #000; margin-top: 35px;margin-bottom: 0px">
                        <h5 style="text-align: center;margin-top: 5px">Account Dept.</h5>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
}
?>