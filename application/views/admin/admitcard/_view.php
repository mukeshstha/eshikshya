<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <link rel="icon" type="image/png" href="assets/img/s-favican.png">
        <meta http-equiv="X-UA-Compatible" content="" />
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="theme-color" content="" />
        <style type="text/css">
            *{padding: 0; margin:0;}
            /*            body{padding: 0; margin:0; font-family: arial; color: #000; font-size: 14px; line-height: normal;}*/
            .tableone{}
            .tableone td{padding:5px 10px}
            table.denifittable  {border: 1px solid #999;border-collapse: collapse;}
            .denifittable th {padding: 10px 10px; font-weight: normal;  border-collapse: collapse;border-right: 1px solid #999; border-bottom: 1px solid #999;}
            .denifittable td {padding: 10px 10px; font-weight: bold;border-collapse: collapse;border-left: 1px solid #999;}

            .mark-container{
                width: 1000px;position: relative;z-index: 2; margin: 0 auto; padding: 20px 30px;}

            .tcmybg {
                background:top center;
                background-size: 100% 100%;
                position: absolute;
                top: 0;
                left: 0;
                bottom: 0;
                z-index: 1;
            }

            @media print {
        .page-break { display: block; page-break-before: always; }
        .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12 {
            float: left;
        }
        .col-sm-12 {
            width: 100%;
        }
        .col-sm-11 {
            width: 91.66666667%;
        }
        .col-sm-10 {
            width: 83.33333333%;
        }
        .col-sm-9 {
            width: 75%;
        }
        .col-sm-8 {
            width: 66.66666667%;
        }
        .col-sm-7 {
            width: 58.33333333%;
        }
        .col-sm-6 {
            width: 50%;
        }
        .col-sm-5 {
            width: 41.66666667%;
        }
        .col-sm-4 {
            width: 33.33333333%;
        }
        .col-sm-3 {
            width: 25%;
        }
        .col-sm-2 {
            width: 16.66666667%;
        }
        .col-sm-1 {
            width: 8.33333333%;
        }
        .col-sm-pull-12 {
            right: 100%;
        }
        .col-sm-pull-11 {
            right: 91.66666667%;
        }
        .col-sm-pull-10 {
            right: 83.33333333%;
        }
        .col-sm-pull-9 {
            right: 75%;
        }
        .col-sm-pull-8 {
            right: 66.66666667%;
        }
        .col-sm-pull-7 {
            right: 58.33333333%;
        }
        .col-sm-pull-6 {
            right: 50%;
        }
        .col-sm-pull-5 {
            right: 41.66666667%;
        }
        .col-sm-pull-4 {
            right: 33.33333333%;
        }
        .col-sm-pull-3 {
            right: 25%;
        }
        .col-sm-pull-2 {
            right: 16.66666667%;
        }
        .col-sm-pull-1 {
            right: 8.33333333%;
        }
        .col-sm-pull-0 {
            right: auto;
        }
        .col-sm-push-12 {
            left: 100%;
        }
        .col-sm-push-11 {
            left: 91.66666667%;
        }
        .col-sm-push-10 {
            left: 83.33333333%;
        }
        .col-sm-push-9 {
            left: 75%;
        }
        .col-sm-push-8 {
            left: 66.66666667%;
        }
        .col-sm-push-7 {
            left: 58.33333333%;
        }
        .col-sm-push-6 {
            left: 50%;
        }
        .col-sm-push-5 {
            left: 41.66666667%;
        }
        .col-sm-push-4 {
            left: 33.33333333%;
        }
        .col-sm-push-3 {
            left: 25%;
        }
        .col-sm-push-2 {
            left: 16.66666667%;
        }
        .col-sm-push-1 {
            left: 8.33333333%;
        }
        .col-sm-push-0 {
            left: auto;
        }
        .col-sm-offset-12 {
            margin-left: 100%;
        }
        .col-sm-offset-11 {
            margin-left: 91.66666667%;
        }
        .col-sm-offset-10 {
            margin-left: 83.33333333%;
        }
        .col-sm-offset-9 {
            margin-left: 75%;
        }
        .col-sm-offset-8 {
            margin-left: 66.66666667%;
        }
        .col-sm-offset-7 {
            margin-left: 58.33333333%;
        }
        .col-sm-offset-6 {
            margin-left: 50%;
        }
        .col-sm-offset-5 {
            margin-left: 41.66666667%;
        }
        .col-sm-offset-4 {
            margin-left: 33.33333333%;
        }
        .col-sm-offset-3 {
            margin-left: 25%;
        }
        .col-sm-offset-2 {
            margin-left: 16.66666667%;
        }
        .col-sm-offset-1 {
            margin-left: 8.33333333%;
        }
        .col-sm-offset-0 {
            margin-left: 0%;
        }
        .visible-xs {
            display: none !important;
        }
        .hidden-xs {
            display: block !important;
        }
        table.hidden-xs {
            display: table;
        }
        tr.hidden-xs {
            display: table-row !important;
        }
        th.hidden-xs,
        td.hidden-xs {
            display: table-cell !important;
        }
        .hidden-xs.hidden-print {
            display: none !important;
        }
        .hidden-sm {
            display: none !important;
        }
        .visible-sm {
            display: block !important;
        }
        table.visible-sm {
            display: table;
        }
        tr.visible-sm {
            display: table-row !important;
        }
        th.visible-sm,
        td.visible-sm {
            display: table-cell !important;
        }
    }

        </style>
        <link rel="stylesheet" href="<?php echo base_url(); ?>backend/bootstrap/css/bootstrap.min.css"> 
    </head>
    <body>
        <?php
        $domain = $_SERVER['SERVER_NAME'];
if ($admitcard->background_img != "") {
    ?>
            <img src="<?php echo base_url('uploads/'.$domain.'/admit_card/' . $admitcard->background_img); ?>" class="tcmybg" width="100%" height="100%" style="opacity: 0.1"/>
            <?php
}
?>
        

        <div class="mark-container"  style="width: 720px;">
            <div class="row">
        
                    <div class="col-sm-2 text-right">
                        <img src="<?php echo base_url(); ?>uploads/<?php echo $domain; ?>/school_content/admin_logo/<?php $this->setting_model->getAdminlogo();?>" alt="<?php echo $this->customlib->getAppName() ?>" style="width: 60px;height: auto;margin-bottom: 0px"/> 
                    </div>
                    <div class="col-sm-9 text-center">
                        <span style="font-weight: bold;font-size: 14px"><?php echo $this->setting_model->getCurrentSchoolName(); ?></span><br>
                        <p style="margin:0px;font-size: 13px;font-weight: 600;"><?php echo $this->setting_model->get_address() ?>,&nbsp;<?php echo $this->lang->line('phone') ?> : <?php echo $this->setting_model->get_phone() ?></p>
                        <p style="margin:0px;font-size: 13px;font-weight: 600;"><?php echo $this->lang->line('email') ?> : <?php echo $this->setting_model->get_email() ?></p>
                    </div>
            </div>
            <table cellpadding="0" cellspacing="0" width="100%">
                <?php
if ($admitcard->title != "" || $admitcard->heading != "" || $admitcard->left_logo != "") {
    ?>
                    <tr>
                        <td valign="top">
                            <table cellpadding="0" cellspacing="0" width="100%">
                                <!-- <tr>
                                    <td valign="top" align="center" width="100%">
                                        <?php
if ($admitcard->left_logo != "") {
        ?>
                                            <img src="<?php echo base_url('uploads/admit_card/' . $admitcard->left_logo); ?>" width="auto" height="100"/>
                                            <?php
}
    ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top">
                                        <table cellpadding="0" cellspacing="0" width="100%">
                                            <?php
if ($admitcard->heading != "") {
        ?>
                                                <tr>
                                                    <td valign="top" style="font-size: 26px; font-weight: bold; text-align: center; text-transform: uppercase; padding-top: 10px;"><?php echo $admitcard->heading; ?></td>
                                                </tr>
                                                <?php
}
    ?>

                                            <tr><td valign="top" height="5"></td></tr>
                                            <?php
if ($admitcard->title != "") {
        ?>
                                                <tr>
                                                    <td valign="top" style="font-size: 20px;text-align: center; text-transform: uppercase; text-decoration: underline;">
                                                        <?php echo $admitcard->title; ?></td>
                                                </tr>
                                                <?php
}
    ?>

                                        </table>
                                    </td>
                                    <td width="100" valign="top" align="center">
                                        <?php
if ($admitcard->right_logo != "") {
        ?>
                                            <img src="<?php echo base_url('uploads/admit_card/' . $admitcard->right_logo); ?>" width="100" height="100">
                                            <?php
}
    ?>
                                    </td>

                                </tr>
                            </table>
                        </td>
                    </tr> -->
                    <?php
}
?>

                <?php
if ($admitcard->exam_name) {
    ?>
                    <tr>
                        <td valign="top" style="text-align: center; text-transform: capitalize; text-decoration: underline; font-weight: bold; padding-top: 5px;"><?php echo $admitcard->exam_name; ?></td>
                    </tr>
                    <?php
}
?>

                <tr><td valign="top" height="10"></td></tr>
                <tr>
                    <td valign="top">
                        <table cellpadding="0" cellspacing="0" width="100%" style="text-transform: uppercase;">
                            <tr>
                                <td valign="top">
                                    <table cellpadding="0" cellspacing="0" width="100%" >
                                        <tr>
                                            <?php
if ($admitcard->is_roll_no) {
    ?>
                                                <td valign="top" width="25%" style="padding-bottom: 10px;"> <?php echo $this->lang->line('roll_no') ?></td>
                                                <td valign="top" width="30%" style="font-weight: bold;padding-bottom: 10px;">10</td>
                                                <?php
}
?>
                                            <?php
if ($admitcard->is_admission_no) {
    ?>
                                                <td valign="top" width="20%" style="padding-bottom: 10px;"> <?php echo $this->lang->line('admission_no') ?></td>
                                                <td valign="top" width="25%" style="font-weight: bold;padding-bottom: 10px;">S155</td>
                                                <?php
}
?>


                                        </tr>

                                        <tr>
                                            <?php
if ($admitcard->is_name) {
    ?>
                                                <td valign="top" style="padding-bottom: 10px;"> <?php echo $this->lang->line('candidates') . " " . $this->lang->line('name') ?></td>
                                                <td valign="top" style="text-transform: uppercase; font-weight: bold;padding-bottom: 10px;">Firstname Lastname</td>
                                                <?php
}
?>


                                            <?php
if ($admitcard->is_class || $admitcard->is_section) {
    ?>

                                                <td valign="top" style="padding-bottom: 10px;"> <?php echo $this->lang->line('class'); ?></td>
                                                <td valign="top" style="text-transform: uppercase; font-weight: bold;padding-bottom: 10px;">

                                                    <?php
if ($admitcard->is_class && $admitcard->is_section) {
        ?>
                                                        1 (A)
                                                        <?php
} elseif ($admitcard->is_class) {
        ?>
                                                        1
                                                        <?php
} elseif ($admitcard->is_section) {
        ?>
                                                        (A)
                                                        <?php
}
    ?>
                                                </td>



                                                <?php
}
?>
                                        </tr>

                                        <tr>
                                            <?php
if ($admitcard->is_dob) {
    ?>
                                                <td valign="top" style="padding-bottom: 10px;"><?php echo $this->lang->line('d_o_b'); ?></td>
                                                <td valign="top" style="text-transform: uppercase; font-weight: bold;padding-bottom: 10px;">8/10/2049</td>
                                                <?php
}
?>

                                            <?php
if ($admitcard->is_gender) {
    ?>
                                                <td valign="top" style="padding-bottom: 10px;"> <?php echo $this->lang->line('gender'); ?></td>
                                                <td valign="top" style="text-transform: uppercase; font-weight: bold;padding-bottom: 10px;"><?php echo $this->lang->line('male'); ?></td>
                                                <?php
}
?>

                                        </tr>

                                        <tr>
                                            <?php
if ($admitcard->is_father_name) {
    ?>
                                                <td valign="top" style="padding-bottom: 10px;"><?php echo $this->lang->line('fathers') . " " . $this->lang->line('name') ?></td>
                                                <td valign="top" style="text-transform: uppercase; font-weight: bold;padding-bottom: 10px;">Father Fullname</td>
                                                <?php
}
?>

                                            <?php
if ($admitcard->is_mother_name) {
    ?>
                                                <td valign="top" style="padding-bottom: 10px;"><?php echo $this->lang->line('mothers') . " " . $this->lang->line('name'); ?></td>
                                                <td valign="top" style="text-transform: uppercase; font-weight: bold;padding-bottom: 10px;">Mother's Fullname</td>
                                                <?php
}
?>

                                        </tr>
                                        <tr>
                                            <?php
if ($admitcard->is_address) {
    ?>
                                                <td valign="top" style="padding-bottom: 10px;"><?php echo $this->lang->line('address'); ?></td>
                                                <td colspan="3" valign="top" style="text-transform: uppercase; font-weight: bold;padding-bottom: 10px;">Kathmandu, Nepal</td>
                                                <?php
}
?>

                                        </tr>
                                        <?php
if ($admitcard->school_name != "") {
    ?>
                                           <!--  <tr>
                                                <td valign="top" style="padding-bottom: 10px;"><?php echo $this->lang->line('school_name') ?></td>
                                                <td valign="top" colspan="3" style="text-transform: uppercase; font-weight: bold;padding-bottom: 10px;"><?php echo $admitcard->school_name; ?></td>
                                            </tr> -->
                                            <?php
}
?>

                                        <?php
if ($admitcard->exam_center != "") {
    ?>
                                            <!-- <tr>
                                                <td valign="top" style="padding-bottom: 10px;"><?php echo $this->lang->line('exam') . " " . $this->lang->line('center'); ?></td>
                                                <td valign="top" colspan="3" style="text-transform: uppercase; font-weight: bold;padding-bottom: 10px;"> <?php echo $admitcard->exam_center; ?></td>
                                            </tr> -->
                                            <?php
}
?>
                                    </table>
                                </td>
                                <?php
if ($admitcard->is_photo) {
    ?>
                                    <td valign="top" width="25%" align="right">
                                        <img src="<?php echo base_url('uploads/'.$domain.'/student_images/no_image.png'); ?>" width="100" height="100">
                                    </td>
                                    <?php
}
?>

                            </tr>
                        </table>
                    </td>
                </tr>
                <tr><td valign="top" height="10"></td></tr>
                <tr>
                    <td valign="top">
                        <table cellpadding="0" cellspacing="0" width="100%" class="denifittable">
                            <tr>
                                <th valign="top" style="text-align: center; text-transform: uppercase;"><?php echo $this->lang->line('theory_exam_date_time'); ?></th>
                                <th valign="top" style="text-align: center; text-transform: uppercase;"><?php echo $this->lang->line('paper_code') ?></th>
                                <th valign="top" style="text-align: center; text-transform: uppercase;"><?php echo $this->lang->line('subject'); ?></th>
                                <th valign="top" style="text-align: center; text-transform: uppercase;"><?php echo $this->lang->line('obted_by_student') ?></th>
                            </tr>
                            <tr>
                                <td valign="top" style="text-align: center;">03-Jun-<?php echo date('Y') ?> 2 P.M. - 5 P.M.</td>
                                <td style="text-align: center;text-transform: uppercase;">7713</td>
                                <td style="text-align: center;text-transform: uppercase;">Mathematics</td>
                                <td style="text-align: center;text-transform: uppercase;">TH</td>
                            </tr>
                            <tr>
                                <td valign="top" style="text-align: center;">03-Jun-<?php echo date('Y') ?> 2 P.M. - 5 P.M.</td>
                                <td style="text-align: center;text-transform: uppercase;">7714</td>
                                <td style="text-align: center;text-transform: uppercase;">Sceince</td>
                                <td style="text-align: center;text-transform: uppercase;">TH</td>
                            </tr>
                            <tr>
                                <td valign="top" style="text-align: center;">03-Jun-<?php echo date('Y') ?> 2 P.M. - 5 P.M.</td>
                                <td style="text-align: center;text-transform: uppercase;">7715</td>
                                <td style="text-align: center;text-transform: uppercase;">English</td>
                                <td style="text-align: center;text-transform: uppercase;">TH</td>
                            </tr>
                            <tr>
                                <td valign="top" style="text-align: center;">03-Jun-<?php echo date('Y') ?> 2 P.M. - 5 P.M.</td>
                                <td style="text-align: center;text-transform: uppercase;">7716</td>
                                <td style="text-align: center;text-transform: uppercase;">Social Science</td>
                                <td style="text-align: center;text-transform: uppercase;">TH</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr><td valign="top" height="5"></td></tr>

                <?php
if ($admitcard->content_footer != "") {
    ?>
                    <tr>
                        <td valign="top" style="padding-bottom: 15px; line-height: normal;"> <?php echo htmlspecialchars_decode($admitcard->content_footer); ?></td>
                    </tr>
                    <?php
}
?>
                <tr><td valign="top" height="20px"></td></tr>
                <?php
if ($admitcard->sign != "") {
    ?>
                    <tr>
                        <td align="right" valign="top">
                            <table cellpadding="0" cellspacing="0" width="100%" style="text-align: center;">
                                <tr style="text-align:right">
                                    <td valign="top">
                                        <img src="<?php echo base_url('uploads/'.$domain.'/admit_card/' . $admitcard->sign); ?>" width="100" height="38"  />

                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 40px;text-align: right;">
                                        <span style="border-top:2px solid #000;padding: 10px 25px 0;"><?= $this->lang->line('exam_head') ?></span>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <?php
}
?>
            </table>
        </div>
    </body>
</html>