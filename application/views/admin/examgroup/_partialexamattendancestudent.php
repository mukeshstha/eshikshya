<form method="post" action="<?php echo site_url('admin/examgroup/studentattendance') ?>">
    <input type="hidden" name="exam_group_class_batch_exam_id" value="<?php echo $exam_id; ?>">
    <?php
    if (isset($resultlist) && !empty($resultlist)) {
        ?>
        <div class="row">

            <div class="col-md-12">


                <div class=" table-responsive ptt10">

                    <table class="table table-striped"> 
                        <tbody>
                            <tr>

                                <th><?php echo $this->lang->line('admission_no'); ?></th>

                                <th><?php echo $this->lang->line('student_name'); ?></th> <th><?php echo $this->lang->line('father_name'); ?></th>
                                <th><?php echo $this->lang->line('gender'); ?></th>
                                <th>Present Days</th>

                            </tr>
                            <?php
                            if (empty($resultlist)) {
                                ?>
                                <tr>
                                    <td colspan="7" class="text-danger text-center"><?php echo $this->lang->line('no_record_found'); ?></td>
                                </tr>
                                <?php
                            } else {
                                $counter = 1; 
                                foreach ($resultlist as $student) {
                                    ?>
                                    <?php if ($student['onlineexam_student_id'] != 0) : ?>
                                        <?php
                                        $student_batch_exam=$exam_id;
                                        $output = $this->examresult_model->getattendance($student_batch_exam,$student['id'],$student['student_session_id']);

                                        ?>
                                    <?php if($output!==null) : ?>
                                    <tr>
                                            <input type="hidden" name="student_<?php echo $student['student_session_id']; ?>" value="<?php echo $student['id']; ?>">
                                            <input type="hidden" name="student_session_id[]"  value="<?php echo $student['student_session_id']; ?>" >
                                            <input type="hidden" name="id_<?php echo $student['student_session_id']; ?>" value="<?php echo $output->id; ?>">
                                    	<td style="display:none"><?php echo $student['id']; ?></td>
                                        <td><?php echo $student['admission_no']; ?></td>

                                        <td><?php echo $this->customlib->getFullName($student['firstname'],$student['middlename'],$student['lastname'],$sch_setting->middlename,$sch_setting->lastname);?></td>
                                        <td><?php echo $student['father_name']; ?></td>
                                        <td><?php echo $student['gender']; ?></td>
                                        <td><input type="text" name="attendance_<?php echo $student['student_session_id']; ?>" value="<?php echo $output->present_days; ?>"></td>


                                    </tr>
                                <?php else : ?>
                                    <tr>
                                        <input type="hidden" name="id_<?php echo $student['student_session_id']; ?>" value="">
                                            <input type="hidden" name="student_<?php echo $student['student_session_id']; ?>" value="<?php echo $student['id']; ?>">
                                            <input type="hidden" name="student_session_id[]"  value="<?php echo $student['student_session_id']; ?>" >
                                        <td style="display:none"><?php echo $student['id']; ?></td>
                                        <td><?php echo $student['admission_no']; ?></td>

                                        <td><?php echo $this->customlib->getFullName($student['firstname'],$student['middlename'],$student['lastname'],$sch_setting->middlename,$sch_setting->lastname);?></td>
                                        <td><?php echo $student['father_name']; ?></td>
                                        <td><?php echo $student['gender']; ?></td>
                                        <td><input type="text" name="attendance_<?php echo $student['student_session_id']; ?>"></td>


                                    </tr>
                                <?php endif; ?>
                                <?php endif; ?>
                                    <?php
                                }
                            }
                            ?>
                        </tbody></table>

                </div>

                <?php if ($this->rbac->hasPrivilege('exam_assign_view_student', 'can_edit')) { ?>
                    <button type="submit" class="btn btn-primary btn-sm pull-right" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Please Wait.."><?php echo $this->lang->line('save'); ?>
                    </button>
                <?php } ?>
            </div>
        </div>
        <?php
    } else {
        ?>        
        <div class="alert alert-danger "><?php echo $this->lang->line('no_record_found'); ?></div>
        <?php
    }
    ?>
</form>