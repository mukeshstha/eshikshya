<?php
$theme = $this->customlib->getCurrentTheme();

if ($this->customlib->getRTL() != "") {
    if ($theme == "white") {
        ?>
        <link rel="stylesheet" href="<?php echo base_url(); ?>backend/rtl/bootstrap-rtl/css/bootstrap-rtl.min.css"/>
        <!-- Theme RTL style -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>backend/rtl/dist/css/white-rtl.css" /> 
        <link rel="stylesheet" href="<?php echo base_url(); ?>backend/rtl/dist/css/AdminLTE-rtl.min.css" />

        <link rel="stylesheet" href="<?php echo base_url(); ?>backend/rtl/dist/css/skins/_all-skins-rtl.min.css" />

        <?php
    } else {
        ?>
        <!-- Bootstrap 3.3.5 RTL -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>backend/rtl/bootstrap-rtl/css/bootstrap-rtl.min.css"/>
        <!-- Theme RTL style -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>backend/rtl/dist/css/AdminLTE-rtl.min.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>backend/rtl/dist/css/ss-rtlmain.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>backend/rtl/dist/css/skins/_all-skins-rtl.min.css" />
        <?php
    }
}

if ($theme == "white") {
    ?>
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/themes/white/skins/_all-skins.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/themes/white/ss-main.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/css/main.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/css/sass/distBlue/all.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/css/sass/distBlue/header.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/css/sass/distBlue/sidebar.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/css/sass/distBlue/content.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/css/sass/distBlue/buttons.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/css/sass/distBlue/footer.css">

    <?php
} elseif ($theme == "default") {
    ?>
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/css/style-main.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/themes/default/skins/_all-skins.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/themes/default/ss-main.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/css/sass/distBlue/all.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/css/sass/distBlue/header.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/css/sass/distBlue/sidebar.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/css/sass/distBlue/content.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/css/sass/distBlue/buttons.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/css/sass/distBlue/footer.css"> 






    <?php
} elseif ($theme == "red") {
    ?>
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/css/style-main.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/themes/red/skins/skin-red.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/themes/red/ss-main-red.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/css/sass/distRed/all.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/css/sass/distRed/header.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/css/sass/distRed/sidebar.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/css/sass/distRed/content.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/css/sass/distRed/buttons.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/css/sass/distRed/footer.css"> 
    <?php
} elseif ($theme == "blue") {
    ?>
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/css/style-main.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/themes/blue/skins/skin-darkblue.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/themes/blue/ss-main-darkblue.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/css/sass/distOrange/all.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/css/sass/distOrange/header.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/css/sass/distOrange/sidebar.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/css/sass/distOrange/content.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/css/sass/distOrange/buttons.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/css/sass/distOrange/footer.css"> 
    <?php
} elseif ($theme == "gray") {
    ?>
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/css/style-main.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/themes/gray/skins/skin-light.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/themes/gray/ss-main-light.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/css/sass/distGrey/all.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/css/sass/distGrey/header.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/css/sass/distGrey/sidebar.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/css/sass/distGrey/content.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/css/sass/distGrey/buttons.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/css/sass/distGrey/footer.css"> 
    <?php
}