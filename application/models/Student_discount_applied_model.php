<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Student_discount_applied_model extends MY_Model
{
    public function __construct() {
        parent::__construct();
    }

public function discount_applied($data)
    {
        $this->db->insert('student_discount_applied', $data);
            $insert_id = $this->db->insert_id();
            $message = INSERT_RECORD_CONSTANT . " On Account Category id " . $insert_id;
            $action = "Insert";
            $record_id = $insert_id;
            //======================Code End==============================

            $this->db->trans_complete(); # Completing transaction
            /* Optional */

            if ($this->db->trans_status() === false) {
                # Something went wrong.
                $this->db->trans_rollback();
                return false;
            } else {
                //return $return_value;
            }
            return $insert_id;
        

}

    // public function discount_available($student_master,$feegroupid){
    //     $sql="Select * from student_discount_applied WHERE student_fees_master_id=" .$student_master ."fee_groups_feetype_id=" . $feegroupid;    
    //     $query = $this->db->query($sql);
    //     return $query->row();

    // }

    public function remove($id){
        $this->db->where('id', $id);
        $this->db->delete('student_discount_applied');

        if ($this->db->trans_status() === false) {
            # Something went wrong.
            $this->db->trans_rollback();
            return false;
        } else {
            return true;
        }
    }

    public function insert($insert){
        $this->db->insert('student_discount_applied',$insert);
        $insert_id = $this->db->insert_id();
        $message = INSERT_RECORD_CONSTANT . " On Account Category id " . $insert_id;
        $action = "Insert";
        $record_id = $insert_id;
        //======================Code End==============================

        $this->db->trans_complete(); # Completing transaction
        /* Optional */

        if ($this->db->trans_status() === false) {
            # Something went wrong.
            $this->db->trans_rollback();
            return false;
        } else {
            //return $return_value;
        }
        return $insert_id;
    }

}


?>