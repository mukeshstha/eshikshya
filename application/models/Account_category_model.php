<?php 

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Account_category_model extends CI_Model {
	public function __construct(){
		parent::__construct();

	}

    public function getParentname($id){
        $giventoken = $_COOKIE["Token"];
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => 'http://actm.prabhumanagement.com/api/ChartOfAccount/GetCOACategory',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
          CURLOPT_HTTPHEADER => array(
            'Authorization: Bearer ' . $giventoken
          ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        $data= json_decode($response, true);

        foreach ($data['Data'] as $key => $value) {
            if($value['GLCode']==$id){
                return $value['Name'];
            }
        }
    }

    public function getvoucherdetails($voucher_id,$voucher_no){
        $curl = curl_init();
        $token = $_COOKIE["Token"];
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'http://actm.prabhumanagement.com/api/Voucher/GetVoucherDetails?Id='.$voucher_id.'&VoucherNo='.$voucher_no,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Authorization: Bearer '. $token
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        $data= json_decode($response, true);
        return $data; 
 
    }

}

?>