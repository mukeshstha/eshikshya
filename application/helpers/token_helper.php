<?php
function tokenhelper(){
  $CI = &get_instance();
  $username = $CI->customlib->getacuser();
  $password = $CI->customlib->getacpwd();

  $filusername = $username['ac_user'];
  $filepassword = $password['ac_pwd'];
  $data = 'username='.$filusername.'&password='.$filepassword.'&grant_type=password';

  $curl = curl_init();

  curl_setopt_array($curl, array(
    CURLOPT_URL => 'http://actm.prabhumanagement.com/token',
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => '',
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => 'GET',
    CURLOPT_POSTFIELDS => $data,
    CURLOPT_HTTPHEADER => array(
      'Content-Type: application/x-www-form-urlencoded'
    ),
  ));

  $response= curl_exec($curl);

  curl_close($curl);
  $text= json_decode($response, true);
  return $text;
}

function gettoken(){
  $giventoken = tokenhelper();
  if(isset($giventoken)){
  if(isset($giventoken['ExpiryDate'])){
    if(!isset($_COOKIE["Token"])) {
      $cookie_name = "Token";
      $cookie_value = $giventoken['access_token']; 
      $cookie_time = $giventoken['ExpiryDate'];
      setcookie($cookie_name, $cookie_value,strtotime($cookie_time));
    }else{
      $token = $_COOKIE["Token"];
      $realtoken = $giventoken['access_token'];
      if($token!==$realtoken){
        $cookie_name = "Token";
        $cookie_value = $giventoken['access_token'];
        $cookie_time = $giventoken['ExpiryDate'];
        setcookie($cookie_name, $cookie_value,strtotime($cookie_time));
      }
    }
  }else{
    if(!isset($_COOKIE["Token"])) {
      $cookie_name = "Token";
      $cookie_value = $giventoken['access_token']; 
      $cookie_time = $giventoken['.expires'];
      setcookie($cookie_name, $cookie_value,strtotime($cookie_time));
    }else{
      $token = $_COOKIE["Token"];
      $realtoken = $giventoken['access_token'];
      if($token!==$realtoken){
        $cookie_name = "Token";
        $cookie_value = $giventoken['access_token'];
        $cookie_time = $giventoken['.expires'];
        setcookie($cookie_name, $cookie_value,strtotime($cookie_time));
      }
    }
  }
}

}


  ?>