<?php 

 function getsubsidiary($id){
  
    $giventoken = $_COOKIE["Token"];
        $parentGl = $id;
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => 'http://actm.prabhumanagement.com/api/ChartOfAccount/GetChildLedgerByGLCode?GLCode='. $parentGl,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
          CURLOPT_HTTPHEADER => array(
            'Authorization: Bearer ' . $giventoken
          ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        $subtext= json_decode($response, true);
        return $subtext;
    }


?>