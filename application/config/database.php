<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------
| DATABASE CONNECTIVITY SETTINGS
| -------------------------------------------------------------------
| This file will contain the settings needed to access your database.
|
| For complete instructions please consult the 'Database Connection'
| page of the User Guide.
|
| -------------------------------------------------------------------
| EXPLANATION OF VARIABLES
| -------------------------------------------------------------------
|
|	['dsn']      The full DSN string describe a connection to the database.
|	['hostname'] The hostname of your database server.
|	['username'] The username used to connect to the database
|	['password'] The password used to connect to the database
|	['database'] The name of the database you want to connect to
|	['dbdriver'] The database driver. e.g.: mysqli.
|			Currently supported:
|				 cubrid, ibase, mssql, mysql, mysqli, oci8,
|				 odbc, pdo, postgre, sqlite, sqlite3, sqlsrv
|	['dbprefix'] You can add an optional prefix, which will be added
				|				 to the table name when using the  Query Builder class
|	['pconnect'] TRUE/FALSE - Whether to use a persistent connection
|	['db_debug'] TRUE/FALSE - Whether database errors should be displayed.
|	['cache_on'] TRUE/FALSE - Enables/disables query caching
|	['cachedir'] The path to the folder where cache files should be stored
|	['char_set'] The character set used in communicating with the database
|	['dbcollat'] The character collation used in communicating with the database
|				 NOTE: For MySQL and MySQLi databases, this setting is only used
| 				 as a backup if your server is running PHP < 5.2.3 or MySQL < 5.0.7
|				 (and in table creation queries made with DB Forge).
| 				 There is an incompatibility in PHP with mysql_real_escape_string() which
| 				 can make your site vulnerable to SQL injection if you are using a
| 				 multi-byte character set and are running versions lower than these.
| 				 Sites using Latin-1 or UTF-8 database character set and collation are unaffected.
|	['swap_pre'] A default table prefix that should be swapped with the dbprefix
|	['encrypt']  Whether or not to use an encrypted connection.
|
|			'mysql' (deprecated), 'sqlsrv' and 'pdo/sqlsrv' drivers accept TRUE/FALSE
|			'mysqli' and 'pdo/mysql' drivers accept an array with the following options:
|
|				'ssl_key'    - Path to the private key file
|				'ssl_cert'   - Path to the public key certificate file
|				'ssl_ca'     - Path to the certificate authority file
|				'ssl_capath' - Path to a directory containing trusted CA certificats in PEM format
|				'ssl_cipher' - List of *allowed* ciphers to be used for the encryption, separated by colons (':')
|				'ssl_verify' - TRUE/FALSE; Whether verify the server certificate or not ('mysqli' only)
|
|	['compress'] Whether or not to use client compression (MySQL only)
|	['stricton'] TRUE/FALSE - forces 'Strict Mode' connections
|							- good for ensuring strict SQL while developing
|	['ssl_options']	Used to set various SSL options that can be used when making SSL connections.
|	['failover'] array - A array with 0 or more data for connections if the main should fail.
|	['save_queries'] TRUE/FALSE - Whether to "save" all executed queries.
| 				NOTE: Disabling this will also effectively disable both
| 				$this->db->last_query() and profiling of DB queries.
| 				When you run a query, with this setting set to TRUE (default),
| 				CodeIgniter will store the SQL statement for debugging purposes.
| 				However, this may cause high memory usage, especially if you run
| 				a lot of SQL queries ... disable this to avoid that problem.
|
| The $active_group variable lets you choose which connection group to
| make active.  By default there is only one group (the 'default' group).
				|
| The $query_builder variables lets you determine whether or not to load
| the query builder class.
						*/
// $active_group = 'default';
// $query_builder = TRUE;

// $db['default'] = array(
// 	'dsn'	=> '',
// 	'hostname' => '10.10.224.3',
// 	// 'username' => 'softnepa_40165',
// 	// 'password' => '1syO[)8_Xg0x5h]n',
// 	'username' => 'eshikshya_mysqluser',
// 	'password' => '7jM#HV7eCA(K',
// 	'database' => 'eshikshya_modernenglish',
// 	'dbdriver' => 'mysqli',
// 	'dbprefix' => '',
// 	'pconnect' => FALSE,
// 	'db_debug' => (ENVIRONMENT !== 'testing'),
// 	'cache_on' => FALSE,
// 	'cachedir' => '',
// 	'char_set' => 'utf8',
// 	'dbcollat' => 'utf8_general_ci',
// 	'swap_pre' => '',
// 	'encrypt' => FALSE,
// 	'compress' => FALSE,
// 	'stricton' => FALSE,
// 	'failover' => array(),
// 	'save_queries' => TRUE
// );

// $db['hostname'] = "10.10.224.3";
// $db['username'] = "finaluser";
// $db['password'] = "Ktmnepal@1";
// $db['database'] = "eshikshya_web";
// $db['dbdriver'] = "mysqli";
// $db['dbprefix'] = "";
// $db['pconnect'] = TRUE;
// $db['db_debug'] = TRUE;
// $db['cache_on'] = FALSE;
// $db['cachedir'] = "";
// $db['char_set'] = "utf8";
// $db['dbcollat'] = "utf8_general_ci";

// $active_group = 'default';
// $query_builder = TRUE;

// $db['default'] = array(
//  'dsn' => '',
//  'hostname' => '10.10.224.3',
//  'username' => 'finaluser',
//  'password' => 'Ktmnepal@1',
//  'database' => 'eshikshya_web',
//  'dbdriver' => 'mysqli',
//  'dbprefix' => '',
//  'pconnect' => FALSE,
//  'db_debug' => (ENVIRONMENT !== 'testing'),
//  'cache_on' => FALSE,
//  'cachedir' => '',
//  'char_set' => 'utf8',
//  'dbcollat' => 'utf8_general_ci',
//  'swap_pre' => '',
//  'encrypt' => FALSE,
//  'compress' => FALSE,
//  'stricton' => FALSE,
//  'failover' => array(),
//  'save_queries' => TRUE
// );


// $sql = "SELECT * FROM domainlisting";

//     $query = $this->db->query($sql);
//     $result = $query->row();

//     foreach($result as $row){
//       if($_SERVER['SERVER_NAME'] == $row["domain"] . '.eshikshya.com.np'){
//         $active_group = $row["domain"]; 
//       }

//       $db[$row["domain"]]['hostname'] = "10.10.224.3";
//       $db[$row["domain"]]['username'] = "eshikshya_mysqluser";
//       $db[$row["domain"]]['password'] = "7jM#HV7eCA(K";
//       $db[$row["domain"]]['database'] = $row["database_name"];
//       $db[$row["domain"]]['dbdriver'] = "mysqli";
//       $db[$row["domain"]]['dbprefix'] = "";
//       $db[$row["domain"]]['pconnect'] = TRUE;
//       $db[$row["domain"]]['db_debug'] = TRUE;
//       $db[$row["domain"]]['cache_on'] = FALSE;
//       $db[$row["domain"]]['cachedir'] = "";
//       $db[$row["domain"]]['char_set'] = "utf8";
//       $db[$row["domain"]]['dbcollat'] = "utf8_general_ci";

//     }





// define(DB_HOST, "10.10.224.3");   // Host
// define(DB_NAME, "eshikshya_web"); // Database
// define(DB_USER, "finaluser");   // Username
// define(DB_PASS, "Ktmnepal@1");   // Password

// $mysqli=new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME) or die("Error " . mysqli_error($link));
// mysqli_set_charset($mysqli, 'utf8');

// $results = $mysqli->query("SELECT * FROM domainlisting");
// while(($array_results[] = $results->fetch_assoc()) || array_pop($array_results));

// foreach($array_results as $row) {
//   if($_SERVER['SERVER_NAME'] == $row["domain"] . '.eshikshya.com.np'){
//         $active_group = $row["domain"]; 
//       }

//       $db[$row["domain"]]['hostname'] = "10.10.224.3";
//       $db[$row["domain"]]['username'] = "eshikshya_mysqluser";
//       $db[$row["domain"]]['password'] = "7jM#HV7eCA(K";
//       $db[$row["domain"]]['database'] = $row["database_name"];
//       $db[$row["domain"]]['dbdriver'] = "mysqli";
//       $db[$row["domain"]]['dbprefix'] = "";
//       $db[$row["domain"]]['pconnect'] = TRUE;
//       $db[$row["domain"]]['db_debug'] = TRUE;
//       $db[$row["domain"]]['cache_on'] = FALSE;
//       $db[$row["domain"]]['cachedir'] = "";
//       $db[$row["domain"]]['char_set'] = "utf8";
//       $db[$row["domain"]]['dbcollat'] = "utf8_general_ci";
// }






$servername = "10.10.224.3";
$username = "finaluser";
$password = "Ktmnepal@1";
$dbname = "eshikshya_web";

// Create connection
$conn = mysqli_connect($servername, $username, $password, $dbname);
// Check connection
if (!$conn) {
  die("Connection failed: " . mysqli_connect_error());
}

$sql = "SELECT * FROM domainlistings";
// $result = mysqli_query($conn, $sql);

if ($result = $conn->query($sql)) {
  // output data of each row
  while($row = $result->fetch_assoc()) {
    // echo("<script>console.log('PHP: ' .$row['domain']."".'.eshikshya.com.np' );</script>");
    // echo [($row['domain'])]['hostname'];
    $domain = $row['domain'];
      // if($_SERVER['SERVER_NAME'] == $domain."".'.eshikshya.com.np'){
      //    $active_group = $domain; 
      // }
      // $db["'".$domain."'"]['hostname'] = "10.10.224.3";
      // $db["'".$domain."'"]['username'] = "eshikshya_mysqluser";
      // $db["'".$domain."'"]['password'] = "7jM#HV7eCA(K";
      // $db["'".$domain."'"]['database'] = $row['database_name'];
      // $db["'".$domain."'"]['dbdriver'] = "mysqli";
      // $db["'".$domain."'"]['dbprefix'] = "";
      // $db["'".$domain."'"]['pconnect'] = TRUE;
      // $db["'".$domain."'"]['db_debug'] = TRUE;
      // $db["'".$domain."'"]['cache_on'] = FALSE;
      // $db["'".$domain."'"]['cachedir'] = "";
      // $db["'".$domain."'"]['char_set'] = "utf8";
      // $db["'".$domain."'"]['dbcollat'] = "utf8_general_ci";
     if($_SERVER['SERVER_NAME'] == $domain."".'.eshikshya.com.np'){
        $active_group = "'".$domain."'";
        $query_builder = TRUE;

        $db["'".$domain."'"] = array(
         'dsn' => '',
         'hostname' => '10.10.224.3',
         'username' => 'finaluser',
         'password' => 'Ktmnepal@1',
         'database' => $row['database_name'],
         'dbdriver' => 'mysqli',
         'dbprefix' => '',
         'pconnect' => FALSE,
         'db_debug' => (ENVIRONMENT !== 'testing'),
         'cache_on' => FALSE,
         'cachedir' => '',
         'char_set' => 'utf8',
         'dbcollat' => 'utf8_general_ci',
         'swap_pre' => '',
         'encrypt' => FALSE,
         'compress' => FALSE,
         'stricton' => FALSE,
         'failover' => array(),
         'save_queries' => TRUE
        );
      }
}
} else {
  redirect('https://eshikshya.com.np/not-found');
}

mysqli_close($conn);





if($_SERVER['SERVER_NAME'] == 'design.eshikshya.com.np'){
  $active_group = "design";  
}else if($_SERVER['SERVER_NAME'] == 'centralhss.eshikshya.com.np'){
  $active_group = "centralhss";  
}
else if($_SERVER['SERVER_NAME'] == 'greencity.eshikshya.com.np'){
  $active_group = "greencity";  
} else if($_SERVER['SERVER_NAME'] == 'kmhdc.eshikshya.com.np'){
  $active_group = "kmhdc";
}else if($_SERVER['SERVER_NAME'] == 'lfms.eshikshya.com.np'){
  $active_group = "littleflower";
}else if($_SERVER['SERVER_NAME'] == 'sabhyata.eshikshya.com.np'){
  $active_group = "sabhyata";
}else if($_SERVER['SERVER_NAME'] == 'sagarmatha.eshikshya.com.np'){
  $active_group = "sagarmatha";
}else if($_SERVER['SERVER_NAME'] == 'spark.eshikshya.com.np'){
  $active_group = "spark";
}else if($_SERVER['SERVER_NAME'] == 'testing.eshikshya.com.np'){
  $active_group = "testing";
}else if($_SERVER['SERVER_NAME'] == 'vatsalya.eshikshya.com.np'){
  $active_group = "vatsalya";
}else if($_SERVER['SERVER_NAME'] == 'demo.eshikshya.com.np'){
  $active_group = "demo";
}else if($_SERVER['SERVER_NAME'] == 'modernenglish.eshikshya.com.np'){
  $active_group = "modernenglish";
}else if($_SERVER['SERVER_NAME'] == 'heimric.eshikshya.com.np'){
  $active_group = "heimric";
}else if($_SERVER['SERVER_NAME'] == 'bajrabarahi.eshikshya.com.np'){
  $active_group = "bajrabarahi";
}else if($_SERVER['SERVER_NAME'] == 'uttarbahini.eshikshya.com.np'){
  $active_group = "utarbahini";
}else if($_SERVER['SERVER_NAME'] == 'shreeamar.eshikshya.com.np'){
  $active_group = "shreeamar";
}
$db['heimric']['hostname'] = "10.10.224.3";
$db['heimric']['username'] = "eshikshya_mysqluser";
$db['heimric']['password'] = "7jM#HV7eCA(K";
$db['heimric']['database'] = "eshikshya_heimric";
$db['heimric']['dbdriver'] = "mysqli";
$db['heimric']['dbprefix'] = "";
$db['heimric']['pconnect'] = TRUE;
$db['heimric']['db_debug'] = TRUE;
$db['heimric']['cache_on'] = FALSE;
$db['heimric']['cachedir'] = "";
$db['heimric']['char_set'] = "utf8";
$db['heimric']['dbcollat'] = "utf8_general_ci";

$db['shreeamar']['hostname'] = "10.10.224.3";
$db['shreeamar']['username'] = "eshikshya_mysqluser";
$db['shreeamar']['password'] = "7jM#HV7eCA(K";
$db['shreeamar']['database'] = "eshikshya_shreeamar";
$db['shreeamar']['dbdriver'] = "mysqli";
$db['shreeamar']['dbprefix'] = "";
$db['shreeamar']['pconnect'] = TRUE;
$db['shreeamar']['db_debug'] = TRUE;
$db['shreeamar']['cache_on'] = FALSE;
$db['shreeamar']['cachedir'] = "";
$db['shreeamar']['char_set'] = "utf8";
$db['shreeamar']['dbcollat'] = "utf8_general_ci";

$db['bajrabarahi']['hostname'] = "10.10.224.3";
$db['bajrabarahi']['username'] = "eshikshya_mysqluser";
$db['bajrabarahi']['password'] = "7jM#HV7eCA(K";
$db['bajrabarahi']['database'] = "eshikshya_bajra";
$db['bajrabarahi']['dbdriver'] = "mysqli";
$db['bajrabarahi']['dbprefix'] = "";
$db['bajrabarahi']['pconnect'] = TRUE;
$db['bajrabarahi']['db_debug'] = TRUE;
$db['bajrabarahi']['cache_on'] = FALSE;
$db['bajrabarahi']['cachedir'] = "";
$db['bajrabarahi']['char_set'] = "utf8";
$db['bajrabarahi']['dbcollat'] = "utf8_general_ci";

$db['modernenglish']['hostname'] = "10.10.224.3";
$db['modernenglish']['username'] = "eshikshya_mysqluser";
$db['modernenglish']['password'] = "7jM#HV7eCA(K";
$db['modernenglish']['database'] = "eshikshya_modernenglish";
$db['modernenglish']['dbdriver'] = "mysqli";
$db['modernenglish']['dbprefix'] = "";
$db['modernenglish']['pconnect'] = TRUE;
$db['modernenglish']['db_debug'] = TRUE;
$db['modernenglish']['cache_on'] = FALSE;
$db['modernenglish']['cachedir'] = "";
$db['modernenglish']['char_set'] = "utf8";
$db['modernenglish']['dbcollat'] = "utf8_general_ci";

$db['design']['hostname'] = "10.10.224.3";
$db['design']['username'] = "eshikshya_mysqluser";
$db['design']['password'] = "7jM#HV7eCA(K";
$db['design']['database'] = "eshikshya_main";
$db['design']['dbdriver'] = "mysqli";
$db['design']['dbprefix'] = "";
$db['design']['pconnect'] = TRUE;
$db['design']['db_debug'] = TRUE;
$db['design']['cache_on'] = FALSE;
$db['design']['cachedir'] = "";
$db['design']['char_set'] = "utf8";
$db['design']['dbcollat'] = "utf8_general_ci";

$db['centralhss']['hostname'] = "10.10.224.3";
$db['centralhss']['username'] = "eshikshya_mysqluser";
$db['centralhss']['password'] = "7jM#HV7eCA(K";
$db['centralhss']['database'] = "eshikshya_centralhss";
$db['centralhss']['dbdriver'] = "mysqli";
$db['centralhss']['dbprefix'] = "";
$db['centralhss']['pconnect'] = TRUE;
$db['centralhss']['db_debug'] = TRUE;
$db['centralhss']['cache_on'] = FALSE;
$db['centralhss']['cachedir'] = "";
$db['centralhss']['char_set'] = "utf8";
$db['centralhss']['dbcollat'] = "utf8_general_ci";

$db['greencity']['hostname'] = "10.10.224.3";
$db['greencity']['username'] = "eshikshya_mysqluser";
$db['greencity']['password'] = "7jM#HV7eCA(K";
$db['greencity']['database'] = "eshikshya_newgreencity";
$db['greencity']['dbdriver'] = "mysqli";
$db['greencity']['dbprefix'] = "";
$db['greencity']['pconnect'] = TRUE;
$db['greencity']['db_debug'] = TRUE;
$db['greencity']['cache_on'] = FALSE;
$db['greencity']['cachedir'] = "";
$db['greencity']['char_set'] = "utf8";
$db['greencity']['dbcollat'] = "utf8_general_ci";

$db['kmhdc']['hostname'] = "10.10.224.3";
$db['kmhdc']['username'] = "eshikshya_mysqluser";
$db['kmhdc']['password'] = "7jM#HV7eCA(K";
$db['kmhdc']['database'] = "eshikshya_kmhdc";
$db['kmhdc']['dbdriver'] = "mysqli";
$db['kmhdc']['dbprefix'] = "";
$db['kmhdc']['pconnect'] = TRUE;
$db['kmhdc']['db_debug'] = TRUE;
$db['kmhdc']['cache_on'] = FALSE;
$db['kmhdc']['cachedir'] = "";
$db['kmhdc']['char_set'] = "utf8";
$db['kmhdc']['dbcollat'] = "utf8_general_ci";

$db['littleflower']['hostname'] = "10.10.224.3";
$db['littleflower']['username'] = "eshikshya_mysqluser";
$db['littleflower']['password'] = "7jM#HV7eCA(K";
$db['littleflower']['database'] = "eshikshya_littleflower";
$db['littleflower']['dbdriver'] = "mysqli";
$db['littleflower']['dbprefix'] = "";
$db['littleflower']['pconnect'] = TRUE;
$db['littleflower']['db_debug'] = TRUE;
$db['littleflower']['cache_on'] = FALSE;
$db['littleflower']['cachedir'] = "";
$db['littleflower']['char_set'] = "utf8";
$db['littleflower']['dbcollat'] = "utf8_general_ci";

$db['sabhyata']['hostname'] = "10.10.224.3";
$db['sabhyata']['username'] = "eshikshya_mysqluser";
$db['sabhyata']['password'] = "7jM#HV7eCA(K";
$db['sabhyata']['database'] = "eshikshya_sabhyata";
$db['sabhyata']['dbdriver'] = "mysqli";
$db['sabhyata']['dbprefix'] = "";
$db['sabhyata']['pconnect'] = TRUE;
$db['sabhyata']['db_debug'] = TRUE;
$db['sabhyata']['cache_on'] = FALSE;
$db['sabhyata']['cachedir'] = "";
$db['sabhyata']['char_set'] = "utf8";
$db['sabhyata']['dbcollat'] = "utf8_general_ci";

$db['sagarmatha']['hostname'] = "10.10.224.3";
$db['sagarmatha']['username'] = "eshikshya_mysqluser";
$db['sagarmatha']['password'] = "7jM#HV7eCA(K";
$db['sagarmatha']['database'] = "eshikshya_sagarmatha";
$db['sagarmatha']['dbdriver'] = "mysqli";
$db['sagarmatha']['dbprefix'] = "";
$db['sagarmatha']['pconnect'] = TRUE;
$db['sagarmatha']['db_debug'] = TRUE;
$db['sagarmatha']['cache_on'] = FALSE;
$db['sagarmatha']['cachedir'] = "";
$db['sagarmatha']['char_set'] = "utf8";
$db['sagarmatha']['dbcollat'] = "utf8_general_ci";

$db['spark']['hostname'] = "10.10.224.3";
$db['spark']['username'] = "eshikshya_mysqluser";
$db['spark']['password'] = "7jM#HV7eCA(K";
$db['spark']['database'] = "eshikshya_spark";
$db['spark']['dbdriver'] = "mysqli";
$db['spark']['dbprefix'] = "";
$db['spark']['pconnect'] = TRUE;
$db['spark']['db_debug'] = TRUE;
$db['spark']['cache_on'] = FALSE;
$db['spark']['cachedir'] = "";
$db['spark']['char_set'] = "utf8";
$db['spark']['dbcollat'] = "utf8_general_ci";

$db['testing']['hostname'] = "10.10.224.3";
$db['testing']['username'] = "eshikshya_mysqluser";
$db['testing']['password'] = "7jM#HV7eCA(K";
$db['testing']['database'] = "eshikshya_testing";
$db['testing']['dbdriver'] = "mysqli";
$db['testing']['dbprefix'] = "";
$db['testing']['pconnect'] = TRUE;
$db['testing']['db_debug'] = TRUE;
$db['testing']['cache_on'] = FALSE;
$db['testing']['cachedir'] = "";
$db['testing']['char_set'] = "utf8";
$db['testing']['dbcollat'] = "utf8_general_ci";

$db['vatsalya']['hostname'] = "10.10.224.3";
$db['vatsalya']['username'] = "eshikshya_mysqluser";
$db['vatsalya']['password'] = "7jM#HV7eCA(K";
$db['vatsalya']['database'] = "eshikshya_vatsalya";
$db['vatsalya']['dbdriver'] = "mysqli";
$db['vatsalya']['dbprefix'] = "";
$db['vatsalya']['pconnect'] = TRUE;
$db['vatsalya']['db_debug'] = TRUE;
$db['vatsalya']['cache_on'] = FALSE;
$db['vatsalya']['cachedir'] = "";
$db['vatsalya']['char_set'] = "utf8";
$db['vatsalya']['dbcollat'] = "utf8_general_ci";

$db['demo']['hostname'] = "10.10.224.3";
$db['demo']['username'] = "eshikshya_mysqluser";
$db['demo']['password'] = "7jM#HV7eCA(K";
$db['demo']['database'] = "eshikshya_demo";
$db['demo']['dbdriver'] = "mysqli";
$db['demo']['dbprefix'] = "";
$db['demo']['pconnect'] = TRUE;
$db['demo']['db_debug'] = TRUE;
$db['demo']['cache_on'] = FALSE;
$db['demo']['cachedir'] = "";
$db['demo']['char_set'] = "utf8";
$db['demo']['dbcollat'] = "utf8_general_ci";

$db['utarbahini']['hostname'] = "10.10.224.3";
$db['utarbahini']['username'] = "eshikshya_mysqluser";
$db['utarbahini']['password'] = "7jM#HV7eCA(K";
$db['utarbahini']['database'] = "eshikshya_utarbahini";
$db['utarbahini']['dbdriver'] = "mysqli";
$db['utarbahini']['dbprefix'] = "";
$db['utarbahini']['pconnect'] = TRUE;
$db['utarbahini']['db_debug'] = TRUE;
$db['utarbahini']['cache_on'] = FALSE;
$db['utarbahini']['cachedir'] = "";
$db['utarbahini']['char_set'] = "utf8";
$db['utarbahini']['dbcollat'] = "utf8_general_ci";